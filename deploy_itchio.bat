del /Q /S Game\Export\*

godot Game\project.godot --export "Windows Desktop" Export\Windows\BronzeAgeTribes.exe

butler push --userversion-file=GameVersion.txt Game\Export\Windows commodoreshawn/bronze-age-tribes:windows

godot Game\project.godot --export "Linux/X11" Export\Linux\BronzeAgeTribes.x86_64

butler push --userversion-file=GameVersion.txt Game\Export\Linux commodoreshawn/bronze-age-tribes:linux