using Autofac;
using BronzeAgeTribes.Logic.Infrastructure;
using BronzeAgeTribes.Logic.Infrastructure.Interfaces;
using Godot;
using System.Linq;

public class WorldPreview : Spatial
{
    private IWorldRenderer _worldRenderer;

    [Export]
    public SpatialMaterial LandMaterial;
    [Export]
    public SpatialMaterial WaterMaterial;
    [Export]
    public SpatialMaterial ShadowMaterial;
    [Export]
    public SpatialMaterial BorderMaterial;

    [Export]
    public PackedScene HexPopPreviewPrefab;

    public override void _Ready()
    {
        _worldRenderer = IocManager.IocContainer.Resolve<IWorldRenderer>();
        IocManager.IocContainer.Resolve<ISoundManager>().SetMusic(true, false);

        var mapSize = _worldRenderer.GetVertexSizeForWorld();

        GetNode<Spatial>("CameraFocus").Translate(new Vector3(mapSize / 2, 0, mapSize / 2));

        _worldRenderer.BuildTerrain(
            this,
            LandMaterial,
            WaterMaterial,
            ShadowMaterial,
            BorderMaterial,
            true);

        GetNode<OrbitingSun>("OrbitingSun").Initialize(mapSize);
        GetNode<OrbitingSun>("CursedMoon").Initialize(mapSize);

        var popUiRoot = GetNode<Control>("PopUiRoot");

        foreach (var empire in IocManager.IocContainer.Resolve<IWorldManager>().Empires)
        {
            var distinctHexes = empire.AdministrativeGroups
                .SelectMany(g => g.Pops)
                .Select(p => p.Location)
                .Distinct()
                .ToArray();

            foreach (var hex in distinctHexes)
            {
                var prefab = HexPopPreviewPrefab.Instance() as HexPopPreviewWidget;
                prefab.InitializeFor(hex);
                popUiRoot.AddChild(prefab);
            }
        }
    }

    public void ChangeView(int mode)
    {
        _worldRenderer.SetColorMode(
            this,
            mode);
    }

    public void GotoMenu()
    {
        GetTree().Root.GetNode<Control>("Startup").Visible = true;
        IocManager.IocContainer.Resolve<ISoundManager>().SetMenuMusic();
        QueueFree();
    }
}
