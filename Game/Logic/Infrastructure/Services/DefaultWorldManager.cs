﻿using BronzeAgeTribes.Logic.Data;
using BronzeAgeTribes.Logic.Data.Game;
using BronzeAgeTribes.Logic.Infrastructure.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace BronzeAgeTribes.Logic.Infrastructure.Services
{
    public class DefaultWorldManager : IWorldManager
    {
        private readonly int BUCKET_SIZE = 10; 
        public IEnumerable<WorldHex> Hexes { get; private set; }
        public IEnumerable<WorldRegion> Regions { get; private set; }
        public int WorldRadiusHex { get; private set; }
        public string Seed { get; private set; }
        public List<Empire> Empires { get; }
        public List<Culture> Cultures { get; }
        public int Turn { get; set; }

        private List<WorldHex>[,] _lookupBuckets;
        
        public DefaultWorldManager()
        {
            Hexes = Enumerable.Empty<WorldHex>();
            Regions = Enumerable.Empty<WorldRegion>();
            Empires = new List<Empire>();
            Cultures = new List<Culture>();
        }

        public void SetWorld(
            int turn,
            string seed,
            int worldRadiusHex,
            IEnumerable<WorldHex> hexes, 
            IEnumerable<WorldRegion> regions)
        {
            Turn = turn;
            Seed = seed;
            WorldRadiusHex = worldRadiusHex;
            Hexes = hexes;
            Regions = regions;
            var worldSize = worldRadiusHex * 2 * Constants.VERTICIES_PER_HEX;
            _lookupBuckets = new List<WorldHex>[worldSize / BUCKET_SIZE + 1, worldSize / BUCKET_SIZE + 1];
            foreach(var hex in hexes)
            {
                GetBucket(hex.Position.RealX, hex.Position.RealY).Add(hex);
            }
        }

        private List<WorldHex> GetBucket(float realX, float realY)
        {
            var bucketX = (int)(realX / BUCKET_SIZE);
            var bucketY = (int)(realY / BUCKET_SIZE);

            if(0 <= bucketX && bucketX < _lookupBuckets.GetLength(0)
                && 0 <= bucketY && bucketY < _lookupBuckets.GetLength(1))
            {
                if(_lookupBuckets[bucketX,bucketY] == null)
                {
                    _lookupBuckets[bucketX, bucketY] = new List<WorldHex>();
                }

                return _lookupBuckets[bucketX, bucketY];
            }

            return new List<WorldHex>();
        }

        public WorldHex GetHexAtWorldPoint(float worldX, float worldY)
        {
            return Enumerable.Range(-1, 2)
                .SelectMany(x => Enumerable.Range(-1, 2).SelectMany(y => GetBucket(worldX + BUCKET_SIZE * x, worldY + BUCKET_SIZE * y)))
                .OrderBy(h => h.Position.DistanceTo(worldX, worldY))
                .FirstOrDefault();
        }

        public WorldHex GetHex(HexPosition position)
        {
            return GetHexAtWorldPoint(position.RealX, position.RealY);
        }
    }
}
