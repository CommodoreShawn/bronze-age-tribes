﻿using BronzeAgeTribes.Logic.Data;
using BronzeAgeTribes.Logic.Data.Game;
using BronzeAgeTribes.Logic.Infrastructure.Interfaces;
using BronzeAgeTribes.Logic.Infrastructure.SimulationEvents;
using Godot;
using System;
using System.Linq;

namespace BronzeAgeTribes.Logic.Infrastructure.Services
{
    public class DefaultWorldSimulator : IWorldSimulator
    {
        private readonly IWorldManager _worldManager;
        private readonly IPopManager _popManager;
        private readonly ISimulationEventBus _simulationEventBus;
        private readonly IWorldSerializer _worldSerializer;
        private readonly ISimpleNotificationBuilder _simpleNotificationBuilder;
        private readonly ILocalPlayerInfo _localPlayerInfo;
        private readonly INotificationTracker _notificationTracker;
        private readonly IAiManager _aiManager;
        private readonly IUserSettings _userSettings;
        private readonly ILogger _logger;
        private readonly Random _random;

        public DefaultWorldSimulator(
            IWorldManager worldManager,
            IPopManager popManager,
            ISimulationEventBus simulationEventBus,
            IWorldSerializer worldSerializer,
            ISimpleNotificationBuilder simpleNotificationBuilder,
            ILocalPlayerInfo localPlayerInfo,
            INotificationTracker notificationTracker,
            IAiManager aiManager,
            IUserSettings userSettings,
            ILogger logger)
        {
            _worldManager = worldManager;
            _popManager = popManager;
            _simulationEventBus = simulationEventBus;
            _worldSerializer = worldSerializer;
            _simpleNotificationBuilder = simpleNotificationBuilder;
            _notificationTracker = notificationTracker;
            _localPlayerInfo = localPlayerInfo;
            _aiManager = aiManager;
            _userSettings = userSettings;
            _logger = logger;
            _random = new Random();
        }

        public void AdvanceTurn(ReportProgress report)
        {
            ReportProgress wrappedReport = (progress, step) =>
            {
                _logger.Info($"Update {progress}: {step}");
                report(progress, step);
            };

            try
            {
                _worldManager.Turn += 1;

                _notificationTracker.ClearNotifications();


                _logger.Info("Starting update");
                wrappedReport(0, "Running AI");
                _aiManager.DoTurnLogic();

                wrappedReport(10, "Pop Interactions");
                DoPopInteractions();

                wrappedReport(20, "Growing Population");
                DoPopulationGrowth();

                wrappedReport(30, "Job Logic");
                DoJobLogic();

                wrappedReport(90, "Updating Pop Movement");
                UpdatePopMovement();

                wrappedReport(95, "Finalizing");
                RemoveDeadPops();
                _simulationEventBus.SendEvent(new TurnAdvancedSimulationEvent());

                if (_userSettings.AutosaveInterval > 0
                    && (_worldManager.Turn % _userSettings.AutosaveInterval) == 0)
                {
                    wrappedReport(99, "Autosaving");
                    _worldSerializer.Autosave((prog, step) => { });
                }
                wrappedReport(100, "Done");
            }
            catch(Exception ex)
            {
                GD.PrintErr($"{ex.GetType().Name}: {ex.Message}\n{ex.StackTrace}");
            }
        }

        private void DoPopInteractions()
        {
            var allPops = _worldManager.Empires
                .SelectMany(e => e.AdministrativeGroups)
                .SelectMany(g => g.Pops)
                .ToArray();

            foreach(var pop in allPops)
            {
                pop.IsBeingRaided = false;
            }

            foreach (var pop in allPops)
            {
                if (pop.InteractionTo != null)
                {
                    if (pop.InteractionType == PopInteractionType.Raid)
                    {
                        pop.InteractionTo.IsBeingRaided = true;
                    }

                    _popManager.RunPopInteractionLogic(
                        pop,
                        pop.InteractionTo,
                        pop.InteractionType);
                }
            }
        }

        private void DoPopulationGrowth()
        {
            foreach (var empire in _worldManager.Empires)
            {
                foreach (var group in empire.AdministrativeGroups)
                {
                    var isStarving = false;

                    var allSpecies = group.Pops
                        .Select(p => p.Species)
                        .Distinct()
                        .ToArray();

                    foreach(var species in allSpecies)
                    {
                        var rate = group.GetGrowthRateFor(species);

                        if(!group.GrowthProgress.ContainsKey(species))
                        {
                            group.GrowthProgress.Add(species, 0);
                        }

                        group.GrowthProgress[species] = Math.Max(0, group.GrowthProgress[species] + rate);

                        if(group.GrowthProgress[species] >= 1)
                        {
                            var potentialMothers = group.Pops
                                .Where(p => p.Species == species && p.Health > 0)
                                .Where(p => p.Location.Neighbors.Any(h => _popManager.CanNewPopBePlaced(h, group, species, p.Culture)))
                                .ToArray();

                            if(potentialMothers.Any())
                            {
                                var mother = _random.Choose(potentialMothers);

                                var potentialLocations = mother.Location.Neighbors
                                    .Where(h => _popManager.CanNewPopBePlaced(h, group, species, mother.Culture))
                                    .ToArray();

                                var location = _random.Choose(potentialLocations);

                                var pop = new Pop(
                                    species,
                                    group.Owner,
                                    group,
                                    mother.Culture,
                                    location);
                                pop.ManagerSpecies = mother.ManagerSpecies;
                                
                                pop.Job = location.Jobs.Where(j => j.Worker == null).First();
                                pop.Job.Worker = pop;
                                group.GrowthProgress[species] = 0;

                                if (group.Owner == _localPlayerInfo.Empire)
                                {
                                    _simpleNotificationBuilder.SendLookAtNotification(
                                        "notification_newpop",
                                        $"Population growth in {group.Name}",
                                        $"A new group of {pop.Culture.Name} {pop.Species.Name} has grown in {group.Name}.",
                                        pop.Location.Position);
                                }
                            }
                            else
                            {
                                group.GrowthProgress[species] = 1;
                            }
                        }

                        var starvationChance = group.GetStarvationChanceFor(species);
                        if (starvationChance > 0)
                        {
                            isStarving = true;

                            var starvationProtection = group.ClaimedRegions
                                .SelectMany(r => r.Hexes.Select(h => h.Development))
                                .Where(d => d != null)
                                .OfType<UrbanDevelopment>()
                                .Where(d => d.Building != null && d.Building.IsConstructed)
                                .Sum(d => d.Building.BackingType.StarvationProtection);

                            if (group.TurnsStarving >= (group.StarvationGracePeriod + starvationProtection))
                            {
                                foreach (var pop in group.Pops.Where(p => p.Species == species))
                                {
                                    if (_random.NextDouble() <= starvationChance)
                                    {
                                        pop.Health -= 0.25f;
                                    }
                                }

                                if(group.Owner == _localPlayerInfo.Empire)
                                {
                                    _simpleNotificationBuilder.SendLookAtNotification(
                                        "notification_starvation",
                                        $"Starvation in {group.Name}",
                                        $"{group.Name} is short on food, and your people are starving.",
                                        group.GroupCenter);
                                }
                            }
                        }
                        else
                        {
                            var healRate = group.GetGrowthRateFor(species) + 0.2f;

                            foreach (var pop in group.Pops.Where(p => p.Species == species && p.Health < 1))
                            {
                                pop.Health = Mathf.Min(1, pop.Health + healRate / 2);
                            }
                        }
                    }

                    if(isStarving)
                    {
                        group.TurnsStarving += 1;
                    }
                    else
                    {
                        group.TurnsStarving = 0;
                    }
                }
            }
        }

        private void DoJobLogic()
        {
            foreach (var empire in _worldManager.Empires)
            {
                foreach (var group in empire.AdministrativeGroups)
                {
                    foreach(var pop in group.Pops)
                    {
                        if(pop.Job != null)
                        {
                            pop.Job.AdvanceTurn(_simulationEventBus);
                        }
                    }

                    if(_localPlayerInfo.Empire == empire 
                        && group.GetExpectedOutput(ResourceType.Production) < 0)
                    {
                        _simpleNotificationBuilder.SendLookAtNotification(
                                "notification_lowprod",
                                $"Low Production in {group.Name}",
                                $"{group.Name} has low prouction, construction will be delayed.",
                                group.GroupCenter);
                    }
                }
            }
        }

        private void UpdatePopMovement()
        {
            foreach (var empire in _worldManager.Empires)
            {
                foreach (var group in empire.AdministrativeGroups)
                {
                    foreach (var pop in group.Pops)
                    {
                        if (pop.MovingFromJob != null)
                        {
                            if (pop.MovingFromJob.MovingOutWorker == pop)
                            {
                                pop.MovingFromJob.MovingOutWorker = null;
                            }
                            pop.MovingFromJob = null;
                        }
                    }

                    group.UpdateGroupCenter();
                }
            }
            foreach(var hex in _worldManager.Hexes)
            {
                foreach(var feature in hex.Features)
                {
                    if (feature.CurrentInteraction != null && hex.Jobs.Any(j => j.Worker == null && j.JobType == feature.CurrentInteraction.InteractionJob))
                    {
                        feature.SetCurrentInteraction(null);
                    }
                }
            }

            foreach (var pop in _worldManager.Empires.SelectMany(e => e.AdministrativeGroups).SelectMany(g => g.Pops))
            {
                if(pop.InteractionTo != null)
                {
                    if(pop.InteractionTo.Health <= 0
                        || !pop.Location.Neighbors.Contains(pop.InteractionTo.Location))
                    {
                        if(pop.Owner == _localPlayerInfo.Empire)
                        {
                            _simpleNotificationBuilder.SendLookAtNotification(
                                "notification_popinteract_end_" + pop.InteractionType.ToString(),
                                Util.InteractionToVerb(pop.InteractionType.Value) + " Attempt Failed",
                                "Target pop is not longer valid.",
                                pop.Location.Position);
                        }

                        pop.InteractionTo = null;
                        pop.InteractionType = null;
                    }
                }
            }
        }

        private void RemoveDeadPops()
        {
            foreach (var empire in _worldManager.Empires)
            {
                foreach (var group in empire.AdministrativeGroups)
                {
                    var deadPops = 0;

                    foreach (var pop in group.Pops.ToArray())
                    {
                        if (pop.Health <= 0)
                        {
                            group.Pops.Remove(pop);
                            if(pop.Job != null)
                            {
                                pop.Job.Worker = null;
                                pop.Job = null;
                                pop.Location.Pops.Remove(pop);
                                pop.Location = null;
                            }

                            deadPops += 1;
                            group.UpdateGroupCenter();
                        }
                    }

                    if(deadPops > 0 && empire == _localPlayerInfo.Empire)
                    {
                        if (deadPops > 1)
                        {
                            _simpleNotificationBuilder.SendLookAtNotification(
                                    "notification_deadpop",
                                    $"Deaths in {group.Name}",
                                    $"{deadPops} population have died.",
                                    group.GroupCenter);
                        }
                    }
                }

                var deadGroups = empire.AdministrativeGroups
                    .Where(g => !g.Pops.Any())
                    .ToArray();

                foreach(var group in deadGroups)
                {
                    empire.AdministrativeGroups.Remove(group);
                    _simulationEventBus.SendEvent(new AdminGroupChangedSimulationEvent(group));

                    if (empire == _localPlayerInfo.Empire)
                    {
                        if (group.IsSettled)
                        {
                            _simpleNotificationBuilder.SendLookAtNotification(
                                "notification_deadgroup",
                                $"{group.Name} has Perished",
                                $"Their land is now vacant and must be reclaimed.",
                                group.GroupCenter);
                        }
                        else
                        {
                            _simpleNotificationBuilder.SendLookAtNotification(
                                "notification_deadgroup",
                                $"{group.Name} has Perished",
                                $"The tribe is no more.",
                                group.GroupCenter);
                        }
                    }
                }
            }
        }
    }
}
