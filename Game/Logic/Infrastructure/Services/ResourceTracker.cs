﻿using BronzeAgeTribes.Logic.Infrastructure.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace BronzeAgeTribes.Logic.Infrastructure.Services
{
    public class ResourceTracker : IResourceTracker, IInitializableService
    {
        public IEnumerable<BiomeType> BiomeTypes { get; private set; }
        public IEnumerable<TerrainType> TerrainTypes { get; private set; }
        public IEnumerable<CellFeatureType> FeatureTypes { get; private set; }
        public IEnumerable<DevelopmentType> DevelopmentTypes { get; private set; }
        public IEnumerable<Trait> Traits { get; private set; }
        public IEnumerable<Species> Species { get; private set; }
        public IEnumerable<BuildingStyle> BuildingStyles { get; private set; }
        public IEnumerable<BuildingType> BuildingTypes { get; private set; }

        private readonly IResourceInspector _resourceInspector;

        public ResourceTracker(IResourceInspector resourceInspector)
        {
            _resourceInspector = resourceInspector;
        }

        public void Initialize()
        {
            BiomeTypes = _resourceInspector.ListResources<BiomeType>("res://Resources/Biome/").ToArray();
            TerrainTypes = _resourceInspector.ListResources<TerrainType>("res://Resources/Terrain/").ToArray();
            FeatureTypes = _resourceInspector.ListResources<CellFeatureType>("res://Resources/CellFeatures/").ToArray();
            DevelopmentTypes = _resourceInspector.ListResources<DevelopmentType>("res://Resources/DevelopmentTypes/").ToArray();
            Traits = _resourceInspector.ListResources<TerrainTrait>("res://Resources/Traits/")
                    .OfType<Trait>()
                    .Concat(_resourceInspector.ListResources<CultureTrait>("res://Resources/Traits/"))
                    .ToArray();
            Species = _resourceInspector.ListResources<Species>("res://Resources/Species/").ToArray();
            BuildingStyles = _resourceInspector.ListResources<BuildingStyle>("res://Resources/BuildingStyles/").ToArray();
            BuildingTypes = _resourceInspector.ListResources<BuildingType>("res://Resources/BuildingTypes/").ToArray();
        }
    }
}
