﻿using BronzeAgeTribes.Logic.Data;
using BronzeAgeTribes.Logic.Data.Game;
using BronzeAgeTribes.Logic.Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using UNM.Parser;
using UNM.Parser.Implementation;
using UNM.Parser.Interfaces;

namespace BronzeAgeTribes.Logic.Infrastructure.Services
{
    public class UnmNameSource : INameSource
    {
        private readonly INamelistSource _namelistSource;
        private readonly IWorldManager _worldManager;
        private readonly ILogger _logger;
        private INameParser _nameParser;

        private List<string> _tribeNames;
        private List<string> _settlementNames;

        public UnmNameSource(
            INamelistSource namelistSource,
            IWorldManager worldManager,
            ILogger logger)
        {
            _namelistSource = namelistSource;
            _worldManager = worldManager;
            _logger = logger;
            InitializeForSeed(new Random().Next());

            _tribeNames = new List<string>();
            _settlementNames = new List<string>();
        }

        public void Clear()
        {
            _tribeNames.Clear();
            _settlementNames.Clear();
        }

        public void InitializeForSeed(int seed)
        {
            _nameParser = new NameParser(_namelistSource, seed);
            _nameParser.Initialize();
        }

        public string MakeRegionName(WorldRegion region)
        {
            try
            {
                return _nameParser.Process(new PatternProcessingParameters(
                    "<^_region_name>")
                {
                    CapitalizationScheme = CapitalizationScheme.BY_WORDS,
                    Context = BuildContextForRegion(region)
                });
            }
            catch (PatternParseException ex)
            {
                throw new PatternParseException(ExpandMessage(ex), ex);
            }
        }

        public string MakeTribeName(Culture dominantCulture)
        {
            try
            {
                return _nameParser.Process(new PatternProcessingParameters(
                    "<^_tribe_name>")
                {
                    CapitalizationScheme = CapitalizationScheme.BY_WORDS,
                    Context = dominantCulture.NamingContext.Yield()
                        .Concat($"naming_tribe".Yield())
                        .Concat(dominantCulture.Traits.Select(t => t.Id))
                        .ToArray()
                });
            }
            catch (PatternParseException ex)
            {
                throw new PatternParseException(ExpandMessage(ex), ex);
            }
        }

        public string MakeEmpireName(Culture dominantCulture, string startingTribeName)
        {
            var variables = new Dictionary<string, string>();
            if(!string.IsNullOrWhiteSpace(startingTribeName) && startingTribeName.Length < 15)
            {
                variables.Add("tribe_name", startingTribeName);
            }

            try
            {
                return _nameParser.Process(new PatternProcessingParameters(
                    "<^_empire_name>")
                {
                    CapitalizationScheme = CapitalizationScheme.BY_WORDS,
                    Context = dominantCulture.NamingContext.Yield()
                    .Concat($"naming_empire".Yield())
                        .Concat(dominantCulture.Traits.Select(t => t.Id))
                        .ToArray(),
                    Variables = variables
                });
            }
            catch (PatternParseException ex)
            {
                throw new PatternParseException(ExpandMessage(ex), ex);
            }
        }

        private IEnumerable<string> BuildContextForRegion(WorldRegion region)
        {
            return $"biome_{region.BiomeType.Name}".Yield()
                .Concat($"terrain_{region.TerrainType.Name}".Yield())
                .Concat($"naming_region".Yield())
                //.Concat(GetCommonTraitsFor(region).Select(t => t.Id))
                //.Concat(region.Neighbors.Select(r => "adjacent_" + r.Biome.Name.Yield().Concat(GetCommonTraitsFor(r).Select(t => "adjacent_" + t.Id))))
                .Concat(region.Neighbors.Where(r => r.BiomeType != null).Select(r => $"adjacent_biome_{r.BiomeType.Name}"))
                .Concat(region.Neighbors.Where(r => r.TerrainType != null).Select(r => $"adjacent_terrain_{r.TerrainType.Name}"))
                .ToArray();
        }

        private string ExpandMessage(Exception ex)
        {
            var msg = "";

            while (ex != null)
            {
                msg += ex.Message + " ";
                ex = ex.InnerException;
            }

            return msg;
        }
    }
}
