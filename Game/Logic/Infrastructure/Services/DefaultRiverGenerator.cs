﻿using BronzeAgeTribes.Logic.Data;
using BronzeAgeTribes.Logic.Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BronzeAgeTribes.Logic.Infrastructure.Services
{
    public class DefaultRiverGenerator : IRiverGenerator
    {
        public void GenerateRivers(
            WorldParameters worldParameters,
            Random random, 
            List<WorldHex> hexes, 
            List<WorldRegion> regions)
        {
            var springHexes = new List<WorldHex>();
            var inRiverRegion = new HashSet<WorldHex>();
            var riverFlow = hexes.ToDictionary(h => h, h => (WorldHex)null);

            // Identify river springs
            foreach (var hex in hexes.Where(h => IsBorderHex(h) && h.Terrain.Height <= 5))
            {
                var springChance = hex.Region.Rainfall * hex.Region.TerrainType.Height * 0.1f;

                if (random.NextDouble() < springChance)
                {
                    springHexes.Add(hex);
                }
            }

            // Determine river paths
            foreach(var spring in springHexes)
            {
                var oceanPath = PathToOcean(random, spring, hexes, worldParameters, riverFlow);

                if (oceanPath.Count > 2)
                {
                    for(var i = 0; i < oceanPath.Count; i++)
                    {
                        oceanPath[i].RiverFlow += 1;

                        if( i < oceanPath.Count - 1)
                        {
                            riverFlow[oceanPath[i]] = oceanPath[i + 1];
                        }
                    }
                }
            }

            // Make river hexes into their own regions
            foreach(var hex in hexes)
            {
                if (hex.RiverFlow > 0)
                {
                    hex.AddFeature(new CellFeature(worldParameters.RiverFeature, hex));

                    if (random.NextDouble() < worldParameters.RiverAdditionalFeatureChance
                        && worldParameters.RiverAdditionalFeatures.Any())
                    {
                        hex.AddFeature(new CellFeature(random.Choose(worldParameters.RiverAdditionalFeatures), hex));
                    }
                }

                if(hex.RiverFlow > 0 && riverFlow[hex] != null)
                {
                    hex.RiverFlowDirection = (HexFacing)Array.FindIndex(hex.Neighbors, h => h == riverFlow[hex]);
                }

                if(hex.RiverFlow > 0 && inRiverRegion.Contains(hex))
                {
                    var riverRegion = new WorldRegion
                    {
                        TerrainType = hex.Terrain.BackingType,
                        BiomeType = hex.BiomeType?.RiverUpgradesTo ?? hex.BiomeType
                    };
                    regions.Add(riverRegion);

                    FloodFillRiver(hex, riverRegion, worldParameters, inRiverRegion);
                }
            }
        }

        private void FloodFillRiver(
            WorldHex start,
            WorldRegion riverRegion,
            WorldParameters worldParameters,
            HashSet<WorldHex> inRiverRegion)
        {
            var open = new List<WorldHex>();
            open.Add(start);

            while(open.Any())
            {
                var current = open.First();
                open.Remove(current);
                
                current.Region.RemoveHex(current);
                riverRegion.AddHex(current);
                inRiverRegion.Add(current);

                var neighbors = current.Neighbors
                    .Where(n => n != null && n.Region != riverRegion && n.RiverFlow > 0 && !worldParameters.RainSources.Contains(n.Terrain.BackingType))
                    .Where(n => !inRiverRegion.Contains(n))
                    .Where(n => !open.Contains(n))
                    .ToArray();
                
                open.AddRange(neighbors);

                current.Region = riverRegion;
            }
        }

        private List<WorldHex> PathToOcean(
            Random random,
            WorldHex start, 
            List<WorldHex> hexes, 
            WorldParameters worldParameters,
            Dictionary<WorldHex, WorldHex> riverFlow)
        {
            var open = new List<WorldHex>();
            var closed = new List<WorldHex>();
            var cameFrom = new Dictionary<WorldHex, WorldHex>();
            var distTo = new Dictionary<WorldHex, int>();

            open.Add(start);
            cameFrom.Add(start, null);
            distTo.Add(start, 0);

            while(open.Any())
            {
                var current = open
                    .Where(h => riverFlow[h] != null)
                    .OrderBy(h => distTo[h])
                    .FirstOrDefault();

                if (current == null)
                {
                    current = open
                        .OrderBy(h => distTo[h])
                        .First();
                }

                open.Remove(current);
                closed.Add(current);

                if(worldParameters.RainSources.Contains(current.Terrain.BackingType)
                    || riverFlow[current] != null)
                {
                    return ConstructPath(current, cameFrom);
                }

                var neighbors = current.Neighbors
                    .Where(n => n != null && IsBorderHex(n) && n.Terrain.Height <= current.Terrain.Height)
                    .Where(n => !closed.Contains(n))
                    .ToList();

                random.Shuffle(neighbors);

                foreach(var neighbor in neighbors)
                {
                    var dist = distTo[current] + 1;

                    if(!open.Contains(neighbor))
                    {
                        open.Add(neighbor);
                        cameFrom.Add(neighbor, current);
                        distTo.Add(neighbor, dist);
                    } 
                    else if(dist < distTo[neighbor])
                    {
                        cameFrom[neighbor] = current;
                        distTo[neighbor] = dist;
                    }
                }       
            }

            return new List<WorldHex>();
        }

        private List<WorldHex> ConstructPath(WorldHex current, Dictionary<WorldHex, WorldHex> cameFrom)
        {
            var path = new List<WorldHex>();

            while(current != null)
            {
                path.Add(current);
                current = cameFrom[current];
            }

            path.Reverse();

            return path;
        }

        private bool IsBorderHex(WorldHex h)
        {
            return h.Neighbors.Any(n => n != null && n.Region != h.Region);
        }
    }
}
