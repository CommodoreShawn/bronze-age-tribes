﻿using BronzeAgeTribes.Logic.Infrastructure.Interfaces;
using Godot;
using System;

namespace BronzeAgeTribes.Logic.Infrastructure.Services
{
    public class ConsoleLogger : ILogger
    {
        public void Error(string message)
        {
            GD.PrintErr(message);
        }

        public void Error(Exception ex, string message)
        {
            GD.PrintErr($"{ex.GetType().Name}, {ex.Message}: {message}\n{ex.StackTrace}");
        }

        public void Info(string message)
        {
            GD.Print(message);
        }
    }
}
