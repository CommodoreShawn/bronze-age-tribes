﻿using BronzeAgeTribes.Logic.Data;
using BronzeAgeTribes.Logic.Data.Game;
using BronzeAgeTribes.Logic.Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BronzeAgeTribes.Logic.Infrastructure.Services
{
    public class DefaultDevelopmentListProvider : IDevelopmentListProvider
    {
        private readonly IResourceTracker _resourceTracker;
        
        public DefaultDevelopmentListProvider(IResourceTracker resourceTracker)
        {
            _resourceTracker  = resourceTracker;
        }

        public IEnumerable<DevelopmentType> GetDevelopmentOptionsFor(WorldHex tile, AdministrativeGroup group, Empire owner)
        {
            return _resourceTracker.DevelopmentTypes
                //.Where(dt=>!dt.RequiredEmpireTraits.Any() || dt.RequiredEmpireTraits.All(et => owner.Traits.Contains(et)))
                //.Where(dt => !dt.RequiredGroupTraits.Any() || dt.RequiredGroupTraits.All(gt => group.Traits.Contains(gt)))
                .Where(dt => !dt.RequiredTileTraits.Any() || dt.RequiredTileTraits.All(tt => tile.TerrainTraits.Contains(tt)))
                .Where(dt => !dt.ForbiddenTileTraits.Any() || !dt.ForbiddenTileTraits.Any(tt => tile.TerrainTraits.Contains(tt)))
                .Where(dt => !dt.RequiredAdjacentTileTraits.Any() || tile.Neighbors.Where(n => n != null).Any(n => dt.RequiredAdjacentTileTraits.All(tt => n.TerrainTraits.Contains(tt))))
                .Where(dt => !dt.OnlySettlement || group.IsSettled)
                .ToArray();
        }

        public IEnumerable<TentativeOptionInfo<DevelopmentType>> GetTentativeDevelopmentOptionsFor(WorldHex tile, AdministrativeGroup group, Empire owner)
        {
            Func<DevelopmentType, IEnumerable<Trait>, bool> matchesTraits = (dt, traits) =>
            {
                //.Where(dt=>!dt.RequiredEmpireTraits.Any() || dt.RequiredEmpireTraits.All(et => owner.Traits.Contains(et)))
                //.Where(dt => !dt.RequiredGroupTraits.Any() || dt.RequiredGroupTraits.All(gt => group.Traits.Contains(gt)))

                return (!dt.RequiredTileTraits.Any() || dt.RequiredTileTraits.All(tt => traits.Contains(tt)))
                    && (!dt.ForbiddenTileTraits.Any() || !dt.ForbiddenTileTraits.Any(tt => traits.Contains(tt)))
                    && (!dt.RequiredAdjacentTileTraits.Any() || tile.Neighbors.Where(n => n != null).Any(n => dt.RequiredAdjacentTileTraits.All(tt => n.TerrainTraits.Contains(tt))))
                    && (!dt.OnlySettlement || group.IsSettled);
            };

            var results = new List<TentativeOptionInfo<DevelopmentType>>();

            var baseTraits = tile.TerrainTraits;

            var traitsWithout = tile.Features
                .Select(f => Tuple.Create(f,
                    tile.Terrain.Traits
                        .OfType<Trait>()
                        .Concat(tile.Features.Where(cf => cf != f).SelectMany(cf => cf.Traits))
                        .Concat(tile.Development?.Traits ?? Enumerable.Empty<Trait>())))
                .ToArray();

            foreach (var developmentType in _resourceTracker.DevelopmentTypes)
            {
                if(matchesTraits(developmentType, baseTraits))
                {
                    results.Add(new TentativeOptionInfo<DevelopmentType>
                    {
                        Option = developmentType,
                    });
                }
                else
                {
                    var featureToClear = traitsWithout
                        .Where(t => matchesTraits(developmentType, t.Item2))
                        .Select(t => t.Item1)
                        .FirstOrDefault();

                    if(featureToClear != null)
                    {
                        results.Add(new TentativeOptionInfo<DevelopmentType>
                        {
                            Option = developmentType,
                            ClearFeature = featureToClear
                        });
                    }
                }
            }

            return results;
        }

        public IEnumerable<BuildingType> GetBuildingOptionsFor(WorldHex tile, AdministrativeGroup group, Empire owner)
        {
            return _resourceTracker.BuildingTypes
                //.Where(dt=>!dt.RequiredEmpireTraits.Any() || dt.RequiredEmpireTraits.All(et => owner.Traits.Contains(et)))
                //.Where(dt => !dt.RequiredGroupTraits.Any() || dt.RequiredGroupTraits.All(gt => group.Traits.Contains(gt)))
                .Where(bt => !bt.RequiredTileTraits.Any() || bt.RequiredTileTraits.All(tt => tile.TerrainTraits.Contains(tt)))
                .Where(bt => !bt.ForbiddenTileTraits.Any() || !bt.ForbiddenTileTraits.Any(tt => tile.TerrainTraits.Contains(tt)))
                .Where(bt => !bt.RequiredAdjacentTileTraits.Any() || tile.Neighbors.Where(n => n != null).Any(n => bt.RequiredAdjacentTileTraits.All(tt => n.TerrainTraits.Contains(tt))))
                .ToArray();
        }
    }
}
