﻿using BronzeAgeTribes.Logic.Data;
using BronzeAgeTribes.Logic.Infrastructure.Interfaces;
using Godot;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BronzeAgeTribes.Logic.Infrastructure.Services
{
    public class NeighborGlomRegionGenerator : IRegionGenerator
    {
        public List<WorldRegion> DetermineRegions(
            Random random,
            List<WorldHex> hexes,
            WorldParameters worldParameters)
        {
            var worldCenter = new HexPosition(worldParameters.WorldRadius, worldParameters.WorldRadius);
            var hexesInOrder = hexes
                .OrderBy(h => h.Position.DistanceTo(worldCenter))
                .ToArray();

            var superHexes = new List<WorldHex>();
            var hexToSuperhex = hexes
                .ToDictionary(h => h, h => (WorldHex)null);
            var superHexToHexes = new Dictionary<WorldHex, List<WorldHex>>();

            // Group hexes into "super hexes" consisting of a hex and all neighbors
            foreach (var hex in hexesInOrder)
            {
                if (hexToSuperhex[hex] == null
                    && hex.Neighbors.All(n => n == null || hexToSuperhex[n] == null))
                {
                    superHexes.Add(hex);
                    hexToSuperhex[hex] = hex;
                    superHexToHexes.Add(hex, new List<WorldHex>());
                    superHexToHexes[hex].Add(hex);

                    foreach (var neighbor in hex.Neighbors.Where(h => h != null))
                    {
                        hexToSuperhex[neighbor] = hex;
                        superHexToHexes[hex].Add(neighbor);
                    }
                }
            }

            // Assign any orphans to super hexes
            foreach (var hex in hexesInOrder)
            {
                if (hexToSuperhex[hex] == null)
                {
                    var superHexToJoin = hex.Neighbors
                        .Where(n => n != null)
                        .Select(h => hexToSuperhex[h])
                        .Where(h => h != null)
                        .FirstOrDefault();

                    if(superHexToJoin != null)
                    {
                        hexToSuperhex[hex] = superHexToJoin;
                        superHexToHexes[superHexToJoin].Add(hex);
                    }
                }
            }

            var superHexesInRegion = superHexes
                .ToDictionary(h => h, h => false);

            random.Shuffle(superHexes);
            
            var worldRegions = superHexes
                .Where(h => superHexToHexes[h].Any())
                .Select(h => MakeRegion(superHexToHexes[h]))
                .ToList();

            LinkRegionsToNeighbors(worldRegions);

            return worldRegions;
        }

        public void LinkRegionsToNeighbors(List<WorldRegion> worldRegions)
        {
            foreach (var region in worldRegions)
            {
                region.Neighbors = region.Hexes
                    .SelectMany(h => h.Neighbors.Where(n => n != null))
                    .Select(h => h.Region)
                    .Where(r => r != region && r != null)
                    .Distinct()
                    .ToList();
            }
        }

        private WorldRegion MakeRegion(List<WorldHex> hexesInRegion)
        {
            var region = new WorldRegion();
            region.SetHexes(hexesInRegion);
            foreach (var hex in hexesInRegion)
            {
                hex.Region = region;
            }

            return region;
        }

        public void MergeRegions(Random random, List<WorldHex> hexes, List<WorldRegion> regions, WorldParameters worldParameters)
        {
            var shouldIterate = true;
            while (shouldIterate)
            {
                shouldIterate = false;

                foreach (var region in regions.OrderBy(r=>r.Hexes.Count()).ToArray())
                {
                    var potentialMerges = region.Hexes
                        .Where(h => h.RiverFlow < 1)
                        .SelectMany(h => h.Neighbors.Where(n => n != null && n.RiverFlow < 1).Select(n => n.Region))
                        .Distinct()
                        .Where(r => r != region 
                            && r.BiomeType == region.BiomeType 
                            && r.Hexes.Count() + region.Hexes.Count() < worldParameters.TargetRegionSize * region.BiomeType.RegionSizeFactor)
                        .ToArray();

                    if(potentialMerges.Any())
                    {
                        shouldIterate = true;
                        var toMerge = random.Choose(potentialMerges);

                        foreach(var hex in toMerge.Hexes.ToArray())
                        {
                            hex.Region = region;
                            region.AddHex(hex);
                            toMerge.RemoveHex(hex);
                        }
                    }
                }

                regions.RemoveAll(r => !r.Hexes.Any());
            }
        }
    }
}
