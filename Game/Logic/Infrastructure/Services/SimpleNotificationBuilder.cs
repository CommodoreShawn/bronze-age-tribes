﻿using BronzeAgeTribes.Logic.Data;
using BronzeAgeTribes.Logic.Infrastructure.Interfaces;
using BronzeAgeTribes.Logic.Infrastructure.SimulationEvents;
using System.Collections.Generic;

namespace BronzeAgeTribes.Logic.Infrastructure.Services
{
    public class SimpleNotificationBuilder : ISimpleNotificationBuilder, INotificationHandler
    {
        private readonly string HANDLER_LOOKAT = "SimpleNotificationBuilder_lookat";
        private readonly ISimulationEventBus _simulationEventBus;
        private readonly ILocalPlayerInfo _localPlayerInfo;

        public SimpleNotificationBuilder(
            ISimulationEventBus simulationEventBus,
            ILocalPlayerInfo localPlayerInfo)
        {
            _simulationEventBus = simulationEventBus;
            _localPlayerInfo = localPlayerInfo;
        }

        public void SendLookAtNotification(
            string icon, 
            string summaryTitle, 
            string summaryBody, 
            HexPosition lookAt)
        {
            _simulationEventBus.SendEvent(new NotificationSimulationEvent(
                new Notification
                {
                    Handler = HANDLER_LOOKAT,
                    Icon = icon,
                    SummaryTitle = summaryTitle,
                    SummaryBody = summaryBody,
                    QuickDismissable = true,
                    Data = new Dictionary<string, string>
                    {
                        {"look_x", lookAt.HexX.ToString() },
                        {"look_y", lookAt.HexY.ToString() }
                    }
                }));
        }

        public void SendSimpleNotification(string icon, string summaryTitle, string summaryBody, string title, string body, HexPosition? lookAt, bool autoOpen)
        {
            throw new System.NotImplementedException();
        }

        public bool CanHandle(Notification notification)
        {
            return notification.Handler == HANDLER_LOOKAT;
        }

        public void Open(Notification notification)
        {
            if(notification.Handler == HANDLER_LOOKAT)
            {
                var lookAt = new HexPosition(
                    int.Parse(notification.Data["look_x"]),
                    int.Parse(notification.Data["look_y"]));

                _localPlayerInfo.LookAt(lookAt);
            }
        }

        public void QuickDismiss(Notification notification)
        {
            if (notification.QuickDismissable)
            {
                notification.IsDismissed = true;
            }
        }
    }
}
