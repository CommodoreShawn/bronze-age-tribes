﻿using BronzeAgeTribes.Logic.Data;
using BronzeAgeTribes.Logic.Data.Serialization;
using BronzeAgeTribes.Logic.Infrastructure.Interfaces;
using BronzeAgeTribes.Logic.Infrastructure.SimulationEvents;
using System.Collections.Generic;
using System.Linq;

namespace BronzeAgeTribes.Logic.Infrastructure.Services
{
    public class DefaultNotificationTracker : INotificationTracker, ISerializableService
    {
        private readonly INotificationHandler[] _handlers;
        private readonly List<Notification> _notifications;
        public IEnumerable<Notification> CurrentNotifications { get; }

        public DefaultNotificationTracker(
            ISimulationEventBus simulationEventBus,
            IEnumerable<INotificationHandler> handlers)
        {
            _handlers = handlers.ToArray();
            simulationEventBus.OnSimulationEvent += OnSimulationEvent;
            _notifications = new List<Notification>();
            CurrentNotifications = _notifications.AsReadOnly();
        }

        private void OnSimulationEvent(ISimulationEvent evt)
        {
            Godot.GD.Print("OnSimulationEvent: NotificationTracker");
            if (evt is NotificationSimulationEvent nevt)
            {
                _notifications.Add(nevt.Notification);
            }
            else if (evt is TurnAdvancedSimulationEvent)
            {
                DoTurnProcessing();
            }
        }

        private void DoTurnProcessing()
        {
            // TODO expiring time tracking, etc
        }

        public void Open(Notification notification)
        {
            var handler = _handlers
                .Where(h => h.CanHandle(notification))
                .FirstOrDefault();

            if(handler != null)
            {
                handler.Open(notification);

                if(notification.DismissOnOpen)
                {
                    notification.IsDismissed = true;
                }
            }
        }

        public void QuickDismiss(Notification notification)
        {
            var handler = _handlers
                .Where(h => h.CanHandle(notification))
                .FirstOrDefault();

            if (handler != null)
            {
                handler.QuickDismiss(notification);
            }
        }

        public void ClearNotifications()
        {
            foreach(var notification in _notifications)
            {
                notification.IsDismissed = true;
            }
            _notifications.Clear();
        }

        public void Clear()
        {
            _notifications.Clear();
        }

        public void WriteTo(SerializedObject root)
        {
            var systemRoot = root.CreateChild("notification_system");

            foreach (var notification in _notifications.Where(n=>!n.IsDismissed))
            {
                var notificationRoot = systemRoot.CreateChild("notification");

                notificationRoot.Set("auto_open", notification.AutoOpen);
                notificationRoot.Set("handler", notification.Handler);
                notificationRoot.Set("icon", notification.Icon);
                notificationRoot.Set("quick_dismiss", notification.QuickDismissable);
                notificationRoot.Set("sound_effect", notification.SoundEffect);
                notificationRoot.Set("summary_title", notification.SummaryTitle);
                notificationRoot.Set("summary_body", notification.SummaryBody);
                notificationRoot.Set("dismiss_on_open", notification.DismissOnOpen);

                var dataRoot = notificationRoot.CreateChild("data");
                foreach (var kvp in notification.Data)
                {
                    var child = dataRoot.CreateChild("kvp");
                    child.Set("key", kvp.Key);
                    child.Set("value", kvp.Value);
                }
            }
        }

        public void ReadFrom(SerializedObject root)
        {
            var systemRoot = root.GetOptionalChild("notification_system");

            if (systemRoot != null)
            {
                foreach (var notificationRoot in systemRoot.GetChildren("notification"))
                {
                    var notification = new Notification
                    {
                        SoundEffect = notificationRoot.GetString("sound_effect"),
                        AutoOpen = notificationRoot.GetBool("auto_open"),
                        QuickDismissable = notificationRoot.GetBool("quick_dismiss"),
                        DismissOnOpen = notificationRoot.GetBool("dismiss_on_open"),
                        SummaryTitle = notificationRoot.GetString("summary_title"),
                        SummaryBody = notificationRoot.GetString("summary_body"),
                        Handler = notificationRoot.GetString("handler"),
                        Icon = notificationRoot.GetString("icon")
                    };

                    var dataRoot = notificationRoot.GetChild("data");
                    foreach (var child in dataRoot.Children)
                    {
                        notification.Data.Add(child.GetString("key"), child.GetOptionalString("value"));
                    }

                    _notifications.Add(notification);
                }
            }
        }
    }
}
