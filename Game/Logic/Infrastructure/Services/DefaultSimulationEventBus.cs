﻿using BronzeAgeTribes.Logic.Infrastructure.Interfaces;
using BronzeAgeTribes.Logic.Infrastructure.SimulationEvents;

namespace BronzeAgeTribes.Logic.Infrastructure.Services
{
    public class DefaultSimulationEventBus : ISimulationEventBus
    {
        public event SimulationEventHandler OnSimulationEvent;

        public void SendEvent(ISimulationEvent evt)
        {
            OnSimulationEvent?.Invoke(evt);
        }
    }
}
