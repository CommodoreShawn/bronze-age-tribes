﻿using BronzeAgeTribes.Logic.Data.Game;
using BronzeAgeTribes.Logic.Data.Game.Ai;
using BronzeAgeTribes.Logic.Data.Serialization;
using BronzeAgeTribes.Logic.Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BronzeAgeTribes.Logic.Infrastructure.Services
{
    public class AiManager : IAiManager, ISerializableService
    {
        private List<AiState> _aiStates;
        private readonly IWorldManager _worldManager;
        private readonly IResourceTracker _resourceTracker;
        private readonly IPopManager _popManager;
        private readonly ILogger _logger;
        private readonly IDevelopmentListProvider _developmentListProvider;
        private readonly Random _random;

        public AiManager(
            IWorldManager worldManager,
            IResourceTracker resourceTracker,
            IPopManager popManager,
            ILogger logger,
            IDevelopmentListProvider developmentListProvider)
        {
            _worldManager = worldManager;
            _resourceTracker = resourceTracker;
            _popManager = popManager;
            _logger = logger;
            _aiStates = new List<AiState>();
            _random = new Random();
        }

        public void Clear()
        {
            _aiStates.Clear();
        }

        public void DoTurnLogic()
        {
            var allPops = _worldManager.Empires
                .SelectMany(e => e.AdministrativeGroups.SelectMany(g => g.Pops))
                .ToArray();


            foreach (var state in _aiStates)
            {
                // TODO at this point break out between creep and empire ai

                try
                {
                    var wanderChance = state.Traits.Sum(t => t.WanderChance);
                    var attackChance = state.Traits.Sum(t => t.AttackChance);
                    var fleeChance = state.Traits.Sum(t => t.FleeChance);
                    var developChance = state.Traits.Sum(t => t.DevelopChance);

                    foreach (var group in state.Empire.AdministrativeGroups)
                    {
                        group.TargetPopulationCap = state.TargetSize;

                        var netFood = group.GetExpectedOutput(ResourceType.Food);

                        foreach (var pop in group.Pops)
                        {
                            var currentFood = pop.Job?.GetExpectedOutput(ResourceType.Food, false) ?? 0;
                            IEnumerable<PopJob> moveJobs;

                            if (netFood < 0)
                            {
                                moveJobs = _popManager.GetMovementOptions(pop)
                                    .Where(h => h != null)
                                    .SelectMany(h => h.Jobs.Where(j => j.Worker == null))
                                    .Where(j => j != null)
                                    .Where(j => j.GetExpectedOutput(ResourceType.Food, true) > currentFood)
                                    .ToArray();

                                if (moveJobs.Any())
                                {
                                    var moveTo = _random.Choose(moveJobs);

                                    netFood -= pop.Job?.GetExpectedOutput(ResourceType.Food, false) ?? 0;
                                    netFood += moveTo.GetExpectedOutput(ResourceType.Food, true);

                                    _popManager.MovePop(pop, moveTo);
                                }
                            }
                            else if (group.TurnsStarving < 1)
                            {
                                // TODO somewhere account for splitting when full

                                // Flee
                                if (_random.NextDouble() <= fleeChance
                                    && allPops.Any(p => p.Owner != pop.Owner
                                    && p.Location.Position.DistanceTo(pop.Location.Position) < 3 * Constants.VERTICIES_PER_HEX))
                                {
                                    moveJobs = _popManager.GetMovementOptions(pop)
                                        .SelectMany(h => h.Jobs.Where(j => j.Worker == null))
                                        .Where(j => j.GetExpectedOutput(ResourceType.Food, true) >= 1)
                                        .ToArray();

                                    if (moveJobs.Any())
                                    {
                                        _popManager.MovePop(pop, _random.Choose(moveJobs));
                                    }
                                } // Develop
                                else if (_random.NextDouble() <= developChance && pop.Location.Development != null)
                                {
                                    Godot.GD.Print("trying for construction");
                                    var options = _developmentListProvider.GetDevelopmentOptionsFor(pop.Location, pop.Group, pop.Owner);

                                    var supportResources = Enum.GetValues(typeof(ResourceType))
                                        .OfType<ResourceType>()
                                        .Where(x => pop.Species.GetUpkeep(x) > 0)
                                        .ToArray();

                                    var popSupportOptions = options
                                        .Where(o => supportResources.Any(x => o.GetExpectedOutput(x) > 0))
                                        .ToArray();

                                    if(popSupportOptions.Any())
                                    {
                                        var option = _random.Choose(popSupportOptions);

                                        Godot.GD.Print("buildig " + option.Name);

                                        pop.Location.Development = option.MakeDevelopment(
                                            pop.Location,
                                            false,
                                            pop.Culture.BuildingStyle);

                                        pop.Location.Development.SetCurrentInteraction(option.ConstructionInteraction);
                                    }
                                } // Wander
                                else if (_random.NextDouble() <= wanderChance)
                                {
                                    if (currentFood >= 1)
                                    {
                                        moveJobs = _popManager.GetMovementOptions(pop)
                                            .SelectMany(h => h.Jobs.Where(j => j.Worker == null))
                                            .Where(j => j.GetExpectedOutput(ResourceType.Food, true) >= 1)
                                            .ToArray();
                                    }
                                    else
                                    {
                                        moveJobs = _popManager.GetMovementOptions(pop)
                                            .SelectMany(h => h.Jobs.Where(j => j.Worker == null))
                                            .Where(j => j.GetExpectedOutput(ResourceType.Food, true) >= currentFood)
                                            .ToArray();
                                    }

                                    if (moveJobs.Any())
                                    {
                                        _popManager.MovePop(pop, _random.Choose(moveJobs));
                                    }
                                } // Attack
                                else if (_random.NextDouble() <= attackChance
                                    && !group.Pops.Any(p => p.InteractionTo != null))
                                {
                                    var target = pop.Location.Neighbors
                                        .Where(h => h != null)
                                        .SelectMany(h => h.Pops)
                                        .Where(p => p.Owner != pop.Owner)
                                        .FirstOrDefault();

                                    if (target != null)
                                    {
                                        pop.InteractionTo = target;
                                        pop.InteractionType = PopInteractionType.Attack;
                                    }
                                    else
                                    {
                                        target = _popManager.GetMovementOptions(pop)
                                            .SelectMany(h => h.Neighbors)
                                            .Where(h => h != null)
                                            .SelectMany(h => h.Pops)
                                            .Where(p => p.Owner != pop.Owner)
                                            .FirstOrDefault();

                                        if (target != null && target.Location != null)
                                        {
                                            var moveTo = target.Location.Neighbors
                                                .Where(h => _popManager.CanPopBeMoved(pop.Location, h, pop))
                                                .FirstOrDefault();

                                            if (moveTo != null)
                                            {
                                                _popManager.MovePop(pop, moveTo.Jobs.Where(j => j.Worker == null).First());
                                                pop.InteractionTo = target;
                                                pop.InteractionType = PopInteractionType.Attack;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                catch(Exception ex)
                {
                    _logger.Error(ex, "Error running creep AI");
                    _logger.Info(ex.StackTrace);
                }
            }
        }

        public void RegisterAi(
            Empire empire,
            IEnumerable<AiTrait> traits,
            float targetSize)
        {
            _aiStates.Add(new AiState
            {
                Empire = empire,
                Traits = traits.ToArray(),
                TargetSize = targetSize
            });
        }

        public void WriteTo(SerializedObject root)
        {
            var serviceRoot = root.CreateChild("ai_manager");

            foreach (var state in _aiStates)
            {
                var stateRoot = serviceRoot.CreateChild("state");
                stateRoot.Set("empire_id", state.Empire.Id);
                stateRoot.Set("group_target_size", state.TargetSize);
                foreach (var trait in state.Traits)
                {
                    stateRoot.CreateChild("trait").Set("id", trait.Id);
                }
            }
        }

        public void ReadFrom(SerializedObject root)
        {
            var serviceRoot = root.GetChild("ai_manager");

            if (serviceRoot != null)
            {
                foreach (var stateRoot in serviceRoot.GetChildren("state"))
                {
                    var empire = stateRoot.GetOptionalObjectReferenceOrDefault(
                        "empire_id",
                        _worldManager.Empires,
                        e => e.Id);

                    if (empire != null)
                    {
                        var traits = stateRoot.GetChildren("trait")
                            .Select(t => t.GetOptionalObjectReferenceOrDefault(
                                "id",
                                _resourceTracker.Traits,
                                x => x.Id))
                            .Where(t => t != null)
                            .OfType<AiTrait>()
                            .ToArray();

                        _aiStates.Add(new AiState
                        {
                            Empire = empire,
                            Traits = traits,
                            TargetSize = stateRoot.GetFloat("group_target_size")
                        });
                    }
                }
            }
        }
    }
}
