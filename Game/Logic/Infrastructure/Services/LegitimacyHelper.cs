﻿using BronzeAgeTribes.Logic.Data;
using BronzeAgeTribes.Logic.Data.Game;
using BronzeAgeTribes.Logic.Infrastructure.Interfaces;
using Godot;
using System.Collections.Generic;
using System.Linq;

namespace BronzeAgeTribes.Logic.Infrastructure.Services
{
    public class LegitimacyHelper : ILegitimacyHelper
    {
        public float CostForSettling(AdministrativeGroup group, IEnumerable<WorldRegion> regionsToAdd)
        {
            var oldHexes = group.ClaimedRegions.Sum(r => r.Hexes.Count());
            var newHexes = regionsToAdd.Sum(r => r.Hexes.Count());

            return (oldHexes + newHexes) * -2;
        }

        public string[] CostForSettlingDetails(AdministrativeGroup group, IEnumerable<WorldRegion> regionsToAdd)
        {
            var oldHexes = group.ClaimedRegions.Sum(r => r.Hexes.Count());
            var newHexes = regionsToAdd.Sum(r => r.Hexes.Count());

            if(oldHexes > 0)
            {
                return new[]
                {
                    $"{oldHexes * -2} from {oldHexes} current tiles",
                    $"{newHexes * -2} from {newHexes} new tiles",
                };
            }

            return new[] { $"{newHexes * -2} from {newHexes} new tiles" };
        }

        public float CostForSplitting(int toGo)
        {
            return toGo * -5;
        }

        public string[] CostForSplittingDetails(int toGo)
        {
            return new[] { $"{toGo * -5} from {toGo} Pops leaving" };
        }
    }
}
