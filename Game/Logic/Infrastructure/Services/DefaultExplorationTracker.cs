﻿using BronzeAgeTribes.Logic.Data;
using BronzeAgeTribes.Logic.Data.Game;
using BronzeAgeTribes.Logic.Data.Serialization;
using BronzeAgeTribes.Logic.Infrastructure.Interfaces;
using BronzeAgeTribes.Logic.Infrastructure.SimulationEvents;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BronzeAgeTribes.Logic.Infrastructure.Services
{
    public class DefaultExplorationTracker : IExplorationTracker, IInitializableService, ISerializableService
    {
        private readonly IWorldManager _worldManager;
        private readonly ILocalPlayerInfo _localPlayerInfo;
        private readonly ISimulationEventBus _simulationEventBus;
        private readonly IResourceTracker _resourceTracker;

        private HashSet<HexPosition> _explored;
        private HashSet<HexPosition> _visible;

        private Dictionary<HexPosition, ExplorationMemory> _explorationMemories;

        public DefaultExplorationTracker(
            IWorldManager worldManager,
            ILocalPlayerInfo localPlayerInfo,
            ISimulationEventBus simulationEventBus,
            IResourceTracker resourceTracker)
        {
            _worldManager = worldManager;
            _localPlayerInfo = localPlayerInfo;
            _simulationEventBus = simulationEventBus;
            _resourceTracker = resourceTracker;

            simulationEventBus.OnSimulationEvent += OnSimulationEvent;

            _explored = new HashSet<HexPosition>();
            _visible = new HashSet<HexPosition>();
            _explorationMemories = new Dictionary<HexPosition, ExplorationMemory>();
        }

        private void OnSimulationEvent(ISimulationEvent evt)
        {
            Godot.GD.Print("OnSimulationEvent: ExplorationTracker");

            if (evt is WorldPopulatedSimulationEvent && _localPlayerInfo.Empire != null)
            {
                ExploreAround(_localPlayerInfo.Empire, false);
            }
            else if (evt is TurnAdvancedSimulationEvent)
            {
                ExploreAround(_localPlayerInfo.Empire, true);
            }
            else if (evt is WorldLoadedSimulationEvent)
            {
                ExploreAround(_localPlayerInfo.Empire, false);
            }
        }

        private void ExploreAround(Empire empire, bool promptRenderUpdates)
        {
            _visible.Clear();
            var regionsWithExploration = new List<WorldRegion>();

            foreach (var group in empire.AdministrativeGroups)
            {
                foreach (var pop in group.Pops)
                {
                    var hexPosInRange = GetLineOfSight(pop, pop.Location, pop.Species.PopVisionDistance);

                    foreach (var pos in hexPosInRange)
                    {
                        if(!_visible.Contains(pos))
                        {
                            _visible.Add(pos);
                        }

                        if (!_explored.Contains(pos))
                        {
                            _explored.Add(pos);

                            if(promptRenderUpdates)
                            {
                                var hex = _worldManager.GetHex(pos);

                                if(hex != null && !regionsWithExploration.Contains(hex.Region))
                                {
                                    regionsWithExploration.Add(hex.Region);
                                }
                            }
                        }
                    }
                }
            }

            if(empire == _localPlayerInfo.Empire)
            {
                foreach(var pos in _visible)
                {
                    var hex = _worldManager.GetHex(pos);
                    if(hex != null)
                    {
                        ExplorationMemory memory = null;
                        if(_explorationMemories.ContainsKey(pos))
                        {
                            memory = _explorationMemories[pos];
                        }
                        else
                        {
                            memory = new ExplorationMemory();
                            _explorationMemories.Add(pos, memory);
                        }

                        var memoryChange = false;
                        DevelopmentType developmentToDisplay = null;
                        if(hex.Development != null && hex.Development.IsConstructed)
                        {
                            developmentToDisplay = hex.Development.BackingType;
                        }

                        if(memory.DevelopmentType != developmentToDisplay)
                        {
                            memoryChange = true;
                            memory.DevelopmentType = developmentToDisplay;
                        }

                        var hexFeatures = hex.Features.ToArray();

                        for (var i = 0; i < memory.CellFeatureTypes.Count || i < hexFeatures.Length; i++)
                        {
                            if(i < memory.CellFeatureTypes.Count && i < hexFeatures.Length)
                            {
                                if(memory.CellFeatureTypes[i] != hexFeatures[i].BackingType)
                                {
                                    memory.CellFeatureTypes[i] = hexFeatures[i].BackingType;
                                    memoryChange = true;
                                }
                            }
                            else if(i < memory.CellFeatureTypes.Count)
                            {
                                memoryChange = true;
                                memory.CellFeatureTypes.RemoveAt(i);
                            }
                            else
                            {
                                memoryChange = true;
                                memory.CellFeatureTypes.Add(hexFeatures[i].BackingType);
                            }
                        }

                        if(memoryChange && !regionsWithExploration.Contains(hex.Region))
                        {
                            regionsWithExploration.Add(hex.Region);
                        }
                    }
                }
            }

            if (regionsWithExploration.Any())
            {
                _simulationEventBus.SendEvent(new ExplorationSimulationEvent(regionsWithExploration));
            }
        }

        private List<HexPosition> GetLineOfSight(Pop pop, WorldHex location, int distance)
        {
            var los = new List<HexPosition>()
            {
                location.Position
            };

            var possibles = new List<HexPosition>();


            for(var dir = 0; dir < 6; dir += 1)
            {
                var current = location;
                
                var blocked = false;
                var adjustedDistance = distance;
                for(var i = 0; i < adjustedDistance && !blocked; i++)
                {
                    current = current.Neighbors[dir];
                    if(current == null)
                    {
                        blocked = true;
                    }
                    else
                    {
                        los.Add(current.Position);

                        if(current.Terrain.Height > location.Terrain.Height)
                        {
                            blocked = true;
                        }
                        else if(current.TerrainTraits.ContainsAny(pop.Species.LosReducedBy))
                        {
                            adjustedDistance -= 1;
                        }
                        else
                        {
                            possibles.AddRange(current.Neighbors.Where(n => n != null).Select(n => n.Position));
                        }
                    }
                }
            }

            var possiblesToAdd = possibles
                .GroupBy(p => p)
                .Where(g => g.Count() > 1)
                .Select(g => g.Key)
                .ToArray();

            los.AddRange(possiblesToAdd);

            return los;
        }

        public bool IsExplored(HexPosition pos)
        {
            return _explored.Contains(pos);
        }

        public void Initialize()
        {
        }

        public void Clear()
        {
            _explored.Clear();
            _visible.Clear();
            _explorationMemories.Clear();
        }

        public void WriteTo(SerializedObject root)
        {
            var serviceRoot = root.CreateChild("DefaultExplorationTracker");
            foreach(var pos in _explored)
            {
                pos.WriteTo(serviceRoot.CreateChild("pos"));
            }

            foreach(var kvp in _explorationMemories)
            {
                var memoryRoot = serviceRoot.CreateChild("memory");

                kvp.Key.WriteTo(memoryRoot.CreateChild("pos"));
                kvp.Value.WriteTo(memoryRoot.CreateChild("mem"));
            }
        }

        public void ReadFrom(SerializedObject root)
        {
            Clear();
            var serviceRoot = root.GetOptionalChild("DefaultExplorationTracker");
            if (serviceRoot != null)
            {
                foreach (var posRoot in serviceRoot.GetChildren("pos"))
                {
                    var pos = HexPosition.ReadFrom(posRoot);

                    _explored.Add(pos);
                }

                foreach(var memoryRoot in serviceRoot.GetChildren("memory"))
                {
                    var pos = HexPosition.ReadFrom(memoryRoot.GetChild("pos"));
                    var memory = ExplorationMemory.ReadFrom(memoryRoot.GetChild("mem"), _resourceTracker);

                    _explorationMemories.Add(pos, memory);
                }
            }
        }

        public bool IsVisible(HexPosition pos)
        {
            return _visible.Contains(pos);
        }

        public ExplorationMemory GetExplorationMemory(HexPosition pos)
        {
            if(_explorationMemories.ContainsKey(pos))
            {
                return _explorationMemories[pos];
            }

            return null;
        }
    }
}
