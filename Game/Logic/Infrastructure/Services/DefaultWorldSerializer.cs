﻿using BronzeAgeTribes.Logic.Data;
using BronzeAgeTribes.Logic.Data.Game;
using BronzeAgeTribes.Logic.Data.Serialization;
using BronzeAgeTribes.Logic.Infrastructure.Interfaces;
using BronzeAgeTribes.Logic.Infrastructure.SimulationEvents;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;

namespace BronzeAgeTribes.Logic.Infrastructure.Services
{
    public class DefaultWorldSerializer : IWorldSerializer
    {
        private const string SAVE_FOLDER = "saves";
        private const string EXTENSION = ".bta";
        private int CURRENT_SCHEMA = 0;

        private readonly IWorldManager _worldManager;
        private readonly ILocalPlayerInfo _localPlayerInfo;
        private readonly ILogger _logger;
        private readonly INameSource _nameSource;
        private readonly IResourceTracker _resourceTracker;
        private readonly ISerializableService[] _serializableServices;
        private readonly ISimulationEventBus _simulationEventBus;
        private readonly IRegionGenerator _regionGenerator;

        public DefaultWorldSerializer(
            IWorldManager worldManager,
            ILocalPlayerInfo localPlayerInfo,
            ILogger logger,
            INameSource nameSource,
            IResourceTracker resourceTracker,
            IEnumerable<ISerializableService> serializableServices,
            ISimulationEventBus simulationEventBus,
            IRegionGenerator regionGenerator)
        {
            _worldManager = worldManager;
            _localPlayerInfo = localPlayerInfo;
            _logger = logger;
            _nameSource = nameSource;
            _resourceTracker = resourceTracker;
            _serializableServices = serializableServices.ToArray();
            _simulationEventBus = simulationEventBus;
            _regionGenerator = regionGenerator;
        }

        public void Autosave(ReportProgress report)
        {
            SaveWorld(report, "autosave");
        }

        public bool CanLoad(int schemaVersion)
        {
            return false;
        }

        public IEnumerable<SaveSummary> ListSaveFiles()
        {
            Directory.CreateDirectory(SAVE_FOLDER);

            var results = new List<SaveSummary>();

            foreach (var filename in Directory.EnumerateFiles(SAVE_FOLDER).Where(filename => filename.EndsWith(EXTENSION)))
            {
                try
                {
                    using (var archiveStream = File.OpenRead(filename))
                    using (var archive = new ZipArchive(archiveStream))
                    {
                        var summaryEntry = archive.Entries
                            .Where(e => e.Name == "summary.json")
                            .FirstOrDefault();

                        using (var summaryStream = summaryEntry.Open())
                        using (var streamReader = new StreamReader(summaryStream))
                        {
                            var summary = JsonConvert.DeserializeObject<SaveSummary>(streamReader.ReadToEnd());

                            summary.FileName = Path.GetFileNameWithoutExtension(filename);

                            results.Add(summary);
                        }
                    }
                }
                catch (Exception ex)
                {
                    _logger.Error(ex, "Error when listing save files");
                }
            }

            return results;
        }

        public void SaveWorld(ReportProgress report, string filename)
        {
            var file = new SaveFile();

            report(0, "Writing Summary");

            file.Summary.FileName = filename;
            file.Summary.LastPlayed = DateTime.Now;
            file.Summary.PlayedTurns = _worldManager.Turn;
            file.Summary.PlayerEmpire = _localPlayerInfo.Empire.Name ?? string.Empty;
            file.Summary.SchemaVersion = CURRENT_SCHEMA;
            file.Summary.Seed = _worldManager.Seed;

            file.Root.Set("turn", _worldManager.Turn);
            file.Root.Set("player_empire_id", _localPlayerInfo.Empire?.Id);
            file.Root.Set("world_radius_hex", _worldManager.WorldRadiusHex);

            SerializedObject.WriteVector3(file.Root.CreateChild("player_view_center"), _localPlayerInfo.ViewCenter);

            report(0.0f, "Writing Structure");
            AddWorldStructure(file);
            report(0.2f, "Writing Cultures");
            AddCultures(file);
            report(0.4f, "Writing Empires");
            AddEmpires(file);
            report(0.8f, "Writing Services");
            foreach(var service in _serializableServices)
            {
                service.WriteTo(file.Root);
            }
            report(0.9f, "Writing File");
            WriteWorldToDisk(file);
        }

        private void AddWorldStructure(SaveFile file)
        {
            var structureRoot = file.Root.CreateChild("structure");

            var hexes = _worldManager.Hexes.ToArray();
            for(var i = 0; i < hexes.Length; i++)
            {
                hexes[i].WriteTo(structureRoot.CreateChild("hex"));
            }

            var regions = _worldManager.Regions.ToArray();
            for (var i = 0; i < regions.Length; i++)
            {
                regions[i].WriteTo(structureRoot.CreateChild("region"));
            }
        }

        private void AddCultures(SaveFile file)
        {
            var cultureRoot = file.Root.CreateChild("culture");

            var cultures = _worldManager.Cultures.ToArray();
            for (var i = 0; i < cultures.Length; i++)
            {
                cultures[i].WriteTo(cultureRoot.CreateChild("culture"));
            }
        }

        private void AddEmpires(SaveFile file)
        {
            var empireRoot = file.Root.CreateChild("empires");

            var empires = _worldManager.Empires.ToArray();
            for (var i = 0; i < empires.Length; i++)
            {
                empires[i].WriteTo(empireRoot.CreateChild("empire"));
            }
        }

        private void WriteWorldToDisk(SaveFile file)
        {
            Directory.CreateDirectory(SAVE_FOLDER);

            using (var saveStream = File.Open(Path.Combine(SAVE_FOLDER, file.FileName + EXTENSION), FileMode.Create))
            using (var archive = new ZipArchive(saveStream, ZipArchiveMode.Create))
            {
                var summaryEntry = archive.CreateEntry("summary.json");
                using (var stream = summaryEntry.Open())
                using (var streamWriter = new StreamWriter(stream))
                {

                    streamWriter.Write(JsonConvert.SerializeObject(file.Summary));
                    streamWriter.Flush();
                }

                var bodyEntry = archive.CreateEntry("body.json");
                using (var stream = bodyEntry.Open())
                using (var streamWriter = new StreamWriter(stream))
                {

                    streamWriter.Write(JsonConvert.SerializeObject(file.Root));
                    streamWriter.Flush();
                }
            }
        }

        public void LoadWorld(ReportProgress report, SaveSummary saveSummary)
        {
            report(0.0f, "Reading File");
            var saveFile = ReadWorldFromDisk(saveSummary);

            var turn = saveFile.Root.GetInt("turn");
            var playerEmpireId = saveFile.Root.GetOptionalString("player_empire_id");
            var worldRadiusHex = saveFile.Root.GetInt("world_radius_hex");

            _nameSource.InitializeForSeed(saveSummary.Seed.GetHashCode());

            report(0.2f, "Reading Structure");
            var regions = ReadRegions(saveFile);
            var hexes = ReadHexes(saveFile, regions, worldRadiusHex);
            _regionGenerator.LinkRegionsToNeighbors(regions);

            report(0.6f, "Reading Cultures");
            var cultures = ReadCultures(saveFile);

            report(0.8f, "Reading Empires");
            var empires = ReadEmpires(saveFile, cultures, hexes, regions);

            report(100f, "Finishing.");
            _worldManager.SetWorld(
                turn,
                saveSummary.Seed,
                worldRadiusHex,
                hexes,
                regions);

            _worldManager.Empires.Clear();
            _worldManager.Cultures.Clear();

            _worldManager.Empires.AddRange(empires);
            _worldManager.Cultures.AddRange(cultures);

            _localPlayerInfo.Empire = empires
                .Where(e => e.Id == playerEmpireId)
                .FirstOrDefault();

            _localPlayerInfo.ViewCenter = SerializedObject.ReadVector3(saveFile.Root.GetChild("player_view_center"));

            foreach (var service in _serializableServices)
            {
                service.ReadFrom(saveFile.Root);
            }

            _simulationEventBus.SendEvent(new WorldLoadedSimulationEvent());
        }

        private SaveFile ReadWorldFromDisk(SaveSummary saveSummary)
        {
            var file = new SaveFile();

            using (var saveStream = File.Open(Path.Combine(SAVE_FOLDER, saveSummary.FileName + EXTENSION), FileMode.Open))
            using (var archive = new ZipArchive(saveStream, ZipArchiveMode.Read))
            {
                var summaryEntry = archive.Entries
                            .Where(e => e.Name == "summary.json")
                            .FirstOrDefault();

                using (var stream = summaryEntry.Open())
                using (var streamReader = new StreamReader(stream))
                {
                    file.Summary = JsonConvert.DeserializeObject<SaveSummary>(streamReader.ReadToEnd());
                }

                var bodyEntry = archive.Entries
                            .Where(e => e.Name == "body.json")
                            .FirstOrDefault();
                using (var stream = bodyEntry.Open())
                using (var streamReader = new StreamReader(stream))
                {
                    file.Root = JsonConvert.DeserializeObject<SerializedObject>(streamReader.ReadToEnd());
                }
            }

            return file;
        }

        private List<WorldRegion> ReadRegions(SaveFile saveFile)
        {
            var regions = new List<WorldRegion>();
            var structureRoot = saveFile.Root.GetChild("structure");

            foreach (var child in structureRoot.GetChildren("region"))
            {
                regions.Add(WorldRegion.ReadFrom(child, _resourceTracker));
            }

            return regions;
        }

        private List<WorldHex> ReadHexes(SaveFile saveFile, List<WorldRegion> regions, int worldRadiusHexes)
        {
            var hexes = new List<WorldHex>();
            var structureRoot = saveFile.Root.GetChild("structure");

            foreach(var child in structureRoot.GetChildren("hex"))
            {
                hexes.Add(WorldHex.ReadFrom(child, _resourceTracker, regions));
            }

            LinkHexesToNeighbors(hexes, worldRadiusHexes);

            return hexes;
        }

        private List<Culture> ReadCultures(SaveFile saveFile)
        {
            var cultures = new List<Culture>();
            var cultureRoot = saveFile.Root.GetChild("culture");

            foreach (var child in cultureRoot.GetChildren("culture"))
            {
                cultures.Add(Culture.ReadFrom(child, _resourceTracker, cultures));
            }

            return cultures;
        }

        private List<Empire> ReadEmpires(SaveFile saveFile, List<Culture> cultures, List<WorldHex> hexes, List<WorldRegion> regions)
        {
            var empires = new List<Empire>();
            var empireRoot = saveFile.Root.GetChild("empires");

            foreach (var child in empireRoot.GetChildren("empire"))
            {
                empires.Add(Empire.ReadFrom(child, _resourceTracker, cultures, hexes, regions));
            }

            foreach (var child in empireRoot.GetChildren("empire"))
            {
                var allPops = empires
                    .SelectMany(e => e.AdministrativeGroups)
                    .SelectMany(g => g.Pops)
                    .ToArray();

                Empire.LateLoadPops(child, allPops, _resourceTracker);
            }

            return empires;
        }


        private void LinkHexesToNeighbors(List<WorldHex> hexes, int worldRadiusHexes)
        {
            var hexLookup = new WorldHex[worldRadiusHexes * 2, worldRadiusHexes * 2];

            foreach(var hex in hexes)
            {
                hexLookup[hex.Position.HexX, hex.Position.HexY] = hex;
            }

            foreach (var hex in hexes)
            {
                if (hex.Position.HexY % 2 == 0)
                {
                    hex.Neighbors[(int)HexFacing.NorthEast] = GetHexIfExists(
                        hex.Position,
                        hexLookup,
                        0,
                        -1);
                    hex.Neighbors[(int)HexFacing.NorthWest] = GetHexIfExists(
                        hex.Position,
                        hexLookup,
                        -1,
                        -1);

                    hex.Neighbors[(int)HexFacing.SouthEast] = GetHexIfExists(
                        hex.Position,
                        hexLookup,
                        0,
                        1);
                    hex.Neighbors[(int)HexFacing.SouthWest] = GetHexIfExists(
                        hex.Position,
                        hexLookup,
                        -1,
                        1);
                }
                else
                {
                    hex.Neighbors[(int)HexFacing.NorthEast] = GetHexIfExists(
                        hex.Position,
                        hexLookup,
                        1,
                        -1);
                    hex.Neighbors[(int)HexFacing.NorthWest] = GetHexIfExists(
                        hex.Position,
                        hexLookup,
                        0,
                        -1);

                    hex.Neighbors[(int)HexFacing.SouthEast] = GetHexIfExists(
                        hex.Position,
                        hexLookup,
                        1,
                        1);
                    hex.Neighbors[(int)HexFacing.SouthWest] = GetHexIfExists(
                        hex.Position,
                        hexLookup,
                        0,
                        1);
                }

                hex.Neighbors[(int)HexFacing.East] = GetHexIfExists(
                        hex.Position,
                        hexLookup,
                        1,
                        0);
                hex.Neighbors[(int)HexFacing.West] = GetHexIfExists(
                    hex.Position,
                    hexLookup,
                    -1,
                    0);
            }
        }

        private WorldHex GetHexIfExists(
            HexPosition position,
            WorldHex[,] hexLookup,
            int dx,
            int dy)
        {
            var x = position.HexX + dx;
            var y = position.HexY + dy;

            if (x < 0
                || y < 0
                || x >= hexLookup.GetLength(0)
                || y >= hexLookup.GetLength(1))
            {
                return null;
            }

            return hexLookup[x, y];
        }

        public void DeleteSave(SaveSummary saveToDelete)
        {
            File.Delete(Path.Combine(SAVE_FOLDER, saveToDelete.FileName + EXTENSION));
        }
    }
}
