﻿using BronzeAgeTribes.Logic.Data;
using BronzeAgeTribes.Logic.Data.Game;
using BronzeAgeTribes.Logic.Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BronzeAgeTribes.Logic.Infrastructure.Services
{
    public class PopManager : IPopManager
    {
        private readonly ILocalPlayerInfo _localPlayerInfo;
        private readonly IExplorationTracker _explorationTracker;
        private readonly IWorldManager _worldManager;
        private readonly ISimpleNotificationBuilder _simpleNotificationBuilder;
        private readonly Random _random;

        public PopManager(
            ILocalPlayerInfo localPlayerInfo,
            IExplorationTracker explorationTracker,
            IWorldManager worldManager,
            ISimpleNotificationBuilder simpleNotificationBuilder)
        {
            _localPlayerInfo = localPlayerInfo;
            _explorationTracker = explorationTracker;
            _worldManager = worldManager;
            _simpleNotificationBuilder = simpleNotificationBuilder;
            _random = new Random();
        }

        public void MovePop(Pop pop, PopJob toJob)
        {
            if (pop.InteractionTo != null)
            {
                pop.InteractionTo = null;
                pop.InteractionType = null;
            }

            // cancelling pop move
            if (pop.MovingFromJob == toJob)
            {
                var oldJob = pop.Job;
                oldJob.Location.Pops.Remove(pop);
                toJob.Location.Pops.Add(pop);
                oldJob.Worker = null;
                toJob.Worker = pop;
                pop.Job = toJob;
                toJob.MovingOutWorker = null;
                pop.MovingFromJob = null;
                pop.Location = toJob.Location;
            }
            else // normal pop move
            {
                if (pop.Job != null)
                {
                    var oldJob = pop.Job;
                    oldJob.Worker = null;
                    oldJob.Location.Pops.Remove(pop);

                    if (pop.MovingFromJob == null)
                    {
                        oldJob.MovingOutWorker = pop;
                        pop.MovingFromJob = oldJob;
                    }
                }
                else
                {
                    pop.MovingFromJob = null;
                }

                toJob.Worker = pop;
                pop.Job = toJob;
                toJob.Location.Pops.Add(pop);
                pop.Location = toJob.Location;   
            }
        }

        public bool CanNewPopBePlaced(
            WorldHex hex,
            AdministrativeGroup group,
            Species species,
            Culture culture)
        {
            // TODO account for caste here

            if (group.IsSettled)
            {
                return species.MovementType.RequiredTraits.Intersect(hex.TerrainTraits).Any()
                    && hex.Region.ClaimedBy == group
                    && hex.Jobs.Any(j => j.Worker == null && j.CanWork(species, culture))
                    && hex.Jobs.All(j => j?.Worker?.Group == null || j?.Worker?.Group == group);
            }

            return species.MovementType.RequiredTraits.Intersect(hex.TerrainTraits).Any()
                && hex.Position.DistanceTo(group.GroupCenter) < group.Owner.TribeCoherency * Constants.VERTICIES_PER_HEX
                && hex.Jobs.Any(j => j.Worker == null && j.CanWork(species, culture))
                && (group.Owner != _localPlayerInfo.Empire || _explorationTracker.IsExplored(hex.Position))
                && hex.Jobs.All(j => j?.Worker?.Group == null || j?.Worker?.Group == group);
        }

        public bool CanPopBePlaced(WorldHex hex, Pop pop)
        {
            return CanNewPopBePlaced(hex, pop.Group, pop.Species, pop.Culture);
        }

        public bool CanPopBeMoved(WorldHex fromHex, WorldHex toHex, Pop pop)
        {
            return CanPopBePlaced(toHex, pop)
                && fromHex.Position.DistanceTo(toHex.Position) < pop.Species.PopMovementDistance * Constants.VERTICIES_PER_HEX;
        }

        public IEnumerable<WorldHex> GetMovementOptions(Pop pop)
        {
            var range = pop.Species.PopMovementDistance;

            return Enumerable.Range(-range, range * 2)
                .SelectMany(i => Enumerable.Range(-range, range * 2).Select(j => Tuple.Create(i, j)))
                .Select(t => _worldManager.GetHexAtWorldPoint(pop.Location.Position.RealX + t.Item1 * Constants.VERTICIES_PER_HEX, pop.Location.Position.RealY + t.Item2 * Constants.VERTICIES_PER_HEX))
                .Distinct()
                .Where(h => h != null)
                .Where(h => h.Position.DistanceTo(pop.Location.Position) < range * Constants.VERTICIES_PER_HEX)
                .Where(h => CanPopBePlaced(h, pop))
                .ToArray();
        }

        public string[] GetPopInteractionChanceBreakdown(
            Pop interactor,
            Pop target,
            PopInteractionType interactionType)
        {
            var chances = GetPopInteractionChances(interactor, target, interactionType);
            var breakdown = new List<string>();

            if (chances.Length > 0)
            {
                breakdown.Add(chances[0].Item1.ToString("P0") + " " + chances[0].Item2);

                breakdown.AddRange(
                    chances.Skip(1)
                        .Select(t => (t.Item1 >= 0 ? "+" + t.Item1.ToString("P0") : t.Item1.ToString("P0")) + " " + t.Item2));
            }

            return breakdown.ToArray();
        }

        private Tuple<float, string>[] GetPopInteractionChances(
            Pop interactor,
            Pop target,
            PopInteractionType interactionType)
        {
            switch (interactionType)
            {
                case PopInteractionType.Assimilate:
                    return new[]
                    {
                        Tuple.Create(0.5f, "Base  chance"),
                        Tuple.Create(interactor.Group.GetExpectedOutput(ResourceType.Luxuries) / 15f, $"{interactor.Group.Name} luxuries"),
                        Tuple.Create(-target.Group.GetExpectedOutput(ResourceType.Luxuries) / 15f, $"{target.Group.Name} luxuries"),
                    };
                case PopInteractionType.Attack:
                    return new[]
                    {
                        Tuple.Create(0.5f, "Base chance")
                    }.Concat(interactor.Traits
                        .Where(t => t.CombatBonus != 0)
                        .Select(t => Tuple.Create(t.CombatBonus, t.Name)))
                    .Concat(target.Traits
                        .Where(t => t.CombatBonus != 0)
                        .Select(t => Tuple.Create(-t.CombatBonus, "Target " + t.Name)))
                    .ToArray();
                case PopInteractionType.Expel:
                    return new[]
                    {
                        Tuple.Create(0.4f, "Base chance"),
                    }.Concat(interactor.Traits
                        .Where(t => t.CombatBonus != 0)
                        .Select(t => Tuple.Create(t.CombatBonus, t.Name)))
                    .Concat(target.Traits
                        .Where(t => t.CombatBonus != 0)
                        .Select(t => Tuple.Create(-t.CombatBonus, "Target " + t.Name)))
                    .ToArray();
                case PopInteractionType.Domesticate:
                    return new[]
                     {
                        Tuple.Create(0.4f, "Base chance"),
                        Tuple.Create(interactor.Group.GetExpectedOutput(ResourceType.Food) / 15f, $"{interactor.Group.Name} food"),
                        Tuple.Create(-target.Group.GetExpectedOutput(ResourceType.Food) / 15f, $"{target.Group.Name} food"),
                    };
                case PopInteractionType.Raid:
                    return new[]
                     {
                        Tuple.Create(0.4f, "Base chance"),
                        Tuple.Create(interactor.Group.GetExpectedOutput(ResourceType.Food) / 15f, $"{interactor.Group.Name} food"),
                        Tuple.Create(-target.Group.GetExpectedOutput(ResourceType.Food) / 15f, $"{target.Group.Name} food"),
                    };
                default:
                    return new Tuple<float, string>[0];
            }
        }

        public void RunPopInteractionLogic(Pop interactor, Pop target, PopInteractionType? interactionType)
        {
            switch (interactionType)
            {
                case PopInteractionType.Assimilate:
                    RunPopAssimilateLogic(interactor, target);
                    break;
                case PopInteractionType.Attack:
                    RunPopAttackLogic(interactor, target);
                    break;
                case PopInteractionType.Expel:
                    RunPopExpelLogic(interactor, target);
                    break;
                case PopInteractionType.Domesticate:
                    RunPopDomesticateLogic(interactor, target);
                    break;
            }
        }

        private void RunPopAssimilateLogic(Pop interactor, Pop target)
        {
            var chance = GetPopInteractionChances(interactor, target, PopInteractionType.Assimilate).Sum(t => t.Item1);

            if (_random.NextDouble() < chance)
            {
                var oldGroup = target.Group;
                interactor.InteractionTo = null;
                interactor.InteractionType = null;

                ChangeOwner(target, interactor.Group);

                if (interactor.Owner == _localPlayerInfo.Empire)
                {
                    _simpleNotificationBuilder
                        .SendLookAtNotification(
                            "notification_popinteract_good_Assimilate",
                            "Assimilation Suceeded",
                            $"We convinced the {target.Species.Name} {target.Culture.Name} to join us.",
                            target.Location.Position);
                }
                else if (oldGroup.Owner == _localPlayerInfo.Empire)
                {
                    _simpleNotificationBuilder
                        .SendLookAtNotification(
                            "notification_popinteract_bigalert_Assimilate",
                            $"Pop Left {oldGroup.Name}",
                            $"The {interactor.Owner.Name} have convinced one of our pops to join their empire.",
                            target.Location.Position);
                }
            }
            else
            {
                if (interactor.Owner == _localPlayerInfo.Empire)
                {
                    _simpleNotificationBuilder
                        .SendLookAtNotification(
                            "notification_popinteract_bad_Assimilate",
                            "Assimilation Attempt Failed",
                            $"We were unable to convince the {target.Species.Name} {target.Culture.Name} to join us.",
                            target.Location.Position);
                }
                else if (target.Owner == _localPlayerInfo.Empire)
                {
                    _simpleNotificationBuilder
                        .SendLookAtNotification(
                            "notification_popinteract_alert_Assimilate",
                            "Assimilation Attempt",
                            $"{interactor.Owner.Name} is trying to convince one of our pops to join them.",
                            interactor.Location.Position);
                }
            }
        }

        private void ChangeOwner(Pop pop, AdministrativeGroup destination)
        {
            pop.Group.Pops.Remove(pop);
            pop.Owner = destination.Owner;
            pop.Group = destination;
            destination.Pops.Add(pop);
            pop.InteractionTo = null;
            pop.InteractionType = null;
        }

        private void RunPopAttackLogic(Pop interactor, Pop target)
        {
            var chance = GetPopInteractionChances(interactor, target, PopInteractionType.Attack).Sum(t => t.Item1);

            if (_random.NextDouble() < chance)
            {
                target.Health -= (float)(_random.NextDouble() * 0.25 + 0.25);

                if(target.Health <= 0)
                {
                    interactor.InteractionTo = null;
                    interactor.InteractionType = null;
                }

                if (interactor.Owner == _localPlayerInfo.Empire)
                {
                    _simpleNotificationBuilder
                        .SendLookAtNotification(
                            "notification_popinteract_good_Attack",
                            "Attack Suceeded",
                            $"Our pop has attacked and injured the {target.Species.Name} {target.Culture.Name} pop.",
                            target.Location.Position);
                }
                else if (target.Owner == _localPlayerInfo.Empire)
                {
                    _simpleNotificationBuilder
                        .SendLookAtNotification(
                            "notification_popinteract_bigalert_Attack",
                            $"Pop Attacked",
                            $"{interactor.Owner.Name} has attacked and injured one of our pops.",
                            target.Location.Position);
                }
            }
            else
            {
                interactor.Health -= (float)(_random.NextDouble() * 0.25 + 0.25);

                if (interactor.Owner == _localPlayerInfo.Empire)
                {
                    _simpleNotificationBuilder
                        .SendLookAtNotification(
                            "notification_popinteract_bad_Attack",
                            "Attack Failed",
                            $"Our pop attacked the {target.Species.Name} {target.Culture.Name} pop, but was injured instead.",
                            target.Location.Position);
                }
                else if (target.Owner == _localPlayerInfo.Empire)
                {
                    _simpleNotificationBuilder
                        .SendLookAtNotification(
                            "notification_popinteract_alert_Attack",
                            $"Pop Attacked",
                            $"{interactor.Owner.Name} tried to attack one of our pops.",
                            target.Location.Position);
                }
            }
        }

        private void RunPopExpelLogic(Pop interactor, Pop target)
        {
            var chance = GetPopInteractionChances(interactor, target, PopInteractionType.Expel).Sum(t => t.Item1);

            if (_random.NextDouble() < chance)
            {
                interactor.InteractionTo = null;
                interactor.InteractionType = null;

                var fleeLocation = target.Location.Neighbors
                    .Where(n => n != null && CanPopBeMoved(target.Location, n, target))
                    .OrderByDescending(n => n.Position.DistanceTo(interactor.Location.Position))
                    .FirstOrDefault();

                if (fleeLocation != null)
                {
                    MovePop(target, fleeLocation.Jobs.Where(j => j.Worker == null && j.CanWork(target)).First());

                    if (interactor.Owner == _localPlayerInfo.Empire)
                    {
                        _simpleNotificationBuilder
                            .SendLookAtNotification(
                                "notification_popinteract_good_Expel",
                                "Expulsion Suceeded",
                                $"Our pop has driven off the {target.Species.Name} {target.Culture.Name} pop.",
                                target.Location.Position);
                    }
                    else if (target.Owner == _localPlayerInfo.Empire)
                    {
                        _simpleNotificationBuilder
                            .SendLookAtNotification(
                                "notification_popinteract_bigalert_Expel",
                                $"Pop Expelled",
                                $"{interactor.Owner.Name} has driven off one of our pops.",
                                target.Location.Position);
                    }
                }
                else
                {
                    if (interactor.Owner == _localPlayerInfo.Empire)
                    {
                        _simpleNotificationBuilder
                            .SendLookAtNotification(
                                "notification_popinteract_bad_Expel",
                                "Expulsion Impossible",
                                $"Our cannot drive off the {target.Species.Name} {target.Culture.Name} pop, as they have nowhere to go.",
                                target.Location.Position);
                    }
                }
            }
            else
            {
                interactor.Health -= (float)(_random.NextDouble() * 0.25 + 0.25);

                if (interactor.Owner == _localPlayerInfo.Empire)
                {
                    _simpleNotificationBuilder
                        .SendLookAtNotification(
                            "notification_popinteract_bad_Expel",
                            "Explusion Failed",
                            $"Our failed to drive off the {target.Species.Name} {target.Culture.Name} pop, and was injured.",
                            target.Location.Position);
                }
                else if (target.Owner == _localPlayerInfo.Empire)
                {
                    _simpleNotificationBuilder
                        .SendLookAtNotification(
                            "notification_popinteract_alert_Expel",
                            $"Pop Attacked",
                            $"{interactor.Owner.Name} tried to drive off one of our pops.",
                            target.Location.Position);
                }
            }
        }

        private void RunPopDomesticateLogic(Pop interactor, Pop target)
        {
            var chance = GetPopInteractionChances(interactor, target, PopInteractionType.Domesticate).Sum(t => t.Item1);

            if (_random.NextDouble() < chance)
            {
                var oldGroup = target.Group;
                interactor.InteractionTo = null;
                interactor.InteractionType = null;

                ChangeOwner(target, interactor.Group);
                target.ManagerSpecies = interactor.ManagerSpecies ?? interactor.Species;

                if (interactor.Owner == _localPlayerInfo.Empire)
                {
                    _simpleNotificationBuilder
                        .SendLookAtNotification(
                            "notification_popinteract_good_Assimilate",
                            "Domestication Suceeded",
                            $"We successfully domesticated the {target.Species.Name}.",
                            target.Location.Position);
                }
                else if (oldGroup.Owner == _localPlayerInfo.Empire)
                {
                    _simpleNotificationBuilder
                        .SendLookAtNotification(
                            "notification_popinteract_bigalert_Assimilate",
                            $"Pop Left {oldGroup.Name}",
                            $"The {interactor.Owner.Name} have domesticated one of our {target.Species.Name} pops.",
                            target.Location.Position);
                }
            }
            else
            {
                if (interactor.Owner == _localPlayerInfo.Empire)
                {
                    _simpleNotificationBuilder
                        .SendLookAtNotification(
                            "notification_popinteract_bad_Assimilate",
                            "Domestication Attempt Failed",
                            $"We were unable to domesticate the {target.Species.Name}.",
                            target.Location.Position);
                }
                else if (target.Owner == _localPlayerInfo.Empire)
                {
                    _simpleNotificationBuilder
                        .SendLookAtNotification(
                            "notification_popinteract_alert_Assimilate",
                            "Domestication Attempt",
                            $"{interactor.Owner.Name} is trying to domesticate one of our {target.Species.Name} pops.",
                            interactor.Location.Position);
                }
            }
        }
    }
}
