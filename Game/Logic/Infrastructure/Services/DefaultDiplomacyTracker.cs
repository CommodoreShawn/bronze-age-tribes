﻿using BronzeAgeTribes.Logic.Data.Serialization;
using BronzeAgeTribes.Logic.Infrastructure.Interfaces;
using BronzeAgeTribes.Logic.Infrastructure.SimulationEvents;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BronzeAgeTribes.Logic.Infrastructure.Services
{
    public class DefaultDiplomacyTracker : IInitializableService, ISerializableService
    {
        private readonly IWorldManager _worldManager;
        private readonly IExplorationTracker _explorationTracker;
        private readonly ILocalPlayerInfo _localPlayerInfo;
        private readonly ISimpleNotificationBuilder _simpleNotificationBuilder;

        private List<string> _previouslyMetEmpires;

        public DefaultDiplomacyTracker(
            IWorldManager worldManager,
            IExplorationTracker explorationTracker,
            ISimulationEventBus simulationEventBus,
            ILocalPlayerInfo localPlayerInfo,
            ISimpleNotificationBuilder simpleNotificationBuilder)
            
        {
            _worldManager = worldManager;
            _explorationTracker = explorationTracker;
            _localPlayerInfo = localPlayerInfo;
            _simpleNotificationBuilder = simpleNotificationBuilder;
            simulationEventBus.OnSimulationEvent += OnSimulationEvent;
            _previouslyMetEmpires = new List<string>();
        }

        private void OnSimulationEvent(ISimulationEvent evt)
        {
            Godot.GD.Print("OnSimulationEvent: DiplomacyTracker");

            if (evt is WorldPopulatedSimulationEvent
                || evt is TurnAdvancedSimulationEvent
                || evt is WorldLoadedSimulationEvent)
            {
                CheckForFirstContact();
            }
        }

        private void CheckForFirstContact()
        {
            foreach(var empire in _worldManager.Empires)
            {
                if(empire != _localPlayerInfo.Empire
                    && !_previouslyMetEmpires.Contains(empire.Id))
                {
                    var contactPop = empire.AdministrativeGroups
                        .SelectMany(g => g.Pops)
                        .Where(p => _explorationTracker.IsVisible(p.Location.Position))
                        .FirstOrDefault();

                    if (contactPop != null)
                    {
                        _previouslyMetEmpires.Add(empire.Id);

                        _simpleNotificationBuilder.SendLookAtNotification(
                            "notification_diplomacy_contact",
                            $"{empire.Name} discovered",
                            $"We have met a {contactPop.Culture.Name} {contactPop.Species.Name} pop belonging to the {empire.Name}.",
                            contactPop.Location.Position);
                    }
                }
            }
        }

        public void Initialize()
        {
        }

        public void Clear()
        {
            _previouslyMetEmpires.Clear();
        }

        public void WriteTo(SerializedObject root)
        {
            var serviceRoot = root.CreateChild("diplomacy_tracker");

            foreach(var id in _previouslyMetEmpires)
            {
                serviceRoot.CreateChild("empire").Set("id", id);
            }
        }

        public void ReadFrom(SerializedObject root)
        {
            var serviceRoot = root.GetOptionalChild("diplomacy_tracker");

            if (serviceRoot != null)
            {
                _previouslyMetEmpires.AddRange(
                    serviceRoot.GetChildren("empire")
                    .Select(x => x.GetString("id")));
            }
        }
    }
}
