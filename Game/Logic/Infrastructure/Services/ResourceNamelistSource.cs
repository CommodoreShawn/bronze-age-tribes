﻿using BronzeAgeTribes.Logic.Infrastructure.Interfaces;
using CsvHelper;
using Godot;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UNM.Parser.Data;
using UNM.Parser.Implementation;
using UNM.Parser.Interfaces;

namespace BronzeAgeTribes.Logic.Infrastructure.Services
{
    public class ResourceNamelistSource : INamelistSource, IInitializableService
    {
        private readonly Dictionary<string, Namelist> _namelists;
        private readonly IContextExpressionParser _expressionParser;
        private readonly IResourceInspector _resourceInspector;

        public ResourceNamelistSource(IResourceInspector resourceInspector)
        {
            _resourceInspector = resourceInspector;
            _namelists = new Dictionary<string, Namelist>();
            _expressionParser = new ContextExpressionParser();
        }

        public void Initialize()
        {
            _expressionParser.Initialize();

            var resources = _resourceInspector.ListTextFiles("res://Resources/Namelists/");

            foreach (var resource in resources)
            {
                using (var reader = new CsvReader(new StringReader(resource)))
                {
                    while (reader.Read())
                    {
                        var listName = reader.GetField(0);

                        if (!_namelists.ContainsKey(listName))
                        {
                            _namelists.Add(listName, new Namelist(listName));
                        }

                        var list = _namelists[listName];

                        var fragment = reader.GetField(1);

                        if (fragment.Length > 1)
                        {
                            var contextExpression = _expressionParser.ParseExpression(reader.GetField(2));

                            list.AddFragment(new NameFragment(fragment, contextExpression));
                        }
                    }
                }
            }
        }

        public void ClearNamelists()
        {
            _namelists.Clear();
        }

        //public void AddFile(Stream fileStream)
        //{
        //    using (var reader = new CsvReader(new StreamReader(fileStream)))
        //    {
        //        while (reader.Read())
        //        {
        //            var listName = reader.GetField(0);

        //            if (!_namelists.ContainsKey(listName))
        //            {
        //                _namelists.Add(listName, new Namelist(listName));
        //            }

        //            var list = _namelists[listName];

        //            var fragment = reader.GetField(1);

        //            if (fragment.Length > 1)
        //            {
        //                var contextExpression = _expressionParser.ParseExpression(reader.GetField(2));

        //                list.AddFragment(new NameFragment(fragment, contextExpression));
        //            }
        //        }
        //    }
        //}

        public Namelist GetNamelist(string name)
        {
            if (_namelists.ContainsKey(name))
            {
                return _namelists[name];
            }

            throw new ArgumentException("No list matching name: " + name);
        }
    }
}
