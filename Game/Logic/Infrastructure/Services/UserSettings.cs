﻿using BronzeAgeTribes.Logic.Infrastructure.Interfaces;
using BronzeAgeTribes.Logic.Infrastructure.SimulationEvents;
using Godot;
using System;

namespace BronzeAgeTribes.Logic.Infrastructure.Services
{
    public class UserSettings : IUserSettings, IInitializableService
    {
        private readonly ISimulationEventBus _simulationEventBus;
        private float _musicVolume;
        private int _autosaveInterval;

        public UserSettings(ISimulationEventBus simulationEventBus)
        {
            _simulationEventBus = simulationEventBus;
        }
        
        public float MusicVolume
        {
            get => _musicVolume;
            set
            {
                _musicVolume = value;
                UpdateSettings();
            }
        }

        public int AutosaveInterval
        {
            get => _autosaveInterval;
            set
            {
                _autosaveInterval = value;
                UpdateSettings();
            }
        }

        public void Initialize()
        {
            var config = new ConfigFile();
            config.Load("user://settings.cfg");

            _autosaveInterval = Convert.ToInt32(config.GetValue("misc", "autosave_interval", 5));
            _musicVolume = Convert.ToSingle(config.GetValue("sound", "music_volume", 0.8f));
            _simulationEventBus.SendEvent(new SettingsChangedSimulationEvent());
        }

        private void UpdateSettings()
        {
            var config = new ConfigFile();
            config.SetValue("misc", "autosave_interval", _autosaveInterval);
            config.SetValue("sound", "music_volume", _musicVolume);
            config.Save("user://settings.cfg");
            _simulationEventBus.SendEvent(new SettingsChangedSimulationEvent());
        }
    }
}
