﻿using BronzeAgeTribes.Logic.Data;
using BronzeAgeTribes.Logic.Infrastructure.Interfaces;
using Godot;
using System.Collections.Generic;
using System.Linq;

namespace BronzeAgeTribes.Logic.Infrastructure.Services
{
    public class LargeHexWorldRenderer : IWorldRenderer, IInitializableService
    {
        private readonly IWorldManager _worldManager;
        private readonly ILocalPlayerInfo _localPlayerInfo;
        private readonly IExplorationTracker _explorationTracker;

        private List<Vector3> _hexInfluencePoints;
        private Vector3[][] _centerRiverInfluencePoints;
        private Vector3[][] _borderColoringInfluencePoints;
        private PackedScene _cellFeatureRoot;
        private float[,] _waterHeightMap;
        private float[,] _landHeightMap;
        private SpatialMaterial _landMaterial;
        private SpatialMaterial _waterMaterial;
        private SpatialMaterial _shadowMaterial;
        private SpatialMaterial _borderMaterial;

        public LargeHexWorldRenderer(
            IWorldManager worldManager,
            ILocalPlayerInfo localPlayerInfo,
            IExplorationTracker explorationTracker)
        {
            _worldManager = worldManager;
            _localPlayerInfo = localPlayerInfo;
            _explorationTracker = explorationTracker;
            _hexInfluencePoints = new List<Vector3>();

            for(var i = -2; i <= 2; i++)
            {
                for (var j = -2; j <= 1; j++)
                {
                    _hexInfluencePoints.Add(new Vector3(i, j, 1f));
                }
            }

            _hexInfluencePoints.Add(new Vector3(0, -3, 1f));
            _hexInfluencePoints.Add(new Vector3(0, 2, 1f));

            _hexInfluencePoints.Add(new Vector3(0, -4, 0.34f));
            _hexInfluencePoints.Add(new Vector3(0, 3, 0.34f));

            _hexInfluencePoints.Add(new Vector3(3, -2, 0.33f));
            _hexInfluencePoints.Add(new Vector3(-3, -2, 0.33f));
            _hexInfluencePoints.Add(new Vector3(3, 1, 0.33f));
            _hexInfluencePoints.Add(new Vector3(-3, 1, 0.33f));

            _hexInfluencePoints.Add(new Vector3(-3, -1, 0.5f));
            _hexInfluencePoints.Add(new Vector3(-3, 0, 0.5f));
            _hexInfluencePoints.Add(new Vector3(3, -1, 0.5f));
            _hexInfluencePoints.Add(new Vector3(3, 0, 0.5f));

            _hexInfluencePoints.Add(new Vector3(-1, -3, 0.75f));
            _hexInfluencePoints.Add(new Vector3(1, -3, 0.75f));
            _hexInfluencePoints.Add(new Vector3(-1, 2, 0.75f));
            _hexInfluencePoints.Add(new Vector3(1, 2, 0.75f));

            _hexInfluencePoints.Add(new Vector3(-2, -3, 0.25f));
            _hexInfluencePoints.Add(new Vector3(2, -3, 0.25f));
            _hexInfluencePoints.Add(new Vector3(-2, 2, 0.25f));
            _hexInfluencePoints.Add(new Vector3(2, 2, 0.25f));

            _centerRiverInfluencePoints = new[]
            {
                new [] { new Vector3(0, 0, 1f), new Vector3(0, -1, 1f) },
                new [] { new Vector3(1, -2, 1f), new Vector3(0, -2, 0.5f), new Vector3(2, -2, 0.5f), new Vector3(1, -3, 0.5f), new Vector3(1, -1, 0.5f) },
                new [] { new Vector3(1, 0, 1f), new Vector3(2, 0, 1f), new Vector3(3, 0, 0.5f), new Vector3(1, -1, 1f), new Vector3(2, -1, 1f), new Vector3(3, -1, 0.5f) },
                new [] { new Vector3(1, 1, 1f), new Vector3(1, 2, 1f), new Vector3(1, 0, 0.5f), new Vector3(0, 1, 0.5f), new Vector3(2, 1, 0.5f) },
                new [] { new Vector3(-1, 1, 1f), new Vector3(-1, 2, 1f), new Vector3(-1, 0, 1.0f), new Vector3(0, 1, 0.5f), new Vector3(-2, 1, 0.5f) },
                new [] { new Vector3(-1, 0, 1f), new Vector3(-2, 0, 1f), new Vector3(-3, 0, 0.5f), new Vector3(-1, -1, 1f), new Vector3(-2, -1, 1f), new Vector3(-3, -1, 0.5f) },
                new [] { new Vector3(-1, -2, 1f), new Vector3(0, -2, 0.5f), new Vector3(-2, -2, 0.5f), new Vector3(-1, -3, 0.5f), new Vector3(-1, -1, 0.5f) },
            };

            _borderColoringInfluencePoints = new[]
            {
                new [] { new Vector3(0, -4, 1f), new Vector3(1, -3, 1), new Vector3(2, -3, 1f), new Vector3(2, -2, 1), new Vector3(3, -2, 1) },
                new [] { new Vector3(3, -2, 1f), new Vector3(3, -1, 1f), new Vector3(3, 0, 1f), new Vector3(3, 1, 1f) },
                new [] { new Vector3(3, 1, 1f), new Vector3(2, 1, 1f), new Vector3(2, 2, 1f), new Vector3(1, 2, 1f), new Vector3(0, 3, 1f) },
                new [] { new Vector3(-3, 1, 1f), new Vector3(-2, 1, 1f), new Vector3(-2, 2, 1f), new Vector3(-1, 2, 1f), new Vector3(0, 3, 1f) },
                new [] { new Vector3(-3, -2, 1f), new Vector3(-3, -1, 1f), new Vector3(-3, 0, 1f), new Vector3(-3, 1, 1f) },
                new [] { new Vector3(0, -4, 1f), new Vector3(-1, -3, 1), new Vector3(-2, -3, 1), new Vector3(-2, -2, 0.5f), new Vector3(-3, -2, 1) },
            };

            _borderColoringInfluencePoints = new[]
            {
                new [] { new Vector3(0, -4, 1f), new Vector3(3, -2, 1) },
                new [] { new Vector3(3, -2, 1f), new Vector3(3, 1, 1f) },
                new [] { new Vector3(3, 1, 1f), new Vector3(0, 3, 1f) },
                new [] { new Vector3(-3, 1, 1f), new Vector3(0, 3, 1f) },
                new [] { new Vector3(-3, -2, 1f), new Vector3(-3, 1, 1f) },
                new [] { new Vector3(0, -4, 1f), new Vector3(-3, -2, 1) },
            };
        }

        public void Initialize()
        {
            _cellFeatureRoot = ResourceLoader.Load<PackedScene>("res://Scenes/Prefabs/CellFeatureRoot.tscn");
        }

        public int GetVertexSizeForWorld()
        {
            return (_worldManager.WorldRadiusHex * 2 * Constants.VERTICIES_PER_HEX) + 1;
        }


        private void CalculateMaps()
        {
            var worldSize = GetVertexSizeForWorld();

            _landHeightMap = new float[worldSize, worldSize];
            _waterHeightMap = new float[worldSize, worldSize];
            var landHeightCount = new float[worldSize, worldSize];

            var heightNoise = new SimplexNoiseGenerator(_worldManager.Seed.GetHashCode());

            var worldCenter = new HexPosition(_worldManager.WorldRadiusHex, _worldManager.WorldRadiusHex);

            var hexesInOrder = _worldManager.Hexes
                .OrderBy(h => h.Position.DistanceTo(worldCenter));

            var radius = 5;
            foreach(var hex in hexesInOrder)
            {
                for(var i = -radius; i<= radius; i++ )
                {
                    for (var j = -radius; j <= radius; j++)
                    {
                        var distance = Mathf.Sqrt(i * i + j * j);

                        var x = hex.Position.RealX + i;
                        var y = hex.Position.RealY + j;

                        if (distance <= radius
                            && x >= 0 && x < worldSize
                            && y >= 0 && y < worldSize)
                        {
                            var strength = 1 - Mathf.Pow(distance / radius, 5);

                            var localHeightNoise = 0f;

                            if (hex.BiomeType.TerrainNoiseFactor != 0)
                            {
                                localHeightNoise = heightNoise.CoherentNoise(
                                        x,
                                        y,
                                        0,
                                        1,
                                        5);

                                localHeightNoise = hex.BiomeType.TerrainNoiseFactor * Mathf.Abs(localHeightNoise);
                            }

                            var heightBoost = 0f;
                            if (hex.RiverFlow <= 0)
                            {
                                heightBoost = hex.BiomeType.TerrainHeightBoost;
                            }
                            
                            _landHeightMap[x, y] += (hex.Terrain.Height + localHeightNoise + heightBoost) * strength;
                            landHeightCount[x, y] += strength;
                        }
                    }
                }
            }

            for(var x = 0; x < worldSize; x++)
            {
                for (var y = 0; y < worldSize; y++)
                {
                    if(landHeightCount[x,y] > 0)
                    {
                        _landHeightMap[x, y] = _landHeightMap[x, y] / landHeightCount[x, y];
                    }
                    else
                    {
                        _landHeightMap[x, y] = -1000;
                    }
                }
            }

            foreach(var hex in hexesInOrder)
            {
                foreach (var point in _hexInfluencePoints)
                {
                    var x = (int)(hex.Position.RealX + point.x);
                    var y = (int)(hex.Position.RealY + point.y);

                    if (x >= 0 && x < worldSize
                        && y >= 0 && y < worldSize)
                    {
                        if (hex.RiverFlow > 0)
                        {
                            var waterHeight = Mathf.Max(0, _landHeightMap[x, y] - 0.1f);

                            _waterHeightMap[x, y] += waterHeight * point.z;
                        }
                    }
                }

                if (hex.RiverFlow > 0)
                {
                    var riverInfluencePointsForHex = _centerRiverInfluencePoints[0].ToList();

                    for (var i = 0; i < 6; i++)
                    {
                        if (hex.Neighbors[i] != null 
                            && hex.Neighbors[i].RiverFlow > 0
                            && ((int)hex.RiverFlowDirection == i || hex.Neighbors[i].RiverFlowDirection == Util.ReverseFacing((HexFacing)i)))
                        {
                            riverInfluencePointsForHex.AddRange(_centerRiverInfluencePoints[i + 1]);
                        }
                    }

                    riverInfluencePointsForHex = riverInfluencePointsForHex
                        .Select(v => System.Tuple.Create($"{v.x},{v.y}", v))
                        .GroupBy(t => t.Item1)
                        .Select(g => g.OrderByDescending(t => t.Item2.z).First().Item2)
                        .ToList();

                    var riverDepth = 0.1f + Mathf.Min(hex.RiverFlow * 0.1f, 0.4f);
                    
                    foreach (var point in riverInfluencePointsForHex)
                    {
                        var x = (int)(hex.Position.RealX + point.x);
                        var y = (int)(hex.Position.RealY + point.y);

                        if (x >= 0 && x < worldSize
                            && y >= 0 && y < worldSize)
                        {
                            _landHeightMap[x, y] -= riverDepth * point.z;
                        }
                    }
                }
            }
        }

        public void BuildTerrain(
            Spatial worldRoot, 
            SpatialMaterial landMaterial, 
            SpatialMaterial waterMaterial,
            SpatialMaterial shadowMaterial,
            SpatialMaterial borderMaterial,
            bool godvision)
        {
            CalculateMaps();
            _landMaterial = landMaterial;
            _waterMaterial = waterMaterial;
            _shadowMaterial = shadowMaterial;
            _borderMaterial = borderMaterial;

            foreach(var region in _worldManager.Regions)
            {
                BuildTerrainForRegion(worldRoot, region, godvision, 0);
            }
        }

        public void UpdateTerrain(
            Spatial worldRoot, 
            IEnumerable<WorldRegion> regions)
        {
            foreach (var region in regions)
            {
                BuildTerrainForRegion(worldRoot, region, false, 0);
            }
        }

        private void BuildTerrainForRegion(
            Spatial worldRoot, 
            WorldRegion region, 
            bool godvision,
            int colorMode)
        {
            try
            {
                var regionRoot = worldRoot.GetNodeOrNull<Spatial>(region.Id);
                MeshInstance landMesh;
                MeshInstance waterMesh;
                MeshInstance shadowMesh;
                MeshInstance borderMesh;

                if (regionRoot == null)
                {
                    regionRoot = new Spatial()
                    {
                        Name = region.Id
                    };

                    landMesh = new MeshInstance
                    {
                        Name = "land",
                        CastShadow = GeometryInstance.ShadowCastingSetting.Off
                    };

                    waterMesh = new MeshInstance
                    {
                        Name = "water",
                        CastShadow = GeometryInstance.ShadowCastingSetting.Off
                    };

                    shadowMesh = new MeshInstance
                    {
                        Name = "shadow",
                        CastShadow = GeometryInstance.ShadowCastingSetting.Off
                    };

                    borderMesh = new MeshInstance
                    {
                        Name = "border",
                        CastShadow = GeometryInstance.ShadowCastingSetting.Off
                    };

                    regionRoot.AddChild(landMesh);
                    regionRoot.AddChild(waterMesh);
                    regionRoot.AddChild(borderMesh);
                    regionRoot.AddChild(shadowMesh);

                    worldRoot.AddChild(regionRoot);
                }
                else
                {
                    landMesh = regionRoot.GetNode<MeshInstance>("land");
                    waterMesh = regionRoot.GetNode<MeshInstance>("water");
                    borderMesh = regionRoot.GetNode<MeshInstance>("border");
                    shadowMesh = regionRoot.GetNode<MeshInstance>("shadow");
                }

                var landSurfaceTool = new SurfaceTool();
                landSurfaceTool.Begin(Mesh.PrimitiveType.Triangles);
                var waterSurfaceTool = new SurfaceTool();
                waterSurfaceTool.Begin(Mesh.PrimitiveType.Triangles);
                var shadowSurfaceTool = new SurfaceTool();
                shadowSurfaceTool.Begin(Mesh.PrimitiveType.Triangles);
                
                var vertOffsets = new List<Vector2>
                {
                    new Vector2(0, -4),
                    new Vector2(-1, -3.3f), new Vector2(1, -3.3f),
                    new Vector2(0, -3),
                    new Vector2(-2, -2.7f), new Vector2(2, -2.7f),
                };
                for (var row = 0; row < 4; row += 1)
                {
                    for (var col = 0; col < 7; col += 1)
                    {
                        vertOffsets.Add(new Vector2(col - 3, row - 2));
                    }
                }
                vertOffsets.AddRange(new Vector2[]
                {
                new Vector2(-2, 1.7f), new Vector2(2, 1.7f),
                new Vector2(0, 2),
                new Vector2(-1, 2.3f), new Vector2(1, 2.3f),
                new Vector2(0,3)
                });

                var triangleOrder = new List<int>()
                {
                    1,3,4,
                    1,4,2,
                    3,11,4,
                    3,6,11,
                    2,4,9,
                    2,9,5,
                    7,5,8,
                    9,4,10,
                    10,4,11,
                    11,6,12,
                    12,6,13,
                    5,9,8
                };

                for (var row = 0; row < 3; row += 1)
                {
                    for (var col = 0; col < 6; col += 1)
                    {
                        triangleOrder.AddRange(new[] { ToVert(col, row), ToVert(col + 1, row), ToVert(col + 1, row + 1) });
                        triangleOrder.AddRange(new[] { ToVert(col + 1, row + 1), ToVert(col, row + 1), ToVert(col, row) });
                    }
                }

                triangleOrder.AddRange(new[]
                {
                28,29,35,
                29,30,35,
                31,32,37,
                32,33,36,
                33,34,36,
                35,30,38,
                30,31,37,
                37,32,39,
                38,37,40,
                37,39,40,
                30,37,38,
                32,36,39
            });

                var borderPoints = new int[][]
                {
                    new [] {1,3,6,13 },
                    new [] {13,20,27,34 },
                    new [] {34,36,39,40 },
                    new [] {40,38,35,28 },
                    new [] {28,21,14,7 },
                    new [] {7,5,2,1 }
                };

                var rnd = new System.Random();
                var regionColor = new Color(
                    (float)rnd.NextDouble(),
                    (float)rnd.NextDouble(),
                    (float)rnd.NextDouble());

                foreach (var hex in region.Hexes)
                {
                    var isExplored = godvision || _explorationTracker.IsExplored(hex.Position);
                    var isVisible = godvision || _explorationTracker.IsVisible(hex.Position);

                    var exploredNeighborPoints = Enumerable.Range(0, 6)
                        .Where(i => hex.Neighbors[i] != null && _explorationTracker.IsExplored(hex.Neighbors[i].Position))
                        .SelectMany(i => borderPoints[i])
                        .ToArray();

                    var visibleNeighborPoints = Enumerable.Range(0, 6)
                        .Where(i => hex.Neighbors[i] != null && _explorationTracker.IsVisible(hex.Neighbors[i].Position))
                        .SelectMany(i => borderPoints[i])
                        .ToArray();

                    foreach (var i in triangleOrder)
                    {
                        var landVert = GetVertex(hex, vertOffsets[i - 1], _landHeightMap);
                        var waterVert = GetVertex(hex, vertOffsets[i - 1], _waterHeightMap);

                        var depth = waterVert.y - landVert.y;
                        var factor = Mathf.Clamp(depth / 2, 0, 1);

                        if (!isVisible)
                        {
                            shadowSurfaceTool.AddColor(new Color(0, 0, 0, 0.5f));

                            if (isExplored || exploredNeighborPoints.Contains(i))
                            {
                                var vertToUse = landVert;
                                if (waterVert.y > landVert.y)
                                {
                                    vertToUse = waterVert;
                                }

                                if (visibleNeighborPoints.Contains(i))
                                {
                                    shadowSurfaceTool.AddVertex(vertToUse);
                                }
                                else
                                {
                                    shadowSurfaceTool.AddVertex(vertToUse + new Vector3(0, 1, 0));
                                }
                            }
                            else
                            {
                                shadowSurfaceTool.AddVertex(new Vector3(landVert.x, 1f, landVert.z));
                            }
                        }

                        if (isExplored || exploredNeighborPoints.Contains(i))
                        {
                            if (colorMode == 0)
                            {
                                if (landVert.y < waterVert.y && landVert.y <= 0)
                                {
                                    landSurfaceTool.AddColor(new Color(1, 1, 0));
                                }
                                else
                                {
                                    landSurfaceTool.AddColor(hex.BiomeType.GroundColor);
                                }
                            }
                            else if (colorMode == 1)
                            {
                                landSurfaceTool.AddColor(new Color(
                                    region.Temperature,
                                    0,
                                    0));
                            }
                            else if (colorMode == 2)
                            {
                                landSurfaceTool.AddColor(new Color(
                                    0,
                                    0,
                                    region.Rainfall));
                            }
                            else if (colorMode == 3)
                            {
                                landSurfaceTool.AddColor(new Color(
                                    region.Drainage,
                                    region.Drainage,
                                    0));
                            }
                            else if (colorMode == 4)
                            {
                                landSurfaceTool.AddColor(regionColor);
                            }
                            landSurfaceTool.AddVertex(landVert);
                            waterSurfaceTool.AddNormal(new Vector3(0, 1, 0));
                            waterSurfaceTool.AddColor(new Color(0, Mathf.Clamp(0.7f - factor, 0, 1), 1));
                            waterSurfaceTool.AddVertex(waterVert);
                        }
                        else
                        {
                            landSurfaceTool.AddColor(new Color(0, 0, 0));
                            landSurfaceTool.AddVertex(new Vector3(landVert.x, 0.1f, landVert.z));
                            waterSurfaceTool.AddNormal(new Vector3(0, 1, 0));
                            waterSurfaceTool.AddColor(new Color(0, 0, 0));
                            waterSurfaceTool.AddVertex(new Vector3(waterVert.x, 0f, waterVert.z));
                        }
                    }

                    hex.RenderedPos = new Vector3(
                        hex.Position.RealX,
                        _landHeightMap[hex.Position.RealX, hex.Position.RealY],
                        hex.Position.RealY);

                    if (isExplored)
                    {
                        UpdateHexModels(
                           worldRoot,
                            hex.Yield(),
                            godvision);
                    }
                }

                landSurfaceTool.GenerateNormals();
                shadowSurfaceTool.GenerateNormals();

                landMesh.Mesh = landSurfaceTool.Commit();
                landMesh.MaterialOverride = _landMaterial;

                waterMesh.Mesh = waterSurfaceTool.Commit();
                waterMesh.MaterialOverride = _waterMaterial;

                shadowMesh.Mesh = shadowSurfaceTool.Commit();
                shadowMesh.MaterialOverride = _shadowMaterial;

                borderMesh.Mesh = MakeBorderLine(region.Hexes.ToArray(), 0.1f, 0.3f, new Color(1, 1, 1), godvision);
                borderMesh.MaterialOverride = _borderMaterial;
            }
            catch (System.Exception ex)
            {
                GD.PrintErr($"{ex.GetType().Name}: {ex.Message}\n{ex.StackTrace}");
            }
        }

        private Vector3 GetVertex(WorldHex hex, Vector2 vector2, float[,] valueMap)
        {
            var vertX = hex.Position.RealX + vector2.x;
            var vertZ = hex.Position.RealY + vector2.y;

            // Disabled lerping becuase it was making prefab placements for features/developments odd. Seems like it's not needed though

            //var a = vertX - (int)vertX;
            //var b = vertZ - (int)vertZ;

            var ul = GetOrZero((int)vertX, (int)vertZ, valueMap);
            //var ur = GetOrZero((int)vertX + 1, (int)vertZ, valueMap);
            //var ll = GetOrZero((int)vertX, (int)vertZ + 1, valueMap);
            //var lr = GetOrZero((int)vertX + 1, (int)vertZ + 1, valueMap);

            //var u = ul * a + ur * (1 - a);
            //var l = ll * a + lr * (1 - a);
            //var vertY = u * b + l * (1 - b);

            ////return new Vector3(vertX, vertY, vertZ);
            return new Vector3(vertX, ul, vertZ);
        }

        private float GetOrZero(int x, int y, float[,] valueMap)
        {
            if(x >= 0 && x < valueMap.GetLength(0)
                && y >= 0 && y < valueMap.GetLength(1))
            {
                return valueMap[x, y];
            }

            return 0;
        }

        private int ToVert(int c, int r)
        {
            return r * 7 + c + 7;
        }

        public void UpdateHexModels(
            Spatial worldRoot,
            IEnumerable<WorldHex> hexes,
            bool godVision)
        {
            foreach(var hex in hexes)
            {
                var container = worldRoot.GetNodeOrNull<Spatial>(hex.Position.ToString());

                if(container != null)
                {
                    if (Object.IsInstanceValid(container))
                    {
                        container.Free();

                        BuildHexModelsFor(worldRoot, hex, godVision);
                    }
                }
                else
                {
                    BuildHexModelsFor(worldRoot, hex, godVision);
                }
            }
        }

        private void BuildHexModelsFor(Spatial worldRoot, WorldHex hex, bool godVision)
        {
            var container = new Spatial();
            container.Name = hex.Position.ToString();
            worldRoot.AddChild(container);

            var isVisible = godVision || _explorationTracker.IsVisible(hex.Position);
            var memory = _explorationTracker.GetExplorationMemory(hex.Position);

            DevelopmentType developmentType = null;
            CellFeatureType[] featureTypes = new CellFeatureType[0];
            if (isVisible)
            {
                if (hex.Development != null && hex.Development.IsConstructed)
                {
                    developmentType = hex.Development.BackingType;
                }

                featureTypes = hex.Features.Select(f => f.BackingType).ToArray();
            }
            else if(memory != null)
            {
                developmentType = memory.DevelopmentType;

                featureTypes = memory.CellFeatureTypes.ToArray();
            }


            if(developmentType != null)
            {
                var random = new System.Random(
                        _worldManager.Seed.GetHashCode()
                        + hex.Position.ToString().GetHashCode()
                        + hex.Development.BackingType.Name.GetHashCode());

                hex.Development.BackingType.MakeMapRepresentation(
                    container,
                    _cellFeatureRoot,
                    hex,
                    _hexInfluencePoints,
                    _landHeightMap,
                    _waterHeightMap,
                    random,
                    hex.Development.BuildingStyle,
                    hex.Development,
                    AddFeaturePrefab);
            }


            foreach (var featureType in featureTypes)
            {
                var random = new System.Random(
                    _worldManager.Seed.GetHashCode()
                    + hex.Position.ToString().GetHashCode()
                    + featureType.Name.GetHashCode());

                var root = _cellFeatureRoot.Instance() as Spatial;
                
                root.Transform = worldRoot.Transform.Translated(
                    new Vector3(
                        hex.Position.RealX,
                        0,
                        hex.Position.RealY));
                container.AddChild(root);

                var rootLod0 = root.GetNode<Spatial>("-lod0");
                var rootLod1 = root.GetNode<Spatial>("-lod1");
                var rootLod2 = root.GetNode<Spatial>("-lod2");

                foreach (var point in _hexInfluencePoints)
                {
                    var adjustedChance = point.z * featureType.PrefabChancePerVertex;

                    if (random.NextDouble() < adjustedChance)
                    {
                        var vertexX = (int)(hex.Position.RealX + point.x);
                        var vertexZ = (int)(hex.Position.RealY + point.y);

                        var inWater = _landHeightMap[vertexX, vertexZ] <= _waterHeightMap[vertexX, vertexZ];

                        if (((inWater && featureType.PrefabPlacementMode  != FeaturePrefabPlacementMode.OnLand)
                                || (!inWater && featureType.PrefabPlacementMode != FeaturePrefabPlacementMode.OnWater))
                            && featureType.MapPrefabs.Any())
                        {
                            var packedPrefab = random.Choose(featureType.MapPrefabs);

                            AddFeaturePrefab(rootLod0, 1, packedPrefab, point.x, _landHeightMap[vertexX, vertexZ], point.y, (float)random.NextDouble() * 2 * Mathf.Pi);

                            if (random.NextDouble() < 0.5)
                            {
                                AddFeaturePrefab(rootLod1, 1.5f, packedPrefab, point.x, _landHeightMap[vertexX, vertexZ], point.y, (float)random.NextDouble() * 2 * Mathf.Pi);

                                if (random.NextDouble() < 0.5)
                                {
                                    AddFeaturePrefab(rootLod2, 2, packedPrefab, point.x, _landHeightMap[vertexX, vertexZ], point.y, (float)random.NextDouble() * 2 * Mathf.Pi);
                                }
                            }
                        }
                    }
                }
            }
        }

        private void AddFeaturePrefab(
            Spatial root, 
            float scale, 
            PackedScene packedPrefab, 
            float x, float y, float z,
            float facing)
        {
            var prefab = packedPrefab.Instance() as Spatial;

            prefab.Transform = root.Transform.Translated(new Vector3(x, y, z));
            prefab.Scale = new Vector3(scale, scale, scale);
            prefab.Rotate(Vector3.Up, facing);

            root.AddChild(prefab);
        }

        public static Color EncodeNormal(Vector3 normal)
        {
            normal = 0.5f * (normal + Vector3.One);

            return new Color(normal.x, normal.z, normal.y);
        }

        public void SetColorMode(Spatial worldRoot, int mode)
        {
            foreach (var region in _worldManager.Regions)
            {
                BuildTerrainForRegion(worldRoot, region, true, mode);
            }
        }

        public float GetVertexHeightAt(int vX, int vY)
        {
            if (vX >= 0 && vX < _landHeightMap.GetLength(0)
                && vY >= 0 && vY < _landHeightMap.GetLength(1))
            {
                return Mathf.Max(_landHeightMap[vX, vY], _waterHeightMap[vX, vY]);
            }

            return 0;
        }

        public ArrayMesh MakeBorderLine(
            WorldHex[] regionToSurround, 
            float lineWidth, 
            float floatHeight,
            Color color,
            bool godvision)
        {
            var halfWidth = lineWidth / 2;
            var borderSurfaceTool = new SurfaceTool();
            borderSurfaceTool.Begin(Mesh.PrimitiveType.Triangles);

            var borderPoints = new Vector2[][]
            {
                new [] {new Vector2(0, -4), new Vector2(3,-2) },
                new [] {new Vector2(3, -2), new Vector2(3, 1) },
                new [] {new Vector2(3, 1), new Vector2(0, 3) },
                new [] {new Vector2(0, 3), new Vector2(-3, 1) },
                new [] {new Vector2(-3, 1), new Vector2(-3, -2) },
                new [] {new Vector2(-3, -2), new Vector2(0, -4) }
            };

            var triangleOrder = new int[]
            {
                0,2,1,
                3,4,5,
                3,2,0,
                3,5,2
            };

            foreach (var hex in regionToSurround)
            {
                if (godvision || _explorationTracker.IsExplored(hex.Position))
                {
                    for (var i = 0; i < 6; i++)
                    {
                        if (hex.Neighbors[i] == null || !regionToSurround.Contains(hex.Neighbors[i]))
                        {
                            var a = borderPoints[i][0];
                            var b = borderPoints[i][1];

                            var dirAB = Mathf.Atan2(b.y - a.y, b.x - a.x);// - Mathf.Pi / 2;
                            var dirBA = Mathf.Atan2(a.y - b.y, a.x - b.x);// - Mathf.Pi / 2;

                            var verteciesA = new Vector2[]
                            {
                            a + new Vector2(halfWidth *  Mathf.Cos(dirBA + Mathf.Pi / 2), halfWidth *  Mathf.Sin(dirBA + Mathf.Pi / 2)),
                            a + new Vector2(halfWidth *  Mathf.Cos(dirBA),                halfWidth *  Mathf.Sin(dirBA)),
                            a + new Vector2(halfWidth *  Mathf.Cos(dirBA - Mathf.Pi / 2), halfWidth *  Mathf.Sin(dirBA - Mathf.Pi / 2))
                            };

                            var verteciesB = new Vector2[]
                            {
                            b + new Vector2(halfWidth *  Mathf.Cos(dirAB - Mathf.Pi / 2), halfWidth *  Mathf.Sin(dirAB - Mathf.Pi / 2)),
                            b + new Vector2(halfWidth *  Mathf.Cos(dirAB),                halfWidth *  Mathf.Sin(dirAB)),
                            b + new Vector2(halfWidth *  Mathf.Cos(dirAB + Mathf.Pi / 2), halfWidth *  Mathf.Sin(dirAB + Mathf.Pi / 2))
                            };

                            var heightA = floatHeight + GetVertexHeightAt((int)(hex.Position.RealX + a.x), (int)(hex.Position.RealY + a.y));
                            var heightB = floatHeight + GetVertexHeightAt((int)(hex.Position.RealX + b.x), (int)(hex.Position.RealY + b.y));

                            var verticies = verteciesA
                                    .Select(v => new Vector3(v.x + hex.Position.RealX, heightA, v.y + hex.Position.RealY))
                                .Concat(verteciesB
                                    .Select(v => new Vector3(v.x + hex.Position.RealX, heightB, v.y + hex.Position.RealY)))
                                .ToArray();

                            foreach (var j in triangleOrder)
                            {
                                borderSurfaceTool.AddNormal(new Vector3(0, 1, 0));
                                borderSurfaceTool.AddColor(color);
                                borderSurfaceTool.AddVertex(verticies[j]);
                            }
                        }
                    }
                }
            }

            return borderSurfaceTool.Commit();
        }
    }
}