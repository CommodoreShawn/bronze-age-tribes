﻿using BronzeAgeTribes.Logic.Data;
using BronzeAgeTribes.Logic.Data.Game;
using BronzeAgeTribes.Logic.Infrastructure.Interfaces;
using Godot;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BronzeAgeTribes.Logic.Infrastructure.Services
{
    public class TooltipManager : ITooltipManager, IGameplayService
    {
        private List<Tuple<int, object, Action>> _lowerPriorityTooltipStack;

        private List<TooltipController> _tooltipControllers;
        private object _tooltipOwner;
        private int _tooltipPriority;
        private Action _tooltipCreateAction;

        public TooltipManager()
        {
            _lowerPriorityTooltipStack = new List<Tuple<int, object, Action>>();
            _tooltipControllers = new List<TooltipController>();
        }

        public void HideTooltipFor(object caller)
        {
            _lowerPriorityTooltipStack.RemoveAll(t => t.Item2 == caller);
            _lowerPriorityTooltipStack.RemoveAll(t => t.Item2 is Godot.Object gobj && !Godot.Object.IsInstanceValid(gobj));

            if (_tooltipOwner == caller)
            {
                var newTooltip = _lowerPriorityTooltipStack
                    .OrderByDescending(t => t.Item1)
                    .FirstOrDefault();

                if(newTooltip != null)
                {
                    _tooltipOwner = newTooltip.Item2;
                    _tooltipPriority = newTooltip.Item1;
                    _tooltipCreateAction = newTooltip.Item3;
                    _tooltipCreateAction();
                }
                else
                {
                    _tooltipOwner = null;
                    _tooltipPriority = 0;
                    _tooltipCreateAction = null;
                    GetTooltipController().Visible = false;
                }
            }
        }

        public void Process()
        {
            if(_tooltipOwner != null && _tooltipOwner is Godot.Object gdObj && !Godot.Object.IsInstanceValid(gdObj))
            {
                HideTooltipFor(gdObj);
            }
        }

        public void RegisterTooltipController(TooltipController tooltipController)
        {
            _tooltipControllers.Add(tooltipController);
        }

        public void ShowTooltipFor(Control caller, Trait trait)
        {
            Action createAction = () =>
            {
                GetTooltipController().RectGlobalPosition = caller.RectGlobalPosition + new Vector2(32, 0);
                _tooltipOwner = caller;
                GetTooltipController().SetContentFor(trait);
                GetTooltipController().Visible = true;
            };

            HandleNewTooltip(caller, 3, createAction);
        }

        public void ShowTooltipFor(Control caller, PopJob popJob)
        {
            Action createAction = () =>
            {
                GetTooltipController().RectGlobalPosition = caller.RectGlobalPosition + new Vector2(58, 0);
                _tooltipOwner = caller;
                GetTooltipController().SetContentFor(popJob);
                GetTooltipController().Visible = true;
            };

            HandleNewTooltip(caller, 2, createAction);
        }

        public void ShowTooltipFor(GameplayWorld gameplayWorld, WorldHex hoveredHex)
        {
            Action createAction = () =>
            {
                var screenPos = gameplayWorld.GetViewport().GetCamera().UnprojectPosition(hoveredHex.RenderedPos);

                GetTooltipController().RectGlobalPosition = screenPos;
                _tooltipOwner = gameplayWorld;
                GetTooltipController().SetContentFor(hoveredHex);
                GetTooltipController().Visible = true;
            };

            HandleNewTooltip(gameplayWorld, 1, createAction);
        }

        public void ShowTooltipFor(Control caller, ResourceType resource, float value, string details)
        {
            Action createAction = () =>
            {
                GetTooltipController().RectGlobalPosition = caller.RectGlobalPosition + new Vector2(32, 0);
                _tooltipOwner = caller;
                GetTooltipController().SetContentFor(resource, value, details);
                GetTooltipController().Visible = true;
            };

            HandleNewTooltip(caller, 4, createAction);
        }

        public void ShowTooltipForGrowth(Control caller, AdministrativeGroup group, Species species)
        {
            Action createAction = () =>
            {
                GetTooltipController().RectGlobalPosition = caller.RectGlobalPosition + new Vector2(32, 0);
                _tooltipOwner = caller;
                GetTooltipController().SetContentForGrowth(group, species);
                GetTooltipController().Visible = true;
            };

            HandleNewTooltip(caller, 5, createAction);
        }

        public void ShowTooltipFor(Control caller, WorldHex hex, CellFeature feature)
        {
            Action createAction = () =>
            {
                GetTooltipController().RectGlobalPosition = caller.RectGlobalPosition + new Vector2(32, 0);
                _tooltipOwner = caller;
                GetTooltipController().SetContentFor(hex, feature);
                GetTooltipController().Visible = true;
            };

            HandleNewTooltip(caller, 5, createAction);
        }

        public void ShowTooltipFor(Control caller, WorldHex hex, CellFeature feature, InteractionOption interactionOption)
        {
            Action createAction = () =>
            {
                GetTooltipController().RectGlobalPosition = caller.RectGlobalPosition + new Vector2(32, 0);
                _tooltipOwner = caller;
                GetTooltipController().SetContentFor(hex, feature, interactionOption);
                GetTooltipController().Visible = true;
            };

            HandleNewTooltip(caller, 6, createAction);

        }

        public void ShowTooltipFor(Control caller, WorldHex hex, Development development)
        {
            Action createAction = () =>
            {
                GetTooltipController().RectGlobalPosition = caller.RectGlobalPosition + new Vector2(32, 0);
                _tooltipOwner = caller;
                GetTooltipController().SetContentFor(hex, development);
                GetTooltipController().Visible = true;
            };

            HandleNewTooltip(caller, 5, createAction);
        }

        public void ShowTooltipFor(Control caller, WorldHex hex, Building building)
        {
            Action createAction = () =>
            {
                GetTooltipController().RectGlobalPosition = caller.RectGlobalPosition + new Vector2(32, 0);
                _tooltipOwner = caller;
                GetTooltipController().SetContentFor(hex, building);
                GetTooltipController().Visible = true;
            };

            HandleNewTooltip(caller, 5, createAction);
        }

        public void ShowTooltipFor(Control caller, WorldHex hex, Development development, InteractionOption interactionOption)
        {
            Action createAction = () =>
            {
                GetTooltipController().RectGlobalPosition = caller.RectGlobalPosition + new Vector2(32, 0);
                _tooltipOwner = caller;
                GetTooltipController().SetContentFor(hex, development, interactionOption);
                GetTooltipController().Visible = true;
            };

            HandleNewTooltip(caller, 6, createAction);
        }

        public void ShowTooltipFor(Control caller, WorldHex hex, Terrain terrain)
        {
            Action createAction = () =>
            {
                GetTooltipController().RectGlobalPosition = caller.RectGlobalPosition + new Vector2(32, 0);
                _tooltipOwner = caller;
                GetTooltipController().SetContentFor(hex, terrain);
                GetTooltipController().Visible = true;
            };

            HandleNewTooltip(caller, 6, createAction);
        }

        public void ShowTooltipFor(Control caller, WorldHex hex, DevelopmentType developmentType, CellFeature featureToClear)
        {
            Action createAction = () =>
            {
                GetTooltipController().RectGlobalPosition = caller.RectGlobalPosition + new Vector2(32, 0);
                _tooltipOwner = caller;
                GetTooltipController().SetContentFor(hex, developmentType, featureToClear);
                GetTooltipController().Visible = true;
            };

            HandleNewTooltip(caller, 6, createAction);
        }

        public void ShowTooltipFor(Control caller, WorldHex hex, BuildingType buildingType)
        {
            Action createAction = () =>
            {
                GetTooltipController().RectGlobalPosition = caller.RectGlobalPosition + new Vector2(32, 0);
                _tooltipOwner = caller;
                GetTooltipController().SetContentFor(hex, buildingType);
                GetTooltipController().Visible = true;
            };

            HandleNewTooltip(caller, 6, createAction);
        }

        public void ShowTooltipFor(Control caller, Pop pop)
        {
            Action createAction = () =>
            {
                GetTooltipController().RectGlobalPosition = caller.RectGlobalPosition + new Vector2(32, 0);
                _tooltipOwner = caller;
                GetTooltipController().SetContentFor(pop);
                GetTooltipController().Visible = true;
            };

            HandleNewTooltip(caller, 6, createAction);
        }

        public void ShowTooltipFor(Control caller, Notification notification)
        {
            Action createAction = () =>
            {
                GetTooltipController().RectGlobalPosition = caller.RectGlobalPosition + new Vector2(32, 0);
                _tooltipOwner = caller;
                GetTooltipController().SetContentFor(notification);
                GetTooltipController().Visible = true;
            };

            HandleNewTooltip(caller, 3, createAction);
        }

        public void ShowTooltipFor(Control caller, WorldRegion region)
        {
            Action createAction = () =>
            {
                GetTooltipController().RectGlobalPosition = caller.RectGlobalPosition + new Vector2(32, 0);
                _tooltipOwner = caller;
                GetTooltipController().SetContentFor(region);
                GetTooltipController().Visible = true;
            };

            HandleNewTooltip(caller, 6, createAction);
        }

        public void ShowTooltipFor(
            Control caller, 
            Pop interactor, 
            Pop target, 
            PopInteractionType interactionType)
        {
            Action createAction = () =>
            {
                GetTooltipController().RectGlobalPosition = caller.RectGlobalPosition + new Vector2(caller.RectSize.x, 0);
                _tooltipOwner = caller;
                GetTooltipController().SetContentFor(interactor, target, interactionType);
                GetTooltipController().Visible = true;
            };

            HandleNewTooltip(caller, 8, createAction);
        }

        public void ShowTooltipFor(Control caller, WorldHex hex, Building building, InteractionOption interactionOption)
        {
            Action createAction = () =>
            {
                GetTooltipController().RectGlobalPosition = caller.RectGlobalPosition + new Vector2(32, 0);
                _tooltipOwner = caller;
                GetTooltipController().SetContentFor(hex, building, interactionOption);
                GetTooltipController().Visible = true;
            };

            HandleNewTooltip(caller, 6, createAction);
        }

        private void HandleNewTooltip(object owner, int priority, Action createTooltipAction)
        {
            if (priority > _tooltipPriority)
            {
                if(_tooltipPriority > 0)
                {
                    _lowerPriorityTooltipStack.Add(Tuple.Create(_tooltipPriority, _tooltipOwner, _tooltipCreateAction));
                }

                _lowerPriorityTooltipStack.RemoveAll(t => t.Item2 == owner);
                _tooltipPriority = priority;
                _tooltipOwner = owner;
                _tooltipCreateAction = createTooltipAction;
                _tooltipCreateAction();
            }
            else
            {
                _lowerPriorityTooltipStack.RemoveAll(t => t.Item2 == owner);
                _lowerPriorityTooltipStack.Add(Tuple.Create(priority, owner, createTooltipAction));
            }
        }

        private TooltipController GetTooltipController()
        {
            _tooltipControllers.RemoveAll(tc => !Godot.Object.IsInstanceValid(tc));
            return _tooltipControllers.LastOrDefault();
        }
    }
}
