﻿using BronzeAgeTribes.Logic.Infrastructure.Interfaces;

namespace BronzeAgeTribes.Logic.Infrastructure.Services
{
    public class SoundManager : ISoundManager
    {
        private MusicController _musicController;

        public void RegisterMusicController(MusicController musicController)
        {
            _musicController = musicController;
        }

        public void SetMenuMusic()
        {
            _musicController?.SetMenuMusic();
        }

        public void SetMusic(bool isExploration, bool isBattle)
        {
            _musicController?.SetMusic(isExploration, isBattle);
        }
    }
}
