﻿using BronzeAgeTribes.Logic.Infrastructure.Interfaces;
using Godot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BronzeAgeTribes.Logic.Infrastructure.Services
{
    public class ResourceInspector : IResourceInspector
    {
        public IEnumerable<TResource> ListResources<TResource>(string path) where TResource : Resource
        {
            var results = new List<TResource>();
            Directory directory = new Directory();
            var err = directory.Open(path);

            if (err != Error.Ok)
            {
                GD.PrintErr($"Unable to load directory {path}: {err}");
            }

            directory.ListDirBegin(true);

            var files = new List<string>();

            AddDirectoryContents(directory, files);
            foreach (var file in files)
            {
                try
                {
                    var resource = ResourceLoader.Load<TResource>(file);

                    if (resource != null)
                    {
                        results.Add(resource);
                    }
                }
                catch (Exception)
                {
                }
            }

            return results;
        }

        public IEnumerable<TResource> ListResources<TResource>(string path, string extension) where TResource : Resource
        {
            var results = new List<TResource>();
            Directory directory = new Directory();
            var err = directory.Open(path);

            if (err != Error.Ok)
            {
                GD.PrintErr($"Unable to load directory {path}: {err}");
            }

            directory.ListDirBegin(true);

            var files = new List<string>();

            AddDirectoryContents(directory, files);

            foreach (var file in files.Where(f => f.EndsWith(extension)))
            {
                try
                {
                    var resource = ResourceLoader.Load<TResource>(file);

                    if (resource != null)
                    {
                        results.Add(resource);
                    }
                }
                catch (Exception)
                {
                }
            }

            return results;
        }

        public IEnumerable<string> ListTextFiles(string path)
        {
            var results = new List<string>();
            Directory directory = new Directory();
            var err = directory.Open(path);

            if (err != Error.Ok)
            {
                GD.PrintErr($"Unable to load directory {path}: {err}");
                return Enumerable.Empty<string>();
            }

            directory.ListDirBegin(true);

            var files = new List<string>();

            var fileReader = new File();
            AddDirectoryContents(directory, files);
            foreach (var file in files)
            {
                if (fileReader.Open(file, File.ModeFlags.Read) == Error.Ok)
                {
                    results.Add(fileReader.GetAsText());
                }
            }

            return results;
        }

        private void AddDirectoryContents(Directory dir, List<string> files)
        {
            var fileName = dir.GetNext();

            while (fileName != string.Empty)
            {
                var path = dir.GetCurrentDir() + "/" + fileName;

                if (dir.CurrentIsDir())
                {
                    var subDir = new Directory();
                    subDir.Open(path);
                    subDir.ListDirBegin(true, false);
                    AddDirectoryContents(subDir, files);
                }
                else
                {
                    files.Add(path);
                }

                fileName = dir.GetNext();
            }
        }
    }
}
