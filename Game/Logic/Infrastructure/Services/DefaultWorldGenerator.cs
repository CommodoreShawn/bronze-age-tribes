﻿using BronzeAgeTribes.Logic.Data;
using BronzeAgeTribes.Logic.Data.Game;
using BronzeAgeTribes.Logic.Infrastructure.Interfaces;
using BronzeAgeTribes.Logic.Infrastructure.SimulationEvents;
using Godot;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BronzeAgeTribes.Logic.Infrastructure.Services
{
    public class DefaultWorldGenerator : IWorldGenerator
    {
        private readonly IWorldManager _worldManager;
        private readonly ILogger _logger;
        private readonly IRegionGenerator _regionGenerator;
        private readonly IRiverGenerator _riverGenerator;
        private readonly INameSource _nameSource;
        private readonly ILocalPlayerInfo _localPlayerInfo;
        private readonly ISimulationEventBus _simulationEventBus;
        private readonly ISerializableService[] _serializableServices;
        private readonly IAiManager _aiManager;

        public DefaultWorldGenerator(
            IWorldManager worldManager,
            ILogger logger,
            IRegionGenerator regionGenerator,
            IRiverGenerator riverGenerator,
            INameSource nameSource,
            ILocalPlayerInfo localPlayerInfo,
            ISimulationEventBus simulationEventBus,
            IEnumerable<ISerializableService> serializableServices,
            IAiManager aiManager)
        {
            _worldManager = worldManager;
            _logger = logger;
            _regionGenerator = regionGenerator;
            _riverGenerator = riverGenerator;
            _nameSource = nameSource;
            _localPlayerInfo = localPlayerInfo;
            _simulationEventBus = simulationEventBus;
            _serializableServices = serializableServices.ToArray();
            _aiManager = aiManager;
        }

        public void GenerateWorld(WorldParameters worldParameters, ReportProgress report)
        {
            foreach(var service in _serializableServices)
            {
                service.Clear();
            }

            report(0f, "Generating World");
            var random = new Random(worldParameters.Seed.GetHashCode());
            _nameSource.InitializeForSeed(worldParameters.Seed.GetHashCode());
            var hexes = MakeHexes(worldParameters);
            
            report(0f, "Generating intial regions.");
            var regions = _regionGenerator.DetermineRegions(random, hexes, worldParameters);

            var heightNoise = new SimplexNoiseGenerator(random.Next());
            var drainageNoise = new SimplexNoiseGenerator(random.Next());

            report(0.2f, "Determining terrain.");
            foreach (var region in regions)
            {
                var heightValue = heightNoise.CoherentNoise(
                    region.Center.HexX,
                    region.Center.HexY,
                    0,
                    2,
                    35,
                    0.5f,
                    2f,
                    0.9f);

                TerrainType terrain;

                if (region.Hexes.Any(h => h.Neighbors.Any(n => n == null)))
                {
                    terrain = worldParameters.TerrainTypes
                        .Where(t => t.PlaceOnWorldEdge)
                        .FirstOrDefault();
                }
                else
                {
                    terrain = worldParameters.TerrainTypes
                        .Where(t => heightValue < t.ParamMaxHeight)
                        .FirstOrDefault();
                }

                if (terrain != null)
                {
                    AssignTerrain(random, region, terrain);
                }
                else
                {
                    _logger.Error($"Could not find terrain for: height {heightValue} worldEdge {region.Hexes.Any(h => h.Neighbors == null)}.");
                    return;
                }
            }

            var toTransition = new List<WorldHex>();
            foreach(var hex in hexes)
            {
                if (hex.Terrain.BackingType.SlopeTransition != null
                    && hex.Neighbors.Any(n => n != null && n.Terrain.Height > hex.Terrain.Height))
                {
                    toTransition.Add(hex);
                }
            }
            foreach(var hex in toTransition)
            {
                hex.Terrain = new Terrain(hex.Terrain.BackingType.SlopeTransition, hex);
            }

            report(0.4f, "Placing rivers.");
            foreach (var region in regions)
            {
                var distanceToRainSource = regions
                    .Where(r => worldParameters.RainSources.Contains(r.TerrainType))
                    .Select(r => r.Center.DistanceTo(region.Center) / 4)
                    .OfType<float?>()
                    .OrderBy(d => d)
                    .FirstOrDefault() ?? worldParameters.RainDistanceScale;

                var rainfall = 0f;
                if (distanceToRainSource < worldParameters.RainDistanceScale)
                {
                    rainfall = 1 - (distanceToRainSource / worldParameters.RainDistanceScale);
                }
                rainfall = Mathf.Clamp(rainfall, 0f, 0.99f);

                region.Rainfall = rainfall;
            }
            _riverGenerator.GenerateRivers(worldParameters, random, hexes, regions);

            _regionGenerator.LinkRegionsToNeighbors(regions);

            report(0.6f, "Assigning biomes.");
            foreach (var region in regions)
            {
                if (region.BiomeType == null)
                {
                    var drainageValue = drainageNoise.CoherentNoise(
                        region.Center.HexX,
                        region.Center.HexY,
                        0,
                        2,
                        25,
                        0.5f,
                        2f,
                        0.9f);

                    var distanceFromCenter = region.Center.DistanceTo(new HexPosition(worldParameters.WorldRadius, worldParameters.WorldRadius)) / Constants.VERTICIES_PER_HEX;
                    var halfRadius = worldParameters.WorldRadius / 2;

                    var temperature = 1 - Mathf.Abs((distanceFromCenter - 0.3f * halfRadius) / halfRadius);

                    drainageValue = Mathf.Clamp((drainageValue + 0.19f) / 0.35f, 0f, 0.99f);
                    temperature = Mathf.Clamp(temperature, 0f, 0.99f);

                    region.Temperature = temperature;
                    region.Drainage = drainageValue;


                    var mapping = worldParameters.BiomeMappings
                        .OrderBy(bm => bm.Score(region.Rainfall, drainageValue, temperature, region.TerrainType.Height, distanceFromCenter / worldParameters.WorldRadius))
                        .FirstOrDefault();


                    if (mapping != null)
                    {
                        AssignBiome(random, region, mapping.Biome);
                    }
                    else
                    {
                        _logger.Error($"Could not find biome for: rainfall {region.Rainfall} drainage {drainageValue} temperature {temperature} terrain height {region.TerrainType.Height}");
                        return;
                    }
                }
            }

            report(0.8f, "Merging regions.");
            _regionGenerator.MergeRegions(random, hexes, regions, worldParameters);

            foreach (var region in regions)
            {
                region.Name = _nameSource.MakeRegionName(region);
            }

            foreach (var hex in hexes)
            {
                if (hex.RiverFlow > 0 
                    && worldParameters.RainSources.Contains(hex.Neighbors[(int)hex.RiverFlowDirection].Terrain.BackingType))
                {
                    var deltas = hex.Yield()
                        .Concat(hex.Neighbors)
                        .Where(h => h != null)
                        .ToArray();

                    foreach(var deltaHex in deltas)
                    {
                        if(deltaHex.BiomeType.DeltaUpgradesTo != null)
                        {
                            deltaHex.BiomeType = deltaHex.BiomeType.DeltaUpgradesTo;
                        }
                    }
                }
            }

            foreach (var hex in hexes)
            {
                if(hex.BiomeType.RiverUpgradesTo != null
                    && (hex.RiverFlow > 0 || hex.Neighbors.Any(n => n != null && n.RiverFlow > 0)))
                {
                    hex.BiomeType = hex.BiomeType.RiverUpgradesTo;
                }
            }

            report(0.9f, "Placing Features");
            foreach (var hex in hexes.Where(h => h.RiverFlow < 1))
            {
                var featuresToPlace = new List<CellFeatureType>();

                if(random.NextDouble() < hex.Terrain.CellFeatureChance && hex.Terrain.CellFeatures.Any())
                {
                    featuresToPlace.Add(random.Choose(hex.Terrain.CellFeatures));
                }

                foreach (var neighbor in hex.Neighbors.Where(n => n != null))
                {
                    if (random.NextDouble() < neighbor.Terrain.AdjacentCellFeatureChance && neighbor.Terrain.AdjacentCellFeatures.Any())
                    {
                        featuresToPlace.Add(random.Choose(neighbor.Terrain.AdjacentCellFeatures));
                    }
                }

                if (hex.Neighbors.Any(n => n != null && n.RiverFlow > 0)
                    && hex.BiomeType.RiverCellFeatures.Any())
                {
                    if (random.NextDouble() < hex.BiomeType.RiverCellFeatureChance)
                    {
                        featuresToPlace.Add(random.Choose(hex.BiomeType.RiverCellFeatures));

                        
                    }
                }
                else
                {
                    if (random.NextDouble() < hex.BiomeType.CellFeatureChance
                        && hex.BiomeType.CellFeatures.Any())
                    {
                        featuresToPlace.Add(random.Choose(hex.BiomeType.CellFeatures));
                    }
                }

                foreach(var featureType in featuresToPlace.Distinct())
                {
                    hex.AddFeature(new CellFeature(featureType, hex));
                }
            }
            
            report(1.0f, "Finishing.");
            _worldManager.SetWorld(
                0,
                worldParameters.Seed,
                worldParameters.WorldRadius,
                hexes,
                regions);
        }



        public void PopulateWorld(
            WorldParameters worldParameters,
            StartingCulture playerCulture,
            string empireName,
            string tribeName,
            Color primaryColor, 
            Color secondaryColor,
            ReportProgress report)
        {
            var random = new Random(worldParameters.Seed.GetHashCode());
            _worldManager.Empires.Clear();
            _worldManager.Cultures.Clear();

            var claimedRegions = new List<WorldRegion>();

            report(0f, "Creating Player Empire");
            var empire = PlaceEmpire(random, playerCulture, empireName, tribeName, true, claimedRegions);
            empire.PrimaryColor = primaryColor;
            empire.SecondaryColor = secondaryColor;
            report(0.2f, "Placing Creeps");
            foreach(var region in _worldManager.Regions)
            {
                if(!claimedRegions.Contains(region)
                    && !claimedRegions.ContainsAny(region.Neighbors))
                {
                    var placed = TryPlaceCreep(random, worldParameters, region);

                    if(placed)
                    {
                        claimedRegions.Add(region);
                    }
                }
            }

            _simulationEventBus.SendEvent(new WorldPopulatedSimulationEvent());
        }

        private Empire PlaceEmpire(
            Random random,
            StartingCulture playerCulture, 
            string empireName, 
            string tribeName,
            bool isPlayer,
            List<WorldRegion> claimedRegions)
        {
            var startingRegion = FindStartingRegionFor(playerCulture, claimedRegions);

            if (startingRegion == null && isPlayer)
            {
                throw new Exception("Could not place player tribe, no valid regions.");
            }

            claimedRegions.Add(startingRegion);

            var empire = new Empire(empireName)
            {
                PrimaryColor = random.Color(1),
                SecondaryColor = random.Color(0.5f)
            };
            _worldManager.Empires.Add(empire);
            var tribe = new AdministrativeGroup(empire, tribeName, false);
            empire.AdministrativeGroups.Add(tribe);
            var culture = GetOrMakeCulture(playerCulture);
            _worldManager.Cultures.Add(culture);

            if(isPlayer)
            {
                _localPlayerInfo.Empire = empire;
            }

            for (var i = 0; i < 3; i++)
            {
                var location = startingRegion.Hexes
                    .Where(h => h.Jobs.Any(j => j.Worker == null))
                    .Where(h => playerCulture.StartingSpecies.MovementType.RequiredTraits.Intersect(h.TerrainTraits).Any())
                    .OrderBy(h => h.Neighbors.Where(n => n != null && n.Pops.Any()).Count())
                    .FirstOrDefault();

                if (location != null)
                {
                    // Pop constructor adds pop to group and hex
                    var pop = new Pop(
                        playerCulture.StartingSpecies,
                        empire,
                        tribe,
                        culture,
                        location);

                    var job = location.Jobs.Where(j => j.Worker == null).First();

                    job.Worker = pop;
                    pop.Job = job;
                    location.Pops.Add(pop);
                }
            }

            tribe.UpdateGroupCenter();

            return empire;
        }

        private bool TryPlaceCreep(
            Random random,
            WorldParameters worldParameters, 
            WorldRegion startingRegion)
        {
            var creepToPlace = worldParameters.Creeps
                .Where(c => c.BiomeTypes.Contains(startingRegion?.BiomeType) || (c.CanSpawnInRivers && startingRegion.Hexes.Any(h => h.RiverFlow > 0)))
                .Select(c => Tuple.Create(random.Next(20) + c.Likelyhood, c))
                .OrderBy(t => t.Item1)
                .Where(t => t.Item1 > 10)
                .Select(t => t.Item2)
                .FirstOrDefault();

            if (creepToPlace != null)
            {
                var traits = creepToPlace.MandatoryAiTraits.ToList();
                for(var i = 0; i < creepToPlace.OptionalTraitPicks; i++)
                {
                    var options = creepToPlace.OptionalAiTraits
                        .Where(o => !traits.Any(t => t.UniquenessCategory == o.UniquenessCategory))
                        .ToArray();

                    if(options.Any())
                    {
                        traits.Add(random.Choose(options));
                    }
                }

                var culture = GetOrMakeCulture(creepToPlace.Culture);

                var tribeName = _nameSource.MakeTribeName(culture);
                var empireName = _nameSource.MakeEmpireName(culture, tribeName);

                var empire = new Empire(empireName)
                {
                    PrimaryColor = random.Color(1),
                    SecondaryColor = random.Color(0.5f)
                };
                _worldManager.Empires.Add(empire);
                var tribe = new AdministrativeGroup(empire, tribeName, false);
                empire.AdministrativeGroups.Add(tribe);

                var targetSize = 3f;

                foreach(var trait in traits)
                {
                    targetSize *= (1 + trait.TargetSizeIncrease);
                }

                _aiManager.RegisterAi(
                    empire,
                    traits,
                    targetSize);

                for (var i = 0; i < targetSize; i++)
                {
                    var location = startingRegion.Hexes
                        .Where(h => h.Jobs.Any(j => j.Worker == null))
                        .Where(h => creepToPlace.Culture.StartingSpecies.MovementType.RequiredTraits.Intersect(h.TerrainTraits).Any())
                        .OrderBy(h => h.Neighbors.Where(n => n != null && n.Pops.Any()).Count())
                        .FirstOrDefault();

                    if (location != null)
                    {
                        // Pop constructor adds pop to group and hex
                        var pop = new Pop(
                            creepToPlace.Culture.StartingSpecies,
                            empire,
                            tribe,
                            culture,
                            location);

                        var job = location.Jobs.Where(j => j.Worker == null).First();

                        job.Worker = pop;
                        pop.Job = job;
                        location.Pops.Add(pop);
                    }
                }

                tribe.UpdateGroupCenter();

                return true;
            }

            return false;
        }

        private Culture GetOrMakeCulture(StartingCulture culture)
        {
            var existingCulture = _worldManager.Cultures
                .Where(c => c.Id == culture.Id)
                .FirstOrDefault();

            if(existingCulture == null)
            {
                existingCulture = culture.MakeCulture();
                _worldManager.Cultures.Add(existingCulture);
            }

            return existingCulture;
        }

        private WorldRegion FindStartingRegionFor(StartingCulture culture, List<WorldRegion> claimedRegions)
        {
            return _worldManager.Regions
                .Where(r => !culture.ForbiddenBiomes.Contains(r.BiomeType))
                .Where(r => !r.Neighbors.Any(n => claimedRegions.Contains(n)))
                .OrderByDescending(r => culture.PreferredBiomes.Contains(r.BiomeType) ? 1 : 0)
                .FirstOrDefault();
        }

        private List<WorldHex> MakeHexes(WorldParameters worldParameters)
        {
            var hexLookup = new WorldHex[worldParameters.WorldRadius * 2, worldParameters.WorldRadius * 2];

            var worldCenter = new HexPosition(worldParameters.WorldRadius, worldParameters.WorldRadius);

            var hexes = new List<WorldHex>();
            for (var hexY = 0; hexY < worldParameters.WorldRadius * 2; hexY += 1)
            {
                var maxX = worldParameters.WorldRadius * 2;
                if (hexY % 2 != 0)
                {
                    maxX -= 1;
                }

                for (var hexX = 0; hexX < maxX; hexX += 1)
                {
                    if (new HexPosition(hexX, hexY).DistanceTo(worldCenter) < worldParameters.WorldRadius * 4)
                    {
                        var worldHex = new WorldHex(hexX, hexY);
                        hexLookup[hexX, hexY] = worldHex;

                        hexes.Add(worldHex);
                    }
                }
            }

            foreach(var hex in hexes)
            {
                if(hex.Position.HexY % 2 == 0)
                {
                    hex.Neighbors[(int)HexFacing.NorthEast] = GetHexIfExists(
                        hex.Position,
                        hexLookup,
                        0,
                        -1);
                    hex.Neighbors[(int)HexFacing.NorthWest] = GetHexIfExists(
                        hex.Position,
                        hexLookup,
                        -1,
                        -1);

                    hex.Neighbors[(int)HexFacing.SouthEast] = GetHexIfExists(
                        hex.Position,
                        hexLookup,
                        0,
                        1);
                    hex.Neighbors[(int)HexFacing.SouthWest] = GetHexIfExists(
                        hex.Position,
                        hexLookup,
                        -1,
                        1);
                }
                else
                {
                    hex.Neighbors[(int)HexFacing.NorthEast] = GetHexIfExists(
                        hex.Position,
                        hexLookup,
                        1,
                        -1);
                    hex.Neighbors[(int)HexFacing.NorthWest] = GetHexIfExists(
                        hex.Position,
                        hexLookup,
                        0,
                        -1);

                    hex.Neighbors[(int)HexFacing.SouthEast] = GetHexIfExists(
                        hex.Position,
                        hexLookup,
                        1,
                        1);
                    hex.Neighbors[(int)HexFacing.SouthWest] = GetHexIfExists(
                        hex.Position,
                        hexLookup,
                        0,
                        1);
                }

                hex.Neighbors[(int)HexFacing.East] = GetHexIfExists(
                        hex.Position,
                        hexLookup,
                        1,
                        0);
                hex.Neighbors[(int)HexFacing.West] = GetHexIfExists(
                    hex.Position,
                    hexLookup,
                    -1,
                    0);


            }

            return hexes;
        }

        private WorldHex GetHexIfExists(
            HexPosition position, 
            WorldHex[,] hexLookup, 
            int dx, 
            int dy)
        {
            var x = position.HexX + dx;
            var y = position.HexY + dy;

            if(x < 0 
                || y < 0
                || x >= hexLookup.GetLength(0)
                || y >= hexLookup.GetLength(1))
            {
                return null;
            }

            return hexLookup[x, y];
        }

        private void AssignTerrain(Random random, WorldRegion region, TerrainType terrain)
        {
            region.TerrainType = terrain;

            foreach (var hex in region.Hexes)
            {
                hex.Terrain = new Terrain(terrain, hex);
            }

            if(terrain.Inclusions?.Length > 0)
            {
                foreach (var hex in region.Hexes)
                {
                    if(random.NextDouble() < terrain.InclusionChance
                        && (terrain.CanInclusionsBorder || hex.Neighbors.All(n=>n.Region == hex.Region && n.Terrain.BackingType == terrain)))
                    {
                        var inclusion = random.Choose(terrain.Inclusions);

                        hex.Terrain = new Terrain(inclusion, hex);
                    }
                }
            }
        }

        private void AssignBiome(Random random, WorldRegion region, BiomeType biome)
        {
            region.BiomeType = biome;
            
            foreach (var hex in region.Hexes)
            {
                var biomeForHex = biome;

                if(biome.HeightTransitionsTo != null && hex.Terrain.Height >= biome.HeightTransitionHeight)
                {
                    biomeForHex = biome.HeightTransitionsTo;
                }

                hex.BiomeType = biomeForHex;
            }
        }
    }
}
