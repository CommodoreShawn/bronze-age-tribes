﻿using BronzeAgeTribes.Logic.Data;
using BronzeAgeTribes.Logic.Data.Game;
using BronzeAgeTribes.Logic.Infrastructure.Interfaces;
using Godot;

namespace BronzeAgeTribes.Logic.Infrastructure.Services
{
    public class DefaultLocalPlayerInfo : ILocalPlayerInfo
    {
        public WorldHex HoveredHex { get; set; }
        public Empire Empire { get; set; }
        public Vector3 ViewCenter { get; set; }
        public CameraFocus CameraFocus { get; set; }
        public bool ShowHexWidgets { get; set; } = true;

        public void LookAt(HexPosition lookAt)
        {
            if(CameraFocus != null &&  Godot.Object.IsInstanceValid(CameraFocus))
            {
                CameraFocus.LookAt(lookAt);
            }
        }
    }
}
