﻿using Autofac;
using BronzeAgeTribes.Logic.Infrastructure.Interfaces;
using System.Collections.Generic;

namespace BronzeAgeTribes.Logic.Infrastructure
{
    public static class IocManager
    {
        public static IContainer IocContainer { get; set; }

        public static void Initialize()
        {
            var builder = new ContainerBuilder();

            var iocModules = new Module[]
            {
                new CoreModule()
            };

            foreach (var module in iocModules)
            {
                builder.RegisterModule(module);
            }

            IocContainer = builder.Build();

            foreach(var initService in IocContainer.Resolve<IEnumerable<IInitializableService>>())
            {
                initService.Initialize();
            }
        }
    }
}
