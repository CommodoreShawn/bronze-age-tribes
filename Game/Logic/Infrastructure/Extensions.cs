﻿using Godot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BronzeAgeTribes.Logic.Infrastructure
{
    public static class Extensions
    {
        public static T Choose<T>(this Random rnd, IEnumerable<T> set)
        {
            var array = (set as T[]) ?? set.ToArray();

            if (array.Length < 1)
            {
                throw new ArgumentException("Cannot pick item from empty set.");
            }

            var r = rnd.Next(array.Length);

            return array[r];
        }

        public static T[] ToArray<T>(this Godot.Collections.Array array)
        {
            var output = new T[array.Count];

            for(var i = 0; i < array.Count; i++)
            {
                output[i] = (T)array[i];
            }

            return output;
        }

        public static Color Color(this Random rnd, float rangeFactor)
        {
            return new Color(
                (float)rnd.NextDouble() * rangeFactor,
                (float)rnd.NextDouble() * rangeFactor,
                (float)rnd.NextDouble() * rangeFactor);
        }

        //public static T ChooseByLikelyhood<T>(this Random rnd, IEnumerable<LikelyhoodFor<T>> set)
        //{
        //    var array = (set as LikelyhoodFor<T>[]) ?? set.ToArray();

        //    if (array.Length < 1)
        //    {
        //        throw new ArgumentException("Cannot pick item from empty set.");
        //    }

        //    var total = array.Sum(a => a.Likelyhood);
        //    var r = rnd.Next(total);

        //    foreach (var item in array)
        //    {
        //        if (r < item.Likelyhood)
        //        {
        //            return item.Value;
        //        }
        //        else
        //        {
        //            r -= item.Likelyhood;
        //        }
        //    }

        //    return array.Last().Value;
        //}

        public static float Variance(this Random rnd, float baseValue, float maxVariance)
        {
            return (float)(baseValue + (rnd.NextDouble() * 2 - 1) * maxVariance);
        }

        public static IEnumerable<T> Yield<T>(this T self)
        {
            yield return self;
        }

        public static bool ContainsAll<T>(this IEnumerable<T> self, IEnumerable<T> other)
        {
            return self.Intersect(other).Count() == other.Count();
        }

        public static bool ContainsAny<T>(this IEnumerable<T> self, IEnumerable<T> other)
        {
            return self.Intersect(other).Any();
        }

        public static Queue<T> ToQueue<T>(this IEnumerable<T> source)
        {
            return new Queue<T>(source);
        }

        public static T RandomOrDefault<T>(this IEnumerable<T> source, Random random)
        {
            var a = source.ToArray();

            if (a.Length < 1)
            {
                return default(T);
            }

            return a[random.Next(a.Length)];
        }

        // Adapted from http://stackoverflow.com/a/1262619
        public static void Shuffle<T>(this Random rnd, List<T> list)
        {
            var n = list.Count;
            while (n > 1)
            {
                n--;
                var k = rnd.Next(n + 1);
                var value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }

        // From https://msdn.microsoft.com/en-us/library/kdcak6ye%28v=vs.110%29.aspx?f=255&MSPPError=-2147217396
        public static string ToAsciiString(this string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return string.Empty;
            }

            Encoding ascii = Encoding.ASCII;
            Encoding unicode = Encoding.Unicode;

            // Convert the string into a byte array.
            byte[] unicodeBytes = unicode.GetBytes(value);

            // Perform the conversion from one encoding to the other.
            byte[] asciiBytes = Encoding.Convert(unicode, ascii, unicodeBytes);

            // Convert the new byte[] into a char[] and then into a string.
            char[] asciiChars = new char[ascii.GetCharCount(asciiBytes, 0, asciiBytes.Length)];
            ascii.GetChars(asciiBytes, 0, asciiBytes.Length, asciiChars, 0);
            string asciiString = new string(asciiChars);

            return asciiString;
        }
    }
}
