﻿using BronzeAgeTribes.Logic.Data.Serialization;
using System.Collections.Generic;

namespace BronzeAgeTribes.Logic.Infrastructure.Interfaces
{
    public interface IWorldSerializer
    {
        IEnumerable<SaveSummary> ListSaveFiles();
        void SaveWorld(ReportProgress report, string filename);
        void LoadWorld(ReportProgress report, SaveSummary saveSummary);
        bool CanLoad(int schemaVersion);
        void Autosave(ReportProgress report);
        void DeleteSave(SaveSummary saveToDelete);
    }
}
