﻿namespace BronzeAgeTribes.Logic.Infrastructure.Interfaces
{
    public interface IWorldSimulator
    {
        void AdvanceTurn(ReportProgress report);
    }
}
