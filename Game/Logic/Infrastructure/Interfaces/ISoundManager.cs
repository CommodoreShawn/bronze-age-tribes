﻿namespace BronzeAgeTribes.Logic.Infrastructure.Interfaces
{
    public interface ISoundManager
    {
        void RegisterMusicController(MusicController musicController);

        void SetMenuMusic();

        void SetMusic(bool isExploration, bool isBattle);
    }
}
