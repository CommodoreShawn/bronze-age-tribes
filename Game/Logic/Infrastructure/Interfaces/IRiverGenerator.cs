﻿using BronzeAgeTribes.Logic.Data;
using System;
using System.Collections.Generic;

namespace BronzeAgeTribes.Logic.Infrastructure.Interfaces
{
    public interface IRiverGenerator
    {
        void GenerateRivers(WorldParameters worldParameters, Random random, List<WorldHex> hexes, List<WorldRegion> regions);
    }
}
