﻿using BronzeAgeTribes.Logic.Data;
using BronzeAgeTribes.Logic.Data.Game;
using System.Collections.Generic;

namespace BronzeAgeTribes.Logic.Infrastructure.Interfaces
{
    public interface IWorldManager
    {
        string Seed { get; }
        int WorldRadiusHex { get; }
        IEnumerable<WorldHex> Hexes { get; }
        IEnumerable<WorldRegion> Regions { get; }
        List<Empire> Empires { get; }
        List<Culture> Cultures { get; }
        int Turn { get; set; }

        void SetWorld(
            int turn,
            string seed,
            int worldRadiusHex,
            IEnumerable<WorldHex> hexes,
            IEnumerable<WorldRegion> regions);

        WorldHex GetHexAtWorldPoint(float worldX, float worldY);

        WorldHex GetHex(HexPosition position);
    }
}
