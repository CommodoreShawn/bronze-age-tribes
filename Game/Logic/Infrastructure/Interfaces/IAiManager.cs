﻿using BronzeAgeTribes.Logic.Data.Game;
using System.Collections.Generic;

namespace BronzeAgeTribes.Logic.Infrastructure.Interfaces
{
    public interface IAiManager
    {
        void RegisterAi(
            Empire empire,
            IEnumerable<AiTrait> traits,
            float targetSize);

        void DoTurnLogic();
    }
}
