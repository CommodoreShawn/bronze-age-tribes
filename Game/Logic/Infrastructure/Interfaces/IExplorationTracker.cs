﻿using BronzeAgeTribes.Logic.Data;

namespace BronzeAgeTribes.Logic.Infrastructure.Interfaces
{
    public interface IExplorationTracker
    {
        bool IsExplored(HexPosition pos);

        bool IsVisible(HexPosition pos);

        ExplorationMemory GetExplorationMemory(HexPosition pos);
    }
}
