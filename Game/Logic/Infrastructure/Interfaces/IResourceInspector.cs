﻿using Godot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BronzeAgeTribes.Logic.Infrastructure.Interfaces
{
    public interface IResourceInspector
    {
        IEnumerable<TResource> ListResources<TResource>(string path) where TResource : Resource;

        IEnumerable<TResource> ListResources<TResource>(string path, string extension) where TResource : Resource;

        IEnumerable<string> ListTextFiles(string path);
    }
}
