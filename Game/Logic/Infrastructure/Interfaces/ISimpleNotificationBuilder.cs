﻿using BronzeAgeTribes.Logic.Data;

namespace BronzeAgeTribes.Logic.Infrastructure.Interfaces
{
    public interface ISimpleNotificationBuilder
    {
        void SendSimpleNotification(
            string icon, 
            string summaryTitle, 
            string summaryBody, 
            string title, 
            string body, 
            HexPosition? lookAt, 
            bool autoOpen);

        void SendLookAtNotification(
            string icon,
            string summaryTitle,
            string summaryBody,
            HexPosition lookAt);
    }
}
