﻿using BronzeAgeTribes.Logic.Data;
using BronzeAgeTribes.Logic.Data.Game;
using System.Collections.Generic;

namespace BronzeAgeTribes.Logic.Infrastructure.Interfaces
{
    public interface IPopManager
    {
        void MovePop(Pop pop, PopJob toJob);

        bool CanNewPopBePlaced(WorldHex hex, AdministrativeGroup group, Species species, Culture culture);

        bool CanPopBePlaced(WorldHex hex, Pop pop);

        bool CanPopBeMoved(WorldHex fromHex, WorldHex toHex, Pop pop);

        IEnumerable<WorldHex> GetMovementOptions(Pop pop);

        string[] GetPopInteractionChanceBreakdown(Pop interactor, Pop target, PopInteractionType interactionType);

        void RunPopInteractionLogic(Pop interactor, Pop target, PopInteractionType? interactionType);
    }
}
