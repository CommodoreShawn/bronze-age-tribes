﻿using BronzeAgeTribes.Logic.Data;

namespace BronzeAgeTribes.Logic.Infrastructure.Interfaces
{
    public interface INotificationHandler
    {
        bool CanHandle(Notification notification);

        void Open(Notification notification);

        void QuickDismiss(Notification notification);
    }
}
