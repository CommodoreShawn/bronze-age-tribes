﻿using BronzeAgeTribes.Logic.Data;
using BronzeAgeTribes.Logic.Data.Game;

namespace BronzeAgeTribes.Logic.Infrastructure.Interfaces
{
    public interface INameSource
    {
        void InitializeForSeed(int seed);

        string MakeRegionName(WorldRegion region);

        string MakeEmpireName(Culture dominantCulture, string startingTribeName);

        string MakeTribeName(Culture dominantCulture);
    }
}
