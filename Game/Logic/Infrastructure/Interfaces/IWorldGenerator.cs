﻿using BronzeAgeTribes.Logic.Data;
using Godot;

namespace BronzeAgeTribes.Logic.Infrastructure.Interfaces
{
    public interface IWorldGenerator
    {
        void GenerateWorld(WorldParameters worldParameters, ReportProgress report);

        void PopulateWorld(WorldParameters worldParameters, StartingCulture playerCulture, string empireName, string tribeName, Color primaryColor, Color secondaryColor, ReportProgress report);
    }
}
