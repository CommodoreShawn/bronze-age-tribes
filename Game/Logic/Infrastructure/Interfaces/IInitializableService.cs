﻿namespace BronzeAgeTribes.Logic.Infrastructure.Interfaces
{
    public interface IInitializableService
    {
        void Initialize();
    }
}
