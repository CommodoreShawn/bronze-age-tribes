﻿using BronzeAgeTribes.Logic.Data;
using BronzeAgeTribes.Logic.Data.Game;
using Godot;

namespace BronzeAgeTribes.Logic.Infrastructure.Interfaces
{
    public interface ILocalPlayerInfo
    {
        WorldHex HoveredHex { get; set; }
        Empire Empire { get; set; }
        Vector3 ViewCenter { get; set; }

        CameraFocus CameraFocus { get; set; }
        bool ShowHexWidgets { get; set; }

        void LookAt(HexPosition lookAt);
    }
}
