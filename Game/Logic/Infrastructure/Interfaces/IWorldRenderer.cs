﻿using BronzeAgeTribes.Logic.Data;
using Godot;
using System.Collections.Generic;

namespace BronzeAgeTribes.Logic.Infrastructure.Interfaces
{
    public interface IWorldRenderer
    {
        int GetVertexSizeForWorld();

        void UpdateHexModels(
            Spatial worldRoot,
            IEnumerable<WorldHex> hexes,
            bool godVision);

        void SetColorMode(Spatial worldRoot, int mode);

        float GetVertexHeightAt(int vX, int vY);

        void BuildTerrain(
            Spatial worldRoot, 
            SpatialMaterial landMaterial, 
            SpatialMaterial waterMaterial, 
            SpatialMaterial shadowMaterial,
            SpatialMaterial borderMaterial,
            bool godvision);

        void UpdateTerrain(Spatial worldRoot, IEnumerable<WorldRegion> regions);

        ArrayMesh MakeBorderLine(
            WorldHex[] regionToSurround,
            float lineWidth,
            float floatHeight,
            Color color,
            bool godvision);
    }
}
