﻿namespace BronzeAgeTribes.Logic.Infrastructure.Interfaces
{
    public interface IGameplayService
    {
        void Process();
    }
}
