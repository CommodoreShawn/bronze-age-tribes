﻿using BronzeAgeTribes.Logic.Data;
using System.Collections.Generic;

namespace BronzeAgeTribes.Logic.Infrastructure.Interfaces
{
    public interface INotificationTracker
    {
        void ClearNotifications();

        IEnumerable<Notification> CurrentNotifications { get; }

        void Open(Notification notification);

        void QuickDismiss(Notification notification);
    }
}
