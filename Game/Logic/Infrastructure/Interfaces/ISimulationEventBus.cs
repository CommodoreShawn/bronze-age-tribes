﻿using BronzeAgeTribes.Logic.Infrastructure.SimulationEvents;

namespace BronzeAgeTribes.Logic.Infrastructure.Interfaces
{
    public delegate void SimulationEventHandler(ISimulationEvent evt);

    public interface ISimulationEventBus
    {
        event SimulationEventHandler OnSimulationEvent;

        void SendEvent(ISimulationEvent evt);
    }
}
