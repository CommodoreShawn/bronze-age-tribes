﻿using System.Collections.Generic;

namespace BronzeAgeTribes.Logic.Infrastructure.Interfaces
{
    public interface IResourceTracker
    {
        IEnumerable<BiomeType> BiomeTypes { get; }
        IEnumerable<TerrainType> TerrainTypes { get; }
        IEnumerable<CellFeatureType> FeatureTypes { get; }
        IEnumerable<DevelopmentType> DevelopmentTypes { get; }
        IEnumerable<Trait> Traits { get; }
        IEnumerable<Species> Species { get; }
        IEnumerable<BuildingStyle> BuildingStyles { get; }
        IEnumerable<BuildingType> BuildingTypes { get; }
    }
}
