﻿using BronzeAgeTribes.Logic.Data;
using BronzeAgeTribes.Logic.Data.Game;
using System.Collections.Generic;

namespace BronzeAgeTribes.Logic.Infrastructure.Interfaces
{
    public interface IDevelopmentListProvider
    {
        IEnumerable<DevelopmentType> GetDevelopmentOptionsFor(
            WorldHex tile, 
            AdministrativeGroup group,
            Empire owner);

        IEnumerable<TentativeOptionInfo<DevelopmentType>> GetTentativeDevelopmentOptionsFor(
            WorldHex tile,
            AdministrativeGroup group,
            Empire owner);

        IEnumerable<BuildingType> GetBuildingOptionsFor(
            WorldHex tile,
            AdministrativeGroup group,
            Empire owner);
    }
}
