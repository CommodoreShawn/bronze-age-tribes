﻿using BronzeAgeTribes.Logic.Data;
using BronzeAgeTribes.Logic.Data.Game;
using Godot;

namespace BronzeAgeTribes.Logic.Infrastructure.Interfaces
{
    public interface ITooltipManager
    {
        void ShowTooltipFor(Control caller, Trait trait);
        void ShowTooltipFor(Control caller, PopJob popJob);
        void HideTooltipFor(object caller);
        void RegisterTooltipController(TooltipController tooltipController);
        void ShowTooltipFor(GameplayWorld gameplayWorld, WorldHex hoveredHex);
        void ShowTooltipFor(Control caller, ResourceType resource, float value, string details);
        void ShowTooltipFor(Control caller, Pop interactor, Pop target, PopInteractionType interactionType);
        void ShowTooltipForGrowth(Control caller, AdministrativeGroup group, Species species);
        void ShowTooltipFor(Control caller, Pop pop);
        void ShowTooltipFor(Control caller, WorldHex hex, CellFeature feature);
        void ShowTooltipFor(Control caller, WorldHex hex, CellFeature feature, InteractionOption interactionOption);
        void ShowTooltipFor(Control caller, WorldHex hex, Development development);
        void ShowTooltipFor(Control caller, WorldHex hex, Building building);
        void ShowTooltipFor(Control caller, WorldHex hex, Development development, InteractionOption interactionOption);
        void ShowTooltipFor(Control caller, WorldHex hex, Building building, InteractionOption interactionOption);
        void ShowTooltipFor(Control caller, WorldHex hex, Terrain terrain);
        void ShowTooltipFor(Control caller, WorldHex hex, DevelopmentType developmentType, CellFeature featureToClear);
        void ShowTooltipFor(Control caller, WorldHex hex, BuildingType buildingType);
        void ShowTooltipFor(Control caller, Notification notification);
        void ShowTooltipFor(Control caller, WorldRegion region);
    }
}