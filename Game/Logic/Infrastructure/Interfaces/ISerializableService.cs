﻿using BronzeAgeTribes.Logic.Data.Serialization;

namespace BronzeAgeTribes.Logic.Infrastructure.Interfaces
{
    public interface ISerializableService
    {
        void Clear();

        void WriteTo(SerializedObject root);

        void ReadFrom(SerializedObject root);
    }
}
