﻿using BronzeAgeTribes.Logic.Data;
using System;
using System.Collections.Generic;

namespace BronzeAgeTribes.Logic.Infrastructure.Interfaces
{
    public interface IRegionGenerator
    {
        List<WorldRegion> DetermineRegions(Random random, List<WorldHex> hexes, WorldParameters worldParameters);
        
        void MergeRegions(Random random, List<WorldHex> hexes, List<WorldRegion> regions, WorldParameters worldParameters);

        void LinkRegionsToNeighbors(List<WorldRegion> worldRegions);
        
    }
}
