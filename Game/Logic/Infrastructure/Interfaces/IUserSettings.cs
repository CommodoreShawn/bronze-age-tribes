﻿namespace BronzeAgeTribes.Logic.Infrastructure.Interfaces
{
    public interface IUserSettings
    {
        float MusicVolume { get; set; }

        int AutosaveInterval { get; set; }
    }
}
