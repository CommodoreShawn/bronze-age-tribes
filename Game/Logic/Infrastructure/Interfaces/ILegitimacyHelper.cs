﻿using BronzeAgeTribes.Logic.Data;
using BronzeAgeTribes.Logic.Data.Game;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BronzeAgeTribes.Logic.Infrastructure.Interfaces
{
    public interface ILegitimacyHelper
    {
        float CostForSettling(AdministrativeGroup group, IEnumerable<WorldRegion> regionsToAdd);
        string[] CostForSettlingDetails(AdministrativeGroup group, IEnumerable<WorldRegion> regionsToAdd);
        float CostForSplitting(int toGo);
        string[] CostForSplittingDetails(int toGo);
    }
}
