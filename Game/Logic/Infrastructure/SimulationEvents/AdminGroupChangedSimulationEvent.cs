﻿using BronzeAgeTribes.Logic.Data.Game;

namespace BronzeAgeTribes.Logic.Infrastructure.SimulationEvents
{
    public class AdminGroupChangedSimulationEvent : ISimulationEvent
    {
        public AdministrativeGroup Group { get; }

        public AdminGroupChangedSimulationEvent(AdministrativeGroup newGroup)
        {
            Group = newGroup;
        }
    }
}
