﻿using BronzeAgeTribes.Logic.Data;
using System.Collections.Generic;

namespace BronzeAgeTribes.Logic.Infrastructure.SimulationEvents
{
    public class WorldChangedSimulationEvent : ISimulationEvent
    {
        public IEnumerable<WorldHex> AffectedHexes { get; }

        public WorldChangedSimulationEvent(IEnumerable<WorldHex> affectedHexes)
        {
            AffectedHexes = affectedHexes;
        }
    }
}
