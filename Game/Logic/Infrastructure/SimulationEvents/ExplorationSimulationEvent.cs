﻿using BronzeAgeTribes.Logic.Data;
using System.Collections.Generic;

namespace BronzeAgeTribes.Logic.Infrastructure.SimulationEvents
{
    public class ExplorationSimulationEvent : ISimulationEvent
    {
        public IEnumerable<WorldRegion> AffectedRegions { get; }

        public ExplorationSimulationEvent(IEnumerable<WorldRegion> affectedRegions)
        {
            AffectedRegions = affectedRegions;
        }
    }
}
