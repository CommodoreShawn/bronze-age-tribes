﻿using BronzeAgeTribes.Logic.Data;

namespace BronzeAgeTribes.Logic.Infrastructure.SimulationEvents
{
    public class NotificationSimulationEvent : ISimulationEvent
    {
        public Notification Notification { get; }

        public NotificationSimulationEvent(Notification notification)
        {
            Notification = notification;
        }
    }
}
