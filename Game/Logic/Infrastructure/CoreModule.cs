﻿using Autofac;
using BronzeAgeTribes.Logic.Infrastructure.Services;

namespace BronzeAgeTribes.Logic.Infrastructure
{
    /// <summary>
    /// Core dependency injection module.
    /// </summary>
    public class CoreModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            // Services
            builder.RegisterType<DefaultWorldGenerator>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<DefaultWorldManager>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<ConsoleLogger>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<NeighborGlomRegionGenerator>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<DefaultLocalPlayerInfo>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<DefaultRiverGenerator>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<LargeHexWorldRenderer>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<UnmNameSource>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<ResourceInspector>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<ResourceInspector>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<ResourceNamelistSource>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<SoundManager>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<TooltipManager>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<DefaultWorldSimulator>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<PopManager>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<DefaultSimulationEventBus>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<DefaultDevelopmentListProvider>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<DefaultWorldSerializer>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<ResourceTracker>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<DefaultExplorationTracker>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<DefaultNotificationTracker>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<SimpleNotificationBuilder>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<AiManager>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<DefaultDiplomacyTracker>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<LegitimacyHelper>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<UserSettings>().AsImplementedInterfaces().SingleInstance();
        }
    }
}
