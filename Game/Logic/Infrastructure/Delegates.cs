﻿namespace BronzeAgeTribes.Logic.Infrastructure
{
    public delegate void ReportProgress(float progress, string stepName);

    public delegate void ReportStepProgress(float stepProgress);
}
