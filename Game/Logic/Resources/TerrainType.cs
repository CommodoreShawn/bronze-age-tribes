using Godot;

public class TerrainType : Resource
{
    [Export]
    public string Id;

    [Export]
    public string Name;

    [Export(PropertyHint.MultilineText)]
    public string Description;

    [Export]
    public float Height;

    [Export]
    public float ParamMaxHeight;

    [Export]
    public bool PlaceOnWorldEdge;

    [Export]
    public float InclusionChance;

    [Export]
    public bool CanInclusionsBorder;

    [Export]
    public TerrainTrait[] Traits;

    [Export]
    public TerrainType[] Inclusions;

    [Export]
    public float CellFeatureChance;

    [Export]
    public CellFeatureType[] CellFeatures;

    [Export]
    public float AdjacentCellFeatureChance;

    [Export]
    public CellFeatureType[] AdjacentCellFeatures;

    [Export]
    public Texture InteractButtonIcon;

    [Export]
    public TerrainType SlopeTransition;
}
