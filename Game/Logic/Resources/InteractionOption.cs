using BronzeAgeTribes.Logic.Data;
using BronzeAgeTribes.Logic.Data.Game;
using BronzeAgeTribes.Logic.Infrastructure;
using Godot;
using System;
using System.Collections.Generic;
using System.Linq;

public class InteractionOption : Resource
{
    [Export]
    public string Id;

    [Export]
    public string Name;

    [Export(PropertyHint.MultilineText)]
    public string Description;

    [Export]
    public Texture Icon;

    [Export]
    public JobCategory Category;

    [Export]
    public int Turns;

    [Export]
    public PopJobType InteractionJob;

    public bool HasJobs => InteractionJob != null;

    public float GetExpectedOutput(ResourceType resource, WorldHex hex)
    {
        if(InteractionJob == null)
        {
            return 0;
        }

        return InteractionJob.GetExpectedOutput(resource, hex);
    }

    public IEnumerable<PopJob> MakeJobs(WorldHex location, CellFeature cellFeature)
    {
        if (InteractionJob == null)
        {
            return Enumerable.Empty<PopJob>();
        }

        return InteractionJob.MakeJob(location, cellFeature, this).Yield();
    }

    public IEnumerable<PopJob> MakeJobs(WorldHex location, Development development)
    {
        if (InteractionJob == null)
        {
            return Enumerable.Empty<PopJob>();
        }

        return InteractionJob.MakeJob(location, development, this).Yield();
    }

    public IEnumerable<PopJob> MakeJobs(WorldHex location, Building building)
    {
        if (InteractionJob == null)
        {
            return Enumerable.Empty<PopJob>();
        }

        return InteractionJob.MakeJob(location, building, this).Yield();
    }

    public IEnumerable<PopJob> MakeJobs(WorldHex location, Terrain terrain)
    {
        if (InteractionJob == null)
        {
            return Enumerable.Empty<PopJob>();
        }

        return InteractionJob.MakeJob(location, terrain, this).Yield();
    }

    public int GetTurnsRemaining(WorldHex hex, CellFeature feature)
    {
        if (InteractionJob == null)
        {
            return Turns;
        }

        return InteractionJob.GetTurnsRemaining(Turns, hex, feature);
    }

    public int GetTurnsRemaining(WorldHex hex, Development development)
    {
        if (InteractionJob == null)
        {
            return Turns;
        }

        return InteractionJob.GetTurnsRemaining(Turns, hex, development);
    }

    public int GetTurnsRemaining(WorldHex hex, Building building)
    {
        if (InteractionJob == null)
        {
            return Turns;
        }

        return InteractionJob.GetTurnsRemaining(Turns, hex, building);
    }
}
