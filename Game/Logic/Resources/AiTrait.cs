﻿using Godot;

public class AiTrait : Trait
{
    [Export]
    public string UniquenessCategory;

    [Export]
    public float WanderChance;

    [Export]
    public float AttackChance;

    [Export]
    public float FleeChance;

    [Export]
    public float DevelopChance;

    [Export]
    public float TargetSizeIncrease;

    [Export]
    public float SplitsWhenFull;
}
