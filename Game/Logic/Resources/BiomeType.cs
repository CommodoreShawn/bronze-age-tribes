using Godot;

public class BiomeType : Resource
{
    [Export]
    public string Id;

    [Export]
    public string Name;

    [Export]
    public Color GroundColor;

    [Export]
    public float ColorStrength;

    [Export]
    public float CellFeatureChance;

    [Export]
    public CellFeatureType[] CellFeatures;

    [Export]
    public float TerrainNoiseFactor;

    [Export]
    public float TerrainHeightBoost;

    [Export]
    public Color WaterColor;

    [Export]
    public float HeightTransitionHeight;

    [Export]
    public BiomeType HeightTransitionsTo;

    [Export]
    public BiomeType RiverUpgradesTo;

    [Export]
    public BiomeType DeltaUpgradesTo;

    [Export]
    public float RegionSizeFactor = 1;

    [Export]
    public TerrainTrait[] Traits;

    [Export]
    public Texture PopBackground;

    [Export]
    public Texture CityBackground;

    [Export]
    public Texture MapHex;

    [Export]
    public PopJobType BaseJob;

    [Export]
    public float FoodWhenWorked;

    [Export]
    public float ProductionWhenWorked;

    [Export]
    public float LuxuriesWhenWorked;

    [Export]
    public float TradeWhenWorked;

    [Export]
    public float WealthWhenWorked;

    [Export]
    public float LegitimacyWhenWorked;

    [Export]
    public float RiverCellFeatureChance;

    [Export]
    public CellFeatureType[] RiverCellFeatures;
}
