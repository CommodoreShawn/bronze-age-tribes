﻿using BronzeAgeTribes.Logic;
using BronzeAgeTribes.Logic.Data;
using BronzeAgeTribes.Logic.Data.Serialization;
using BronzeAgeTribes.Logic.Infrastructure;
using BronzeAgeTribes.Logic.Infrastructure.Interfaces;
using Godot;
using System;
using System.Collections.Generic;
using System.Linq;

public class UrbanDevelopmentType : DevelopmentType
{
    [Export]
    public bool KeepCenterClear;

    [Export]
    public float BorderPrefabChance;

    [Export]
    public TerrainTrait[] BorderPrefabTraits;

    [Export]
    public PackedScene[] BorderPrefabOptions;

    [Export]
    public FeaturePrefabPlacementMode BorderPrefabPlacementMode;

    private Vector2[] _centerPoints;
    private Vector2[][] _borderPoints;

    public UrbanDevelopmentType()
    {
        _centerPoints = new Vector2[]
        {
            new Vector2(0,0),
            new Vector2(0,-1),
            new Vector2(1,0),
            new Vector2(1,-1),
            new Vector2(-1,0),
            new Vector2(-1,-1),
        };

        _borderPoints = new[]
        {
            new [] { new Vector2(1, -3), new Vector2(2, -2) },
            new [] { new Vector2(3, -1), new Vector2(3, 0) },
            new [] { new Vector2(1, 2), new Vector2(2, 1) },
            new [] { new Vector2(-1, 2), new Vector2(-2, 1) },
            new [] { new Vector2(-3, -1), new Vector2(-3, 0) },
            new [] { new Vector2(-1, -3), new Vector2(-2, -2) }
        };
    }

    public override Development MakeDevelopment(
        WorldHex hex, 
        bool isConstructed, 
        BuildingStyle buildingStyle)
    {
        return new UrbanDevelopment(
            this,
            hex,
            isConstructed,
            buildingStyle);
    }

    public override void WriteTo(SerializedObject developmentRoot, Development development)
    {
        base.WriteTo(developmentRoot, development);

        if (development is UrbanDevelopment urbanDevelopment
            && urbanDevelopment.Building != null)
        {
            urbanDevelopment.Building.WriteTo(developmentRoot.CreateChild("building_root"));
        }
    }

    protected override Development Deserialize(
        SerializedObject developmentRoot, 
        WorldHex hex, 
        IResourceTracker resourceTracker)
    {
        var currentInteraction = developmentRoot.GetOptionalObjectReference(
                "current_interaction",
                InteractionOptions.Concat(ConstructionInteraction.Yield()),
                x => x.Id);

        var development = new UrbanDevelopment(
            this,
            hex,
            developmentRoot.GetBool("is_constructed"),
            developmentRoot.GetObjectReference(
                "building_style",
                resourceTracker.BuildingStyles,
                x => x.Id))
        {
            ConstructProgress = developmentRoot.GetFloat("construct_progress"),
        };

        var buildingRoot = developmentRoot.GetOptionalChild("building_root");
        if(buildingRoot != null)
        {
            development.Building = BuildingType.ReadFrom(buildingRoot, development, resourceTracker);
        }

        development.SetCurrentInteraction(currentInteraction);

        return development;
    }

    public override void MakeMapRepresentation(
        Spatial container,
        PackedScene cellFeatureRoot,
        WorldHex hex,
        List<Vector3> hexInfluencePoints,
        float[,] landHeightMap,
        float[,] waterHeightMap,
        Random random,
        BuildingStyle buildingStyle,
        Development development,
        Action<Spatial, float, PackedScene, float, float, float, float> addFeaturePrefab)
    {
        var root = cellFeatureRoot.Instance() as Spatial;

        root.Transform = container.Transform.Translated(
            new Vector3(
                hex.Position.RealX,
                0,
                hex.Position.RealY));
        container.AddChild(root);

        var rootLod0 = root.GetNode<Spatial>("-lod0");
        var rootLod1 = root.GetNode<Spatial>("-lod1");
        var rootLod2 = root.GetNode<Spatial>("-lod2");

        var borderPoints = new List<Vector3>();

        if (KeepCenterClear)
        {
            hexInfluencePoints = hexInfluencePoints
                .Where(p => !_centerPoints.Any(c => c.x == p.x && c.y == p.y))
                .ToList();
        }

        if (BorderPrefabOptions.Any())
        {
            for (var i = 0; i < 6; i++)
            {
                if (hex.Neighbors[i] != null && hex.Neighbors[i].TerrainTraits.ContainsAll(BorderPrefabTraits))
                {
                    var points = _borderPoints[i];

                    hexInfluencePoints = hexInfluencePoints
                        .Where(p => !points.Any(c => c.x == p.x && c.y == p.y))
                        .ToList();

                    MakeBorderPrefabs(
                        rootLod0,
                        rootLod1,
                        rootLod2,
                        hex,
                        points,
                        landHeightMap,
                        waterHeightMap,
                        random,
                        (HexFacing)i,
                        addFeaturePrefab);
                }
            }
        }

        MakeCenterRepresentation(
            rootLod0,
            rootLod1,
            rootLod2,
            hex,
            hexInfluencePoints,
            landHeightMap,
            waterHeightMap,
            random,
            buildingStyle,
            addFeaturePrefab);

        if(development is UrbanDevelopment urbanDevelopment 
            && urbanDevelopment.Building != null
            && urbanDevelopment.Building.IsConstructed)
        {
            urbanDevelopment.Building.BackingType
                .MakeMapRepresentation(
                    rootLod0,
                    rootLod1,
                    rootLod2,
                    hex,
                    landHeightMap,
                    waterHeightMap,
                    random,
                    urbanDevelopment.Building.BuildingStyle,
                    addFeaturePrefab);
        }
    }

    private void MakeCenterRepresentation(
        Spatial rootLod0,
        Spatial rootLod1,
        Spatial rootLod2,
        WorldHex hex,
        List<Vector3> hexInfluencePoints,
        float[,] landHeightMap,
        float[,] waterHeightMap,
        Random random,
        BuildingStyle buildingStyle,
        Action<Spatial, float, PackedScene, float, float, float, float> addFeaturePrefab)
    {
        var mapPrefabOptions = MapPrefabOptions;

        if (UseBuildingStyleBuildings)
        {
            mapPrefabOptions = mapPrefabOptions
                .Concat(buildingStyle.StandardMapBuildingPrefabs)
                .ToArray();
        }

        if (mapPrefabOptions.Any())
        {
            var prefabPoints = new List<Vector3>();

            foreach (var point in hexInfluencePoints)
            {
                var adjustedChance = point.z * PrefabChancePerVertex;

                var isCrowded = PreventAdjacentPrefabs
                    && prefabPoints.Any(v => Math.Abs(v.x - point.x) <= 1 && Math.Abs(v.y - point.y) <= 1);

                var vertexX = (int)(hex.Position.RealX + point.x);
                var vertexZ = (int)(hex.Position.RealY + point.y);

                var inWater = landHeightMap[vertexX, vertexZ] <= waterHeightMap[vertexX, vertexZ];

                var canPlace = (inWater && PrefabPlacementMode != FeaturePrefabPlacementMode.OnLand)
                    || (!inWater && PrefabPlacementMode != FeaturePrefabPlacementMode.OnWater);

                if (random.NextDouble() < adjustedChance && !isCrowded && canPlace)
                {
                    prefabPoints.Add(point);


                    var packedPrefab = random.Choose(mapPrefabOptions);

                    var facing = 0f;

                    if (RotatePrefabs)
                    {
                        facing = (float)random.NextDouble() * 2 * Mathf.Pi;
                    }

                    var height = landHeightMap[vertexX, vertexZ];

                    if (PrefabPlacementMode == FeaturePrefabPlacementMode.WaterSurface)
                    {
                        height = waterHeightMap[vertexX, vertexZ];
                    }

                    addFeaturePrefab(rootLod0, 1, packedPrefab, point.x, height, point.y, facing);

                    if (random.NextDouble() < 0.5)
                    {
                        addFeaturePrefab(rootLod1, 1.5f, packedPrefab, point.x, height, point.y, facing);

                        if (random.NextDouble() < 0.5)
                        {
                            addFeaturePrefab(rootLod2, 2, packedPrefab, point.x, height, point.y, facing);
                        }
                    }
                }
            }
        }
    }

    private void MakeBorderPrefabs(
        Spatial rootLod0,
        Spatial rootLod1,
        Spatial rootLod2,
        WorldHex hex,
        Vector2[] borderPoints,
        float[,] landHeightMap,
        float[,] waterHeightMap,
        Random random,
        HexFacing facingToNeighbor,
        Action<Spatial, float, PackedScene, float, float, float, float> addFeaturePrefab)
    {
        var prefabPoints = new List<Vector2>();

        var prefabOptions = BorderPrefabOptions;
        
        foreach (var point in borderPoints)
        {
            var vertexX = (int)(hex.Position.RealX + point.x);
            var vertexZ = (int)(hex.Position.RealY + point.y);

            var isCrowded = PreventAdjacentPrefabs
                && prefabPoints.Any(v => Math.Abs(v.x - point.x) <= 1 && Math.Abs(v.y - point.y) <= 1);

            var inWater = landHeightMap[vertexX, vertexZ] <= waterHeightMap[vertexX, vertexZ];

            var canPlace = (inWater && BorderPrefabPlacementMode != FeaturePrefabPlacementMode.OnLand)
                || (!inWater && BorderPrefabPlacementMode != FeaturePrefabPlacementMode.OnWater);

            if (random.NextDouble() < BorderPrefabChance && !isCrowded && canPlace)
            {
                prefabPoints.Add(point);

                var packedPrefab = random.Choose(BorderPrefabOptions);

                var facing = Util.FacingToRadians(facingToNeighbor);

                var height = landHeightMap[vertexX, vertexZ];

                if (BorderPrefabPlacementMode == FeaturePrefabPlacementMode.WaterSurface)
                {
                    height = waterHeightMap[vertexX, vertexZ];
                }

                addFeaturePrefab(rootLod0, 1, packedPrefab, point.x, height, point.y, facing);

                if (random.NextDouble() < 0.5)
                {
                    addFeaturePrefab(rootLod1, 1.5f, packedPrefab, point.x, height, point.y, facing);

                    if (random.NextDouble() < 0.5)
                    {
                        addFeaturePrefab(rootLod2, 2, packedPrefab, point.x, height, point.y, facing);
                    }
                }
            }
        }
    }
}