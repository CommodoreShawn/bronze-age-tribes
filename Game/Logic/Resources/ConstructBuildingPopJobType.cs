﻿using BronzeAgeTribes.Logic.Data;
using BronzeAgeTribes.Logic.Data.Game;

public class ConstructBuildingPopJobType : PopJobType
{
    public override PopJob MakeJob(
        WorldHex location,
        Building building,
        InteractionOption interactionOption)
    {
        return new ConstructBuildingPopJob(location, this, building, interactionOption);
    }

    public override int GetTurnsRemaining(int baseTurns, WorldHex hex, Building building)
    {
        return (int)(baseTurns * (1 - building.ConstructProgress));
    }
}
