﻿using Godot;

public class CreepSpawn : Resource
{
    [Export]
    public string Id;

    [Export]
    public BiomeType[] BiomeTypes;

    [Export]
    public bool CanSpawnInRivers;

    [Export]
    public StartingCulture Culture;

    [Export]
    public float Likelyhood;

    [Export]
    public int StartingPops;

    [Export]
    public int TargetSize;

    [Export]
    public AiTrait[] MandatoryAiTraits;

    [Export]
    public int OptionalTraitPicks;

    [Export]
    public AiTrait[] OptionalAiTraits;
}
