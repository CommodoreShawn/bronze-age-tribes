﻿using Godot;

public class CellFeatureType : Resource
{
    [Export]
    public string Id;

    [Export]
    public string Name;

    [Export(PropertyHint.MultilineText)]
    public string Description;

    [Export]
    public float PrefabChancePerVertex;

    [Export]
    public FeaturePrefabPlacementMode PrefabPlacementMode;

    [Export]
    public PackedScene[] MapPrefabs;

    [Export]
    public TerrainTrait[] Traits;
    
    [Export]
    public Texture PopBackground;

    [Export]
    public Texture InteractButtonIcon;

    [Export]
    public float FoodWhenWorked;

    [Export]
    public float ProductionWhenWorked;

    [Export]
    public float LuxuriesWhenWorked;

    [Export]
    public float TradeWhenWorked;

    [Export]
    public float WealthWhenWorked;

    [Export]
    public float LegitimacyWhenWorked;

    [Export]
    public InteractionOption[] InteractionOptions;

    [Export]
    public bool ClearedByDevelopment;

    public float GetExpectedOutput(ResourceType resource)
    {
        switch (resource)
        {
            case ResourceType.Food:
                return FoodWhenWorked;
            case ResourceType.Production:
                return ProductionWhenWorked;
            case ResourceType.Luxuries:
                return LuxuriesWhenWorked;
            case ResourceType.Trade:
                return TradeWhenWorked;
            case ResourceType.Wealth:
                return WealthWhenWorked;
            case ResourceType.Legitimacy:
                return LegitimacyWhenWorked;
        }

        return 0;
    }
}