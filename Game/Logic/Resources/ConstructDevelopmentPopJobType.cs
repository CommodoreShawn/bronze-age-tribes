﻿using BronzeAgeTribes.Logic.Data;
using BronzeAgeTribes.Logic.Data.Game;

public class ConstructDevelopmentPopJobType : PopJobType
{
    public override PopJob MakeJob(
        WorldHex location,
        Development development,
        InteractionOption interactionOption)
    {
        return new ConstructDevelopmentPopJob(location, this, development, interactionOption);
    }

    public override int GetTurnsRemaining(int baseTurns, WorldHex hex, Development development)
    {
        return (int)(baseTurns * (1 - development.ConstructProgress));
    }
}
