﻿using Godot;

public class BuildingPrefabOption : Resource
{
    [Export]
    public string Key;

    [Export]
    public PackedScene Prefab;
}