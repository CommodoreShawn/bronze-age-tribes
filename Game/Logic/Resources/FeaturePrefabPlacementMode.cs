﻿public enum FeaturePrefabPlacementMode
{
    OnLand,
    OnWater,
    LandOrWater,
    WaterSurface
}