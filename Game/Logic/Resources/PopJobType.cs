﻿using BronzeAgeTribes.Logic.Data;
using BronzeAgeTribes.Logic.Data.Game;
using Godot;
using System;

public class PopJobType : Resource
{
    [Export]
    public JobCategory Category;

    [Export]
    public string Title;

    [Export]
    public float BaseFoodFactor;

    [Export]
    public float BaseProductionFactor;

    [Export]
    public float BaseLuxuriesFactor;

    [Export]
    public float BaseTradeFactor;

    [Export]
    public float BaseWealthFactor;

    [Export]
    public float BaseLegitimacyFactor;

    [Export]
    public float FoodFromJob;

    [Export]
    public float ProductionFromJob;

    [Export]
    public float LuxuriesFromJob;

    [Export]
    public float TradeFromJob;

    [Export]
    public float WealthFromJob;

    [Export]
    public float LegitimacyFromJob;

    public virtual PopJob MakeJob(
        WorldHex location, 
        CellFeature cellFeature, 
        InteractionOption interactionOption)
    {
        return new PopJob(location, this);
    }

    public virtual PopJob MakeJob(
        WorldHex location,
        Development development,
        InteractionOption interactionOption)
    {
        return new PopJob(location, this);
    }

    public virtual PopJob MakeJob(
        WorldHex location,
        Building building,
        InteractionOption interactionOption)
    {
        return new PopJob(location, this);
    }

    public virtual PopJob MakeJob(
        WorldHex location,
        Terrain terrain,
        InteractionOption interactionOption)
    {
        return new PopJob(location, this);
    }

    public virtual PopJob MakeJob(
        WorldHex location,
        Development development)
    {
        return new PopJob(location, this);
    }

    public virtual PopJob MakeJob(
        WorldHex location,
        Building building)
    {
        return new PopJob(location, this);
    }

    public float GetExpectedOutput(ResourceType resource, WorldHex hex)
    {
        switch (resource)
        {
            case ResourceType.Food:
                return BaseFoodFactor * hex.GetBaseOutput(resource) + FoodFromJob;
            case ResourceType.Production:
                return BaseProductionFactor * hex.GetBaseOutput(resource) + ProductionFromJob;
            case ResourceType.Luxuries:
                return BaseLuxuriesFactor * hex.GetBaseOutput(resource) + LuxuriesFromJob;
            case ResourceType.Trade:
                return BaseTradeFactor * hex.GetBaseOutput(resource) + TradeFromJob;
            case ResourceType.Wealth:
                return BaseWealthFactor * hex.GetBaseOutput(resource) + WealthFromJob;
            case ResourceType.Legitimacy:
                return BaseLegitimacyFactor * hex.GetBaseOutput(resource) + LegitimacyFromJob;
        }

        return 0;
    }

    public float GetExpectedOutputWithDevelopment(ResourceType resource, WorldHex hex, DevelopmentType developmentType)
    {
        switch (resource)
        {
            case ResourceType.Food:
                return BaseFoodFactor * (hex.GetBaseOutput(resource) + developmentType.GetExpectedOutput(resource)) + FoodFromJob;
            case ResourceType.Production:
                return BaseProductionFactor * (hex.GetBaseOutput(resource) + developmentType.GetExpectedOutput(resource)) + ProductionFromJob;
            case ResourceType.Luxuries:
                return BaseLuxuriesFactor * (hex.GetBaseOutput(resource) + developmentType.GetExpectedOutput(resource)) + LuxuriesFromJob;
            case ResourceType.Trade:
                return BaseTradeFactor * (hex.GetBaseOutput(resource) + developmentType.GetExpectedOutput(resource)) + TradeFromJob;
            case ResourceType.Wealth:
                return BaseWealthFactor * (hex.GetBaseOutput(resource) + developmentType.GetExpectedOutput(resource)) + WealthFromJob;
            case ResourceType.Legitimacy:
                return BaseLegitimacyFactor * (hex.GetBaseOutput(resource) + developmentType.GetExpectedOutput(resource)) + LegitimacyFromJob;
        }

        return 0;
    }

    public float GetExpectedOutputWithBuilding(ResourceType resource, WorldHex hex, BuildingType buildingType)
    {
        switch (resource)
        {
            case ResourceType.Food:
                return BaseFoodFactor * (hex.GetBaseOutput(resource) + buildingType.GetExpectedOutput(resource)) + FoodFromJob;
            case ResourceType.Production:
                return BaseProductionFactor * (hex.GetBaseOutput(resource) + buildingType.GetExpectedOutput(resource)) + ProductionFromJob;
            case ResourceType.Luxuries:
                return BaseLuxuriesFactor * (hex.GetBaseOutput(resource) + buildingType.GetExpectedOutput(resource)) + LuxuriesFromJob;
            case ResourceType.Trade:
                return BaseTradeFactor * (hex.GetBaseOutput(resource) + buildingType.GetExpectedOutput(resource)) + TradeFromJob;
            case ResourceType.Wealth:
                return BaseWealthFactor * (hex.GetBaseOutput(resource) + buildingType.GetExpectedOutput(resource)) + WealthFromJob;
            case ResourceType.Legitimacy:
                return BaseLegitimacyFactor * (hex.GetBaseOutput(resource) + buildingType.GetExpectedOutput(resource)) + LegitimacyFromJob;
        }

        return 0;
    }

    public virtual int GetTurnsRemaining(int baseTurns, WorldHex hex, CellFeature feature)
    {
        return baseTurns;
    }

    public virtual int GetTurnsRemaining(int baseTurns, WorldHex hex, Development development)
    {
        return baseTurns;
    }

    public virtual int GetTurnsRemaining(int baseTurns, WorldHex hex, Building building)
    {
        return baseTurns;
    }
}
