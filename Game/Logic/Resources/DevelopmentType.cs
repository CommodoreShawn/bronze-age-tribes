﻿using BronzeAgeTribes.Logic.Data;
using BronzeAgeTribes.Logic.Data.Serialization;
using BronzeAgeTribes.Logic.Infrastructure;
using BronzeAgeTribes.Logic.Infrastructure.Interfaces;
using Godot;
using System;
using System.Collections.Generic;
using System.Linq;

public class DevelopmentType : Resource
{
    [Export]
    public string Id;

    [Export]
    public string Name;

    [Export(PropertyHint.MultilineText)]
    public string Description;

    [Export]
    public PackedScene[] MapPrefabOptions;

    [Export]
    public bool UseBuildingStyleBuildings;

    [Export]
    public float PrefabChancePerVertex;

    [Export]
    public bool PreventAdjacentPrefabs;

    [Export]
    public FeaturePrefabPlacementMode PrefabPlacementMode;

    [Export]
    public bool RotatePrefabs;

    [Export]
    public TerrainTrait[] Traits;

    [Export]
    public Texture PopBackground;

    [Export]
    public Texture InteractButtonIcon;

    [Export]
    public float FoodWhenWorked;

    [Export]
    public float ProductionWhenWorked;

    [Export]
    public float LuxuriesWhenWorked;

    [Export]
    public float TradeWhenWorked;

    [Export]
    public float WealthWhenWorked;

    [Export]
    public float LegitimacyWhenWorked;

    [Export]
    public InteractionOption ConstructionInteraction;

    [Export]
    public PopJobType[] ProvidedJobs;

    [Export]
    public TerrainTrait[] RequiredTileTraits;

    [Export]
    public TerrainTrait[] RequiredAdjacentTileTraits;

    [Export]
    public TerrainTrait[] ForbiddenTileTraits;

    [Export]
    public TerrainTrait[] RequiredEmpireTraits;

    [Export]
    public InteractionOption[] InteractionOptions;

    [Export]
    public bool OnlySettlement;

    public float GetExpectedOutput(ResourceType resource)
    {
        switch (resource)
        {
            case ResourceType.Food:
                return FoodWhenWorked;
            case ResourceType.Production:
                return ProductionWhenWorked;
            case ResourceType.Luxuries:
                return LuxuriesWhenWorked;
            case ResourceType.Trade:
                return TradeWhenWorked;
            case ResourceType.Wealth:
                return WealthWhenWorked;
            case ResourceType.Legitimacy:
                return LegitimacyWhenWorked;
        }

        return 0;
    }

    public virtual void MakeMapRepresentation(
        Spatial container,
        PackedScene cellFeatureRoot,
        WorldHex hex,
        List<Vector3> hexInfluencePoints,
        float[,] landHeightMap,
        float[,] waterHeightMap,
        Random random,
        BuildingStyle buildingStyle,
        Development development,
        Action<Spatial, float, PackedScene, float, float, float, float> addFeaturePrefab)
    {
        var mapPrefabOptions = MapPrefabOptions;

        if (UseBuildingStyleBuildings)
        {
            mapPrefabOptions = mapPrefabOptions
                .Concat(buildingStyle.StandardMapBuildingPrefabs)
                .ToArray();
        }

        if (mapPrefabOptions.Any())
        {
            var prefabPoints = new List<Vector3>();

            var root = cellFeatureRoot.Instance() as Spatial;

            root.Transform = container.Transform.Translated(
                new Vector3(
                    hex.Position.RealX,
                    0,
                    hex.Position.RealY));
            container.AddChild(root);

            var rootLod0 = root.GetNode<Spatial>("-lod0");
            var rootLod1 = root.GetNode<Spatial>("-lod1");
            var rootLod2 = root.GetNode<Spatial>("-lod2");

            foreach (var point in hexInfluencePoints)
            {
                var adjustedChance = point.z * PrefabChancePerVertex;

                var isCrowded = PreventAdjacentPrefabs
                    && prefabPoints.Any(v => Math.Abs(v.x - point.x) <= 1 && Math.Abs(v.y - point.y) <= 1);

                var vertexX = (int)(hex.Position.RealX + point.x);
                var vertexZ = (int)(hex.Position.RealY + point.y);

                var inWater = landHeightMap[vertexX, vertexZ] <= waterHeightMap[vertexX, vertexZ];

                var canPlace = (inWater && PrefabPlacementMode != FeaturePrefabPlacementMode.OnLand)
                    || (!inWater && PrefabPlacementMode != FeaturePrefabPlacementMode.OnWater);

                if (random.NextDouble() < adjustedChance && !isCrowded && canPlace)
                {
                    prefabPoints.Add(point);
                    

                    var packedPrefab = random.Choose(mapPrefabOptions);

                    var facing = 0f;

                    if(RotatePrefabs)
                    {
                        facing = (float)random.NextDouble() * 2 * Mathf.Pi;
                    }

                    var height = landHeightMap[vertexX, vertexZ];

                    if(PrefabPlacementMode == FeaturePrefabPlacementMode.WaterSurface)
                    {
                        height = waterHeightMap[vertexX, vertexZ];
                    }

                    addFeaturePrefab(rootLod0, 1, packedPrefab, point.x, height, point.y, facing);

                    if (random.NextDouble() < 0.5)
                    {
                        addFeaturePrefab(rootLod1, 1.5f, packedPrefab, point.x, height, point.y, facing);

                        if (random.NextDouble() < 0.5)
                        {
                            addFeaturePrefab(rootLod2, 2, packedPrefab, point.x, height, point.y, facing);
                        }
                    }
                }
            }
        }
    }

    public virtual Development MakeDevelopment(
        WorldHex hex, 
        bool isConstructed, 
        BuildingStyle buildingStyle)
    {
        return new Development(
            this,
            hex,
            isConstructed,
            buildingStyle);
    }

    public virtual void WriteTo(SerializedObject developmentRoot, Development development)
    {
        developmentRoot.Set("development_id", Id);
        developmentRoot.Set("current_interaction", development.CurrentInteraction?.Id);
        developmentRoot.Set("construct_progress", development.ConstructProgress);
        developmentRoot.Set("is_constructed", development.IsConstructed);
        developmentRoot.Set("building_style", development.BuildingStyle.Id);
    }

    protected virtual Development Deserialize(
        SerializedObject developmentRoot,
        WorldHex hex,
        IResourceTracker resourceTracker)
    {
        var currentInteraction = developmentRoot.GetOptionalObjectReference(
                "current_interaction",
                InteractionOptions.Concat(ConstructionInteraction.Yield()),
                x => x.Id);

        var development = new Development(
            this,
            hex,
            developmentRoot.GetBool("is_constructed"),
            developmentRoot.GetObjectReference(
                "building_style",
                resourceTracker.BuildingStyles,
                x => x.Id))
        {
            ConstructProgress = developmentRoot.GetFloat("construct_progress"),
        };

        development.SetCurrentInteraction(currentInteraction);

        return development;
    }

    public static Development ReadFrom(SerializedObject developmentRoot, WorldHex hex, IResourceTracker resourceTracker)
    {
        var developmentType = developmentRoot.GetObjectReference(
            "development_id",
            resourceTracker.DevelopmentTypes,
            x => x.Id);

        return developmentType.Deserialize(developmentRoot, hex, resourceTracker);
    }
}