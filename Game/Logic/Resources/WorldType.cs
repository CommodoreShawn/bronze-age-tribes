using Godot;

public class WorldType : Resource
{
    [Export]
    public string FriendlyName;

    [Export(PropertyHint.MultilineText)]
    public string Description;

    [Export]
    public int WorldRadius = 64;

    [Export]
    public int TargetRegionSize = 15;

    [Export]
    public CellFeatureType RiverCellFeature;

    [Export]
    public float RiverAdditionalFeatureChance;

    [Export]
    public CellFeatureType[] RiverAdditionalFeatures;

    [Export]
    public TerrainType[] RainSources;

    [Export]
    public float RainDistanceScale = 16;

    [Export]
    public TerrainType[] TerrainTypes;

    [Export]
    public BiomeMapping[] BiomeMappings;
    
    [Export]
    public CreepSpawn[] Creeps;
}
