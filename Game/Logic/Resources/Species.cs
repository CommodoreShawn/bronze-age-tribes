﻿using BronzeAgeTribes.Logic.Data.Game;
using Godot;
using System;

public class Species : Resource
{
    [Export]
    public string Id;

    [Export]
    public string Name;

    [Export]
    public MovementType MovementType;

    [Export]
    public Texture PopIcon;

    [Export]
    public Texture PopIconSmall;

    [Export]
    public Texture MovingIcon;

    [Export]
    public Texture MoveGhost;

    [Export]
    public Texture ForageIcon;
    
    [Export]
    public Texture FarmIcon;

    [Export]
    public Texture CraftIcon;

    [Export]
    public Texture MineIcon;

    [Export]
    public Texture WoodcutIcon;

    [Export]
    public Texture HuntIcon;

    [Export]
    public Texture GiftIcon;
    
    [Export]
    public Texture PriestIcon;

    [Export]
    public Texture BuildIcon;

    [Export]
    public float GrowthRate;

    [Export]
    public float FoodUpkeep;

    [Export]
    public float ProductionUpkeep;

    [Export]
    public float LuxuriesUpkeep;

    [Export]
    public float TradeUpkeep;

    [Export]
    public float WealthUpkeep;

    [Export]
    public float LegitimacyUpkeep;

    [Export]
    public int PopMovementDistance;

    [Export]
    public TerrainTrait[] LosReducedBy;

    [Export]
    public int PopVisionDistance;

    [Export]
    public bool CanBeAssimilated;

    [Export]
    public bool CanBeDomesticated;

    [Export]
    public Trait[] Traits;

    public Texture GetJobIcon(JobCategory jobCategory)
    {
        switch (jobCategory)
        {
            case JobCategory.Crafting:
                return CraftIcon ?? PopIcon;
            case JobCategory.Farming:
                return FarmIcon ?? PopIcon;
            case JobCategory.Foraging:
                return ForageIcon ?? PopIcon;
            case JobCategory.Mining:
                return MineIcon ?? PopIcon;
            case JobCategory.Woodcutting:
                return WoodcutIcon ?? PopIcon;
            case JobCategory.Hunting:
                return HuntIcon ?? PopIcon;
            case JobCategory.Building:
                return BuildIcon ?? PopIcon;
            case JobCategory.Preaching:
                return PriestIcon ?? PopIcon;
            default:
                return PopIcon;
        }
    }

    public float GetUpkeep(ResourceType resource)
    {
        switch (resource)
        {
            case ResourceType.Food:
                return FoodUpkeep;
            case ResourceType.Production:
                return ProductionUpkeep;
            case ResourceType.Luxuries:
                return LuxuriesUpkeep;
            case ResourceType.Trade:
                return TradeUpkeep;
            case ResourceType.Wealth:
                return WealthUpkeep;
            case ResourceType.Legitimacy:
                return LegitimacyUpkeep;
        }

        return 0;
    }

    public Texture GetInteractionIcon(PopInteractionType interactionType)
    {
        switch(interactionType)
        {
            case PopInteractionType.Domesticate:
            case PopInteractionType.Assimilate:
                return GiftIcon ?? PopIcon;
            case PopInteractionType.Attack:
                return HuntIcon ?? PopIcon;
            case PopInteractionType.Expel:
                return HuntIcon ?? PopIcon;
            default:
                return PopIcon;
        }
    }
}
