﻿using Godot;

namespace BronzeAgeTribes.Logic.Resources
{
    public class JobBonus : Resource
    {
        [Export]
        public JobCategory Category;

        [Export]
        public float Factor;
    }
}
