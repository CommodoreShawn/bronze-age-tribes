using Godot;
using System.Collections.Generic;
using System.Linq;

public class BiomeMapping : Resource
{
    [Export]
    public float MinHeight;

    [Export]
    public float MaxHeight;

    [Export]
    public float MinTemperature;

    [Export]
    public float MaxTemperature;

    [Export]
    public float MinRadius;

    [Export]
    public float MaxRadius;

    [Export]
    public float IdealMoisture = -1;
    
    [Export]
    public BiomeType Biome;

    public float Score(float rainfall, float drainage, float temperature, float terrainHeight, float radius)
    {
        if(MinHeight <= terrainHeight && terrainHeight <= MaxHeight
            && MinTemperature <= temperature && temperature <= MaxTemperature
            && MinRadius <= radius && radius <= MaxRadius)
        {
            var moisture = rainfall - (drainage / 4);

            var scores = new List<float>();
            if (IdealMoisture >= 0)
            {
                scores.Add(Mathf.Abs(IdealMoisture - moisture));
            }
            
            if(scores.Any())
            {
                return scores.Average();
            }
            else
            {
                return 0;
            }
        }

        return float.MaxValue;
    }
}
