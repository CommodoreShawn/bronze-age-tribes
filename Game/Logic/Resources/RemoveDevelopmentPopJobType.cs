﻿using BronzeAgeTribes.Logic.Data;
using BronzeAgeTribes.Logic.Data.Game;

public class RemoveDevelopmentPopJobType : PopJobType
{
    public override PopJob MakeJob(
        WorldHex location,
        Development development,
        InteractionOption interactionOption)
    {
        return new RemoveDevelopmentPopJob(location, this, development, interactionOption);
    }

    public override int GetTurnsRemaining(int baseTurns, WorldHex hex, Development development)
    {
        return 1;
    }
}
