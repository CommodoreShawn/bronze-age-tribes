﻿using BronzeAgeTribes.Logic.Data;
using BronzeAgeTribes.Logic.Data.Game;

public class RemoveBuildingPopJobType : PopJobType
{
    public override PopJob MakeJob(
        WorldHex location,
        Building building,
        InteractionOption interactionOption)
    {
        return new RemoveBuildingPopJob(location, this, building, interactionOption);
    }

    public override int GetTurnsRemaining(int baseTurns, WorldHex hex, Building building)
    {
        return 1;
    }
}
