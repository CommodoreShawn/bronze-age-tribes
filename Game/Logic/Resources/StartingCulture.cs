﻿using BronzeAgeTribes.Logic.Data;
using BronzeAgeTribes.Logic.Data.Game;
using Godot;

public class StartingCulture : Resource
{
    [Export]
    public string Id;

    [Export]
    public string Name;

    [Export]
    public string NamingContext;

    [Export(PropertyHint.MultilineText)]
    public string Description;

    [Export]
    public CultureTrait[] Traits;

    [Export]
    public BiomeType[] PreferredBiomes;

    [Export]
    public BiomeType[] ForbiddenBiomes;

    [Export]
    public Species StartingSpecies;

    [Export]
    public BuildingStyle BuildingStyle;

    public Culture MakeCulture()
    {
        return new Culture(
            Id,
            Name,
            null,
            Traits,
            NamingContext,
            BuildingStyle);
    }
}
