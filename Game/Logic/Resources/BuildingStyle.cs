using BronzeAgeTribes.Logic.Data.Game;
using Godot;
using System.Linq;

public class BuildingStyle : Resource
{
    [Export]
    public string Id;

    [Export]
    public Texture TribeBackground;

    [Export]
    public Texture SettlementBackground;

    [Export]
    public PackedScene[] StandardMapBuildingPrefabs;

    [Export]
    public BuildingPrefabOption[] BuildingPrefabOptions = new BuildingPrefabOption[0];

    public PackedScene GetBuildingPrefab(string buildingPrefabType)
    {
        var option = BuildingPrefabOptions
            .Where(p => p.Key == buildingPrefabType)
            .Select(p => p.Prefab)
            .FirstOrDefault();

        if(option == null)
        {
            option = BuildingPrefabOptions
                .Where(p => buildingPrefabType.StartsWith(p.Key))
                .Select(p => p.Prefab)
                .FirstOrDefault();
        }

        return option;
    }
}
