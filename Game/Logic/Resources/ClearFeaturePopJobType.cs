﻿using BronzeAgeTribes.Logic.Data;
using BronzeAgeTribes.Logic.Data.Game;

public class ClearFeaturePopJobType : PopJobType
{
    public override PopJob MakeJob(
        WorldHex location,
        CellFeature cellFeature,
        InteractionOption interactionOption)
    {
        return new ClearFeaturePopJob(location, this, cellFeature, interactionOption);
    }

    public override int GetTurnsRemaining(int baseTurns, WorldHex hex, CellFeature feature)
    {
        return (int)(baseTurns * (1 - feature.ClearProgress));
    }
}
