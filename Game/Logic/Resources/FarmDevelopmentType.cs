﻿using BronzeAgeTribes.Logic.Data;
using BronzeAgeTribes.Logic.Infrastructure;
using Godot;
using System;
using System.Collections.Generic;
using System.Linq;

public class FarmDevelopmentType : DevelopmentType
{
    [Export]
    public SpatialMaterial DirtMaterial;

    [Export]
    public float RowWidth;

    [Export]
    public float RowSlopeWidth;

    [Export]
    public float RowHeight;

    public override void MakeMapRepresentation(
        Spatial container,
        PackedScene cellFeatureRoot,
        WorldHex hex,
        List<Vector3> hexInfluencePoints,
        float[,] landHeightMap,
        float[,] waterHeightMap,
        Random random,
        BuildingStyle buildingStyle,
        Development development,
        Action<Spatial, float, PackedScene, float, float, float, float> addFeaturePrefab)
    {
        base.MakeMapRepresentation(
            container,
            cellFeatureRoot,
            hex,
            hexInfluencePoints,
            landHeightMap,
            waterHeightMap,
            random,
            buildingStyle,
            development,
            addFeaturePrefab);

        var surfaceTool = new SurfaceTool();
        surfaceTool.Begin(Mesh.PrimitiveType.Triangles);

        var startVertOffsets = new Vector3[]
        {
            new Vector3(-RowSlopeWidth, -RowHeight / 2, -RowWidth / 2),
            new Vector3(             0, -RowHeight / 2, -RowWidth / 2 - RowSlopeWidth),
            new Vector3(             0,  RowHeight / 2, -RowWidth / 2),
            new Vector3(-RowSlopeWidth, -RowHeight / 2,  RowWidth / 2),
            new Vector3(             0,  RowHeight / 2,  RowWidth / 2),
            new Vector3(             0, -RowHeight / 2,  RowWidth / 2 + RowSlopeWidth),
        };

        var midVertOffsets = new Vector3[]
        {
            new Vector3(0, -RowHeight / 2, -RowWidth / 2 - RowSlopeWidth),
            new Vector3(0,  RowHeight / 2,  -RowWidth / 2),
            new Vector3(0,  RowHeight / 2,   RowWidth / 2),
            new Vector3(0, -RowHeight / 2,   RowWidth / 2 + RowSlopeWidth),
        };

        var endVertOffsets = new Vector3[]
        {
            new Vector3(RowSlopeWidth, -RowHeight / 2, -RowWidth / 2),
            new Vector3(            0, -RowHeight / 2, -RowWidth / 2 - RowSlopeWidth),
            new Vector3(            0,  RowHeight / 2, -RowWidth / 2),
            new Vector3(RowSlopeWidth, -RowHeight / 2,  RowWidth / 2),
            new Vector3(            0,  RowHeight / 2,  RowWidth / 2),
            new Vector3(            0, -RowHeight / 2,  RowWidth / 2 + RowSlopeWidth),
        };

        foreach (var row in hexInfluencePoints.GroupBy(v => v.y))
        {
            var verts = row.OrderBy(v => v.x).ToArray();

            if (verts.Length > 1)
            {
                for (var i = 0; i < verts.Length; i++)
                {
                    var vertX = (int)(hex.Position.RealX + verts[i].x);
                    var vertZ = (int)(hex.Position.RealY + verts[i].y);
                    var vertY = landHeightMap[vertX, vertZ];

                    if (i == 0)
                    {
                        var localVerts = startVertOffsets
                            .Select(vo => new Vector3(vo.x + vertX, vo.y + vertY, vo.z + vertZ))
                            .ToArray();

                        foreach (var j in new[] { 0, 1, 2, 2, 4, 3, 3, 0, 2, 4, 5, 3 })
                        {
                            surfaceTool.AddColor(new Color(1, 1, 1, 1));
                            surfaceTool.AddVertex(localVerts[j]);
                        }
                    }
                    if (i < verts.Length - 1)
                    {
                        var nVertX = (int)(hex.Position.RealX + verts[i + 1].x);
                        var nVertZ = (int)(hex.Position.RealY + verts[i + 1].y);
                        var nVertY = landHeightMap[nVertX, nVertZ];

                        var localVerts = midVertOffsets
                            .Select(vo => new Vector3(vo.x + vertX, vo.y + vertY, vo.z + vertZ))
                            .Concat(midVertOffsets
                                .Select(vo => new Vector3(vo.x + nVertX, vo.y + nVertY, vo.z + nVertZ)))
                            .ToArray();

                        foreach (var j in new[] { 0, 4, 5, 5, 1, 0, 1, 5, 6, 6, 2, 1, 2, 6, 7, 7, 3, 2 })
                        {
                            surfaceTool.AddColor(new Color(1, 1, 1, 1));
                            surfaceTool.AddVertex(localVerts[j]);
                        }
                    }
                    else
                    {
                        var localVerts = endVertOffsets
                            .Select(vo => new Vector3(vo.x + vertX, vo.y + vertY, vo.z + vertZ))
                            .ToArray();

                        foreach (var j in new[] { 1, 0, 2, 0, 3, 4, 4, 2, 0, 3, 5, 4 })
                        {
                            surfaceTool.AddColor(new Color(1, 1, 1, 1));
                            surfaceTool.AddVertex(localVerts[j]);
                        }
                    }
                }
            }
        }

        surfaceTool.GenerateNormals();

        var meshChild = new MeshInstance();
        container.AddChild(meshChild);

        meshChild.Mesh = surfaceTool.Commit();
        meshChild.MaterialOverride = DirtMaterial;
    }
}