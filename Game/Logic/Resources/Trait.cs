﻿using BronzeAgeTribes.Logic.Data.Game;
using BronzeAgeTribes.Logic.Resources;
using Godot;

public class Trait : Resource
{
    [Export]
    public string Id { get; set; }

    [Export]
    public string Name { get; set; }

    [Export]
    public Texture Icon { get; set; }

    [Export(PropertyHint.MultilineText)]
    public string Description { get; set; }

    [Export]
    public JobBonus[] JobBonuses = new JobBonus[0];

    [Export]
    public JobCategory[] ForbiddenJobs = new JobCategory[0];

    [Export]
    public float CombatBonus;

    [Export]
    public float GrowthBonus;

    [Export]
    public bool PreventSettling;

    [Export]
    public PopInteractionType[] BonusInteractions = new PopInteractionType[0];
}
