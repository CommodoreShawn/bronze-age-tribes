﻿using BronzeAgeTribes.Logic.Data;
using BronzeAgeTribes.Logic.Data.Serialization;
using BronzeAgeTribes.Logic.Infrastructure;
using BronzeAgeTribes.Logic.Infrastructure.Interfaces;
using Godot;
using System;
using System.Collections.Generic;
using System.Linq;

public class BuildingType : Resource
{
    [Export]
    public string Id;

    [Export]
    public string Name;

    [Export(PropertyHint.MultilineText)]
    public string Description;

    [Export]
    public TerrainTrait[] Traits;

    [Export]
    public int StarvationProtection;

    [Export]
    public Texture PopBackground;

    [Export]
    public Texture SettlementBackground;

    [Export]
    public int SettlementBackgroundSlot;

    [Export]
    public int SettlementBackgroundPriority;

    [Export]
    public Texture InteractButtonIcon;

    [Export]
    public float FoodWhenWorked;

    [Export]
    public float ProductionWhenWorked;

    [Export]
    public float LuxuriesWhenWorked;

    [Export]
    public float TradeWhenWorked;

    [Export]
    public float WealthWhenWorked;

    [Export]
    public float LegitimacyWhenWorked;

    [Export]
    public InteractionOption ConstructionInteraction;

    [Export]
    public PopJobType[] ProvidedJobs;

    [Export]
    public TerrainTrait[] RequiredTileTraits;

    [Export]
    public TerrainTrait[] RequiredAdjacentTileTraits;

    [Export]
    public TerrainTrait[] ForbiddenTileTraits;

    [Export]
    public TerrainTrait[] RequiredEmpireTraits;

    [Export]
    public InteractionOption[] InteractionOptions;

    [Export]
    public bool RotatePrefabs;
    
    [Export]
    public FeaturePrefabPlacementMode PrefabPlacementMode;

    [Export]
    public string BuildingPrefabType;

    public float GetExpectedOutput(ResourceType resource)
    {
        switch (resource)
        {
            case ResourceType.Food:
                return FoodWhenWorked;
            case ResourceType.Production:
                return ProductionWhenWorked;
            case ResourceType.Luxuries:
                return LuxuriesWhenWorked;
            case ResourceType.Trade:
                return TradeWhenWorked;
            case ResourceType.Wealth:
                return WealthWhenWorked;
            case ResourceType.Legitimacy:
                return LegitimacyWhenWorked;
        }

        return 0;
    }

    public void MakeMapRepresentation(
        Spatial rootLod0,
        Spatial rootLod1,
        Spatial rootLod2,
        WorldHex hex,
        float[,] landHeightMap,
        float[,] waterHeightMap,
        Random random,
        BuildingStyle buildingStyle,
        Action<Spatial, float, PackedScene, float, float, float, float> addFeaturePrefab)
    {
        var packedPrefab = buildingStyle.GetBuildingPrefab(BuildingPrefabType);

        if(packedPrefab != null)
        {
            var facing = 0f;

            if (RotatePrefabs)
            {
                facing = (float)random.NextDouble() * 2 * Mathf.Pi;
            }

            var vertexX = (int)(hex.Position.RealX);
            var vertexZ = (int)(hex.Position.RealY);

            var height = landHeightMap[vertexX, vertexZ];

            if (PrefabPlacementMode == FeaturePrefabPlacementMode.WaterSurface)
            {
                height = waterHeightMap[vertexX, vertexZ];
            }

            addFeaturePrefab(rootLod0, 1, packedPrefab, 0, height, 0, facing);

            addFeaturePrefab(rootLod1, 1.5f, packedPrefab, 0, height, 0, facing);

            addFeaturePrefab(rootLod2, 2, packedPrefab, 0, height, 0, facing);
        }
    }

    public static Building ReadFrom(
        SerializedObject buildingRoot, 
        UrbanDevelopment development, 
        IResourceTracker resourceTracker)
    {
        var buildingType = buildingRoot.GetObjectReference(
            "building_id",
            resourceTracker.BuildingTypes,
            x => x.Id);

        return buildingType.Deserialize(buildingRoot, development, resourceTracker);
    }

    public void WriteTo(SerializedObject buildingRoot, Building building)
    {
        buildingRoot.Set("building_id", Id);
        buildingRoot.Set("current_interaction", building.CurrentInteraction?.Id);
        buildingRoot.Set("construct_progress", building.ConstructProgress);
        buildingRoot.Set("is_constructed", building.IsConstructed);
        buildingRoot.Set("building_style", building.BuildingStyle.Id);
    }

    private Building Deserialize(SerializedObject buildingRoot, UrbanDevelopment development, IResourceTracker resourceTracker)
    {
        var currentInteraction = buildingRoot.GetOptionalObjectReference(
                "current_interaction",
                InteractionOptions.Concat(ConstructionInteraction.Yield()),
                x => x.Id);

        var building = new Building(
            this,
            development,
            buildingRoot.GetBool("is_constructed"),
            buildingRoot.GetObjectReference(
                "building_style",
                resourceTracker.BuildingStyles,
                x => x.Id))
        {
            ConstructProgress = buildingRoot.GetFloat("construct_progress"),
        };

        building.SetCurrentInteraction(currentInteraction);

        return building;
    }

    public virtual Building MakeBuilding(
        UrbanDevelopment development,
        bool isConstructed,
        BuildingStyle buildingStyle)
    {
        return new Building(
            this,
            development,
            isConstructed,
            buildingStyle);
    }
}