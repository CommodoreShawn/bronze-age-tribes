using Godot;

public class Rotation : Spatial
{
    [Export]
    public float Rate;

    public override void _Ready()
    {
        base._Ready();

        Rotate(Vector3.Left, (float)new System.Random().NextDouble() * 2);
    }

    public override void _Process(float delta)
    {
        base._Process(delta);
        RotateObjectLocal(Vector3.Left, Rate * delta);
    }
}
