using Autofac;
using BronzeAgeTribes.Logic.Data;
using BronzeAgeTribes.Logic.Data.Game;
using BronzeAgeTribes.Logic.Infrastructure;
using BronzeAgeTribes.Logic.Infrastructure.Interfaces;
using BronzeAgeTribes.Logic.Infrastructure.SimulationEvents;
using Godot;
using System.Collections.Generic;
using System.Linq;

public class GameplayWorld : Spatial
{
    [Inject]
    private IWorldRenderer _worldRenderer;
    [Inject]
    private ILocalPlayerInfo _localPlayerInfo;
    [Inject]
    private ITooltipManager _tooltipManager;
    [Inject]
    private ISimulationEventBus _simulationEventBus;
    [Inject]
    private IWorldManager _worldManager;

    [Export]
    public PackedScene HexHighlightPrefab;

    [Export]
    public SpatialMaterial LandMaterial;
    [Export]
    public SpatialMaterial WaterMaterial;
    [Export]
    public SpatialMaterial ShadowMaterial;
    [Export]
    public SpatialMaterial BorderMaterial;

    private Spatial _hexHighlight;
    private WorldHex _previouslyHighlightedHex;
    private IGameplayService[] _gameplayServices;

    private Dictionary<AdministrativeGroup, TribeBorderDisplay> _borderDisplays;

    private bool _wasPopupActive;

    public override void _Ready()
    {
        this.ResolveDependencies();
        _simulationEventBus.OnSimulationEvent += OnSimulationEvent;

        if (HexHighlightPrefab != null)
        {
            _hexHighlight = HexHighlightPrefab.Instance() as Spatial;
            _hexHighlight.Visible = false;
            AddChild(_hexHighlight);
        }

        IocManager.IocContainer.Resolve<ISoundManager>().SetMusic(true, false);

        _gameplayServices = IocManager.IocContainer.Resolve<IEnumerable<IGameplayService>>().ToArray();

        var mapSize = _worldRenderer.GetVertexSizeForWorld();

        GetNode<Spatial>("CameraFocus").Translate(new Vector3(mapSize / 2, 0, mapSize / 2));

        _worldRenderer.BuildTerrain(
            this,
            LandMaterial,
            WaterMaterial,
            ShadowMaterial,
            BorderMaterial,
            false);

        _borderDisplays = new Dictionary<AdministrativeGroup, TribeBorderDisplay>();

        UpdateBorderDisplays();

        GetNode<OrbitingSun>("OrbitingSun").Initialize(mapSize);
        GetNode<OrbitingSun>("CursedMoon").Initialize(mapSize);
    }

    private void OnSimulationEvent(ISimulationEvent evt)
    {
        GD.Print("OnSimulationEvent: GameplayWorld " + evt.GetType().Name);
        if(evt is WorldChangedSimulationEvent worldChangedEvent)
        {
            _worldRenderer.UpdateHexModels(this, worldChangedEvent.AffectedHexes, false);
        }
        else if(evt is TurnAdvancedSimulationEvent)
        {
            if (_localPlayerInfo.Empire != null && !_localPlayerInfo.Empire.AdministrativeGroups.Any(g => g.Pops.Any()))
            {
                GetNode<GameoverPopup>("GameoverPopup").InitalizeForNoPops();
            }
        }
        else if (evt is AdminGroupChangedSimulationEvent)
        {
            UpdateBorderDisplays();
        }
        else if (evt is ExplorationSimulationEvent explorationEvt)
        {
            _worldRenderer.UpdateTerrain(this, explorationEvt.AffectedRegions);
        }
    }

    private void UpdateBorderDisplays()
    {
        // add and update new displays
        foreach (var adminGroup in _worldManager.Empires.SelectMany(e => e.AdministrativeGroups))
        {
            if(_borderDisplays.ContainsKey(adminGroup))
            {
                _borderDisplays[adminGroup].InitializeFor(adminGroup);
            }
            else if(adminGroup.IsSettled || adminGroup.Owner == _localPlayerInfo.Empire)
            {
                var borderDisplay = ResourceLoader.Load<PackedScene>("res://Scenes/TribeBorderDisplay.tscn").Instance() as TribeBorderDisplay;
                borderDisplay.InitializeFor(adminGroup);
                AddChild(borderDisplay);
                _borderDisplays.Add(adminGroup, borderDisplay);
            }
        }

        // remove old displays
        foreach(var adminGroup in _borderDisplays.Keys.ToArray())
        {
            if(!adminGroup.Owner.AdministrativeGroups.Contains(adminGroup))
            {
                _borderDisplays[adminGroup].QueueFree();
                _borderDisplays.Remove(adminGroup);
            }
        }
    }

    public override void _ExitTree()
    {
        base._ExitTree();

        _simulationEventBus.OnSimulationEvent -= OnSimulationEvent;
    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        _wasPopupActive = IsPopupActive();

        if(_previouslyHighlightedHex != _localPlayerInfo.HoveredHex && _hexHighlight != null)
        {
            if(_localPlayerInfo.HoveredHex == null)
            {
                _hexHighlight.Visible = false;
                _tooltipManager.HideTooltipFor(this);
            }
            else
            {
                var heightIncrease = Mathf.Max(_localPlayerInfo.HoveredHex.RenderedPos.y, 0) + 0.5f - _localPlayerInfo.HoveredHex.RenderedPos.y;

                _hexHighlight.Visible = true;
                _hexHighlight.Transform = Transform.Translated(_localPlayerInfo.HoveredHex.RenderedPos + new Vector3(0, heightIncrease, 0));
                _previouslyHighlightedHex = _localPlayerInfo.HoveredHex;
                _tooltipManager.HideTooltipFor(this);
                _tooltipManager.ShowTooltipFor(this, _localPlayerInfo.HoveredHex);
            }
        }

        foreach(var service in _gameplayServices)
        {
            service.Process();
        }
    }

    public void ShowProgressModal(string taskName, System.Action<ReportProgress> work, System.Action whenDone)
    {
        GetNode<ProgressPopupController>("ProgressPopup").StartTask(taskName, work, whenDone);
    }

    public void ShowGroupDetailsModal(AdministrativeGroup group)
    {
        GetNode<AdminGroupDetailPopup>("AdminGroupDetailPopup").ShowFor(group);
    }

    public void ShowMenu()
    {
        if (!_wasPopupActive)
        {
            GetNode<GameMenuPopup>("GameMenuPopup").InitializeAndShow();
        }
    }

    public bool IsPopupActive()
    {
        return
            GetNode<GameMenuPopup>("GameMenuPopup").Visible
            || GetNode<LoadWorldController>("LoadPanel").Visible
            || GetNode<SaveWorldController>("SavePanel").Visible
            || GetNode<AdminGroupDetailPopup>("AdminGroupDetailPopup").Visible
            || GetNode<GameoverPopup>("GameoverPopup").Visible;
    }

    public void ToggleShowPops(bool isToggled)
    {
        _localPlayerInfo.ShowHexWidgets = isToggled;
    }
}
