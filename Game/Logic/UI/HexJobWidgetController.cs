using BronzeAgeTribes.Logic.Data;
using BronzeAgeTribes.Logic.Data.Game;
using BronzeAgeTribes.Logic.Infrastructure;
using BronzeAgeTribes.Logic.Infrastructure.Interfaces;
using Godot;
using System.Collections.Generic;
using System.Linq;

public class HexJobWidgetController : Control
{
    private WorldHex _hex;
    private List<PopJob> _lastJobs;
    private HBoxContainer _jobBox;
    private GridContainer _outputsBox;
    private VBoxContainer _interactBox;
    private HBoxContainer _interactButtonContainer;
    private VBoxContainer _idlePopBox;

    [Export]
    public PackedScene PopJobPortraitPrefab;

    [Export]
    public PackedScene HexInteractButton;

    [Export]
    public PackedScene HexInteractOptionButton;

    [Export]
    public PackedScene IdlePopPortrait;

    private Empire _previousOwner;

    [Inject]
    private ILocalPlayerInfo _localPlayerInfo;
    [Inject]
    private ILogger _logger;

    public void InitializeFor(WorldHex hex)
    {
        _hex = hex;
        _lastJobs = new List<PopJob>();
        _jobBox = GetNode<HBoxContainer>("PanelContainer/JobBox");
        _outputsBox = GetNode<GridContainer>("OutputsBox");
        _interactBox = GetNode<VBoxContainer>("InteractBox");
        _interactButtonContainer = GetNode<HBoxContainer>("InteractButtonContainer");
        _idlePopBox = GetNode<VBoxContainer>("ExtraPopBox");

        foreach (var child in _outputsBox.GetChildren())
        {
            (child as ResourceDisplayController).SetSource(hex);
        }

        _previousOwner = _hex.Jobs.Where(j => j.Worker != null).Select(j => j.Worker.Owner).FirstOrDefault();

        this.ResolveDependencies();

        UpdateColor();
    }

    public void Reinitialize()
    {

    }

    public override void _Process(float delta)
    {
        base._Process(delta);
        try
        {
            var owner = _hex.Jobs.Where(j => j.Worker != null).Select(j => j.Worker.Owner).FirstOrDefault();

            if (owner != _previousOwner)
            {
                _previousOwner = owner;
                UpdateColor();
            }

            var isOffScreen = GetViewport().GetCamera().IsPositionBehind(_hex.RenderedPos);
            var cameraDist = (_hex.RenderedPos - GetViewport().GetCamera().GlobalTransform.origin).Length();

            if ((isOffScreen && cameraDist > 60) || !_localPlayerInfo.ShowHexWidgets)
            {
                Visible = false;
            }
            else
            {
                var screenPos = GetViewport().GetCamera().UnprojectPosition(_hex.RenderedPos);
                
                var jobPos = _jobBox.RectGlobalPosition - RectGlobalPosition;

                RectGlobalPosition = screenPos - jobPos - _jobBox.RectSize / 2;

                for (var i = 0; i < _hex.Jobs.Count || i < _lastJobs.Count; i++)
                {
                    if (i < _hex.Jobs.Count && i < _lastJobs.Count)
                    {
                        if (_lastJobs[i] != _hex.Jobs[i])
                        {
                            (_jobBox.GetChild(i) as PopJobPortraitController).InitializeFor(_hex.Jobs[i]);
                            _lastJobs[i] = _hex.Jobs[i];
                        }
                    }
                    else if (i < _hex.Jobs.Count)
                    {
                        var newElement = PopJobPortraitPrefab.Instance() as PopJobPortraitController;
                        newElement.InitializeFor(_hex.Jobs[i]);
                        _jobBox.AddChild(newElement);
                        _lastJobs.Add(_hex.Jobs[i]);
                    }
                    else
                    {
                        _lastJobs[i] = null;

                        (_jobBox.GetChild(i) as PopJobPortraitController).QueueFree();
                    }
                }

                _lastJobs.RemoveAll(j => j == null);

                var interactingFeaturesOrPops = _hex.Features
                    .Where(f => f.CurrentInteraction != null)
                    .OfType<object>()
                    .Concat(_hex.Pops.Where(p => p.InteractionTo != null))
                    .ToArray();
                var interactButtons = _interactButtonContainer.GetChildren().ToArray<HexInteractOptionButton>();

                if (_hex.Development != null && _hex.Development.CurrentInteraction != null)
                {
                    if (interactButtons.Any())
                    {
                        interactButtons[0].InitializeFor(_hex, _hex.Development, _hex.Development.CurrentInteraction);
                        interactButtons = interactButtons.Skip(1).ToArray();
                    }
                    else
                    {
                        var newElement = HexInteractOptionButton.Instance() as HexInteractOptionButton;
                        newElement.InitializeFor(_hex, _hex.Development, _hex.Development.CurrentInteraction);
                        _interactButtonContainer.AddChild(newElement);
                    }
                }

                if (_hex.Development != null && _hex.Development is UrbanDevelopment urbanDevelopment
                    && urbanDevelopment.Building != null && urbanDevelopment.Building.CurrentInteraction != null)
                {
                    if (interactButtons.Any())
                    {
                        interactButtons[0].InitializeFor(_hex, urbanDevelopment.Building, urbanDevelopment.Building.CurrentInteraction);
                        interactButtons = interactButtons.Skip(1).ToArray();
                    }
                    else
                    {
                        var newElement = HexInteractOptionButton.Instance() as HexInteractOptionButton;
                        newElement.InitializeFor(_hex, urbanDevelopment.Building, urbanDevelopment.Building.CurrentInteraction);
                        _interactButtonContainer.AddChild(newElement);
                    }
                }

                for (var i = 0; i < interactingFeaturesOrPops.Length || i < interactButtons.Length; i++)
                {
                    if (i < interactingFeaturesOrPops.Length && i < interactButtons.Length)
                    {
                        if (interactingFeaturesOrPops[i] is CellFeature interactingFeature)
                        {
                            interactButtons[i].InitializeFor(_hex, interactingFeature, interactingFeature.CurrentInteraction);
                        }
                        else if (interactingFeaturesOrPops[i] is Pop interactingPop)
                        {
                            interactButtons[i].InitializeFor(_hex, interactingPop, interactingPop.InteractionTo, interactingPop.InteractionType);
                        }
                    }
                    else if (i < interactingFeaturesOrPops.Length)
                    {
                        var newElement = HexInteractOptionButton.Instance() as HexInteractOptionButton;
                        _interactButtonContainer.AddChild(newElement);

                        if (interactingFeaturesOrPops[i] is CellFeature interactingFeature)
                        {
                            newElement.InitializeFor(_hex, interactingFeature, interactingFeature.CurrentInteraction);
                        }
                        else if (interactingFeaturesOrPops[i] is Pop interactingPop)
                        {
                            newElement.InitializeFor(_hex, interactingPop, interactingPop.InteractionTo, interactingPop.InteractionType);
                        }
                    }
                    else
                    {
                        interactButtons[i].QueueFree();
                    }
                }

                var interactChildren = _interactBox.GetChildren().ToArray<HexInteractButton>();
                var features = _hex.Features.ToArray();

                if (_hex.Development != null)
                {
                    if (interactChildren.Any())
                    {
                        interactChildren[0].InitializeFor(_hex, _hex.Development);
                        interactChildren[0].Visible = true;
                        interactChildren = interactChildren.Skip(1).ToArray();
                    }
                    else
                    {
                        var newInteract = HexInteractButton.Instance() as HexInteractButton;
                        newInteract.InitializeFor(_hex, _hex.Development);
                        _interactBox.AddChild(newInteract);
                    }
                }
                else if (_hex.Terrain != null)
                {
                    if (interactChildren.Any())
                    {
                        interactChildren[0].InitializeFor(_hex, _hex.Terrain);
                        interactChildren[0].Visible = true;
                        interactChildren = interactChildren.Skip(1).ToArray();
                    }
                    else
                    {
                        var newInteract = HexInteractButton.Instance() as HexInteractButton;
                        newInteract.InitializeFor(_hex, _hex.Terrain);
                        _interactBox.AddChild(newInteract);
                    }
                }

                if (_hex.Development != null && _hex.Development is UrbanDevelopment urbanDevelopment1
                    && urbanDevelopment1.Building != null)
                {
                    if (interactChildren.Any())
                    {
                        interactChildren[0].InitializeFor(_hex, urbanDevelopment1.Building);
                        interactChildren[0].Visible = true;
                        interactChildren = interactChildren.Skip(1).ToArray();
                    }
                    else
                    {
                        var newInteract = HexInteractButton.Instance() as HexInteractButton;
                        newInteract.InitializeFor(_hex, urbanDevelopment1.Building);
                        _interactBox.AddChild(newInteract);
                    }
                }

                for (var i = 0; i < features.Length || i < interactChildren.Length; i++)
                {
                    if (i < features.Length && i < interactChildren.Length)
                    {
                        interactChildren[i].InitializeFor(_hex, features[i]);
                        interactChildren[i].Visible = true;
                    }
                    if (i < features.Length && i >= interactChildren.Length)
                    {
                        var newInteract = HexInteractButton.Instance() as HexInteractButton;
                        newInteract.InitializeFor(_hex, features[i]);
                        _interactBox.AddChild(newInteract);
                    }
                    else if (i >= features.Length)
                    {
                        interactChildren[i].Visible = false;
                    }
                }

                var idlePopPortraits = _idlePopBox.GetChildren().OfType<IdlePopPortraitController>().ToArray();
                var idlePops = _hex.Pops.Where(p => p.Job == null).ToArray();

                for (var i = 0; i < idlePopPortraits.Length || i < idlePops.Length; i++)
                {
                    if (i < idlePopPortraits.Length && i < idlePops.Length)
                    {
                        idlePopPortraits[i].InitializeFor(idlePops[i]);
                    }
                    else if (i < idlePopPortraits.Length)
                    {
                        idlePopPortraits[i].QueueFree();
                    }
                    else if (i < idlePops.Length)
                    {
                        var popPortrait = IdlePopPortrait.Instance() as IdlePopPortraitController;
                        popPortrait.InitializeFor(idlePops[i]);
                        _idlePopBox.AddChild(popPortrait);
                    }
                }

                _interactButtonContainer.Visible = cameraDist < 30;
                _outputsBox.Visible = cameraDist < 30;
                _interactBox.Visible = cameraDist < 30;
                _idlePopBox.Visible = cameraDist < 30;
                Visible = true;
                _outputsBox.Columns = Mathf.Max(1, _lastJobs.Count);
            }
        }
        catch (System.Exception ex)
        {
            _logger.Error(ex, "HexJobWidgetController._Process");
        }
    }

    public void ShowInteractionTarget(Pop target)
    {
        foreach (var child in _jobBox.GetChildren().OfType<PopJobPortraitController>())
        {
            child.ShowInteractionTarget(target);
        }
    }

    public void ClearInteractionTarget()
    {
        foreach(var child in _jobBox.GetChildren().OfType<PopJobPortraitController>())
        {
            child.ClearInteractionTarget();
        }
    }

    public void UpdateColor()
    {
        var jobContainer = GetNode<PanelContainer>("PanelContainer");
        var style = new StyleBoxFlat()
        {
            BorderWidthBottom = 2,
            BorderWidthLeft = 2,
            BorderWidthRight = 2,
            BorderWidthTop = 2,
            BorderColor = new Color(1, 1, 1),
            BgColor = new Color(0.5f, 0.5f, 0.5f),
            ExpandMarginBottom = 4,
            ExpandMarginLeft = 4,
            ExpandMarginRight = 4,
            ExpandMarginTop = 4,
            CornerRadiusBottomLeft = 2,
            CornerRadiusBottomRight = 2,
            CornerRadiusTopLeft = 2,
            CornerRadiusTopRight = 2,
            ContentMarginRight = 2,
            ShadowSize = 0
        };
        jobContainer.AddStyleboxOverride("panel", style);

        if (_hex.Jobs.Any(j => j.Worker != null))
        {
            var owner = _hex.Jobs.Where(j => j.Worker != null).First().Worker.Owner;
            style.BgColor = owner.SecondaryColor;
            style.BorderColor = owner.PrimaryColor;
            Update();
        }
    }

    public void SetForInteractions()
    {
        foreach(var controller in _jobBox.GetChildren().OfType<PopJobPortraitController>())
        {
            if(controller.Job.Worker != null)
            {
                controller.GetNode<TextureRect>("InteractTextureRect").Visible = true;
            }
        }
    }

    public void ClearForInteractions()
    {
        foreach (var controller in _jobBox.GetChildren().OfType<PopJobPortraitController>())
        {
            if (controller.Job.Worker != null)
            {
                controller.GetNode<TextureRect>("InteractTextureRect").Visible = false;
            }
        }
    }
}
