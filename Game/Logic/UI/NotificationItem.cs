using BronzeAgeTribes.Logic.Data;
using BronzeAgeTribes.Logic.Infrastructure.Interfaces;
using Godot;
using System.Linq;

public class NotificationItem : Control
{
    [Export]
    public Image FrameNormal;
    [Export]
    public Image FrameGhost;
    [Export]
    public Image FrameHighlight;

    [Inject]
    private ITooltipManager _tooltipManager;
    [Inject]
    private IResourceInspector _resourceInspector;
    [Inject]
    private INotificationTracker _notificationTracker;

    public Vector2 Target { get; set; }
    private float _currentX;

    public Notification NotificationObj { get; private set; }

    private TextureRect _frameTexture;
    private TextureRect _iconTexture;

    public override void _Ready()
    {
        _frameTexture = GetNode<TextureRect>("FrameTexture");
        _frameTexture.Texture = new ImageTexture
        {
            Flags = 0,
        };
        _iconTexture = GetNode<TextureRect>("IconTexture");
        _iconTexture.Texture = new ImageTexture
        {
            Flags = 0,
        };
        (_frameTexture.Texture as ImageTexture).CreateFromImage(FrameNormal, 0);
        this.ResolveDependencies();

        Target = new Vector2(GetParent<NotificationsPanel>().RectSize.x, 0);
    }

    public void InitializeFor(Notification notification, Vector2 startingPos)
    {
        NotificationObj = notification;
        RectPosition = startingPos;

        var icon = _resourceInspector.ListResources<Image>("res://.import/", ".image")
            .Where(t => t.ResourcePath.Contains(notification.Icon))
            .FirstOrDefault();

        GD.Print("notification " + notification.Icon);

        if(icon != null)
        {
            (_iconTexture.Texture as ImageTexture).CreateFromImage(icon, 0);
        }
        else
        {
            GD.Print("Notification icon not found " + notification.Icon);
        }
    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        RectPosition = RectPosition.MoveToward(Target, delta * 1000);
    }

    public void MouseEntered()
    {
        (_frameTexture.Texture as ImageTexture).CreateFromImage(FrameHighlight, 0);
        _tooltipManager.ShowTooltipFor(this, NotificationObj);
    }

    public void MouseExited()
    {
        (_frameTexture.Texture as ImageTexture).CreateFromImage(FrameNormal, 0);
        _tooltipManager.HideTooltipFor(this);
    }

    public void OnGuiInput(InputEvent evt)
    {
        if(evt is InputEventMouseButton btnEvt && btnEvt.Pressed)
        {
            if(btnEvt.ButtonIndex == 1)
            {
                _notificationTracker.Open(NotificationObj);
            }
            else if(NotificationObj.QuickDismissable)
            {
                _notificationTracker.QuickDismiss(NotificationObj);
            }
        }
    }
}
