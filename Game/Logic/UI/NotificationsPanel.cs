using BronzeAgeTribes.Logic.Infrastructure.Interfaces;
using BronzeAgeTribes.Logic.Infrastructure.SimulationEvents;
using Godot;
using System.Collections.Generic;
using System.Linq;

public class NotificationsPanel : Control
{
    [Inject]
    private INotificationTracker _notificationTracker;

    [Inject]
    private ISimulationEventBus _simulationEventBus;

    [Export]
    public PackedScene NotificationPrefab;

    private readonly int NOTIFICATION_SPACING = 50;

    private List<NotificationItem> _notifications = new List<NotificationItem>();

    public override void _Ready()
    {
        this.ResolveDependencies();
        _simulationEventBus.OnSimulationEvent += OnSimulationEvent;
        AddNewNotifications();
    }

    public override void _ExitTree()
    {
        base._ExitTree();

        _simulationEventBus.OnSimulationEvent -= OnSimulationEvent;
    }

    private void OnSimulationEvent(ISimulationEvent evt)
    {
        Godot.GD.Print("OnSimulationEvent: NotificationsPanel");
        if (evt is TurnAdvancedSimulationEvent
            || evt is WorldLoadedSimulationEvent)
        {
            AddNewNotifications();
        }
    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        for(var i = 0; i < _notifications.Count; i++)
        {
            if(_notifications[i].NotificationObj.IsDismissed)
            {
                _notifications[i].QueueFree();
                _notifications.RemoveAt(i);
                i--;
            }
            else
            {
                _notifications[i].Target = new Vector2(i * NOTIFICATION_SPACING, 0);
            }
        }
    }

    private void AddNewNotifications()
    {
        foreach(var notification in _notificationTracker.CurrentNotifications)
        {
            var existingItem = _notifications
                .Where(n => n.NotificationObj == notification)
                .FirstOrDefault();

            if(existingItem == null)
            {
                var newItem = NotificationPrefab.Instance() as NotificationItem;
                AddChild(newItem);
                newItem.Target = new Vector2(_notifications.Count * NOTIFICATION_SPACING, 0);
                newItem.InitializeFor(notification, newItem.Target + new Vector2(RectSize.x, 0));
                _notifications.Add(newItem);
            }
        }
    }
}
