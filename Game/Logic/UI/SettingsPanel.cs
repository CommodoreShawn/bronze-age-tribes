using BronzeAgeTribes.Logic.Infrastructure.Interfaces;
using Godot;

public class SettingsPanel : Popup
{
    [Inject]
    private IUserSettings _userSettings;

    private SpinBox _autosaveBox;
    private HSlider _musicSlider;
    private bool _inInit;

    public void InitalizeAndShow()
    {
        _inInit = true;
        _autosaveBox.Value = _userSettings.AutosaveInterval;
        _musicSlider.Value = _userSettings.MusicVolume * 100;
        Visible = true;
        _inInit = false;
    }

    public override void _Ready()
    {
        base._Ready();
        this.ResolveDependencies();
        _autosaveBox = GetNode<SpinBox>("SettingsPanel/VBoxContainer/HBoxContainer/SpinBox");
        _musicSlider = GetNode<HSlider>("SettingsPanel/VBoxContainer/MusicVolumeSlider");
    }

    public override void _Input(InputEvent @event)
    {
        base._Input(@event);

        if (Visible
            && @event is InputEventKey keyEvt
            && keyEvt.Scancode == (uint)KeyList.Escape)
        {
            GetTree().SetInputAsHandled();
            Close();
        }
    }

    public void ValueChanged(float value)
    {
        UpdateSettings();
    }

    public void UpdateSettings()
    {
        if (!_inInit)
        {
            _userSettings.AutosaveInterval = (int)_autosaveBox.Value;
            _userSettings.MusicVolume = (float)_musicSlider.Value / 100f;
        }
    }

    public void Close()
    {
        Visible = false;
    }
}
