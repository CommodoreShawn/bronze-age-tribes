using Autofac;
using BronzeAgeTribes.Logic.Infrastructure;
using BronzeAgeTribes.Logic.Infrastructure.Interfaces;
using BronzeAgeTribes.Logic.Infrastructure.SimulationEvents;
using Godot;
using System;
using System.Linq;

public class MusicController : AudioStreamPlayer
{
    [Export]
    public AudioStream MenuMusic;

    [Export]
    public MusicOption[] Options;

    private bool _playingMenu;
    private bool _playingExploration;
    private bool _playingBattle;
    private MusicOption _currentOption;
    private Random _random;

    private ISimulationEventBus _simulationEventBus;
    private IUserSettings _userSettings;

    public override void _Ready()
    {
        Stream = MenuMusic;
        IocManager.IocContainer.Resolve<ISoundManager>().RegisterMusicController(this);
        _random = new Random();

        _simulationEventBus = IocManager.IocContainer.Resolve<ISimulationEventBus>();
        _simulationEventBus.OnSimulationEvent += OnSimulationEvent;
        _userSettings = IocManager.IocContainer.Resolve<IUserSettings>();
    }

    public override void _ExitTree()
    {
        base._ExitTree();
        _simulationEventBus.OnSimulationEvent -= OnSimulationEvent;
    }

    private void OnSimulationEvent(ISimulationEvent evt)
    {
        Godot.GD.Print("OnSimulationEvent: MusicController");
        if (evt is SettingsChangedSimulationEvent)
        {
            UpdateVolume();
        }
    }

    public void SetMenuMusic()
    {
        if(!_playingMenu)
        {
            _playingMenu = true;
            _playingBattle = false;
            _playingExploration = false;

            PlayNewTrack();
        }
    }

    public void SetMusic(bool isExploration, bool isBattle)
    {
        _playingMenu = false;
        _playingExploration = isExploration;
        _playingBattle = isExploration;

        if (_currentOption == null
            || (_currentOption.IsExploration && !isExploration )
            || (_currentOption.IsBattle && !isBattle))
        {
            PlayNewTrack();
        }
    }

    public void OnTrackFinished()
    {
        PlayNewTrack();
    }

    private void PlayNewTrack()
    {
        if(_playingMenu)
        {
            _currentOption = null;
            Stream = MenuMusic;
        }
        else
        {
            var choice = _random.Choose(Options.Where(o => o.IsBattle == _playingBattle || o.IsExploration == _playingExploration));

            _currentOption = choice;
            Stream = choice.MusicStream;
        }

        UpdateVolume();
        Playing = true;
    }

    private void UpdateVolume()
    {
        VolumeDb = GD.Linear2Db(_userSettings.MusicVolume);
    }
}
