using BronzeAgeTribes.Logic.Data;
using BronzeAgeTribes.Logic.Data.Game;
using Godot;
using System.Collections.Generic;

public class HexPopPreviewWidget : Control
{
    private WorldHex _hex;
    private List<PopJob> _lastJobs;
    private HBoxContainer _jobBox;

    [Export]
    public PackedScene PopJobPortraitPrefab;

    public void InitializeFor(WorldHex hex)
    {
        _hex = hex;
        _lastJobs = new List<PopJob>();
        _jobBox = GetNode<HBoxContainer>("PanelContainer/JobBox");
    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        var isOffScreen = GetViewport().GetCamera().IsPositionBehind(_hex.RenderedPos);

        if (isOffScreen)
        {
            Visible = false;
        }
        else
        {
            Visible = true;
            var screenPos = GetViewport().GetCamera().UnprojectPosition(_hex.RenderedPos);

            var jobPos = _jobBox.RectGlobalPosition - RectGlobalPosition;

            RectGlobalPosition = screenPos - jobPos - _jobBox.RectSize / 2;

            for (var i = 0; i < _hex.Jobs.Count || i < _lastJobs.Count; i++)
            {
                if (i < _hex.Jobs.Count && i < _lastJobs.Count)
                {
                    if (_lastJobs[i] != _hex.Jobs[i])
                    {
                        (_jobBox.GetChild(i) as PopJobPortraitController).InitializeFor(_hex.Jobs[i]);
                        _lastJobs[i] = _hex.Jobs[i];
                    }
                }
                else if (i < _hex.Jobs.Count)
                {
                    var newElement = PopJobPortraitPrefab.Instance() as PopJobPortraitController;
                    newElement.InitializeFor(_hex.Jobs[i]);
                    _jobBox.AddChild(newElement);
                    _lastJobs.Add(_hex.Jobs[i]);
                }
                else
                {
                    _lastJobs[i] = null;

                    (_jobBox.GetChild(i) as PopJobPortraitController).QueueFree();

                    i -= 1;
                }
            }
            _lastJobs.RemoveAll(j => j == null);
        }
    }
}
