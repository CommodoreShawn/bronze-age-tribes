using BronzeAgeTribes.Logic.Data;
using BronzeAgeTribes.Logic.Data.Game;
using BronzeAgeTribes.Logic.Infrastructure.Interfaces;
using BronzeAgeTribes.Logic.Infrastructure.SimulationEvents;
using Godot;
using System;
using System.Linq;

public class IdlePopPortraitController : Control
{
    private Pop _pop;
    
    private TextureRect _popImage;
    
    [Inject]
    private ITooltipManager _tooltipManager;
    [Inject]
    private ILocalPlayerInfo _localPlayerInfo;

    private PopManagementController _popManagementController;
    private HexJobWidgetController _hexJobWidgetController;
    
    public void InitializeFor(Pop pop)
    {
        _pop = pop;
        GetNode<TextureRect>("PopImage").Texture = _pop.Species.PopIconSmall;
    }

    public override void _Ready()
    {
        this.ResolveDependencies();

        _popManagementController = this.FindParent<PopManagementController>();
        _hexJobWidgetController = this.FindParent<HexJobWidgetController>();
    }

    public void OnMouseEntered()
    {
        if (_popManagementController != null)
        {
            _popManagementController.HoveredPops.Add(_pop);
            _tooltipManager.ShowTooltipFor(this, _pop);
        }
    }

    public void OnMouseExited()
    {
        if (_popManagementController != null)
        {
            _tooltipManager.HideTooltipFor(this);
            _popManagementController.HoveredPops.Remove(_pop);
        }
    }
}
