using BronzeAgeTribes.Logic;
using BronzeAgeTribes.Logic.Infrastructure.Interfaces;
using Godot;

public class TimePanelController : PanelContainer
{
    [Inject]
    private IWorldManager _worldManager;

    [Inject]
    private IWorldSimulator _worldSimulator;

    private Label _timeDisplay;

    public override void _Ready()
    {
        this.ResolveDependencies();

        _timeDisplay = GetNode<Label>("GridContainer/TurnLabel");
    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        _timeDisplay.Text = Util.FriendlyTimeDisplay(_worldManager.Turn);
    }

    public void OnEndTurn()
    {
        GetParent<GameplayWorld>().ShowProgressModal(
            "Advancing Turn",
            report =>
            {
                _worldSimulator.AdvanceTurn(report);
            },
            () =>
            {
            });
    }
}
