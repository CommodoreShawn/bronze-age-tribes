using Autofac;
using BronzeAgeTribes.Logic.Infrastructure;
using BronzeAgeTribes.Logic.Infrastructure.Interfaces;
using Godot;

public class TraitIconController : TextureRect
{
    private Trait _trait;

    [Inject]
    private ITooltipManager _tooltipManager;

    public override void _Ready()
    {
        _tooltipManager = IocManager.IocContainer.Resolve<ITooltipManager>();
    }

    public void InitializeFor(Trait trait)
    {
        _trait = trait;
        Texture = trait.Icon;
    }

    public void OnMouseEntered()
    {
        _tooltipManager.ShowTooltipFor(this, _trait);
    }

    public void OnMouseExited()
    {
        _tooltipManager.HideTooltipFor(this);
    }
}
