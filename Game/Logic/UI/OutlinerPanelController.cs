using BronzeAgeTribes.Logic.Data;
using BronzeAgeTribes.Logic.Infrastructure.Interfaces;
using BronzeAgeTribes.Logic.Infrastructure.SimulationEvents;
using Godot;
using System.Linq;

public class OutlinerPanelController : PanelContainer
{
    [Export]
    public PackedScene OutlinerGroupElementPrefab;

    [Inject]
    private ILocalPlayerInfo _localPlayerInfo;
    [Inject]
    private ISimulationEventBus _eventBus;

    private GridContainer _outlinerGrid;
    private bool _showOutlinerGroups;
    private bool _showOutlinerExpeditions;
    private Button _outlinerGroupsToggle;
    private Button _outlinerExpeditionsToggle;
    private GridContainer _groupsGrid;
    private bool _didInitialFocus;

    public override void _Ready()
    {
        _didInitialFocus = false;
        this.ResolveDependencies();
        _outlinerGrid = GetNode<GridContainer>("ScrollContainer/GridContainer");

        _groupsGrid = _outlinerGrid.GetNode<GridContainer>("GroupsGrid");
        _outlinerGroupsToggle = _outlinerGrid.GetNode<Button>("GroupsGrid/GroupsToggle");
        if (_localPlayerInfo.Empire != null)
        {
            foreach (var adminGroup in _localPlayerInfo.Empire.AdministrativeGroups)
            {
                var gridElement = OutlinerGroupElementPrefab.Instance() as GroupOutlinerElementController;
                gridElement.InitializeFor(adminGroup);
                _groupsGrid.AddChild(gridElement);
            }
        }

        _eventBus.OnSimulationEvent += OnSimulationEvent;
    }

    private void OnSimulationEvent(ISimulationEvent evt)
    {
        Godot.GD.Print("OnSimulationEvent: OutlinerPanelController");

        if (evt is AdminGroupChangedSimulationEvent groupEvt && groupEvt.Group.Owner == _localPlayerInfo.Empire)
        {
            var existingRow = _groupsGrid.GetChildren()
                .OfType<GroupOutlinerElementController>()
                .Where(x => x.Group == groupEvt.Group)
                .FirstOrDefault();

            // new group
            if (existingRow == null)
            {
                var gridElement = OutlinerGroupElementPrefab.Instance() as GroupOutlinerElementController;
                gridElement.InitializeFor(groupEvt.Group);
                _groupsGrid.AddChild(gridElement);
            }
            // deleted group
            else if (!_localPlayerInfo.Empire.AdministrativeGroups.Contains(groupEvt.Group))
            {
                existingRow.QueueFree();
            }
        }
    }

    public void LookAt(HexPosition position)
    {
        var cameraFocus = GetParent().GetNode<CameraFocus>("CameraFocus");

        if (cameraFocus != null)
        {
            cameraFocus.Transform = (cameraFocus.GetParent() as Spatial).Transform.Translated(new Vector3(position.RealX, 0, position.RealY));
        }
    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        if (!_didInitialFocus && _localPlayerInfo != null)
        {
            _didInitialFocus = true;
            LookAt(_localPlayerInfo.Empire.AdministrativeGroups.First().GroupCenter);
        }

        if (_showOutlinerGroups != _outlinerGroupsToggle.Pressed)
        {
            _showOutlinerGroups = _outlinerGroupsToggle.Pressed;

            var children = _groupsGrid.GetChildren();
            for (var i = 1; i < children.Count; i++)
            {
                (children[i] as Control).Visible = _showOutlinerGroups;
            }
        }
    }
}
