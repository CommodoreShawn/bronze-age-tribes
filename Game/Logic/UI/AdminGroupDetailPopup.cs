using BronzeAgeTribes.Logic.Data;
using BronzeAgeTribes.Logic.Data.Game;
using BronzeAgeTribes.Logic.Infrastructure.Interfaces;
using BronzeAgeTribes.Logic.Infrastructure.SimulationEvents;
using Godot;
using System.Linq;

public class AdminGroupDetailPopup : Popup
{
    [Export]
    public PackedScene DemographicsRowPrefab;
    [Export]
    public Texture TribeIcon;
    [Export]
    public Texture SettlementIcon;

    [Inject]
    private ISimulationEventBus _eventBus;

    private AdministrativeGroup _group;
    private TextEdit _nameText;

    public override void _Ready()
    {
        base._Ready();
        this.ResolveDependencies();
    }

    public override void _Input(InputEvent @event)
    {
        base._Input(@event);

        if (Visible
            && GetNode<Control>("PanelContainer").Visible
            && @event is InputEventKey keyEvt
            && keyEvt.Scancode == (uint)KeyList.Escape
            && keyEvt.Pressed)
        {
            GetTree().SetInputAsHandled();
            Close();
        }
    }

    public void ShowFor(AdministrativeGroup group)
    {
        var couldSettle = !group.Pops.All(p => p.Traits.Any(t => t.PreventSettling));

        var dominantBuildingStyle = group.Pops
            .GroupBy(p => p.Culture.BuildingStyle)
            .OrderByDescending(g => g.Count())
            .Select(g => g.Key)
            .FirstOrDefault();

        var buildingRects = new[]
        {
            GetNode<TextureRect>("PanelContainer/VBoxContainer/BodyGrid/BackgroundRect/BuildingRect0"),
            GetNode<TextureRect>("PanelContainer/VBoxContainer/BodyGrid/BackgroundRect/BuildingRect1"),
            GetNode<TextureRect>("PanelContainer/VBoxContainer/BodyGrid/BackgroundRect/BuildingRect2"),
            GetNode<TextureRect>("PanelContainer/VBoxContainer/BodyGrid/BackgroundRect/BuildingRect3"),
        };

        _group = group;
        GetNode<Control>("SplitOrSettleContainer").Visible = false;
        GetNode<Control>("PanelContainer").Visible = true;
        if (group.IsSettled)
        {
            GetNode<TextureRect>("PanelContainer/VBoxContainer/TitleRow/IconRect").Texture = SettlementIcon;
            GetNode<Button>("PanelContainer/VBoxContainer/BodyGrid/ButtonBox/SettleButton").Visible = false;
            GetNode<Button>("PanelContainer/VBoxContainer/BodyGrid/ButtonBox/Expandbutton").Visible = true;
            GetNode<TextureRect>("PanelContainer/VBoxContainer/BodyGrid/BackgroundRect/ForegroundRect").Texture = dominantBuildingStyle?.SettlementBackground;

        }
        else
        {
            GetNode<TextureRect>("PanelContainer/VBoxContainer/TitleRow/IconRect").Texture = TribeIcon;
            GetNode<Button>("PanelContainer/VBoxContainer/BodyGrid/ButtonBox/SettleButton").Visible = true;
            GetNode<Button>("PanelContainer/VBoxContainer/BodyGrid/ButtonBox/SettleButton").Disabled = !couldSettle;
            GetNode<Button>("PanelContainer/VBoxContainer/BodyGrid/ButtonBox/Expandbutton").Visible = false;
            GetNode<TextureRect>("PanelContainer/VBoxContainer/BodyGrid/BackgroundRect/ForegroundRect").Texture = dominantBuildingStyle?.TribeBackground;
        }

        foreach(var rect in buildingRects)
        {
            rect.Visible = false;
        }

        var buildingGroups = group.ClaimedRegions
            .SelectMany(r => r.Hexes.Where(h => h.Development != null))
            .Select(h => h.Development)
            .OfType<UrbanDevelopment>()
            .Where(d => d.Building != null && d.Building.BackingType.SettlementBackground != null)
            .Select(d => d.Building)
            .GroupBy(d => d.BackingType.SettlementBackgroundSlot)
            .ToArray();

        foreach(var buildingGroup in buildingGroups)
        {
            var building = buildingGroup
                .OrderBy(d => d.BackingType.SettlementBackgroundPriority)
                .First();

            var slot = building.BackingType.SettlementBackgroundSlot;

            if (slot < buildingRects.Length && slot >= 0)
            {
                buildingRects[slot].Visible = true;
                buildingRects[slot].Texture = building.BackingType.SettlementBackground;
            }
        }

        _nameText = GetNode<TextEdit>("PanelContainer/VBoxContainer/TitleRow/NameText");
        _nameText.Text = group.Name;

        GetNode<TextureRect>("PanelContainer/VBoxContainer/BodyGrid/BackgroundRect").Texture = group.Pops.First().Location.BiomeType.CityBackground;
        
        var demographicsList = GetNode<BoxContainer>("PanelContainer/VBoxContainer/BodyGrid/DemographicsBox/DemographicsList");
        demographicsList.ClearChildren();
        foreach (var species in group.Pops.Select(p => p.Species).Distinct())
        {
            var row = DemographicsRowPrefab.Instance() as DemographicsRowController;

            row.InitializeFor(group, species);
            demographicsList.AddChild(row);
        }

        Visible = true;
    }

    public override void _Process(float delta)
    {
        if (Visible)
        {
            if (!string.IsNullOrWhiteSpace(_nameText.Text))
            {
                _group.Name = _nameText.Text;
            }
        }
    }

    public void Close()
    {
        _eventBus.SendEvent(new AdminGroupChangedSimulationEvent(_group));
        Hide();
    }

    public void SplitGroup()
    {
        GetNode<Control>("PanelContainer").Visible = false;
        GetNode<SplitOrSettleContainer>("SplitOrSettleContainer").ShowForSplit(_group);
    }

    public void SettleTribe()
    {
        GetNode<Control>("PanelContainer").Visible = false;
        GetNode<SplitOrSettleContainer>("SplitOrSettleContainer").ShowForSettle(_group);
    }

    public void ExpandSettlement()
    {
        GetNode<Control>("PanelContainer").Visible = false;
        GetNode<SplitOrSettleContainer>("SplitOrSettleContainer").ShowForExpand(_group);
    }
}
