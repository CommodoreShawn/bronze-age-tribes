using BronzeAgeTribes.Logic.Data;
using BronzeAgeTribes.Logic.Data.Game;
using BronzeAgeTribes.Logic.Infrastructure.Interfaces;
using BronzeAgeTribes.Logic.Infrastructure.SimulationEvents;
using Godot;
using System.Linq;

public class PopJobPortraitController : Control
{
    private static readonly int STATE_NORMAL = 0;
    private static readonly int STATE_MOVING = 1;
    private static readonly int STATE_GHOST = 2;
    private static readonly int STATE_INTERACTING_TO = 3;

    public PopJob Job { get; private set; }
    private Pop _displayedPop;
    private int _displayPopState;

    private TextureRect _popImage;
    private TextureRect _managerImage;

    [Inject]
    private ITooltipManager _tooltipManager;
    [Inject]
    private ISimulationEventBus _simulationEventBus;
    [Inject]
    private ILocalPlayerInfo _localPlayerInfo;

    private PopManagementController _popManagementController;
    private HexJobWidgetController _hexJobWidgetController;
    private TextureProgress _healthBar;
    private TextureRect _interactTextureRect;
    private TextureRect _interactTargetTextureRect;

    [Export]
    public Texture PlaceholderCrafting;
    [Export]
    public Texture PlaceholderFarming;
    [Export]
    public Texture PlaceholderForaging;
    [Export]
    public Texture PlaceholderMining;
    [Export]
    public Texture PlaceholderWoodcutting;
    [Export]
    public Texture PlaceholderHunting;
    [Export]
    public Texture PlaceholderPreaching;
    [Export]
    public Texture PlaceholderBuilding;

    public void InitializeFor(PopJob job)
    {
        Job = job;
        _displayedPop = null;
        _popImage = GetNode<TextureRect>("PopImage");
        _managerImage = GetNode<TextureRect>("ManagerImage");
        SetPopImage();
        _healthBar = GetNode<TextureProgress>("HealthBar");
        _healthBar.Visible = false;
        _interactTextureRect = GetNode<TextureRect>("InteractTextureRect");
        _interactTextureRect.Visible = false;
        _interactTargetTextureRect = GetNode<TextureRect>("InteracTargetTextureRect");
        _interactTargetTextureRect.Visible = false;
    }

    public override void _Ready()
    {
        this.ResolveDependencies();

        RebuildPortraits();


        _popManagementController = this.FindParent<PopManagementController>();
        _hexJobWidgetController = this.FindParent<HexJobWidgetController>();

        _simulationEventBus.OnSimulationEvent += OnSimulationEvent;
    }

    private void OnSimulationEvent(ISimulationEvent evt)
    {
        Godot.GD.Print("OnSimulationEvent: PopJobPortraitController");
        if (evt is TurnAdvancedSimulationEvent)
        {
            RebuildPortraits();
        }
    }

    private void RebuildPortraits()
    {
        if(!IsInstanceValid(this))
        {
            return;
        }

        GetNode<TextureRect>("BackgroundImage").Texture = Job.Location.BiomeType.PopBackground;

        var features = Job.Location.Features.ToArray();

        if (features.Length > 0 && features[0].BackingType.PopBackground != null)
        {
            GetNode<TextureRect>("MidGroundImage0").Texture = features[0].BackingType.PopBackground;
        }
        else
        {
            GetNode<TextureRect>("MidGroundImage0").Texture = null;
        }

        if(Job.Location.Development != null 
            && Job.Location.Development is UrbanDevelopment urbanDevelopment
            && urbanDevelopment.Building != null && urbanDevelopment.Building.IsConstructed)
        {
            GetNode<TextureRect>("MidGroundImage1").Texture = urbanDevelopment.Building.BackingType.PopBackground;
        }
        else if (features.Length > 1 && features[1].BackingType.PopBackground != null)
        {
            GetNode<TextureRect>("MidGroundImage1").Texture = features[1].BackingType.PopBackground;
        }
        else
        {
            GetNode<TextureRect>("MidGroundImage1").Texture = null;
        }

        if (Job.Location.Development != null && Job.Location.Development.IsConstructed)
        {
            GetNode<TextureRect>("MidGroundImage2").Texture = Job.Location.Development.BackingType.PopBackground;
        }
        else if (features.Length > 2 && features[2].BackingType.PopBackground != null)
        {
            GetNode<TextureRect>("MidGroundImage2").Texture = features[2].BackingType.PopBackground;
        }
        else
        {
            GetNode<TextureRect>("MidGroundImage2").Texture = null;
        }
    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        Pop popToDisplay = null;
        int popState = STATE_NORMAL;

        if(Job.Worker != null)
        {
            popToDisplay = Job.Worker;
            if(Job.Worker.MovingFromJob != null)
            {
                popState = STATE_MOVING;
            }

            if(Job.Worker.InteractionTo != null)
            {
                popState = STATE_INTERACTING_TO;
            }
        }
        else if(Job.MovingOutWorker != null)
        {
            popToDisplay = Job.MovingOutWorker;
            popState = STATE_GHOST;
        }

        if (_displayedPop != popToDisplay || popState != _displayPopState)
        {
            _displayedPop = popToDisplay;
            _displayPopState = popState;

            if(_hexJobWidgetController!= null)
            {
                _hexJobWidgetController.UpdateColor();
            }

            SetPopImage();
        }

        if(Job.Worker != null && Job.Worker.Health < 1)
        {
            _healthBar.Visible = true;
            _healthBar.Value = 100 * Job.Worker.Health;
        }
        else
        {
            _healthBar.Visible = false;
        }
    }

    private void SetPopImage()
    {
        _popImage.Visible = true;

        _managerImage.Visible = _displayedPop?.ManagerSpecies != null;

        if (_displayedPop == null)
        {
            switch (Job.JobType.Category)
            {
                case JobCategory.Crafting:
                    _popImage.Texture = PlaceholderCrafting;
                    break;
                case JobCategory.Farming:
                    _popImage.Texture = PlaceholderFarming;
                    break;
                case JobCategory.Foraging:
                    _popImage.Texture = PlaceholderForaging;
                    break;
                case JobCategory.Mining:
                    _popImage.Texture = PlaceholderMining;
                    break;
                case JobCategory.Woodcutting:
                    _popImage.Texture = PlaceholderWoodcutting;
                    break;
                case JobCategory.Hunting:
                    _popImage.Texture = PlaceholderHunting;
                    break;
                case JobCategory.Preaching:
                    _popImage.Texture = PlaceholderPreaching;
                    break;
                case JobCategory.Building:
                    _popImage.Texture = PlaceholderBuilding;
                    break;
                default:
                    _popImage.Visible = false;
                    break;
            }
        }
        else
        {
            if (_displayPopState == STATE_MOVING)
            {
                _popImage.Texture = _displayedPop.Species.MovingIcon;
                _managerImage.Texture = _displayedPop.ManagerSpecies?.MovingIcon;
            }
            else if (_displayPopState == STATE_GHOST)
            {
                _popImage.Texture = _displayedPop.Species.MoveGhost;
                _managerImage.Texture = _displayedPop.ManagerSpecies?.MoveGhost;
            }
            else if (_displayPopState == STATE_INTERACTING_TO && Job.Worker.InteractionType.HasValue)
            {
                _popImage.Texture = _displayedPop.Species.GetInteractionIcon(Job.Worker.InteractionType.Value);

                _managerImage.Texture = _displayedPop.ManagerSpecies?.GetInteractionIcon(Job.Worker.InteractionType.Value);
            }
            else
            {
                _popImage.Texture = _displayedPop.Species.GetJobIcon(Job.JobType.Category);
                _managerImage.Texture = _displayedPop.ManagerSpecies?.GetJobIcon(Job.JobType.Category);
            }
        }
    }

    public void OnMouseEntered()
    {
        if (_popManagementController != null)
        {
            _popManagementController.HoveredJobs.Add(this);
            _tooltipManager.ShowTooltipFor(this, Job);
        }
    }

    public void OnMouseExited()
    {
        if (_popManagementController != null)
        {
            _tooltipManager.HideTooltipFor(this);
            _popManagementController.HoveredJobs.Remove(this);
        }
    }

    public void ClearInteractionTarget()
    {
        _interactTargetTextureRect.Visible = false;
    }

    public void ShowInteractionTarget(Pop target)
    {
        if(target == Job.Worker)
        {
            _interactTargetTextureRect.Visible = true;
        }
    }
}
