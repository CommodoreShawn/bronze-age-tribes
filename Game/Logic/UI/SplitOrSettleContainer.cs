using BronzeAgeTribes.Logic;
using BronzeAgeTribes.Logic.Data;
using BronzeAgeTribes.Logic.Data.Game;
using BronzeAgeTribes.Logic.Infrastructure.Interfaces;
using BronzeAgeTribes.Logic.Infrastructure.SimulationEvents;
using Godot;
using System;
using System.Collections.Generic;
using System.Linq;

public class SplitOrSettleContainer : PanelContainer
{
    private static readonly int MODE_SPLIT = 0;
    private static readonly int MODE_SETTLE = 1;
    private static readonly int MODE_EXPAND = 2;

    [Export]
    public PackedScene PopRowPrefab;

    [Export]
    public Texture MapHexSelect;

    [Export]
    public Texture MapHexHighlight;

    [Export]
    public Texture MapHexInvalid;

    [Inject]
    private IWorldManager _worldManager;
    [Inject]
    private ISimulationEventBus _eventBus;
    [Inject]
    private ILegitimacyHelper _legitimacyHelper;
    [Inject]
    private IExplorationTracker _explorationTracker;

    private int _mode;
    private float _cost;

    private Label _titleLabel;
    private Control _mapBox;
    private VBoxContainer _popSelectVBox;
    private GridContainer _regionSelectGrid;
    private TextEdit _fromText;
    private TextEdit _fromPopsText;
    private TextEdit _toText;
    private TextEdit _toPopsText;
    private Label _toLabel;
    private Dictionary<WorldHex, TextureRect> _hexes;
    private Dictionary<Pop, SplitSettlePopInfoRow> _popToRow;
    private Dictionary<WorldRegion, SplitSettlePopInfoRow> _regionToRow;
    private Button _okButton;
    private ResourceDisplayController _costDisplay;
    private HBoxContainer _toPopBox;

    private AdministrativeGroup _group;
    private bool _doSizeFix;
    private List<WorldRegion> _regionsToClaim;

    public override void _Ready()
    {
        this.ResolveDependencies();
        _titleLabel = GetNode<Label>("VBoxContainer/Label");
        _mapBox = GetNode<Control>("VBoxContainer/HBoxContainer/MapBox");
        _popSelectVBox = GetNode<VBoxContainer>("VBoxContainer/HBoxContainer/PopSelectVBox/VBoxContainer");
        _fromText = GetNode<TextEdit>("VBoxContainer/HBoxContainer/ControlsSelectVBox/FromText");
        _fromPopsText = GetNode<TextEdit>("VBoxContainer/HBoxContainer/ControlsSelectVBox/FromPopBox/FromPopText");
        _toText = GetNode<TextEdit>("VBoxContainer/HBoxContainer/ControlsSelectVBox/ToText");
        _toPopsText = GetNode<TextEdit>("VBoxContainer/HBoxContainer/ControlsSelectVBox/ToPopBox/ToPopText");
        _okButton = GetNode<Button>("VBoxContainer/HBoxContainer/ControlsSelectVBox/ButtonGrid/OkButton");
        _regionSelectGrid = GetNode<GridContainer>("VBoxContainer/HBoxContainer/RegionSelectGrid");
        _costDisplay = GetNode<ResourceDisplayController>("VBoxContainer/HBoxContainer/ControlsSelectVBox/SettleCostBox/CostDisplay");
        _toLabel = GetNode<Label>("VBoxContainer/HBoxContainer/ControlsSelectVBox/ToLabel");
        _toPopBox = GetNode<HBoxContainer>("VBoxContainer/HBoxContainer/ControlsSelectVBox/ToPopBox");


        _hexes = new Dictionary<WorldHex, TextureRect>();
        _popToRow = new Dictionary<Pop, SplitSettlePopInfoRow>();
        _regionsToClaim = new List<WorldRegion>();
        _regionToRow = new Dictionary<WorldRegion, SplitSettlePopInfoRow>();
    }

    public override void _Process(float delta)
    {
        base._Process(delta);
        if(_doSizeFix)
        {
            _doSizeFix = false;
            RectSize = Vector2.Zero;
        }
    }

    public override void _Input(InputEvent @event)
    {
        base._Input(@event);

        if (Visible
            && this.FindParent<AdminGroupDetailPopup>().Visible
            && @event is InputEventKey keyEvt
            && keyEvt.Scancode == (uint)KeyList.Escape
            && keyEvt.Pressed)
        {
            GetTree().SetInputAsHandled();
            Close();
        }
    }

    public void ShowForSplit(AdministrativeGroup group)
    {
        _mode = MODE_SPLIT;
        _group = group;
        _titleLabel.Text = $"Splitting {group.Name}";
        _okButton.Text = "Split";
        _fromText.Text = group.Name;
        _fromText.Readonly = true;
        _toText.Text = string.Empty;
        _toText.Readonly = false;
        Visible = true;
        _toText.Visible = true;
        _toPopsText.Visible = true;
        _toLabel.Visible = false;

        _toPopBox.Visible = true;
        _toLabel.Visible = true;
        _popSelectVBox.Visible = true;
        _regionSelectGrid.Visible = false;

        _popSelectVBox.ClearChildren();
        _popToRow.Clear();
        foreach(var pop in group.Pops)
        {
            var row = PopRowPrefab.Instance() as SplitSettlePopInfoRow;
            _popSelectVBox.AddChild(row);
            row.InitializeFor(pop);
            _popToRow.Add(pop, row);
        }

        MakeMap(group);

        TallyPops();
        _doSizeFix = true;
    }

    public void ShowForSettle(AdministrativeGroup group)
    {
        _mode = MODE_SETTLE;
        _group = group;
        _titleLabel.Text = $"Settling {group.Name}";
        _okButton.Text = "Settle";
        _fromText.Text = group.Name;
        _fromText.Readonly = true;
        _fromPopsText.Text = group.Pops.Count.ToString();
        _toText.Text = string.Empty;
        _toText.Readonly = false;
        Visible = true;
        _toText.Visible = false;
        _toPopBox.Visible = false;
        _toLabel.Visible = false;

        _popSelectVBox.Visible = false;
        _popSelectVBox.ClearChildren();
        _regionSelectGrid.Visible = true;

        foreach(var child in _regionSelectGrid.GetChildren().OfType<Control>().Skip(3))
        {
            child.QueueFree();
        }

        MakeMap(group);

        var occupiedRegions = _group.Pops
            .Select(p => p.Location.Region)
            .Distinct()
            .ToArray();

        foreach(var region in occupiedRegions)
        {
            if (region.ClaimedBy == null)
            {
                _regionsToClaim.Add(region);

                _regionSelectGrid.AddChild(new Label
                {
                    Text = region.Name
                });
            }
            else
            {
                var label = new Label
                {
                    Text = region.Name,
                };
                label.AddColorOverride("font_color", new Color(1, 0, 0));
                _regionSelectGrid.AddChild(label);
            }
            
            _regionSelectGrid.AddChild(new Label
            {
                Text = region.Hexes.Count().ToString(),
                Align = Label.AlignEnum.Right
            });
            _regionSelectGrid.AddChild(new Label
            {
                Text = _group.Pops.Where(p => p.Location.Region == region).Count().ToString(),
                Align = Label.AlignEnum.Right
            });

            foreach(var hex in region.Hexes)
            {
                if(_hexes.ContainsKey(hex))
                {
                    if (region.ClaimedBy == null)
                    {
                        _hexes[hex].Texture = MapHexSelect;
                    }
                    else
                    {
                        _hexes[hex].Texture = MapHexInvalid;
                    }
                }
            }
        }

        _cost = _legitimacyHelper.CostForSettling(_group, occupiedRegions);

        _costDisplay.SetSource(
            true,
            _cost,
            _legitimacyHelper.CostForSettlingDetails(_group, occupiedRegions));

        _doSizeFix = true;

        _okButton.Disabled = !_regionsToClaim.Any();
    }

    public void ShowForExpand(AdministrativeGroup group)
    {
        _mode = MODE_EXPAND;
        _group = group;
        _titleLabel.Text = $"Expand {group.Name}";
        _okButton.Text = "Expand";
        _fromText.Text = group.Name;
        _fromText.Readonly = true;
        _fromPopsText.Text = group.Pops.Count.ToString();
        _toText.Text = string.Empty;
        _toText.Readonly = false;
        Visible = true;
        _toText.Visible = false;
        _toPopBox.Visible = false;
        _toLabel.Visible = false;

        _popSelectVBox.Visible = true;
        _popSelectVBox.ClearChildren();
        _regionSelectGrid.Visible = false;
        _regionToRow.Clear();

        var expandOptions = _group.ClaimedRegions
            .SelectMany(r => r.Neighbors)
            .Except(_group.ClaimedRegions)
            .Distinct()
            .Where(r => r.Hexes.Any(h => _explorationTracker.IsExplored(h.Position)))
            .ToArray();

        foreach (var region in expandOptions)
        {
            var row = PopRowPrefab.Instance() as SplitSettlePopInfoRow;
            row.InitializeFor(region);
            _popSelectVBox.AddChild(row);
            _regionToRow.Add(region, row);
        }

        MakeMap(group);

        foreach (var hex in _group.ClaimedRegions.SelectMany(r => r.Hexes))
        {
            if (_hexes.ContainsKey(hex))
            {
                _hexes[hex].Texture = MapHexSelect;
            }
        }

        TallyRegions();
        _doSizeFix = true;
    }

    public void MakeMap(AdministrativeGroup group)
    {
        var range = group.Owner.TribeCoherency * Constants.VERTICIES_PER_HEX / 2;

        var hexes = Enumerable.Range(-range, range * 2)
            .SelectMany(i => Enumerable.Range(-range, range * 2).Select(j => Tuple.Create(i, j)))
            .Select(t => new HexPosition(group.GroupCenter.HexX + t.Item1, group.GroupCenter.HexY + t.Item2))
            .Select(p => _worldManager.GetHex(p))
            .Distinct()
            .Where(h => h != null)
            .Where(h => _explorationTracker.IsExplored(h.Position))
            .ToArray();

        var viewCenter = new Vector2(
            group.GroupCenter.RealX / (float)Constants.VERTICIES_PER_HEX,
            group.GroupCenter.RealY / (float)Constants.VERTICIES_PER_HEX);


        _mapBox.ClearChildren();
        _hexes.Clear();
        foreach (var hex in hexes)
        {
            var hexPos = new Vector2(
                (hex.Position.RealX / (float)Constants.VERTICIES_PER_HEX) - viewCenter.x,
                (hex.Position.RealY / (float)Constants.VERTICIES_PER_HEX) - viewCenter.y) * 14
                + _mapBox.RectSize / 2;

            var tile = new TextureRect
            {
                Texture = hex.BiomeType.MapHex
            };
            _mapBox.AddChild(tile);
            tile.RectPosition = hexPos;
            
            _hexes.Add(hex, tile);

            if (hex.Pops.Any(p => p.Group == group))
            {
                var popTile = new TextureRect
                {
                    Texture = hex.Pops.Where(p => p.Group == group).First().Species.PopIconSmall,
                    RectScale = new Vector2(0.75f, 0.75f),
                };
                _mapBox.AddChild(popTile);
                popTile.RectPosition = hexPos;
            }
        }
    }

    public void TallyPops()
    {
        var toGo = 0;
        var toStay = 0;

        foreach(var child in _popSelectVBox.GetChildren())
        {
            if(child is SplitSettlePopInfoRow row && row.Pop != null)
            {
                if(row.Pressed)
                {
                    toGo += 1;
                }
                else
                {
                    toStay += 1;
                }

                if (_hexes.ContainsKey(row.Pop.Location))
                {
                    if(row.Pressed)
                    {
                        _hexes[row.Pop.Location].Texture = MapHexSelect;
                    }
                    else
                    {
                        _hexes[row.Pop.Location].Texture = row.Pop.Location.BiomeType.MapHex;
                    }
                }
            }
        }

        _fromPopsText.Text = toStay.ToString();
        _toPopsText.Text = toGo.ToString();

        _okButton.Disabled = toGo == 0
            || toStay == 0
            || string.IsNullOrWhiteSpace(_toText.Text);

        _cost = toGo * -5;

        _cost = _legitimacyHelper.CostForSplitting(toGo);

        _costDisplay.SetSource(
            true,
            _cost,
            _legitimacyHelper.CostForSplittingDetails(toGo));
    }

    public void TallyRegions()
    {
        _regionsToClaim.Clear();
        
        foreach (var child in _popSelectVBox.GetChildren())
        {
            if (child is SplitSettlePopInfoRow row && row.Region != null)
            {
                if (row.Pressed)
                {
                    _regionsToClaim.Add(row.Region);
                }

                foreach (var hex in row.Region.Hexes)
                {
                    if (_hexes.ContainsKey(hex))
                    {
                        if (row.Pressed)
                        {
                            _hexes[hex].Texture = MapHexSelect;
                        }
                        else
                        {
                            _hexes[hex].Texture = hex.BiomeType.MapHex;
                        }
                    }
                }
            }
        }

        _okButton.Disabled = _regionsToClaim.Count < 1;
        
        _cost = _legitimacyHelper.CostForSettling(_group, _regionsToClaim);

        _costDisplay.SetSource(
            true,
            _cost,
            _legitimacyHelper.CostForSettlingDetails(_group, _regionsToClaim));
    }

    public void Close()
    {
        Visible = false;
        this.FindParent<AdminGroupDetailPopup>().ShowFor(_group);
    }

    public void HighlightPop(Pop pop)
    {
        if(_hexes.ContainsKey(pop.Location))
        {
            _hexes[pop.Location].Texture = MapHexHighlight;
        }
    }

    public void HighlightRegion(WorldRegion region)
    {
        foreach (var hex in region.Hexes)
        {
            if (_hexes.ContainsKey(hex))
            {
                _hexes[hex].Texture = MapHexHighlight;
            }
        }
    }

    public void UnHighlightPop(Pop pop)
    {
        if (_hexes.ContainsKey(pop.Location))
        {
            if(_popToRow[pop].Pressed)
            {
                _hexes[pop.Location].Texture = MapHexSelect;
            }
            else
            {
                _hexes[pop.Location].Texture = pop.Location.BiomeType.MapHex;
            }
        }
    }

    public void UnHighlightRegion(WorldRegion region)
    {
        foreach (var hex in region.Hexes)
        {
            if (_hexes.ContainsKey(hex))
            {
                if (_regionToRow[region].Pressed)
                {
                    _hexes[hex].Texture = MapHexSelect;
                }
                else
                {
                    _hexes[hex].Texture = hex.BiomeType.MapHex;
                }
            }
        }
    }

    public void OnOk()
    {
        // TODO deduct cost from legitimacy

        if (_mode == MODE_SPLIT)
        {
            var newGroup = new AdministrativeGroup(_group.Owner, _toText.Text, false);
            _group.Owner.AdministrativeGroups.Add(newGroup);

            foreach (var pop in _popToRow.Keys)
            {
                if (_popToRow[pop].Pressed)
                {
                    pop.Group = newGroup;
                    newGroup.Pops.Add(pop);
                    _group.Pops.Remove(pop);
                }
            }

            _eventBus.SendEvent(new AdminGroupChangedSimulationEvent(newGroup));
        }
        else if(_mode == MODE_SETTLE)
        {
            foreach(var region in _regionsToClaim)
            {
                region.ClaimedBy = _group;
                _group.ClaimedRegions.Add(region);
            }

            _group.IsSettled = true;
            _eventBus.SendEvent(new AdminGroupChangedSimulationEvent(_group));
        }
        else if (_mode == MODE_EXPAND)
        {
            foreach (var region in _regionsToClaim)
            {
                region.ClaimedBy = _group;
                _group.ClaimedRegions.Add(region);
            }

            _eventBus.SendEvent(new AdminGroupChangedSimulationEvent(_group));
        }
        Close();
    }
}
