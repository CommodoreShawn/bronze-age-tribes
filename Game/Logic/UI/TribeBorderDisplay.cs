using BronzeAgeTribes.Logic;
using BronzeAgeTribes.Logic.Data;
using BronzeAgeTribes.Logic.Data.Game;
using BronzeAgeTribes.Logic.Infrastructure.Interfaces;
using BronzeAgeTribes.Logic.Infrastructure.SimulationEvents;
using Godot;
using System;
using System.Linq;

public class TribeBorderDisplay : MeshInstance
{
    [Inject]
    public ISimulationEventBus _simulationEventBus;
    [Inject]
    public IWorldManager _worldManager;
    [Inject]
    public IWorldRenderer _worldRenderer;

    [Export]
    public SpatialMaterial Material;

    private AdministrativeGroup _group;

    public void InitializeFor(AdministrativeGroup group)
    {
        _group = group;
    }

    public override void _Ready()
    {
        this.ResolveDependencies();
        _simulationEventBus.OnSimulationEvent += OnSimulationEvent;

        RebuildMesh();
    }

    public override void _ExitTree()
    {
        base._ExitTree();
        _simulationEventBus.OnSimulationEvent -= OnSimulationEvent;
    }

    private void OnSimulationEvent(ISimulationEvent evt)
    {
        if (evt is TurnAdvancedSimulationEvent && IsInstanceValid(this))
        {
            RebuildMesh();
        }
    }

    private void RebuildMesh()
    {
        WorldHex[] hexes;
        if (_group.IsSettled)
        {
            hexes = _group.ClaimedRegions
                .SelectMany(r => r.Hexes)
                .ToArray();
        }
        else
        {
            var radius = (int)(_group.Owner.TribeCoherency * 2);

            hexes = Enumerable.Range(-radius, 2 * radius)
                .SelectMany(x => Enumerable.Range(-radius, 2 * radius)
                    .Select(y => new HexPosition(_group.GroupCenter.HexX + x, _group.GroupCenter.HexY + y)))
                .Where(p => p.DistanceTo(_group.GroupCenter) < _group.Owner.TribeCoherency * Constants.VERTICIES_PER_HEX)
                .Select(p => _worldManager.GetHex(p))
                .Where(h => h != null)
                .ToArray();
        }

        Mesh = _worldRenderer.MakeBorderLine(hexes, 0.5f, 0.7f, _group.Owner.PrimaryColor, false);
        MaterialOverride = Material;
    }
}
