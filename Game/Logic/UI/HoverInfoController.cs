using Autofac;
using BronzeAgeTribes.Logic.Infrastructure;
using BronzeAgeTribes.Logic.Infrastructure.Interfaces;
using Godot;
using System.Linq;

public class HoverInfoController : Control
{
    private Label _regionLabel;
    private Label _terrainLabel;
    private Label _biomeLabel;
    private Label _featureLabel;
    private ILocalPlayerInfo _localPlayerInfo;

    public override void _Ready()
    {
        _regionLabel = GetNode<Label>("GridContainer/RegionLabel");
        _terrainLabel = GetNode<Label>("GridContainer/TerrainLabel");
        _biomeLabel = GetNode<Label>("GridContainer/BiomeLabel");
        _featureLabel = GetNode<Label>("GridContainer/FeatureLabel");
        _localPlayerInfo = IocManager.IocContainer.Resolve<ILocalPlayerInfo>();
    }

    public override void _Input(InputEvent @event)
    {
        base._Input(@event);

        var mousePos = GetViewport().GetMousePosition();

        RectPosition = new Vector2(
            mousePos.x + 10,
            mousePos.y);
    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        Visible = _localPlayerInfo.HoveredHex != null;

        if(_localPlayerInfo.HoveredHex != null)
        {
            _regionLabel.Text = _localPlayerInfo.HoveredHex.Region.Name;
            _terrainLabel.Text = _localPlayerInfo.HoveredHex.Terrain.Name;
            _biomeLabel.Text = $"{_localPlayerInfo.HoveredHex.BiomeType.Name} d: {_localPlayerInfo.HoveredHex.Region.Drainage:0.##} r: {_localPlayerInfo.HoveredHex.Region.Rainfall:0.##} t: {_localPlayerInfo.HoveredHex.Region.Temperature:0.##}";
            _featureLabel.Text = string.Join("\n", _localPlayerInfo.HoveredHex.Features.Select(f => f.BackingType.Name));
        }
    }
}
