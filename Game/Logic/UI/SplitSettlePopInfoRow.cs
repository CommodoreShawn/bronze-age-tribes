using BronzeAgeTribes.Logic.Data;
using BronzeAgeTribes.Logic.Data.Game;
using BronzeAgeTribes.Logic.Infrastructure.Interfaces;
using Godot;

public class SplitSettlePopInfoRow : Button
{
    public Pop Pop { get; private set; }
    public WorldRegion Region { get; private set; }

    [Inject]
    private ITooltipManager _tooltipManager;

    public void InitializeFor(Pop pop)
    {
        Pop = pop;
        GetNode<TextureRect>("SplitSettlePopInfoRow/TextureRect").Texture = Pop.Species.PopIconSmall;
        GetNode<Label>("SplitSettlePopInfoRow/Label").Text = $"{Pop.Culture.Name} {Pop.Job?.JobType?.Title}";
    }
    public void InitializeFor(WorldRegion region)
    {
        Region = region;
        GetNode<TextureRect>("SplitSettlePopInfoRow/TextureRect").Texture = region.TerrainType.InteractButtonIcon;
        GetNode<Label>("SplitSettlePopInfoRow/Label").Text = region.Name;
    }

    public override void _Ready()
    {
        this.ResolveDependencies();
    }

    public void MouseEntered()
    {
        if (Pop != null)
        {
            _tooltipManager.ShowTooltipFor(this, Pop);
            this.FindParent<SplitOrSettleContainer>()?.HighlightPop(Pop);
        }
        else if (Region != null)
        {
            _tooltipManager.ShowTooltipFor(this, Region);
            this.FindParent<SplitOrSettleContainer>()?.HighlightRegion(Region);
        }
    }

    public void MouseExited()
    {
        if (Pop != null)
        {
            _tooltipManager.HideTooltipFor(this);
            this.FindParent<SplitOrSettleContainer>()?.UnHighlightPop(Pop);
        }
        else if (Region != null)
        {
            _tooltipManager.HideTooltipFor(this);
            this.FindParent<SplitOrSettleContainer>()?.UnHighlightRegion(Region);
        }
    }

    public void OnPressed()
    {
        if (Pop != null)
        {
            this.FindParent<SplitOrSettleContainer>()?.TallyPops();
        }
        else if (Region != null)
        {
            this.FindParent<SplitOrSettleContainer>()?.TallyRegions();
        }
    }
}
