using Autofac;
using BronzeAgeTribes.Logic.Infrastructure;
using BronzeAgeTribes.Logic.Infrastructure.Interfaces;
using Godot;
using System.Linq;

public class GameMenuPopup : Popup
{
    [Inject]
    private IWorldSerializer _worldSerializer;

    public override void _Ready()
    {
        base._Ready();
        this.ResolveDependencies();
    }

    public override void _Input(InputEvent @event)
    {
        base._Input(@event);

        if (Visible
            && @event is InputEventKey keyEvt
            && keyEvt.Scancode == (uint)KeyList.Escape)
        {
            GetTree().SetInputAsHandled();
            ReturnToGameplay();
        }
    }

    public void InitializeAndShow()
    {
        Visible = true;
        GetNode<Button>("PanelContainer/GridContainer/LoadButton").Disabled = !_worldSerializer.ListSaveFiles().Any();
    }

    public void ReturnToGameplay()
    {
        Visible = false;
    }

    public void QuitToMenu()
    {
        this.FindParent<GameplayWorld>().ShowProgressModal(
            "Saving",
            report =>
            {
                _worldSerializer.Autosave(report);
            },
            () =>
            {
                GetTree().Root.GetNode<StartupController>("Startup").ShowStartupMenu();
                GetTree().Root.GetNode<Control>("Startup").Visible = true;
                IocManager.IocContainer.Resolve<ISoundManager>().SetMenuMusic();
                this.FindParent<GameplayWorld>().QueueFree();
            });
    }

    public void QuitGame()
    {
        this.FindParent<GameplayWorld>().ShowProgressModal(
            "Saving",
            report =>
            {
                _worldSerializer.Autosave(report);
            },
            () =>
            {
                GetTree().Quit();
            });
    }

    public void LoadGame()
    {
        GetParent().GetNode<LoadWorldController>("LoadPanel").InitalizeAndShow();
    }

    public void SaveGame()
    {
        GetParent().GetNode<SaveWorldController>("SavePanel").InitalizeAndShow();
    }

    public void ShowSettings()
    {
        GetParent().GetNode<SettingsPanel>("SettingsPanel").InitalizeAndShow();
    }
}
