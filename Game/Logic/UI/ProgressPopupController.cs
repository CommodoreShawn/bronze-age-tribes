using Autofac;
using BronzeAgeTribes.Logic.Infrastructure;
using BronzeAgeTribes.Logic.Infrastructure.Interfaces;
using Godot;
using System;
using System.Threading.Tasks;

public class ProgressPopupController : Popup
{
    private Label _overallLabel;
    private ProgressBar _progressBar;
    private Label _detailLabel;
    private Task _workTask;
    private Action _whenDone;
    private ILogger _logger;

    public override void _Ready()
    {
        _overallLabel = GetNode<Label>("PanelContainer/GridContainer/OverallLabel");
        _progressBar = GetNode<ProgressBar>("PanelContainer/GridContainer/ProgressBar");
        _detailLabel = GetNode<Label>("PanelContainer/GridContainer/DetailLabel");
        _logger = IocManager.IocContainer.Resolve<ILogger>();
    }

    public void StartTask(
        string taskName, 
        Action<ReportProgress> work, 
        Action whenDone)
    {
        _overallLabel.Text = taskName;
        _whenDone = whenDone;
        ReportProgress reportDelegate = (progress, stepName) =>
        {
            _progressBar.Value = progress * 100;
            _detailLabel.Text = stepName;
        };

        Visible = true;

        _workTask = Task.Run(() =>
        {
            try
            {
                work(reportDelegate);
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "When running: " + taskName + "\n" + ex.StackTrace);
                Visible = false;
            }
        });
    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        if (_workTask != null && _workTask.IsCompleted)
        {
            _workTask = null;

            _whenDone?.Invoke();

            Visible = false;

        }
    }
}
