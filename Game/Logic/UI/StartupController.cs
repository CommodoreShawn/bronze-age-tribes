using BronzeAgeTribes.Logic.Data.Serialization;
using BronzeAgeTribes.Logic.Infrastructure;
using BronzeAgeTribes.Logic.Infrastructure.Interfaces;
using Godot;
using System;
using System.Linq;

public class StartupController : Control
{
    private SaveSummary _latestSave;

    [Inject]
    private IWorldSerializer _worldSerializer;
    [Inject]
    private ISoundManager _soundManager;

    public StartupController()
    {
        IocManager.Initialize();

        AppDomain.CurrentDomain.UnhandledException += UnhandledExceptionHandler;
    }

    private void UnhandledExceptionHandler(object sender, UnhandledExceptionEventArgs e)
    {
        GD.PrintErr($"Unhandled  {e.ExceptionObject.GetType().Name}: {(e.ExceptionObject as Exception)?.Message}");
        GD.PrintErr((e.ExceptionObject as Exception)?.StackTrace);
    }

    public override void _Ready()
    {
        base._Ready();
        this.ResolveDependencies();

        ShowStartupMenu();
    }

    public void ShowWorldGenMenu()
    {
        GetNode<Control>("WorldgenPanel").Visible = true;
        GetNode<Control>("StartupPanel").Visible = false;
    }

    public void ShowStartupMenu()
    {
        _latestSave = _worldSerializer.ListSaveFiles()
            .OrderByDescending(s => s.LastPlayed)
            .FirstOrDefault();

        if (_latestSave != null)
        {
            GetNode<Label>("StartupPanel/GridContainer/ContinueLabel").Text = _latestSave.PlayerEmpire;
            GetNode<Button>("StartupPanel/GridContainer/ContinueButton").Disabled = false;
            GetNode<Button>("StartupPanel/GridContainer/LoadWorldButton").Disabled = false;
        }
        else
        {
            GetNode<Label>("StartupPanel/GridContainer/ContinueLabel").Text = string.Empty;
            GetNode<Button>("StartupPanel/GridContainer/ContinueButton").Disabled = true;
            GetNode<Button>("StartupPanel/GridContainer/LoadWorldButton").Disabled = true;
        }


        _soundManager.SetMenuMusic();
        GetNode<Control>("StartupPanel").Visible = true;
        GetNode<Control>("WorldgenPanel").Visible = false;
    }

    public void ShowProgressModal(string taskName, Action<ReportProgress> work, Action whenDone)
    {
        GetNode<Control>("StartupPanel").Visible = false;
        GetNode<Control>("WorldgenPanel").Visible = false;
        GetNode<ProgressPopupController>("ProgressPopup").StartTask(taskName, work, whenDone);
    }

    public void ShowLoadMenu()
    {
        GetNode<LoadWorldController>("LoadPanel").InitalizeAndShow();
    }

    public void ShowSettings()
    {
        GetNode<SettingsPanel>("SettingsPanel").InitalizeAndShow();
    }

    public void OnExitButton()
    {
        GetTree().Quit();
    }

    public void LoadLatestSave()
    {
        LoadSave(_latestSave);
    }

    public void LoadSave(SaveSummary saveToLoad)
    {
        ShowProgressModal(
            "Loading World",
            report =>
            {
                _worldSerializer.LoadWorld(report, saveToLoad);
            },
            () =>
            {
                GetTree().Root.GetNode<Control>("Startup").Visible = false;
                GetTree().Root.AddChild(ResourceLoader.Load<PackedScene>("res://Scenes/Gameplay.tscn").Instance());
            });
    }
}
