using BronzeAgeTribes.Logic.Data.Game;
using BronzeAgeTribes.Logic.Infrastructure.Interfaces;
using BronzeAgeTribes.Logic.Infrastructure.SimulationEvents;
using Godot;

public class GroupOutlinerElementController : Control
{
    public AdministrativeGroup Group { get; private set; }

    [Export]
    public Texture SettlementIcon;

    [Export]
    public Texture TribeIcon;

    [Inject]
    private ISimulationEventBus _eventBus;

    public void InitializeFor(AdministrativeGroup group)
    {
        Group = group;

        if (Group.IsSettled)
        {
            GetNode<Button>("TopRow/DetailsButton").Icon = SettlementIcon;
        }
        else
        {
            GetNode<Button>("TopRow/DetailsButton").Icon = TribeIcon;
        }
        GetNode<Button>("TopRow/DetailsButton").Text = group.Name;

        foreach (var child in GetNode<HBoxContainer>("BottomRow").GetChildren())
        {
            (child as ResourceDisplayController).SetSource(group);
        }
    }

    public override void _Ready()
    {
        this.ResolveDependencies();
        _eventBus.OnSimulationEvent += OnSimulationEvent;
    }

    private void OnSimulationEvent(ISimulationEvent evt)
    {
        Godot.GD.Print("OnSimulationEvent: GroupOutlinerElementController");
        if (evt is AdminGroupChangedSimulationEvent groupEvt && groupEvt.Group == Group)
        {
            InitializeFor(Group);
        }
    }

    public void ViewGroup()
    {
        var uiController = this.FindParent<OutlinerPanelController>();

        if(uiController != null)
        {
            uiController.LookAt(Group.GroupCenter);
        }
    }

    public void ShowGroupDetails()
    {
        this.FindParent<GameplayWorld>().ShowGroupDetailsModal(Group);
    }
}
