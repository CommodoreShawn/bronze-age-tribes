using BronzeAgeTribes.Logic;
using BronzeAgeTribes.Logic.Data;
using BronzeAgeTribes.Logic.Data.Game;
using BronzeAgeTribes.Logic.Infrastructure.Interfaces;
using BronzeAgeTribes.Logic.Infrastructure.SimulationEvents;
using Godot;
using System;
using System.Collections.Generic;
using System.Linq;

public class PopManagementController : Node
{
    [Export]
    public PackedScene HexJobWidgetController;

    [Inject]
    private IWorldManager _worldManager;
    [Inject]
    private ILocalPlayerInfo _localPlayerInfo;
    [Inject]
    private IPopManager _popManager;
    [Inject]
    private ISimulationEventBus _simulationEventBus;
    [Inject]
    private IExplorationTracker _explorationTracker;

    public List<PopJobPortraitController> HoveredJobs { get; private set; }
    public List<Pop> HoveredPops { get; private set; }

    private Pop _movingPop;
    private TextureRect _dragTexture;
    private Dictionary<WorldHex, HexJobWidgetController> _jobWidgetsByHex;

    public override void _Ready()
    {
        HoveredJobs = new List<PopJobPortraitController>();
        HoveredPops = new List<Pop>();
        _jobWidgetsByHex = new Dictionary<WorldHex, HexJobWidgetController>();
        this.ResolveDependencies();

        AddWidgetsForPops();

        _simulationEventBus.OnSimulationEvent += OnSimulationEvent;
    }

    public override void _ExitTree()
    {
        base._ExitTree();

        _simulationEventBus.OnSimulationEvent -= OnSimulationEvent;
    }

    private void OnSimulationEvent(ISimulationEvent evt)
    {
        Godot.GD.Print("OnSimulationEvent: PopManagementController");
        if (evt is TurnAdvancedSimulationEvent)
        {
            CleanupMoveSlots();

            AddWidgetsForPops();
        }
    }

    private void AddWidgetsForPops()
    {
        var distinctHexes = _worldManager.Empires
            .SelectMany(e => e.AdministrativeGroups.SelectMany(g => g.Pops))
            .Select(p => p.Location)
            .Distinct()
            .Where(h => _explorationTracker.IsVisible(h.Position))
            .ToArray();

        foreach (var hex in distinctHexes)
        {
            if (!_jobWidgetsByHex.ContainsKey(hex))
            {
                var prefab = HexJobWidgetController.Instance() as HexJobWidgetController;
                prefab.InitializeFor(hex);
                AddChild(prefab);
                _jobWidgetsByHex.Add(hex, prefab);
            }
            else
            {
                _jobWidgetsByHex[hex].Reinitialize();
            }
        }
    }

    public override void _Input(InputEvent @event)
    {
        base._Input(@event);

        if (@event is InputEventMouseButton btnEvt)
        {
            if (HoveredJobs.Any() && btnEvt.Pressed)
            {
                btnEvt.Pressed = false;
            }
        }

        if (@event is InputEventMouseMotion
            && Input.IsMouseButtonPressed(1)
            && (HoveredJobs.Any(j => j.Job.Worker != null && j.Job.Worker.Owner == _localPlayerInfo.Empire)
                || HoveredPops.Any(p => p.Owner == _localPlayerInfo.Empire))
            && _movingPop == null)
        {
            if (HoveredJobs.Any(j => j.Job.Worker != null && j.Job.Worker.Owner == _localPlayerInfo.Empire))
            {
                var sourceJobWidget = HoveredJobs.First(j => j.Job.Worker != null && j.Job.Worker.Owner == _localPlayerInfo.Empire);
                _movingPop = sourceJobWidget.Job.Worker;
                EnableMoveSlots(sourceJobWidget.Job.Location, _movingPop);
            }
            else
            {
                _movingPop = HoveredPops.First(p => p.Owner == _localPlayerInfo.Empire);
                EnableMoveSlots(_movingPop.Location, _movingPop);
            }

            _dragTexture = new TextureRect
            {
                Texture = _movingPop.Species.PopIcon,
                RectScale = new Vector2(0.6f, 0.6f)
            };
            AddChild(_dragTexture);
        }
    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        if (_movingPop != null)
        {
            _dragTexture.RectPosition = GetViewport().GetMousePosition() - new Vector2(32, 0);

            if (!Input.IsMouseButtonPressed(1))
            {
                var newJob = HoveredJobs
                    .Where(j => j.Job.Worker == null)
                    .Select(j => j.Job)
                    .FirstOrDefault();

                var fromHex = _movingPop.Location;
                if (_movingPop.MovingFromJob != null)
                {
                    fromHex = _movingPop.MovingFromJob.Location;
                }

                if (newJob != null && _popManager.CanPopBeMoved(fromHex, newJob.Location, _movingPop))
                {
                    _popManager.MovePop(_movingPop, newJob);
                }
                else
                {
                    var popToInteract = HoveredJobs
                        .Select(j => j.Job.Worker)
                        .Where(p => p != null && p.Owner != _movingPop.Owner)
                        .Where(p => fromHex.Neighbors.Contains(p.Location))
                        .FirstOrDefault();

                    if (popToInteract != null)
                    {
                        GetParent<GameplayWorld>().GetNode<PopInteractionPopup>("PopInteractionPopup").Initialize(_movingPop, popToInteract);
                    }
                }

                _movingPop = null;
                _dragTexture.QueueFree();
                _dragTexture = null;
                CleanupMoveSlots();
                HoveredJobs.Clear();
            }
        }
    }

    private void EnableMoveSlots(WorldHex fromHex, Pop movingPop)
    {
        if(movingPop.MovingFromJob != null)
        {
            fromHex = movingPop.MovingFromJob.Location;
        }

        var range = movingPop.Species.PopMovementDistance;

        var hexes = Enumerable.Range(-range, range * 2)
            .SelectMany(i => Enumerable.Range(-range, range * 2).Select(j => Tuple.Create(i, j)))
            .Select(t => _worldManager.GetHexAtWorldPoint(fromHex.Position.RealX + t.Item1 * Constants.VERTICIES_PER_HEX, fromHex.Position.RealY + t.Item2 * Constants.VERTICIES_PER_HEX))
            .Where(h => h != null)
            .Distinct()
            .Where(h => !_jobWidgetsByHex.ContainsKey(h))
            .Where(h => h.Position.DistanceTo(fromHex.Position) < range * Constants.VERTICIES_PER_HEX)
            .Where(h => _popManager.CanPopBePlaced(h, movingPop))
            .ToArray();

        foreach (var hex in hexes)
        {
            var prefab = HexJobWidgetController.Instance() as HexJobWidgetController;
            prefab.InitializeFor(hex);
            AddChild(prefab);
            _jobWidgetsByHex.Add(hex, prefab);
        }

        foreach(var hex in fromHex.Neighbors)
        {
            if (hex != null 
                && _jobWidgetsByHex.ContainsKey(hex)
                && hex.Jobs.Any(j => j.Worker != null && j.Worker.Owner != movingPop.Owner))
            {
                _jobWidgetsByHex[hex].SetForInteractions();
            }
        }
    }

    public void ClearInteractionTarget()
    {
        foreach(var value in _jobWidgetsByHex.Values)
        {
            value.ClearInteractionTarget();
        }
    }

    public void ShowInteractionTarget(Pop target)
    {
        if(target.Location != null && _jobWidgetsByHex.ContainsKey(target.Location))
        {
            _jobWidgetsByHex[target.Location].ShowInteractionTarget(target);
        }
    }

    private void CleanupMoveSlots()
    {
        foreach (var hex in _jobWidgetsByHex.Keys.ToArray())
        {
            if (!hex.Jobs.Any(j => j.Worker != null || j.MovingOutWorker != null)
                || !_explorationTracker.IsVisible(hex.Position))
            {
                _jobWidgetsByHex[hex].QueueFree();
                _jobWidgetsByHex.Remove(hex);
            }
            else if(hex.Jobs.Any(j=>j.Worker != null && j.Worker.Owner != _localPlayerInfo.Empire))
            {
                _jobWidgetsByHex[hex].ClearForInteractions();
            }
        }
    }
}
