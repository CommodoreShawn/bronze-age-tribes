using BronzeAgeTribes.Logic.Data;
using BronzeAgeTribes.Logic.Data.Game;
using BronzeAgeTribes.Logic.Infrastructure.Interfaces;
using Godot;
using System;
using System.Collections.Generic;
using System.Linq;

public class ResourceDisplayController : Control
{
    [Inject]
    private ITooltipManager _tooltipController;

    public AdministrativeGroup SourceGroup;
    public Empire SourceEmpire;

    private Label _text;

    private float _previousValue;
    private bool _showAsStockpile;
    private Func<float> _getValue;
    private Func<IEnumerable<string>> _getDetails;

    [Export]
    public ResourceType Resource;

    [Export]
    public Texture Texture;

    [Export]
    public bool AlwaysShow;

    [Export]
    public int SignificantFigures;

    public override void _Ready()
    {
        this.ResolveDependencies();
        _text = GetNode<Label>("Text");
        GetNode<TextureRect>("Icon").Texture = Texture;
        _previousValue = float.MinValue;
    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        var newValue = _getValue?.Invoke() ?? 0;

        if(newValue != _previousValue)
        {
            if (_showAsStockpile)
            {
                _text.Text = newValue.ToString("N" + SignificantFigures);
            }
            else if (newValue >= 0)
            {
                _text.Text = "+" + newValue.ToString("N" + SignificantFigures);
            }
            else
            {
                _text.Text = newValue.ToString("N" + SignificantFigures);
            }

            _previousValue = newValue;

            Visible = newValue != 0 || AlwaysShow;
        }
    }

    public void OnMouseEntered()
    {
        string details = null;

        if(_getDetails != null)
        {
            if (_previousValue >= 0)
            {
                details = "+" + _previousValue.ToString("N1");
            }
            else
            {
                details = _previousValue.ToString("N1");
            }

            details = details + "\n" + string.Join("\n", (_getDetails() ?? Enumerable.Empty<string>()).Select(d => "   " + d));
        }

        _tooltipController.ShowTooltipFor(
            this,
            Resource,
            _previousValue,
            details);
    }

    public void OnMouseExited()
    {
        _tooltipController.HideTooltipFor(this);
    }

    public void SetSource(WorldHex hex)
    {
        _showAsStockpile = false;
        _getValue = () => hex.GetExpectedOutput(Resource);
        _getDetails = () => hex.GetProductionDetails(Resource);
    }

    public void SetSource(PopJob popJob)
    {
        _showAsStockpile = false;
        _getValue = () => popJob.GetExpectedOutput(Resource, true);
        _getDetails = () => null;
    }

    public void SetSource(AdministrativeGroup group)
    {
        _showAsStockpile = false;
        _getValue = () => group.GetExpectedOutput(Resource);
        _getDetails = () => group.GetProductionDetails(Resource);
    }

    public void SetSource(CellFeature feature)
    {
        _showAsStockpile = false;
        _getValue = () => feature.BackingType.GetExpectedOutput(Resource);
        _getDetails = null;
    }

    public void SetSource(Development development)
    {
        _showAsStockpile = false;
        _getValue = () => development.BackingType.GetExpectedOutput(Resource);
        _getDetails = null;
    }

    public void SetSource(Building building)
    {
        _showAsStockpile = false;
        _getValue = () => building.BackingType.GetExpectedOutput(Resource);
        _getDetails = null;
    }

    public void SetSource(InteractionOption interactionOption, WorldHex hex)
    {
        _showAsStockpile = false;
        _getValue = () => interactionOption.GetExpectedOutput(Resource, hex);
        _getDetails = null;
    }

    public void SetSource(DevelopmentType developmentType, WorldHex hex, bool duringConstruction)
    {
        _showAsStockpile = false;

        if(duringConstruction)
        {
            _getValue = () => developmentType.ConstructionInteraction.InteractionJob.GetExpectedOutput(Resource, hex);
        }
        else
        {
            _getValue = () => developmentType.ProvidedJobs.Sum(j => j.GetExpectedOutputWithDevelopment(Resource, hex, developmentType));
        }
        _getDetails = null;
    }

    public void SetSource(BuildingType buildingType, WorldHex hex, bool duringConstruction)
    {
        _showAsStockpile = false;

        if (duringConstruction)
        {
            _getValue = () => buildingType.ConstructionInteraction.InteractionJob.GetExpectedOutput(Resource, hex);
        }
        else
        {
            _getValue = () => buildingType.ProvidedJobs.Sum(j => j.GetExpectedOutputWithBuilding(Resource, hex, buildingType));
        }
        _getDetails = null;
    }

    public void SetSource(
        bool showAsStockpile,
        float value,
        IEnumerable<string> details)
    {
        _showAsStockpile = showAsStockpile;
        _getValue = () => value;
        _getDetails = () => details;
    }
}
