using Autofac;
using BronzeAgeTribes.Logic.Data;
using BronzeAgeTribes.Logic.Infrastructure;
using BronzeAgeTribes.Logic.Infrastructure.Interfaces;
using Godot;

public class WorldgenController : Control
{
    [Export]
    public WorldType[] WorldTypes;

    [Export]
    public StartingCulture[] StartingCultures;

    private WorldType _selectedWorldType;
    private StartingCulture _selectedStartingCulture;
    private TextEdit _descriptionText;
    private TextEdit _cultureText;
    private GridContainer _traitGrid;
    private StartupController _startupController;
    private TextEdit _seedText;
    private TextEdit _empireText;
    private TextEdit _tribeText;
    private INameSource _nameSource;
    private ColorPickerButton _primaryColorPicker;
    private ColorPickerButton _secondaryColorPicker;


    public override void _Ready()
    {
        base._Ready();

        _startupController = GetParent<StartupController>();
        _nameSource = IocManager.IocContainer.Resolve<INameSource>();

        var worldTypeList = GetNode<ItemList>("RootGrid/ContentGrid/ListGrid/WorldTypeList");
        foreach (var worldType in WorldTypes)
        {
            worldTypeList.AddItem(worldType.FriendlyName, null, true);
        }

        var startingCultureList = GetNode<ItemList>("RootGrid/ContentGrid/ListGrid/StartingCultureList");
        foreach (var culture in StartingCultures)
        {
            startingCultureList.AddItem(culture.Name, null, true);
        }

        _descriptionText = GetNode<TextEdit>("RootGrid/ContentGrid/DetailGrid/DescriptionText");
        _cultureText = GetNode<TextEdit>("RootGrid/ContentGrid/DetailGrid/CultureDescriptionText");
        _traitGrid = GetNode<GridContainer>("RootGrid/ContentGrid/ParamsGrid/TraitsContainer");
        _seedText = GetNode<TextEdit>("RootGrid/ContentGrid/ParamsGrid/SeedRow/SeedText");
        _empireText = GetNode<TextEdit>("RootGrid/ContentGrid/ParamsGrid/EmpireRow/EmpireText");
        _tribeText = GetNode<TextEdit>("RootGrid/ContentGrid/ParamsGrid/TribeRow/TribeText");
        _primaryColorPicker = GetNode<ColorPickerButton>("RootGrid/ContentGrid/ParamsGrid/EmpireColorRow/PrimaryColorPicker");
        _secondaryColorPicker = GetNode<ColorPickerButton>("RootGrid/ContentGrid/ParamsGrid/EmpireColorRow/SecondaryColorPicker");
        _seedText.Text = new System.Random().Next().ToString();

        worldTypeList.Select(0);
        OnWorldTypeSelected(0);

        startingCultureList.Select(0);
        OnStartingCultureSelected(0);
        RandomizeEmpireColor();
    }

    public void OnWorldTypeSelected(int itemSelected)
    {
        if (itemSelected >= 0 && itemSelected < WorldTypes.Length)
        {
            _selectedWorldType = WorldTypes[itemSelected];
            _descriptionText.Text = _selectedWorldType.Description;
        }
        else
        {
            _selectedWorldType = null;
            _descriptionText.Text = string.Empty;
        }
    }

    public void OnStartingCultureSelected(int itemSelected)
    {
        if (itemSelected >= 0 && itemSelected < StartingCultures.Length)
        {
            _selectedStartingCulture = StartingCultures[itemSelected];
            _cultureText.Text = _selectedStartingCulture.Description;
            var traits = _traitGrid.GetChildren();
            for(var i = 0; i < traits.Count; i++)
            {
                (traits[i] as Control).QueueFree();
            }

            var prefab = ResourceLoader.Load<PackedScene>("res://Scenes/Ui/TraitIcon.tscn");
            foreach (var trait in _selectedStartingCulture.StartingSpecies.Traits)
            {
                var icon = prefab.Instance() as TraitIconController;
                _traitGrid.AddChild(icon);
                icon.InitializeFor(trait);
            }
            foreach (var trait in _selectedStartingCulture.Traits)
            {
                var icon = prefab.Instance() as TraitIconController;
                _traitGrid.AddChild(icon);
                icon.InitializeFor(trait);
            }

            RandomizeTribeName();
            RandomizeEmpireName();
        }
        else
        {
            _selectedWorldType = null;
            _descriptionText.Text = string.Empty;
        }
    }

    public void GeneratePreview()
    {
        var parameters = new WorldParameters
        {
            Seed = _seedText.Text,
            RainDistanceScale = _selectedWorldType.RainDistanceScale,
            BiomeMappings = _selectedWorldType.BiomeMappings,
            RainSources = _selectedWorldType.RainSources,
            TargetRegionSize = _selectedWorldType.TargetRegionSize,
            TerrainTypes = _selectedWorldType.TerrainTypes,
            WorldRadius = _selectedWorldType.WorldRadius,
            RiverFeature = _selectedWorldType.RiverCellFeature,
            RiverAdditionalFeatureChance = _selectedWorldType.RiverAdditionalFeatureChance,
            RiverAdditionalFeatures = _selectedWorldType.RiverAdditionalFeatures,
            Creeps = _selectedWorldType.Creeps
        };
        
        var worldGenerator = IocManager.IocContainer.Resolve<IWorldGenerator>();
        
        _startupController.ShowProgressModal(
            "Generating Preview",
            report =>
            {
                worldGenerator.GenerateWorld(parameters, report);
                worldGenerator.PopulateWorld(
                    parameters, 
                    _selectedStartingCulture, 
                    _empireText.Text, 
                    _tribeText.Text, 
                    _primaryColorPicker.Color,
                    _secondaryColorPicker.Color,
                    report);
            },
            () =>
            {
                _startupController.ShowWorldGenMenu();
                GetTree().Root.GetNode<Control>("Startup").Visible = false;
                GetTree().Root.AddChild(ResourceLoader.Load<PackedScene>("res://Scenes/WorldPreview.tscn").Instance());
            });
    }

    public void GenerateWorld()
    {
        var parameters = new WorldParameters
        {
            Seed = _seedText.Text,
            RainDistanceScale = _selectedWorldType.RainDistanceScale,
            BiomeMappings = _selectedWorldType.BiomeMappings,
            RainSources = _selectedWorldType.RainSources,
            TargetRegionSize = _selectedWorldType.TargetRegionSize,
            TerrainTypes = _selectedWorldType.TerrainTypes,
            WorldRadius = _selectedWorldType.WorldRadius,
            RiverFeature = _selectedWorldType.RiverCellFeature,
            RiverAdditionalFeatureChance = _selectedWorldType.RiverAdditionalFeatureChance,
            RiverAdditionalFeatures = _selectedWorldType.RiverAdditionalFeatures,
            Creeps = _selectedWorldType.Creeps
        };

        var worldGenerator = IocManager.IocContainer.Resolve<IWorldGenerator>();

        _startupController.ShowProgressModal(
            "Generating World",
            report =>
            {
                worldGenerator.GenerateWorld(parameters, report);
                worldGenerator.PopulateWorld(
                    parameters, 
                    _selectedStartingCulture, 
                    _empireText.Text, 
                    _tribeText.Text,
                    _primaryColorPicker.Color,
                    _secondaryColorPicker.Color, 
                    report);
            },
            () =>
            {
                _startupController.ShowWorldGenMenu();
                GetTree().Root.GetNode<Control>("Startup").Visible = false;
                GetTree().Root.AddChild(ResourceLoader.Load<PackedScene>("res://Scenes/Gameplay.tscn").Instance());
            });
    }

    public void RandomizeSeed()
    {
        _seedText.Text = new System.Random().Next().ToString();
    }

    public void RandomizeTribeName()
    {
        _tribeText.Text = _nameSource.MakeTribeName(_selectedStartingCulture.MakeCulture());
    }

    public void RandomizeEmpireName()
    {
        _empireText.Text = _nameSource.MakeEmpireName(_selectedStartingCulture.MakeCulture(), _tribeText.Text);
    }

    public void RandomizeEmpireColor()
    {
        var rnd = new System.Random();
        _primaryColorPicker.Color = rnd.Color(1);
        _secondaryColorPicker.Color = rnd.Color(0.5f);
    }
}
