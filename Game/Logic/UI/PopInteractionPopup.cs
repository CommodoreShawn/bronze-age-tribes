using BronzeAgeTribes.Logic.Data.Game;
using BronzeAgeTribes.Logic.Infrastructure.Interfaces;
using Godot;
using System.Linq;

public class PopInteractionPopup : Popup
{
    [Inject]
    public ITooltipManager _tooltipManager;

    private Pop _interactor;
    private Pop _target;

    private PanelContainer _panelContainer;

    public void Initialize(Pop interactor, Pop target)
    {
        this.ResolveDependencies();
        _interactor = interactor;
        _target = target;
        Visible = true;
        _panelContainer = GetNode<PanelContainer>("PanelContainer");

        GetNode<Button>("PanelContainer/VBoxContainer/AssimilateButton").Visible = target.Species.CanBeAssimilated;
        GetNode<Button>("PanelContainer/VBoxContainer/DomesticateButton").Visible = target.Species.CanBeDomesticated;
        GetNode<Button>("PanelContainer/VBoxContainer/RaidButton").Visible = interactor.Traits.Any(t => t.BonusInteractions.Contains(PopInteractionType.Raid));
    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        if(_target?.Location != null)
        {
            var screenPos = GetViewport().GetCamera().UnprojectPosition(_target.Location.RenderedPos);

            RectGlobalPosition = screenPos;
        }
    }

    public void MouseExitedPopup()
    {
        if (!_panelContainer.GetGlobalRect().HasPoint(GetViewport().GetMousePosition()))
        {
            Visible = false;
            _tooltipManager.HideTooltipFor(this);
        }
    }

    public void MouseEnteredButton(int button)
    {
        _tooltipManager.ShowTooltipFor(this, _interactor, _target, (PopInteractionType)button);
    }

    public void MouseExitedButton(int button)
    {
        _tooltipManager.HideTooltipFor(this);
    }

    public void ButtonPressed(int button)
    {
        _interactor.InteractionTo = _target;
        _interactor.InteractionType = (PopInteractionType)button;
        Visible = false;
        _tooltipManager.HideTooltipFor(this);

        if(_interactor.InteractionType == PopInteractionType.Raid)
        {
            _interactor.InteractionTo.IsBeingRaided = true;
        }
    }
}
