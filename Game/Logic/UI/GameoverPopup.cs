using Autofac;
using BronzeAgeTribes.Logic.Infrastructure;
using BronzeAgeTribes.Logic.Infrastructure.Interfaces;
using Godot;

public class GameoverPopup : Popup
{
    private Label _overallLabel;
    private Label _detailLabel;

    public override void _Ready()
    {
        _overallLabel = GetNode<Label>("PanelContainer/GridContainer/TitleLabel");
        _detailLabel = GetNode<Label>("PanelContainer/GridContainer/DetailLabel");
    }
        public void ReturnToMenu()
    {
        GetTree().Root.GetNode<StartupController>("Startup").ShowStartupMenu();
        GetTree().Root.GetNode<Control>("Startup").Visible = true;
        IocManager.IocContainer.Resolve<ISoundManager>().SetMenuMusic();
        this.FindParent<GameplayWorld>().QueueFree();
    }

    public void InitalizeForNoPops()
    {
        _overallLabel.Text = "Game Over";
        _detailLabel.Text = "Your people are no more. Their homes lie empty, their lands abandoned\n\nBetter luck next time.";
        Visible = true;
    }
}
