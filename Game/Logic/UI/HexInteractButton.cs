using BronzeAgeTribes.Logic.Data;
using BronzeAgeTribes.Logic.Infrastructure.Interfaces;
using Godot;
using System.Linq;

public class HexInteractButton : Control
{
    [Inject]
    private ITooltipManager _tooltipManager;
    [Inject]
    private IDevelopmentListProvider _developmentListProvider;
    [Inject]
    private ILocalPlayerInfo _localPlayerInfo;

    [Export]
    public PackedScene HexInteractOptionButtonPrefab;

    private WorldHex _hex;
    private Button _interactButton;
    private CellFeature _feature;
    private Terrain _terrain;
    private Development _development;
    private Building _building;

    public void InitializeFor(WorldHex hex, CellFeature feature)
    {
        this.ResolveDependencies();
        if (_feature == feature)
        {
            return;
        }

        _interactButton = GetNode<Button>("InteractButton");

        _interactButton.Icon = feature.BackingType.InteractButtonIcon;
        _hex = hex;
        _feature = feature;
        _development = null;
        _terrain = null;
        _building = null;
    }

    public void InitializeFor(WorldHex hex, Development development)
    {
        this.ResolveDependencies();
        if (_development == development)
        {
            return;
        }

        _interactButton = GetNode<Button>("InteractButton");

        _interactButton.Icon = development.BackingType.InteractButtonIcon;
        _hex = hex;
        _feature = null;
        _development = development;
        _terrain = null;
        _building = null;
    }

    public void InitializeFor(WorldHex hex, Terrain terrain)
    {
        this.ResolveDependencies();
        if (_terrain == terrain)
        {
            return;
        }

        _interactButton = GetNode<Button>("InteractButton");

        _interactButton.Icon = terrain.BackingType.InteractButtonIcon;
        _hex = hex;
        _feature = null;
        _development = null;
        _terrain = terrain;
        _building = null;
    }

    public void InitializeFor(WorldHex hex, Building building)
    {
        this.ResolveDependencies();
        if (_building == building)
        {
            return;
        }

        _interactButton = GetNode<Button>("InteractButton");

        _interactButton.Icon = building.BackingType.InteractButtonIcon;
        _hex = hex;
        _feature = null;
        _development = null;
        _terrain = null;
        _building = building;
    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        _interactButton.Disabled = !_hex.Jobs.Any(j => j.Worker?.Owner == _localPlayerInfo.Empire);
    }

    public void InteractButtonToggled(bool isPressed)
    {
        if(isPressed)
        {
            if(_feature != null)
            {
                foreach (var option in _feature.BackingType.InteractionOptions)
                {
                    var optionButton = HexInteractOptionButtonPrefab.Instance() as HexInteractOptionButton;
                    optionButton.InitializeFor(_hex, _feature, option);
                    AddChild(optionButton);
                    MoveChild(optionButton, 0);
                }
            }
            if (_development != null)
            {
                foreach (var option in _development.BackingType.InteractionOptions)
                {
                    var optionButton = HexInteractOptionButtonPrefab.Instance() as HexInteractOptionButton;
                    optionButton.InitializeFor(_hex, _development, option);
                    AddChild(optionButton);
                    MoveChild(optionButton, 0);
                }

                if (_development is UrbanDevelopment urbanDevelopment && urbanDevelopment.Building == null)
                {
                    var buildingOptions = _developmentListProvider.GetBuildingOptionsFor(_hex, _hex.Pops.First().Group, _localPlayerInfo.Empire);

                    foreach (var option in buildingOptions)
                    {
                        var optionButton = HexInteractOptionButtonPrefab.Instance() as HexInteractOptionButton;
                        optionButton.InitializeFor(_hex, option);
                        AddChild(optionButton);
                        MoveChild(optionButton, 0);
                    }
                }
            }
            if (_terrain != null)
            {
                var developmentOptions = _developmentListProvider.GetTentativeDevelopmentOptionsFor(_hex, _hex.Pops.First().Group, _localPlayerInfo.Empire);

                foreach (var option in developmentOptions)
                {
                    var optionButton = HexInteractOptionButtonPrefab.Instance() as HexInteractOptionButton;
                    optionButton.InitializeFor(_hex, option);
                    AddChild(optionButton);
                    MoveChild(optionButton, 0);
                }
            }
            if (_building != null)
            {
                foreach (var option in _building.BackingType.InteractionOptions)
                {
                    var optionButton = HexInteractOptionButtonPrefab.Instance() as HexInteractOptionButton;
                    optionButton.InitializeFor(_hex, _building, option);
                    AddChild(optionButton);
                    MoveChild(optionButton, 0);
                }
            }
        }
        else
        {
            foreach(var child in GetChildren())
            {
                if(child != _interactButton && child is Control controlChild)
                {
                    controlChild.QueueFree();
                }
            }    
        }
    }

    public void Collapse()
    {
        _interactButton.Pressed = false;
        InteractButtonToggled(false);
    }

    public void OnMouseEntered()
    {
        if (_feature != null)
        {
            _tooltipManager.ShowTooltipFor(
                this,
                _hex,
                _feature);
        }
        if(_terrain != null)
        {
            _tooltipManager.ShowTooltipFor(
                this,
                _hex,
                _terrain);
        }
        if (_development != null)
        {
            _tooltipManager.ShowTooltipFor(
                this,
                _hex,
                _development);
        }
        if (_building != null)
        {
            _tooltipManager.ShowTooltipFor(
                this,
                _hex,
                _building);
        }
    }

    public void OnMouseExited()
    {
        _tooltipManager.HideTooltipFor(this);
    }
}
