using BronzeAgeTribes.Logic.Data.Game;
using BronzeAgeTribes.Logic.Infrastructure.Interfaces;
using Godot;

public class DemographicsRowController : HBoxContainer
{
    [Inject]
    private ITooltipManager _tooltipManager;

    private TextureRect _speciesIcon;
    private Label _speciesLabel;
    private Label _growRateLabel;
    private ProgressBar _growthProgress;
    private AdministrativeGroup _group;
    private Species _species;

    public void InitializeFor(AdministrativeGroup group, Species species)
    {
        this.ResolveDependencies();

        _group = group;
        _species = species;

        _speciesIcon = GetNode<TextureRect>("SubBox1/SpeciesIcon");
        _speciesLabel = GetNode<Label>("SubBox1/SpeciesLabel");
        _growRateLabel = GetNode<Label>("SubBox2/GrowRateLabel");
        _growthProgress = GetNode<ProgressBar>("GrowthProgress");

        _speciesIcon.Texture = species.PopIconSmall;
        _speciesLabel.Text = species.Name;
        var rate = group.GetGrowthRateFor(species);
        if(rate < 0)
        {
            _growRateLabel.Text = $"{rate:N2}";
        }
        else
        {
            _growRateLabel.Text = $"+{rate:N2}"; 
        }

        if(group.GrowthProgress.ContainsKey(species))
        {
            _growthProgress.Value = 100 * group.GrowthProgress[species];
        }
        else
        {
            _growthProgress.Value = 0;
        }
    }

    public override void _Ready()
    {
    }

    public void GrowthRateMouseEntered()
    {
        _tooltipManager.ShowTooltipForGrowth(
            GetNode<Control>("SubBox2"),
            _group,
            _species);
    }

    public void GrowthRateMouseExited()
    {
        _tooltipManager.HideTooltipFor(GetNode<Control>("SubBox2"));
    }
}
