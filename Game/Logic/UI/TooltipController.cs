using Autofac;
using BronzeAgeTribes.Logic;
using BronzeAgeTribes.Logic.Data;
using BronzeAgeTribes.Logic.Data.Game;
using BronzeAgeTribes.Logic.Infrastructure;
using BronzeAgeTribes.Logic.Infrastructure.Interfaces;
using Godot;
using System.Linq;

public class TooltipController : Control
{
    [Export]
    public Texture PopHappinessIcon;

    [Export]
    public Texture PopHealthIcon;

    [Export]
    public Texture TimeIcon;

    [Export]
    public Texture ForagerIcon;

    [Export]
    public Texture FarmerIcon;

    [Export]
    public Texture CrafterIcon;

    [Export]
    public Texture MinerIcon;

    [Export]
    public Texture WoodcutterIcon;

    [Export]
    public Texture HunterIcon;

    [Export]
    public Texture PriestIcon;

    [Export]
    public Texture BuilderIcon;

    [Export]
    public PackedScene IconTextPrefab;

    [Inject]
    private IExplorationTracker _explorationTracker;
    [Inject]
    private IPopManager _popManager;

    private Label _titleLabel;
    private Label _bodyLabel;
    private Label _redLabel;
    private Label _redLabel2;
    private HBoxContainer _iconBox;
    private GridContainer _outputsBox;
    private HBoxContainer _iconBox2;
    private GridContainer _outputsBox2;
    public bool _doSizeFix;
    public override void _Ready()
    {
        this.ResolveDependencies();
        IocManager.IocContainer.Resolve<ITooltipManager>().RegisterTooltipController(this);
        _titleLabel = GetNode<Label>("Grid/TitleLabel");
        _bodyLabel = GetNode<Label>("Grid/BodyLabel");
        _iconBox = GetNode<HBoxContainer>("Grid/IconBox");
        _outputsBox = GetNode<GridContainer>("Grid/OutputsBox");
        _redLabel = GetNode<Label>("Grid/RedLabel");
        _iconBox2 = GetNode<HBoxContainer>("Grid/IconBox2");
        _outputsBox2 = GetNode<GridContainer>("Grid/OutputsBox2");
        _redLabel2 = GetNode<Label>("Grid/RedLabel2");
        Visible = false;
    }

    public void SetContentFor(Trait trait)
    {
        _iconBox.ClearChildren();
        _titleLabel.Text = trait.Name;
        _bodyLabel.Text = trait.Description;
        _bodyLabel.Visible = true;
        _bodyLabel.Autowrap = true;
        _bodyLabel.RectMinSize = new Vector2(200, 0);

        _redLabel.Visible = true;
        _redLabel.Text = string.Empty;
        foreach(var jobBonus in trait.JobBonuses)
        {
            if(jobBonus.Factor > 0)
            {
                _bodyLabel.Text += $"\n+{(int)(jobBonus.Factor * 100)}% from {Util.FriendlyCategory(jobBonus.Category)} jobs";
            }
            else
            {
                _redLabel.Text += $"\n{(int)(jobBonus.Factor * 100)}% from {Util.FriendlyCategory(jobBonus.Category)} jobs";
            }
        }
        foreach (var jobCategory in trait.ForbiddenJobs)
        {
            _redLabel.Text += $"\nCannot perform {Util.FriendlyCategory(jobCategory)} jobs";
        }

        if(trait.CombatBonus < 0)
        {
            _redLabel.Text += $"\n{(int)(trait.CombatBonus * 100)}% on Attack or Expel attempts";
        }
        else if (trait.CombatBonus > 0)
        {
            _bodyLabel.Text += $"\n+{(int)(trait.CombatBonus * 100)}% on Attack or Expel attempts";
        }

        if (trait.GrowthBonus < 0)
        {
            _redLabel.Text += $"\n{(int)(trait.GrowthBonus * 100)}% growth rate";
        }
        else if (trait.GrowthBonus > 0)
        {
            _bodyLabel.Text += $"\n+{(int)(trait.GrowthBonus * 100)}% growth rate";
        }

        if(trait.PreventSettling)
        {
            _redLabel.Text += $"\nCannot make settlements.";
        }

        foreach(var interaction in trait.BonusInteractions)
        {
            _bodyLabel.Text += $"\nCan perform {Util.InteractionToVerb(interaction)} interactions.";
        }

        if (_redLabel.Text.Length < 1)
        {
            _redLabel.Visible = false;
        }
        else
        {
            // drop first \n
            _redLabel.Text = _redLabel.Text.Substring(1);
        }

        _outputsBox.Visible = false;
        _redLabel2.Visible = false;
        _iconBox2.Visible = false;
        _outputsBox2.Visible = false;
        RectSize = new Vector2(0,0);
        _doSizeFix = true;
    }

    public void SetContentFor(PopJob popJob)
    {
        var title = popJob.JobType.Title;
        
        _iconBox.ClearChildren();
        _bodyLabel.Visible = true;
        _bodyLabel.Autowrap = false;
        _redLabel.Visible = false;
        _redLabel2.Visible = false;

        _bodyLabel.Text = $"{popJob.Location.BiomeType.Name} ({popJob.Location.Terrain.Name})";

        if (popJob.Location.Development != null)
        {
            _bodyLabel.Text += $"\n{popJob.Location.Development.BackingType.Name}";
        }

        if (popJob.Location.Features.Any())
        {
            _bodyLabel.Text += $"\n{string.Join("\n", popJob.Location.Features.Select(f => f.BackingType.Name))}";
        }

        var worker = popJob.Worker;
        if (worker != null)
        {
            _titleLabel.Text = $"{worker.Culture.Name} {title}";
            _bodyLabel.Text = $"{worker.Group.Name} ({worker?.Group?.Owner?.Name})"
                + "\n" + _bodyLabel.Text;
            var healthIcon = IconTextPrefab.Instance() as IconTextController;
            healthIcon.Initialize(PopHealthIcon, () => $"{(int)(100 * worker?.Health ?? 0)}%");
            _iconBox.AddChild(healthIcon);
            var happyIcon = IconTextPrefab.Instance() as IconTextController;
            happyIcon.Initialize(PopHappinessIcon, () => $"{(int)(100 * worker?.Happiness ?? 0)}%");
            _iconBox.AddChild(happyIcon);
            _redLabel.Text = string.Empty;

            if(worker.MovingFromJob != null)
            {
                _redLabel.Visible = true;
                _redLabel.Text = "Moving (No production)";
            }
            else if (worker.InteractionTo != null)
            {
                _redLabel.Visible = true;

                switch(worker.InteractionType)
                {
                    case PopInteractionType.Assimilate:
                        _redLabel.Text = "Assimilating other pop (No production)";
                        break;
                    case PopInteractionType.Attack:
                        _redLabel.Text = "Attacking other pop (No production)";
                        break;
                    case PopInteractionType.Expel:
                        _redLabel.Text = "Expelling other pop (No production)";
                        break;
                    case PopInteractionType.Domesticate:
                        _redLabel.Text = "Domesticating other pop (No production)";
                        break;
                    case PopInteractionType.Raid:
                        _redLabel.Text = "Raiding other pop (No production)";
                        break;
                    default:
                        _redLabel.Text = "Interacting with other pop (No production)";
                        break;
                }
            }

            if(worker.IsBeingRaided)
            {
                if(_redLabel.Visible && _redLabel.Text.Any())
                {
                    _redLabel.Text += "\n";
                }
                else
                {
                    _redLabel.Visible = true;
                    _redLabel.Text += "Being raided (production reduced)";
                }
            }
        }
        else
        {
            
            _titleLabel.Text = $"{title} Job";
        }
        _outputsBox.Visible = true;

        foreach (var child in _outputsBox.GetChildren())
        {
            (child as ResourceDisplayController).SetSource(popJob);
        }

        _iconBox2.Visible = false;
        _outputsBox2.Visible = false;

        RectSize = new Vector2(0, 0);
        _doSizeFix = true;
    }

    public void SetContentFor(WorldHex hex)
    {
        if (_explorationTracker.IsExplored(hex.Position))
        {
            _titleLabel.Text = hex.Region.Name;
            _bodyLabel.Text = $"{hex.BiomeType.Name} ({hex.Terrain.Name})";
            if (hex.Development != null)
            {
                _bodyLabel.Text += $"\n{hex.Development.BackingType.Name}";
            }
            if (hex.Features.Any())
            {
                _bodyLabel.Text += $"\n{string.Join("\n", hex.Features.Select(f => f.BackingType.Name))}";
            }

            _iconBox.ClearChildren();
            foreach (var trait in hex.TerrainTraits)
            {
                _iconBox.AddChild(new TextureRect
                {
                    Texture = trait.Icon
                });
            }
        }
        else
        {
            _titleLabel.Text = "Unknown";
            _bodyLabel.Text = $"You need to explore this area.";
            _iconBox.ClearChildren();
        }

        _bodyLabel.Visible = true;
        _bodyLabel.Autowrap = false;
        _outputsBox.Visible = false;
        _redLabel.Visible = false;
        _redLabel2.Visible = false;
        _iconBox2.Visible = false;
        _outputsBox2.Visible = false;
        RectSize = new Vector2(0, 0);
        _doSizeFix = true;
    }

    public void SetContentFor(ResourceType resource, float value, string details)
    {
        _iconBox.ClearChildren();
        _titleLabel.Text = resource.ToString();
        _bodyLabel.Text = details;
        _bodyLabel.Visible = true;
        _bodyLabel.Autowrap = false;
        RectSize = new Vector2(0, 0);
        _doSizeFix = true;
        _outputsBox.Visible = false;
        _redLabel.Visible = false;
        _redLabel2.Visible = false;
        _iconBox2.Visible = false;
        _outputsBox2.Visible = false;
    }

    public void SetContentForGrowth(AdministrativeGroup group, Species species)
    {
        var details = string.Join("\n", group.GetGrowthRateDetailsFor(species));
        _iconBox.ClearChildren();
        _titleLabel.Text = $"{species.Name} Growth";
        _bodyLabel.Text = details;
        _bodyLabel.Visible = true;
        _bodyLabel.Autowrap = false;
        RectSize = new Vector2(0, 0);
        _doSizeFix = true;
        _outputsBox.Visible = false;
        _redLabel.Visible = false;
        _redLabel2.Visible = false;
        _iconBox2.Visible = false;
        _outputsBox2.Visible = false;
    }

    public void SetContentFor(WorldHex hex, CellFeature feature)
    {
        _iconBox.ClearChildren();
        _titleLabel.Text = feature.BackingType.Name;
        _bodyLabel.Text = feature.BackingType.Description;
        _bodyLabel.Autowrap = true;
        _bodyLabel.Visible = true;
        _bodyLabel.RectMinSize = new Vector2(200, 0);

        _outputsBox.Visible = true;
        foreach (var child in _outputsBox.GetChildren())
        {
            (child as ResourceDisplayController).SetSource(feature);
        }

        
        foreach(var trait in feature.Traits)
        {
            _iconBox.AddChild(new TextureRect
            {
                Texture = trait.Icon
            });
        }

        RectSize = new Vector2(0, 0);
        _doSizeFix = true;
        _outputsBox.Visible = true;
        _redLabel.Visible = false;
        _redLabel2.Visible = false;
        _iconBox2.Visible = false;
        _outputsBox2.Visible = false;
    }

    public void SetContentFor(WorldHex hex, CellFeature feature, InteractionOption interactionOption)
    {
        _iconBox.ClearChildren();
        _titleLabel.Text = interactionOption.Name;
        _bodyLabel.Text =  interactionOption.Description;
        _bodyLabel.Autowrap = true;
        _bodyLabel.Visible = true;
        _bodyLabel.RectMinSize = new Vector2(200, 0);

        _outputsBox.Visible = true;
        foreach (var child in _outputsBox.GetChildren())
        {
            (child as ResourceDisplayController).SetSource(interactionOption, hex);
        }

        if (interactionOption.Turns > 0)
        {
            var timeIcon = IconTextPrefab.Instance() as IconTextController;
            timeIcon.Initialize(TimeIcon, () => interactionOption.GetTurnsRemaining(hex, feature).ToString());
            _iconBox.AddChild(timeIcon);
        }
        
        RectSize = new Vector2(0, 0);
        _doSizeFix = true;
        _outputsBox.Visible = true;
        _redLabel.Visible = false;
        _redLabel2.Visible = false;
        _iconBox2.Visible = false;
        _outputsBox2.Visible = false;
    }

    public void SetContentFor(WorldHex hex, Development development)
    {
        _titleLabel.Text = development.BackingType.Name;
        _bodyLabel.Text = development.BackingType.Description;
        _bodyLabel.Autowrap = true;
        _bodyLabel.Visible = true;
        _bodyLabel.RectMinSize = new Vector2(200, 0);

        _outputsBox.Visible = true;
        foreach (var child in _outputsBox.GetChildren())
        {
            (child as ResourceDisplayController).SetSource(development);
        }

        _iconBox.ClearChildren();
        foreach (var trait in development.Traits)
        {
            _iconBox.AddChild(new TextureRect
            {
                Texture = trait.Icon
            });
        }



        RectSize = new Vector2(0, 0);
        _doSizeFix = true;
        _outputsBox.Visible = true;
        _redLabel.Visible = false;
        _redLabel2.Visible = false;
        _iconBox2.Visible = false;
        _outputsBox2.Visible = false;
    }

    public void SetContentFor(WorldHex hex, Building building)
    {
        _titleLabel.Text = building.BackingType.Name;
        _bodyLabel.Text = building.BackingType.Description;
        _bodyLabel.Autowrap = true;
        _bodyLabel.Visible = true;
        _bodyLabel.RectMinSize = new Vector2(200, 0);

        _outputsBox.Visible = true;
        foreach (var child in _outputsBox.GetChildren())
        {
            (child as ResourceDisplayController).SetSource(building);
        }

        _iconBox.ClearChildren();
        foreach (var trait in building.Traits)
        {
            _iconBox.AddChild(new TextureRect
            {
                Texture = trait.Icon
            });
        }

        RectSize = new Vector2(0, 0);
        _doSizeFix = true;
        _outputsBox.Visible = true;
        _redLabel.Visible = false;
        _redLabel2.Visible = false;
        _iconBox2.Visible = false;
        _outputsBox2.Visible = false;
    }

    public void SetContentFor(Pop interactor, Pop target, PopInteractionType interactionType)
    {
        switch (interactionType)
        {
            case PopInteractionType.Assimilate:
                _titleLabel.Text = "Assimilate Pop";
                _bodyLabel.Text = $"Convince the {target.Culture.Name} {target.Species.Name} to join {interactor.Group.Name}.\n";
                _bodyLabel.Text += string.Join("\n", _popManager.GetPopInteractionChanceBreakdown(interactor, target, interactionType));
                break;
            case PopInteractionType.Attack:
                _titleLabel.Text = "Attack Pop";
                _bodyLabel.Text = $"Attack the {target.Culture.Name} {target.Species.Name} pop.\n";
                _bodyLabel.Text += string.Join("\n", _popManager.GetPopInteractionChanceBreakdown(interactor, target, interactionType));
                break;
            case PopInteractionType.Expel:
                _titleLabel.Text = "Expel Pop";
                _bodyLabel.Text = $"Drive the {target.Culture.Name} {target.Species.Name} pop away.\n";
                _bodyLabel.Text += string.Join("\n", _popManager.GetPopInteractionChanceBreakdown(interactor, target, interactionType));
                break;
            case PopInteractionType.Domesticate:
                _titleLabel.Text = "Domesticate Pop";
                _bodyLabel.Text = $"Domesticate the {target.Culture.Name} {target.Species.Name} pop to join {interactor.Group.Name}.\n";
                _bodyLabel.Text += string.Join("\n", _popManager.GetPopInteractionChanceBreakdown(interactor, target, interactionType));
                break;
            case PopInteractionType.Raid:
                _titleLabel.Text = "Raid Pop";
                _bodyLabel.Text = $"Raid the {target.Group.Name} pop.\n";
                _bodyLabel.Text += string.Join("\n", _popManager.GetPopInteractionChanceBreakdown(interactor, target, interactionType));
                break;
        }

        _bodyLabel.Autowrap = true;
        _bodyLabel.Visible = true;
        _bodyLabel.RectMinSize = new Vector2(200, 0);

        _outputsBox.Visible = false;
        _iconBox.ClearChildren();
        RectSize = new Vector2(0, 0);
        _doSizeFix = true;
        _redLabel.Visible = false;
        _redLabel2.Visible = false;
        _iconBox2.Visible = false;
        _outputsBox2.Visible = false;
    }

    public void SetContentFor(WorldHex hex, Development development, InteractionOption interactionOption)
    {
        _iconBox.ClearChildren();
        _titleLabel.Text = interactionOption.Name;
        _bodyLabel.Text = interactionOption.Description;
        _bodyLabel.Autowrap = true;
        _bodyLabel.Visible = true;
        _bodyLabel.RectMinSize = new Vector2(200, 0);

        _outputsBox.Visible = true;
        foreach (var child in _outputsBox.GetChildren())
        {
            (child as ResourceDisplayController).SetSource(interactionOption, hex);
        }

        var timeIcon = IconTextPrefab.Instance() as IconTextController;
        timeIcon.Initialize(TimeIcon, () => interactionOption.GetTurnsRemaining(hex, development).ToString());
        _iconBox.AddChild(timeIcon);

        RectSize = new Vector2(0, 0);
        _doSizeFix = true;
        _outputsBox.Visible = true;
        _redLabel.Visible = false;
        _redLabel2.Visible = false;
        _iconBox2.Visible = false;
        _outputsBox2.Visible = false;
    }

    public void SetContentFor(WorldHex hex, Terrain terrain)
    {
        _titleLabel.Text = terrain.BackingType.Name;
        _bodyLabel.Text = terrain.BackingType.Description;
        _bodyLabel.Autowrap = true;
        _bodyLabel.Visible = true;
        _bodyLabel.RectMinSize = new Vector2(200, 0);

        _outputsBox.Visible = false;

        _iconBox.ClearChildren();
        foreach (var trait in terrain.Traits)
        {
            _iconBox.AddChild(new TextureRect
            {
                Texture = trait.Icon
            });
        }


        RectSize = new Vector2(0, 0);
        _doSizeFix = true;
        _outputsBox.Visible = true;
        _redLabel.Visible = false;
        _redLabel2.Visible = false;
        _iconBox2.Visible = false;
        _outputsBox2.Visible = false;
    }

    public void SetContentFor(WorldHex hex, DevelopmentType developmentType, CellFeature featureToClear)
    {
        _titleLabel.Text = developmentType.Name;
        _bodyLabel.Text = developmentType.Description;
        _bodyLabel.Autowrap = true;
        _bodyLabel.Visible = true;
        _bodyLabel.RectMinSize = new Vector2(200, 0);

        _outputsBox.Visible = true;
        foreach (var child in _outputsBox.GetChildren())
        {
            (child as ResourceDisplayController).SetSource(developmentType, hex, false);
        }

        _iconBox.ClearChildren();
        foreach (var trait in developmentType.Traits)
        {
            _iconBox.AddChild(new TextureRect
            {
                Texture = trait.Icon
            });
        }
        foreach (var jobType in developmentType.ProvidedJobs)
        {
            _iconBox.AddChild(new TextureRect
            {
                Texture = GetJobIcon(jobType.Category)
            });
        }

        RectSize = new Vector2(0, 0);
        _doSizeFix = true;
        _redLabel.Visible = true;
        _redLabel.Text = "Construction";
        _iconBox2.ClearChildren();
        _iconBox2.Visible = true;
        var timeIcon = IconTextPrefab.Instance() as IconTextController;
        timeIcon.Initialize(TimeIcon, () => developmentType.ConstructionInteraction.Turns.ToString());
        _iconBox2.AddChild(timeIcon);
        _outputsBox2.Visible = true;
        foreach (var child in _outputsBox2.GetChildren())
        {
            (child as ResourceDisplayController).SetSource(developmentType, hex, true);
        }

        if (featureToClear == null)
        {
            if (hex.Features.Any(f => f.BackingType.ClearedByDevelopment))
            {
                _redLabel2.Visible = true;
                _redLabel.Text = string.Join("\n",
                    hex.Features
                        .Where(f => f.BackingType.ClearedByDevelopment)
                        .Select(f => $"This will remove {f.BackingType.Name}"));

            }
            else
            {
                _redLabel2.Visible = false;
            }
        }
        else
        {
            _redLabel2.Visible = true;
            _redLabel2.Text = $"Blocked by {featureToClear.BackingType.Name}"; 
        }
    }

    public void SetContentFor(WorldHex hex, BuildingType buildingType)
    {
        _titleLabel.Text = buildingType.Name;
        _bodyLabel.Text = buildingType.Description;
        _bodyLabel.Autowrap = true;
        _bodyLabel.Visible = true;
        _bodyLabel.RectMinSize = new Vector2(200, 0);

        _outputsBox.Visible = true;
        foreach (var child in _outputsBox.GetChildren())
        {
            (child as ResourceDisplayController).SetSource(buildingType, hex, false);
        }

        _iconBox.ClearChildren();
        foreach (var trait in buildingType.Traits)
        {
            _iconBox.AddChild(new TextureRect
            {
                Texture = trait.Icon
            });
        }

        foreach(var jobType in buildingType.ProvidedJobs)
        {
            _iconBox.AddChild(new TextureRect
            {
                Texture = GetJobIcon(jobType.Category)
            });
        }

        RectSize = new Vector2(0, 0);
        _doSizeFix = true;
        _redLabel.Visible = true;
        _redLabel.Text = "Construction";
        _iconBox2.ClearChildren();
        _iconBox2.Visible = true;
        var timeIcon = IconTextPrefab.Instance() as IconTextController;
        timeIcon.Initialize(TimeIcon, () => buildingType.ConstructionInteraction.Turns.ToString());
        _iconBox2.AddChild(timeIcon);
        _outputsBox2.Visible = true;
        foreach (var child in _outputsBox2.GetChildren())
        {
            (child as ResourceDisplayController).SetSource(buildingType, hex, true);
        }
        _redLabel2.Visible = false;
    }

    

    public void SetContentFor(Pop pop)
    {
        _bodyLabel.Visible = true;
        _bodyLabel.Autowrap = false;
        _redLabel.Visible = false;
        _redLabel2.Visible = false;

        _titleLabel.Text = $"{pop.Culture.Name} {pop.Job?.JobType?.Title ?? ""}";
        _bodyLabel.Text = $"{pop.Species.Name}";
        
        _iconBox.ClearChildren();
        var healthIcon = IconTextPrefab.Instance() as IconTextController;
        healthIcon.Initialize(PopHealthIcon, () => $"{(int)(100 * pop.Health)}%");
        _iconBox.AddChild(healthIcon);
        var happyIcon = IconTextPrefab.Instance() as IconTextController;
        happyIcon.Initialize(PopHappinessIcon, () => $"{(int)(100 * pop.Happiness)}%");
        _iconBox.AddChild(happyIcon);

        if (pop.MovingFromJob != null)
        {
            _redLabel.Visible = true;
            _redLabel.Text = "Moving (No production)";
        }

        _outputsBox.Visible = pop.Job != null;
        if (pop.Job != null)
        {
            foreach (var child in _outputsBox.GetChildren())
            {
                (child as ResourceDisplayController).SetSource(pop.Job);
            }
        }

        _iconBox2.Visible = true;
        _iconBox2.ClearChildren();
        foreach (var trait in pop.Traits)
        {
            _iconBox2.AddChild(new TextureRect
            {
                Texture = trait.Icon
            });
        }

        RectSize = new Vector2(0, 0);
        _doSizeFix = true;
    }

    public void SetContentFor(Notification notification)
    {
        _titleLabel.Text = notification.SummaryTitle;
        _bodyLabel.Text = notification.SummaryBody;
        _bodyLabel.Visible = true;
        _bodyLabel.Autowrap = true;
        _bodyLabel.RectMinSize = new Vector2(200, 0);

        _iconBox.ClearChildren();
        _outputsBox.Visible = false;
        _redLabel.Visible = false;
        _redLabel2.Visible = false;
        _iconBox2.Visible = false;
        _outputsBox2.Visible = false;
        RectSize = new Vector2(0, 0);
        _doSizeFix = true;
    }

    public void SetContentFor(WorldRegion region)
    {
        _titleLabel.Text = region.Name;
        _bodyLabel.Text = $"{region.BiomeType.Name} {region.TerrainType.Name}";
        if (region.Hexes.All(h => _explorationTracker.IsExplored(h.Position)))
        {
            _bodyLabel.Text += $"\n{region.Hexes.Count()} tiles";
        }
        else
        {
            _bodyLabel.Text += $"\n{region.Hexes.Where(h => _explorationTracker.IsExplored(h.Position)).Count()}+ tiles";
        }
        _bodyLabel.Visible = true;
        _bodyLabel.Autowrap = true;
        _bodyLabel.RectMinSize = new Vector2(200, 0);

        _iconBox.ClearChildren();
        _outputsBox.Visible = false;
        _redLabel.Visible = false;
        _redLabel2.Visible = false;
        _iconBox2.Visible = false;
        _outputsBox2.Visible = false;
        RectSize = new Vector2(0, 0);
        _doSizeFix = true;
    }

    public void SetContentFor(WorldHex hex, Building building, InteractionOption interactionOption)
    {
        _iconBox.ClearChildren();
        _titleLabel.Text = interactionOption.Name;
        _bodyLabel.Text = interactionOption.Description;
        _bodyLabel.Autowrap = true;
        _bodyLabel.Visible = true;
        _bodyLabel.RectMinSize = new Vector2(200, 0);

        _outputsBox.Visible = true;
        foreach (var child in _outputsBox.GetChildren())
        {
            (child as ResourceDisplayController).SetSource(interactionOption, hex);
        }

        var timeIcon = IconTextPrefab.Instance() as IconTextController;
        timeIcon.Initialize(TimeIcon, () => interactionOption.GetTurnsRemaining(hex, building).ToString());
        _iconBox.AddChild(timeIcon);

        RectSize = new Vector2(0, 0);
        _doSizeFix = true;
        _outputsBox.Visible = true;
        _redLabel.Visible = false;
        _redLabel2.Visible = false;
        _iconBox2.Visible = false;
        _outputsBox2.Visible = false;
    }

    public override void _Process(float delta)
    {
        base._Process(delta);
        if(_doSizeFix)
        {
            _doSizeFix = false;
            RectSize = new Vector2(0, 0);
        }

        RectPosition = GetViewport().GetMousePosition() + new Vector2(16, 0);
    }

    private Texture GetJobIcon(JobCategory jobCategory)
    {
        switch (jobCategory)
        {
            case JobCategory.Crafting:
                return CrafterIcon;
            case JobCategory.Farming:
                return FarmerIcon;
            case JobCategory.Foraging:
                return ForagerIcon;
            case JobCategory.Mining:
                return MinerIcon;
            case JobCategory.Woodcutting:
                return WoodcutterIcon;
            case JobCategory.Hunting:
                return HunterIcon;
            case JobCategory.Building:
                return BuilderIcon;
            case JobCategory.Preaching:
                return PriestIcon;
            default:
                return null;
        }
    }
}
