using BronzeAgeTribes.Logic.Data;
using BronzeAgeTribes.Logic.Data.Game;
using BronzeAgeTribes.Logic.Infrastructure.Interfaces;
using Godot;
using System;
using System.Linq;

public class HexInteractOptionButton : Button
{
    [Inject]
    private ITooltipManager _tooltipManager;
    [Inject]
    private ILocalPlayerInfo _localPlayerInfo;

    private WorldHex _hex;
    private CellFeature _feature;
    private Development _development;
    private DevelopmentType _developmentType;
    private InteractionOption _interactionOption;
    private Building _building;
    private BuildingType _buildingType;
    private Pop _pop;
    private Pop _target;
    private PopInteractionType? _popInteractionType;
    private CellFeature _featureToClear;

    [Export]
    public Texture InteractAttackIcon;
    [Export]
    public Texture InteractExpelIcon;
    [Export]
    public Texture InteractAssimilateIcon;

    public void InitializeFor(WorldHex hex, CellFeature feature, InteractionOption interactionOption)
    {
        if (_feature == feature)
        {
            return;
        }

        this.ResolveDependencies();

        Icon = interactionOption.Icon;
        _hex = hex;
        _feature = feature;
        _development = null;
        _developmentType = null;
        _interactionOption = interactionOption;
        _pop = null;
        _target = null;
        _building = null;
        _buildingType = null;
        _featureToClear = null;
    }

    public void InitializeFor(WorldHex hex, Development development, InteractionOption interactionOption)
    {
        if (_development == development)
        {
            return;
        }

        this.ResolveDependencies();

        Icon = interactionOption.Icon;
        _hex = hex;
        _development = development;
        _feature = null;
        _developmentType = null;
        _interactionOption = interactionOption;
        _pop = null;
        _target = null;
        _building = null;
        _buildingType = null;
        _featureToClear = null;
    }

    public void InitializeFor(WorldHex hex, TentativeOptionInfo<DevelopmentType>  developmentOption)
    {
        if (_developmentType == developmentOption.Option)
        {
            return;
        }

        this.ResolveDependencies();

        Icon = developmentOption.Option.InteractButtonIcon;
        _hex = hex;
        _developmentType = developmentOption.Option;
        _development = null;
        _feature = null;
        _interactionOption = null;
        _pop = null;
        _target = null;
        _building = null;
        _buildingType = null;
        _featureToClear = developmentOption.ClearFeature;
    }

    public void InitializeFor(WorldHex hex, Pop interactingPop, Pop interactionTo, PopInteractionType? interactionType)
    {
        if (_pop == interactingPop)
        {
            return;
        }

        this.ResolveDependencies();

        switch (interactionType)
        {
            case PopInteractionType.Domesticate:
            case PopInteractionType.Assimilate:
                Icon = InteractAssimilateIcon;
                break;
            case PopInteractionType.Attack:
                Icon = InteractAttackIcon;
                break;
            case PopInteractionType.Expel:
                Icon = InteractExpelIcon;
                break;
        }

        _hex = hex;
        _developmentType = null;
        _development = null;
        _feature = null;
        _interactionOption = null;
        _pop = interactingPop;
        _target = interactionTo;
        _popInteractionType = interactionType;
        _building = null;
        _buildingType = null;
        _featureToClear = null;
    }

    public void InitializeFor(WorldHex hex, Building building, InteractionOption interactionOption)
    {
        if (_building == building)
        {
            return;
        }

        this.ResolveDependencies();

        Icon = interactionOption.Icon;
        _hex = hex;
        _development = null;
        _feature = null;
        _developmentType = null;
        _interactionOption = interactionOption;
        _pop = null;
        _target = null;
        _building = building;
        _buildingType = null;
        _featureToClear = null;
    }

    public void InitializeFor(WorldHex hex, BuildingType buildingType)
    {
        if (_buildingType == buildingType)
        {
            return;
        }

        this.ResolveDependencies();

        Icon = buildingType.InteractButtonIcon;
        _hex = hex;
        _developmentType = null;
        _development = null;
        _feature = null;
        _interactionOption = null;
        _pop = null;
        _target = null;
        _building = null;
        _buildingType = buildingType;
        _featureToClear = null;
    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        Disabled = !_hex.Jobs.Any(j => j.Worker?.Owner == _localPlayerInfo.Empire)
            || _featureToClear != null;
    }

    public void OnButtonPressed()
    {
        if (_feature != null)
        {
            if (_feature.CurrentInteraction != _interactionOption)
            {
                _feature.SetCurrentInteraction(_interactionOption);
            }
            else
            {
                _feature.SetCurrentInteraction(null);
            }
        }
        if (_development != null)
        {
            if (_development.CurrentInteraction != _interactionOption)
            {
                _development.SetCurrentInteraction(_interactionOption);
            }
            else
            {
                _development.SetCurrentInteraction(null);
            }
        }
        if (_developmentType != null)
        {
            _hex.Development = _developmentType.MakeDevelopment(
                _hex,
                false,
                _hex.Pops.First().Culture.BuildingStyle);

            _hex.Development.SetCurrentInteraction(_developmentType.ConstructionInteraction);
        }
        if(_pop != null)
        {
            _pop.InteractionTo = null;
            _pop.InteractionType = null;
            this.FindParent<PopManagementController>()?.ClearInteractionTarget();
        }
        if (_building != null)
        {
            if (_building.CurrentInteraction != _interactionOption)
            {
                _building.SetCurrentInteraction(_interactionOption);
            }
            else
            {
                _building.SetCurrentInteraction(null);
            }
        }
        if (_buildingType != null
            && _hex.Development is UrbanDevelopment urbanDevelopment)
        {
            urbanDevelopment.Building =  _buildingType.MakeBuilding(
                urbanDevelopment,
                false,
                _hex.Pops.First().Culture.BuildingStyle);

            urbanDevelopment.Building.SetCurrentInteraction(_buildingType.ConstructionInteraction);
        }

        if (GetParent() is HexInteractButton hexInteractButton)
        {
            hexInteractButton.Collapse();
        }
    }

    public void OnMouseEntered()
    {
        if (_feature != null)
        {
            _tooltipManager.ShowTooltipFor(
                this,
                _hex,
                _feature,
                _interactionOption);
        }

        if (_development != null)
        {
            _tooltipManager.ShowTooltipFor(
                this,
                _hex,
                _development,
                _interactionOption);
        }

        if (_developmentType != null)
        {
            _tooltipManager.ShowTooltipFor(
                this,
                _hex,
                _developmentType,
                _featureToClear);
        }

        if(_pop != null && _popInteractionType.HasValue)
        {
            _tooltipManager.ShowTooltipFor(
                this,
                _pop,
                _target,
                _popInteractionType.Value);

            this.FindParent<PopManagementController>()?.ShowInteractionTarget(_target);
        }

        if (_building != null)
        {
            _tooltipManager.ShowTooltipFor(
                this,
                _hex,
                _building,
                _interactionOption);
        }

        if (_buildingType != null)
        {
            _tooltipManager.ShowTooltipFor(
                this,
                _hex,
                _buildingType);
        }
    }

    public void OnMouseExited()
    {
        _tooltipManager.HideTooltipFor(this);
        this.FindParent<PopManagementController>()?.ClearInteractionTarget();
    }
}
