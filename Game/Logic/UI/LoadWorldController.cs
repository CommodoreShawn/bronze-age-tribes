using Autofac;
using BronzeAgeTribes.Logic.Data;
using BronzeAgeTribes.Logic.Data.Serialization;
using BronzeAgeTribes.Logic.Infrastructure;
using BronzeAgeTribes.Logic.Infrastructure.Interfaces;
using Godot;
using System.Linq;

public class LoadWorldController : Control
{
    [Inject]
    private IWorldSerializer _worldSerializer;

    private SaveSummary[] _saves;
    private SaveSummary _selectedSave;

    public override void _Ready()
    {
        base._Ready();
        this.ResolveDependencies();

        Visible = false;
    }

    public override void _Input(InputEvent @event)
    {
        base._Input(@event);

        if(Visible 
            && @event is InputEventKey keyEvt
            && keyEvt.Scancode == (uint)KeyList.Escape)
        {
            GetTree().SetInputAsHandled();
            Close();
        }
    }

    public void InitalizeAndShow()
    {
        this.Visible = true;
        _saves = _worldSerializer.ListSaveFiles()
            .OrderByDescending(s => s.LastPlayed)
            .ToArray();

        var saveList = GetNode<ItemList>("LoadPanel/RootGrid/ContentGrid/FileList");
        saveList.Clear();
        foreach (var save in _saves)
        {
            saveList.AddItem(save.FileName, null, true);
        }

        saveList.Select(0);
        OnSaveSelected(0);
    }

    public void OnSaveSelected(int itemSelected)
    {
        _selectedSave = _saves[itemSelected];

        GetNode<TextEdit>("LoadPanel/RootGrid/ContentGrid/DetailsGrid/EmpireNameRow/EmpireText").Text = _selectedSave.PlayerEmpire;
        GetNode<TextEdit>("LoadPanel/RootGrid/ContentGrid/DetailsGrid/Time Row/TurnText").Text = _selectedSave.PlayedTurns.ToString();
        GetNode<TextEdit>("LoadPanel/RootGrid/ContentGrid/DetailsGrid/Time Row/DateText").Text = _selectedSave.LastPlayed.ToShortDateString();
    }

    public void Close()
    {
        Visible = false;
    }

    public void LoadSave()
    {
        Visible = false;
        var gameplayWorld = this.FindParent<GameplayWorld>();
        if(gameplayWorld != null)
        {
            gameplayWorld.QueueFree();
        }

        GetTree().Root.GetNode<StartupController>("Startup").LoadSave(_selectedSave);
    }

    public void DeleteSave()
    {
        _worldSerializer.DeleteSave(_selectedSave);

        if(!_worldSerializer.ListSaveFiles().Any())
        {
            Close();
        }
        else
        {
            InitalizeAndShow();
        }
    }
}
