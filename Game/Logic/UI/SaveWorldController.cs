using BronzeAgeTribes.Logic.Data.Serialization;
using BronzeAgeTribes.Logic.Infrastructure;
using BronzeAgeTribes.Logic.Infrastructure.Interfaces;
using Godot;
using System.Linq;
using System.Text.RegularExpressions;

public class SaveWorldController : Control
{
    [Inject]
    private IWorldSerializer _worldSerializer;

    private SaveSummary[] _saves;
    private SaveSummary _selectedSave;

    private TextEdit _saveText;

    public override void _Ready()
    {
        base._Ready();
        this.ResolveDependencies();

        Visible = false;
        _saveText = GetNode<TextEdit>("SavePanel/RootGrid/ContentGrid/VBoxContainer/SaveText");
    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        var isFileValid = !string.IsNullOrEmpty(_saveText.Text)
            && !Regex.IsMatch(_saveText.Text, "[^a-zA-Z\\d\\s_-]", RegexOptions.IgnoreCase);

        GetNode<Button>("SavePanel/RootGrid/ContentGrid/DetailsGrid/ButtonGrid/SaveButton").Disabled = !isFileValid;
    }

    public override void _Input(InputEvent @event)
    {
        base._Input(@event);

        if (Visible
            && @event is InputEventKey keyEvt
            && keyEvt.Scancode == (uint)KeyList.Escape)
        {
            GetTree().SetInputAsHandled();
            Close();
        }
    }

    public void InitalizeAndShow()
    {
        Visible = true;
        _saves = _worldSerializer.ListSaveFiles()
            .OrderByDescending(s => s.LastPlayed)
            .ToArray();

        var saveList = GetNode<ItemList>("SavePanel/RootGrid/ContentGrid/VBoxContainer/FileList");
        saveList.Clear();
        foreach (var save in _saves)
        {
            saveList.AddItem(save.FileName, null, true);
        }
    }

    public void OnSaveSelected(int itemSelected)
    {
        _selectedSave = _saves[itemSelected];

        GetNode<TextEdit>("SavePanel/RootGrid/ContentGrid/DetailsGrid/EmpireNameRow/EmpireText").Text = _selectedSave.PlayerEmpire;
        GetNode<TextEdit>("SavePanel/RootGrid/ContentGrid/DetailsGrid/Time Row/TurnText").Text = _selectedSave.PlayedTurns.ToString();
        GetNode<TextEdit>("SavePanel/RootGrid/ContentGrid/DetailsGrid/Time Row/DateText").Text = _selectedSave.LastPlayed.ToShortDateString();
        _saveText.Text = _selectedSave.FileName;
    }

    public void Close()
    {
        Visible = false;
    }

    public void Save()
    {
        this.FindParent<GameplayWorld>().ShowProgressModal(
            "Saving",
            report =>
            {
                _worldSerializer.SaveWorld(report, _saveText.Text);
            },
            () =>
            {
                Visible = false;
                GetParent().GetNode<GameMenuPopup>("GameMenuPopup").Visible = false;
            });
    }
}
