using Godot;
using System;

public class IconTextController : HBoxContainer
{
    private TextureRect _icon;
    private Label _text;
    private string _lastValue;
    private Func<string> _getValue;

    public void Initialize(Texture icon, Func<string> getValue)
    {
        _icon = GetNode<TextureRect>("Icon");
        _icon.Texture = icon;
        _text = GetNode<Label>("Text");
        _getValue = getValue;
    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        var newValue = _getValue();

        if(newValue != _lastValue)
        {
            _lastValue = newValue;
            _text.Text = newValue;
        }
    }
}
