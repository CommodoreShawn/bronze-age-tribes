using BronzeAgeTribes.Logic;
using BronzeAgeTribes.Logic.Data;
using BronzeAgeTribes.Logic.Infrastructure.Interfaces;
using Godot;

public class CameraFocus : Spatial
{
    public const float MOUSEWHEEL_SENSITIVITY = 1;

    public float _zoomMax = 300;
    public float _zoomMin = 10;
    public float _zoom = 15;
    public float _zoomSpeed = 60;
    
    private Camera _camera;

    public float _pitchMax = 80;
    public float _pitchMin = 20;
    public float _pitch = 45;

    private Vector2 _lastMousePosition;

    [Inject]
    private ILocalPlayerInfo _localPlayerInfo;

    private GameplayWorld _gameplayWorld;

    public override void _Ready()
    {
        this.ResolveDependencies();
        _camera = GetNode<Camera>("Camera");
        _gameplayWorld = this.FindParent<GameplayWorld>();
        _localPlayerInfo.CameraFocus = this;
    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        if(_gameplayWorld != null && _gameplayWorld.IsPopupActive())
        {
            return;
        }

        var panRight = Input.GetActionStrength("pan_right") - Input.GetActionStrength("pan_left");
        var panForward = Input.GetActionStrength("pan_back") - Input.GetActionStrength("pan_forward");

        Transform = Transform.Translated(new Vector3(panRight, 0, panForward));

        _localPlayerInfo.ViewCenter = Transform.origin;

        _zoom += (Input.GetActionStrength("camera_zoom_out") - Input.GetActionStrength("camera_zoom_in")) * delta * _zoomSpeed;

        if (Input.IsActionJustReleased("mousewheel_up"))
        {
            _zoom += delta * MOUSEWHEEL_SENSITIVITY * _zoomSpeed;
        }
        if (Input.IsActionJustReleased("mousewheel_down"))
        {
            _zoom -= delta * MOUSEWHEEL_SENSITIVITY * _zoomSpeed;
        }

        _zoom = Mathf.Clamp(_zoom, _zoomMin, _zoomMax);

        var newCameraPos = new Vector3(
            0,
            Mathf.Sin(Mathf.Deg2Rad(_pitch)) * _zoom,
            Mathf.Cos(Mathf.Deg2Rad(_pitch)) * _zoom);

        _camera.Translation = newCameraPos;
        _camera.LookAt(Transform.origin, Vector3.Up);

        if (Input.IsMouseButtonPressed(2))
        {
            var mouseDelta = GetViewport().GetMousePosition() - _lastMousePosition;

            Rotate(Vector3.Up, mouseDelta.x * delta * -0.5f);

            _pitch = Mathf.Clamp(_pitch + mouseDelta.y * delta * 10, _pitchMin, _pitchMax);
        }

        _lastMousePosition = GetViewport().GetMousePosition();
    }

    public void LookAt(HexPosition hexPosition)
    {
        Transform = Transform.Translated(new Vector3(
            hexPosition.RealX - Transform.origin.x,
            0,
            hexPosition.RealY - Transform.origin.z));
    }
}
