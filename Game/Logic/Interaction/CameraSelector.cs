﻿using Autofac;
using BronzeAgeTribes.Logic.Infrastructure;
using BronzeAgeTribes.Logic.Infrastructure.Interfaces;
using Godot;
using System.Collections.Generic;
using System.Linq;

public class CameraSelector : Camera
{
    private Plane _basePlane;

    private IWorldManager _worldManager;
    private ILocalPlayerInfo _localPlayerInfo;
    private GameplayWorld _gameplayWorld;

    private List<Control> _controls;

    public override void _Ready()
    {
        _basePlane = new Plane(Vector3.Up, 0);

        _worldManager = IocManager.IocContainer.Resolve<IWorldManager>();
        _localPlayerInfo = IocManager.IocContainer.Resolve<ILocalPlayerInfo>();
        _gameplayWorld = this.FindParent<GameplayWorld>();

        _controls = new List<Control>();
        if (_gameplayWorld != null)
        {
            foreach (var child in _gameplayWorld.GetChildren())
            {
                if (child is Control childControl && !(child is TooltipController))
                {
                    _controls.Add(childControl);
                }
            }
        }
    }

    public override void _Process(float delta)
    {
        base._Process(delta);
        var mousePos = GetViewport().GetMousePosition();


        var uiCaptured = _controls
            .Where(c => c.Visible && c.GetGlobalRect().HasPoint(mousePos))
            .Any();

        if (!uiCaptured)
        {
            var mouseRay = GetViewport().GetCamera().ProjectRayNormal(mousePos);

            var intersect = _basePlane.IntersectRay(GlobalTransform.origin, mouseRay);

            if (intersect != null)
            {
                _localPlayerInfo.HoveredHex = _worldManager.GetHexAtWorldPoint(intersect.Value.x, intersect.Value.z);
            }
            else
            {
                _localPlayerInfo.HoveredHex = null;
            }
        }
        else
        {
            _localPlayerInfo.HoveredHex = null;
        }
    }
}
