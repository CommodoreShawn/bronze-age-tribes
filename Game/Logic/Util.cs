﻿using BronzeAgeTribes.Logic.Data;
using BronzeAgeTribes.Logic.Data.Game;
using Godot;

namespace BronzeAgeTribes.Logic
{
    public static class Util
    {
        public static HexFacing ReverseFacing(HexFacing facing)
        {
            switch(facing)
            {
                case HexFacing.NorthEast:
                    return HexFacing.SouthWest;
                case HexFacing.East:
                    return HexFacing.West;
                case HexFacing.SouthEast:
                    return HexFacing.NorthWest;
                case HexFacing.SouthWest:
                    return HexFacing.NorthEast;
                case HexFacing.West:
                    return HexFacing.East;
                case HexFacing.NorthWest:
                    return HexFacing.SouthEast;
            }

            return HexFacing.NorthEast;
        }

        public static string FriendlyTimeDisplay(int turn)
        {
            var year = turn / 4 + 580;
            var season = turn % 4;

            switch(season)
            {
                case 0:
                    return $"Spring {year}";
                case 1:
                    return $"Summer {year}";
                case 2:
                    return $"Autumn {year}";
                default:
                    return $"Winter {year}";
            }
        }

        public static float FacingToRadians(HexFacing facing)
        {
            var thirdPi = Mathf.Pi / 3;

            switch (facing)
            {
                case HexFacing.NorthEast:
                    return thirdPi;
                case HexFacing.East:
                    return 0;
                case HexFacing.SouthEast:
                    return -thirdPi;
                case HexFacing.SouthWest:
                    return -2 * thirdPi;
                case HexFacing.West:
                    return -3 * thirdPi;
                case HexFacing.NorthWest:
                    return 2 * thirdPi;
            }

            return 0;
        }

        public static string InteractionToVerb(PopInteractionType interactionType)
        {
            switch(interactionType)
            {
                case PopInteractionType.Assimilate:
                    return "Assimilation";
                case PopInteractionType.Attack:
                    return "Attack";
                case PopInteractionType.Expel:
                    return "Expulsion";
                case PopInteractionType.Domesticate:
                    return "Domestication";
                case PopInteractionType.Raid:
                    return "Raiding";
                default:
                    return "Interaction";
            }
        }

        public static string FriendlyCategory(JobCategory category)
        {
            return category.ToString();
        }
    }
}
