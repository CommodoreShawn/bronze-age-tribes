using Godot;
using System;

public class IdleAnimator : AnimationPlayer
{
    public override void _Ready()
    {
        GetAnimation("default").Loop = true;
        Play("default");
    }
}
