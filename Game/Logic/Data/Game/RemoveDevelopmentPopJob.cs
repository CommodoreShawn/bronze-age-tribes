﻿using Autofac;
using BronzeAgeTribes.Logic.Infrastructure;
using BronzeAgeTribes.Logic.Infrastructure.Interfaces;
using BronzeAgeTribes.Logic.Infrastructure.SimulationEvents;
using System.Linq;

namespace BronzeAgeTribes.Logic.Data.Game
{
    public class RemoveDevelopmentPopJob : PopJob
    {
        private Development _development;
        private InteractionOption _interactionOption;

        public RemoveDevelopmentPopJob(
            WorldHex location,
            RemoveDevelopmentPopJobType jobType,
            Development development,
            InteractionOption interactionOption)
            : base(location, jobType)
        {
            _development = development;
            _interactionOption = interactionOption;
        }

        public override void AdvanceTurn(ISimulationEventBus simulationEventBus)
        {
            base.AdvanceTurn(simulationEventBus);

            if (Worker != null && Worker.MovingFromJob == null && Worker.InteractionTo == null)
            {
                Location.Development = null;

                simulationEventBus.SendEvent(new WorldChangedSimulationEvent(Location.Yield()));
            }
        }
    }
}
