﻿namespace BronzeAgeTribes.Logic.Data.Game
{
    public class TentativeOptionInfo<T>
    {
        public T Option { get; set; }
        public CellFeature ClearFeature { get; set; }
    }
}
