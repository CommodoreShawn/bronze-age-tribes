﻿using Autofac;
using BronzeAgeTribes.Logic.Infrastructure;
using BronzeAgeTribes.Logic.Infrastructure.Interfaces;
using BronzeAgeTribes.Logic.Infrastructure.SimulationEvents;

namespace BronzeAgeTribes.Logic.Data.Game
{
    public class ClearFeaturePopJob : PopJob
    {
        private CellFeature _cellFeature;
        private InteractionOption _interactionOption;

        public ClearFeaturePopJob(
            WorldHex location,
            ClearFeaturePopJobType jobType,
            CellFeature cellFeature,
            InteractionOption interactionOption)
            : base(location, jobType)
        {
            _cellFeature = cellFeature;
            _interactionOption = interactionOption;
        }

        public override void AdvanceTurn(ISimulationEventBus simulationEventBus)
        {
            base.AdvanceTurn(simulationEventBus);

            if(Worker != null && Worker.MovingFromJob == null && Worker.InteractionTo == null)
            {
                _cellFeature.ClearProgress += 1 / (float)_interactionOption.Turns;
            }

            if(_cellFeature.ClearProgress >= 1)
            {
                Location.RemoveFeature(_cellFeature);

                simulationEventBus.SendEvent(new WorldChangedSimulationEvent(Location.Yield()));

                var localPlayerEmpire = IocManager.IocContainer.Resolve<ILocalPlayerInfo>().Empire;

                if (Worker != null && localPlayerEmpire == Worker.Owner)
                {
                    IocManager.IocContainer.Resolve<ISimpleNotificationBuilder>()
                        .SendLookAtNotification(
                            "notification_featureclear",
                            $"{_cellFeature.BackingType.Name} Cleared",
                            $"One of your workers in {Worker.Group.Name} has finished clearing a {_cellFeature.BackingType.Name}.",
                            Worker.Location.Position);
                }
            }
        }
    }
}
