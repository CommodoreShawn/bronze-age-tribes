﻿using BronzeAgeTribes.Logic.Data.Serialization;
using BronzeAgeTribes.Logic.Infrastructure.Interfaces;
using Godot;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BronzeAgeTribes.Logic.Data.Game
{
    public class AdministrativeGroup
    {
        public string Id { get; private set; }
        public Empire Owner { get; private set; }
        public string Name { get; set; }
        public bool IsSettled { get; set; }
        public string Noun => IsSettled ? "Village" : "Tribe";
        public List<Pop> Pops { get; private set; }
        public Dictionary<Species, float> GrowthProgress { get; private set; }
        public HexPosition GroupCenter { get; private set; }
        public int TurnsStarving { get; set; }
        public int StarvationGracePeriod => 1;
        public List<WorldRegion> ClaimedRegions { get; }
        public float TargetPopulationCap { get; set; }

        public AdministrativeGroup(
            Empire owner,
            string name,
            bool isSettled)
        {
            Id = Guid.NewGuid().ToString();
            Owner = owner;
            Name = name;
            IsSettled = isSettled;
            Pops = new List<Pop>();
            GrowthProgress = new Dictionary<Species, float>();
            ClaimedRegions = new List<WorldRegion>();
            TargetPopulationCap = -1;
        }

        public float GetGrowthRateFor(Species species)
        {
            var needSum = 0f;
            var needCount = 0;

            var popOfSpecies = Pops
                .Where(p => p.Species == species && p.Health >= 1)
                .Count();

            if(popOfSpecies < 1)
            {
                return 0;
            }


            for (var i = 0; i < 6; i++)
            {
                var upkeep = species.GetUpkeep((ResourceType)i);

                if(upkeep > 0)
                {
                    var supply = Pops
                        .Where(p => p.Job != null)
                        .Sum(p => p.Job.GetExpectedOutput((ResourceType)i, false));

                    var need = Pops
                        .Sum(p => p.GetUpkeep((ResourceType)i));

                    var needingPops = Pops
                        .Where(p => p.GetUpkeep((ResourceType)i) > 0)
                        .Count();

                    needSum += (supply - need) / Mathf.Pow(needingPops, 2);
                    needCount += 1;
                }
            }

            var growthBonus = species.Traits.Sum(t => t.GrowthBonus) + 1;

            var rate = growthBonus * (species.GrowthRate * needSum / needCount);

            if(TargetPopulationCap > 0)
            {
                rate *= Math.Max(0, 1 - Pops.Count() / TargetPopulationCap);
            }

            return popOfSpecies * rate;
        }

        public float GetStarvationChanceFor(Species species)
        {
            var popOfSpecies = Pops.Where(p => p.Species == species).Count();

            if (popOfSpecies < 1)
            {
                return 0;
            }

            var starveChance = 0f;

            for (var i = 0; i < 6; i++)
            {
                var upkeep = species.GetUpkeep((ResourceType)i);

                if (upkeep > 0)
                {
                    var surplus = GetExpectedOutput((ResourceType)i);

                    if(surplus < 0)
                    {
                        starveChance += -surplus / Pops.Count(p => p.GetUpkeep((ResourceType)i) > 0);
                    }
                }
            }

            return starveChance;
        }

        public IEnumerable<string> GetGrowthRateDetailsFor(Species species)
        {
            var needSum = 0f;
            var needCount = 0;

            var popOfSpecies = Pops.Where(p => p.Species == species).Count();
            var healthyPopOfSpecies = Pops.Where(p => p.Species == species && p.Health >= 1).Count();

            var details = new List<string>();

            if (popOfSpecies < 1)
            {
                return details;
            }

            for (var i = 0; i < 6; i++)
            {
                var upkeep = species.GetUpkeep((ResourceType)i);

                if (upkeep > 0)
                {
                    var supply = Pops
                        .Where(p => p.Job != null)
                        .Sum(p => p.Job.GetExpectedOutput((ResourceType)i, false));

                    var need = Pops
                        .Sum(p => p.GetUpkeep((ResourceType)i));

                    var needingPops = Pops
                        .Where(p => p.GetUpkeep((ResourceType)i) > 0)
                        .Count();

                    var factor = (supply - need) / Mathf.Pow(needingPops, 2);

                    needSum += factor;
                    needCount += 1;

                    details.Add($"{(int)(factor * 100)}% surplus {(ResourceType)i} supply");
                }
            }

            if (needCount > 1)
            {
                details.Add($"Average {(int)(needSum / needCount * 100)}%");
            }

            var adjGrowth = (species.GrowthRate * needSum / needCount);
            details.Add($"Base growth: {species.GrowthRate:N2}");
            details.Add($"Adjusted growth: {adjGrowth:N2}");
            details.Add($"Healthy Population: {healthyPopOfSpecies}");

            if (adjGrowth >= 0)
            {
                details.Add($"+{(int)(100 * healthyPopOfSpecies * adjGrowth)}% progress per turn");
            }
            else
            {
                details.Add($"{(int)(100 * healthyPopOfSpecies * adjGrowth)}% progress per turn");
            }
            return details;
        }

        

        public float GetExpectedOutput(ResourceType resource)
        {
            float output = 0;
            for (var i = 0; i < Pops.Count; i++)
            {
                var pop = Pops[i];

                output += pop.Job?.GetExpectedOutput(resource, false) ?? 0;

                output -= pop.GetUpkeep(resource);
            }

            return output;
        }

        public IEnumerable<string> GetProductionDetails(ResourceType resource)
        {
            var jobNet = Pops
                .Where(p => p.Job != null)
                .Select(p => p.Job.GetExpectedOutput(resource, false))
                .ToArray();

            var jobProd = jobNet
                .Where(n => n > 0)
                .Sum();

            var jobCon = jobNet
                .Where(n => n < 0)
                .Sum();

            var upkeep = Pops
                .Sum(p => -p.GetUpkeep(resource));

            var details = new List<string>();

            if(jobProd > 0)
            {
                details.Add($"+{jobProd:N1} from job production");
            }
            if(jobCon < 0)
            {
                details.Add($"{jobCon:N1} from job consumption");
            }
            if(upkeep < 0)
            {
                details.Add($"{upkeep:N1} from pop needs");
            }

            foreach (var species in Pops.Select(p => p.Species).Distinct())
            {
                if(species.GetUpkeep(resource) > 0)
                {
                    var supply = jobProd + jobCon;

                    var needingPops = Pops
                        .Where(p => p.GetUpkeep(resource) > 0)
                        .Count();

                    var factor = (supply + upkeep) / Mathf.Pow(needingPops, 2);

                    if (factor > 0)
                    {
                        details.Add($"+{(int)(factor * 100)}% {species.Name} growth");
                    }
                    else
                    {
                        details.Add($"{(int)(factor * 100)}% {species.Name} growth");
                    }

                    if(supply + upkeep < -2)
                    {
                        details.Add($"Severe {species.Name} starvation!");
                    }
                    else if(supply + upkeep < 0)
                    {
                        details.Add($"{species.Name} starvation!");
                    }
                }
            }

            if(resource == ResourceType.Production)
            {
                if(jobProd + jobCon < 0)
                {
                    details.Add($"-50% construction rate");
                }
            }

            return details;
        }

        public void UpdateGroupCenter()
        {
            if(!IsSettled)
            {
                if(Pops.Any())
                {
                    GroupCenter = new HexPosition(
                        (int)Pops.Average(p => p.Location.Position.HexX),
                        (int)Pops.Average(p => p.Location.Position.HexY));
                }
            }
            else
            {
                GroupCenter = ClaimedRegions.First().Center;
            }
        }

        public void WriteTo(SerializedObject groupRoot)
        {
            groupRoot.Set("id", Id);
            groupRoot.Set("name", Name);
            groupRoot.Set("is_settled", IsSettled);
            groupRoot.Set("turns_starving", TurnsStarving);

            foreach(var pop in Pops)
            {
                pop.WriteTo(groupRoot.CreateChild("pop"));
            }

            foreach(var region in ClaimedRegions)
            {
                groupRoot.CreateChild("claim").Set("id", region.Id);
            }
        }

        public static AdministrativeGroup ReadFrom(
            SerializedObject groupRoot, 
            IResourceTracker resourceTracker, 
            List<Culture> cultures, 
            Empire empire,
            List<WorldHex> hexes,
            List<WorldRegion> regions)
        {
            var group = new AdministrativeGroup(
                empire,
                groupRoot.GetString("name"),
                groupRoot.GetBool("is_settled"))
            {
                Id = groupRoot.GetString("id"),
                TurnsStarving = groupRoot.GetInt("turns_starving")
            };

            foreach (var popRoot in groupRoot.GetChildren("pop"))
            {
                // don't need to add the pops to the group, they do that in their constructor
                Pop.ReadFrom(popRoot, resourceTracker, group, cultures, hexes);
            }

            foreach(var claim in groupRoot.GetChildren("claim"))
            {
                var region = claim.GetObjectReference(
                    "id",
                    regions,
                    r => r.Id);

                group.ClaimedRegions.Add(region);
                region.ClaimedBy = group;
            }

            group.UpdateGroupCenter();

            return group;
        }

        public static void LateLoadPops(
            SerializedObject groupRoot,
            Pop[] allPops, 
            IResourceTracker resourceTracker)
        {
            foreach (var popRoot in groupRoot.GetChildren("pop"))
            {
                Pop.LateLoad(popRoot,  allPops, resourceTracker);
            }
        }
    }
}
