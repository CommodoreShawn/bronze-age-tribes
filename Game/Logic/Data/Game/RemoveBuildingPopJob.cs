﻿using BronzeAgeTribes.Logic.Infrastructure;
using BronzeAgeTribes.Logic.Infrastructure.Interfaces;
using BronzeAgeTribes.Logic.Infrastructure.SimulationEvents;

namespace BronzeAgeTribes.Logic.Data.Game
{
    public class RemoveBuildingPopJob : PopJob
    {
        private Building _building;
        private InteractionOption _interactionOption;

        public RemoveBuildingPopJob(
            WorldHex location,
            RemoveBuildingPopJobType jobType,
            Building building,
            InteractionOption interactionOption)
            : base(location, jobType)
        {
            _building = building;
            _interactionOption = interactionOption;
        }

        public override void AdvanceTurn(ISimulationEventBus simulationEventBus)
        {
            base.AdvanceTurn(simulationEventBus);

            if(Worker != null && Worker.MovingFromJob == null && Worker.InteractionTo == null)
            {
                
                if(Location.Development is UrbanDevelopment urbanDevelopment)
                {
                    urbanDevelopment.Building = null;

                    simulationEventBus.SendEvent(new WorldChangedSimulationEvent(Location.Yield()));
                }
            }
        }
    }
}
