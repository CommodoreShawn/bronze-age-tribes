﻿using BronzeAgeTribes.Logic.Data.Serialization;
using BronzeAgeTribes.Logic.Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BronzeAgeTribes.Logic.Data.Game
{
    public class Culture
    {
        public string Id { get; }
        public string Name { get; }
        public string NamingContext { get; }
        public Culture ParentCulture { get; }
        public CultureTrait[] Traits { get; }
        public BuildingStyle BuildingStyle { get; }

        public Culture(
            string id, 
            string name, 
            Culture parentCulture, 
            CultureTrait[] traits,
            string namingContext,
            BuildingStyle buildingStyle)
        {
            Id = id;
            Name = name;
            ParentCulture = parentCulture;
            Traits = traits;
            NamingContext = namingContext;
            BuildingStyle = buildingStyle;
        }

        public void WriteTo(SerializedObject cultureRoot)
        {
            cultureRoot.Set("id", Id);
            cultureRoot.Set("name", Name);
            cultureRoot.Set("naming_context", NamingContext);
            cultureRoot.Set("parent_culture", ParentCulture?.Id);
            cultureRoot.Set("building_style", BuildingStyle.Id);
            foreach(var trait in Traits)
            {
                cultureRoot.CreateChild("trait").Set("trait_id", trait.Id);
            }
        }

        public static Culture ReadFrom(SerializedObject cultureRoot, IResourceTracker resourceTracker, List<Culture> cultures)
        {
            return new Culture(
                cultureRoot.GetString("id"),
                cultureRoot.GetString("name"),
                cultureRoot.GetOptionalObjectReference(
                    "parent_culture",
                    cultures,
                    c => c.Id),
                cultureRoot.GetChildren("trait")
                    .Select(x => x.GetObjectReference(
                        "trait_id",
                        resourceTracker.Traits,
                        t => t.Id))
                    .OfType<CultureTrait>()
                    .ToArray(),
                cultureRoot.GetString("naming_context"),
                cultureRoot.GetOptionalObjectReference(
                    "building_style",
                    resourceTracker.BuildingStyles,
                    c => c.Id));
        }
    }
}
