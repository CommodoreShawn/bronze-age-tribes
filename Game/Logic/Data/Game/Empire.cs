﻿using BronzeAgeTribes.Logic.Data.Serialization;
using BronzeAgeTribes.Logic.Infrastructure.Interfaces;
using Godot;
using System;
using System.Collections.Generic;

namespace BronzeAgeTribes.Logic.Data.Game
{
    public class Empire
    {
        public string Id { get; private set; }
        public string Name { get; set; }
        public List<AdministrativeGroup> AdministrativeGroups { get; }
        public int TribeCoherency => 3;

        public IEnumerable<object> SelectMany { get; internal set; }

        public Color PrimaryColor { get; set; }
        public Color SecondaryColor { get; set; }

        public Empire(
            string name)
        {
            Id = Guid.NewGuid().ToString();
            Name = name;
            AdministrativeGroups = new List<AdministrativeGroup>();
        }

        public void WriteTo(SerializedObject empireRoot)
        {
            empireRoot.Set("id", Id);
            empireRoot.Set("name", Name);
            empireRoot.Set("primary_color", PrimaryColor.ToHtml());
            empireRoot.Set("secondary_color", SecondaryColor.ToHtml());

            foreach(var group in AdministrativeGroups)
            {
                group.WriteTo(empireRoot.CreateChild("group"));
            }
        }

        public static Empire ReadFrom(
            SerializedObject empireRoot, 
            IResourceTracker resourceTracker, 
            List<Culture> cultures,
            List<WorldHex> hexes,
            List<WorldRegion> regions)
        {
            var empire = new Empire(empireRoot.GetString("name"))
            {
                Id = empireRoot.GetString("id"),
                PrimaryColor = new Color(empireRoot.GetString("primary_color")),
                SecondaryColor = new Color(empireRoot.GetString("secondary_color")),
            };

            foreach(var groupRoot in empireRoot.GetChildren("group"))
            {
                empire.AdministrativeGroups.Add(AdministrativeGroup.ReadFrom(groupRoot, resourceTracker, cultures, empire, hexes, regions));
            }

            return empire;
        }

        public static void LateLoadPops(SerializedObject empireRoot, Pop[] allPops, IResourceTracker resourceTracker)
        {
            foreach (var groupRoot in empireRoot.GetChildren("group"))
            {
                AdministrativeGroup.LateLoadPops(groupRoot, allPops, resourceTracker);
            }
        }
    }
}
