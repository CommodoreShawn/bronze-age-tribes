﻿
namespace BronzeAgeTribes.Logic.Data.Game.Ai
{
    public class AiState
    {
        public float TargetSize { get; set; }
        public Empire Empire { get; set; }
        public AiTrait[] Traits { get; set; }
    }
}
