﻿using BronzeAgeTribes.Logic.Data.Serialization;
using BronzeAgeTribes.Logic.Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BronzeAgeTribes.Logic.Data.Game
{
    public class Pop
    {
        public string Id { get; private set; }
        public Species Species { get; set; }
        public Species ManagerSpecies { get; set; }
        public Empire Owner { get; set; }
        public WorldHex Location { get; set; }
        public AdministrativeGroup Group { get; set; }
        public Culture Culture { get; set; }
        public float Happiness { get; set; }
        public float Health { get; set; }
        public PopJob Job { get; set; }
        public PopJob MovingFromJob { get; set; }
        public Pop InteractionTo { get; set; }
        public PopInteractionType? InteractionType { get; set; }

        public IEnumerable<Trait> Traits { get; private set; }

        public bool IsBeingRaided { get; set; }

        public Pop(
            Species species,
            Empire owner,
            AdministrativeGroup group,
            Culture culture,
            WorldHex location)
        {
            Id = Guid.NewGuid().ToString();
            Species = species;
            Owner = owner;
            Group = group;
            Culture = culture;
            Location = location;
            Happiness = 1;
            Health = 1;

            location.Pops.Add(this);
            group.Pops.Add(this);

            Traits = culture.Traits.Concat(species.Traits);
        }

        public float GetUpkeep(ResourceType resource)
        {
            return Species.GetUpkeep(resource);
        }

        public void WriteTo(SerializedObject popRoot)
        {
            popRoot.Set("id", Id);
            popRoot.Set("species_id", Species.Id);
            Location.Position.WriteTo(popRoot.CreateChild("location"));
            popRoot.Set("culture_id", Culture.Id);
            popRoot.Set("happiness", Happiness);
            popRoot.Set("health", Health);
            popRoot.Set("job", Job?.Id);
            popRoot.Set("interacting_to", InteractionTo?.Id);
            popRoot.Set("interaction_type", InteractionType?.ToString());

            if (MovingFromJob != null)
            {
                popRoot.Set("moving_from_job", MovingFromJob?.Id);
                MovingFromJob.Location.Position.WriteTo(popRoot.CreateChild("moving_from_location"));
            }
            
            if(ManagerSpecies != null)
            {
                popRoot.Set("manager_species_id", ManagerSpecies.Id);
            }
        }

        public static Pop ReadFrom(
            SerializedObject popRoot,
            IResourceTracker resourceTracker,
            AdministrativeGroup group,
            List<Culture> cultures,
            List<WorldHex> worldHexes)
        {
            var hexPos = HexPosition.ReadFrom(popRoot.GetChild("location"));

            var location = worldHexes
                .Where(h => h.Position.HexX == hexPos.HexX && h.Position.HexY == hexPos.HexY)
                .First();

            var pop = new Pop(
                popRoot.GetObjectReference(
                    "species_id",
                    resourceTracker.Species,
                    x => x.Id),
                group.Owner,
                group,
                popRoot.GetObjectReference(
                    "culture_id",
                    cultures,
                    x => x.Id),
                location)
            {
                Id = popRoot.GetString("id"),
                Happiness = popRoot.GetFloat("happiness"),
                Health = popRoot.GetFloat("health"),
            };

            if(popRoot.Attributes.ContainsKey("job"))
            {
                var jobId = popRoot.GetInt("job");

                pop.Job = location.Jobs
                    .Where(j => j.Id == jobId)
                    .FirstOrDefault();

                if(pop.Job != null)
                {
                    pop.Job.Worker = pop;
                }
            }

            if (popRoot.Attributes.ContainsKey("moving_from_job"))
            {
                var jobId = popRoot.GetInt("moving_from_job");

                var fromPos = HexPosition.ReadFrom(popRoot.GetChild("moving_from_location"));

                var fromLocation = worldHexes
                    .Where(h => h.Position.HexX == fromPos.HexX && h.Position.HexY == fromPos.HexY)
                    .First();

                if (fromLocation != null)
                {
                    pop.MovingFromJob = fromLocation.Jobs
                        .Where(j => j.Id == jobId)
                        .FirstOrDefault();

                    if (pop.MovingFromJob != null)
                    {
                        pop.MovingFromJob.MovingOutWorker = pop;
                    }
                }
            }

            if (popRoot.Attributes.ContainsKey("manager_species_id"))
            {
                pop.ManagerSpecies = popRoot.GetObjectReference(
                    "manager_species_id",
                    resourceTracker.Species,
                    x => x.Id);
            }

            return pop;
        }

        public static void LateLoad(SerializedObject popRoot, Pop[] allPops, IResourceTracker resourceTracker)
        {
            var pop = popRoot.GetObjectReference(
                "id",
                allPops,
                p => p.Id);

            if(popRoot.Attributes.ContainsKey("interacting_to"))
            {
                pop.InteractionTo = popRoot.GetObjectReference(
                    "interacting_to",
                    allPops,
                    p => p.Id);
                pop.InteractionType = popRoot.GetEnum<PopInteractionType>("interaction_type");

                if(pop.InteractionType == PopInteractionType.Raid)
                {
                    pop.InteractionTo.IsBeingRaided = true;
                }
            }
        }
    }
}
