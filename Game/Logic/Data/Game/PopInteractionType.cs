﻿namespace BronzeAgeTribes.Logic.Data.Game
{
    public enum PopInteractionType
    {
        // Common
        Attack = 0,
        Expel = 1,
        Assimilate = 2,
        Domesticate = 3,

        // Uncommon
        Raid = 4
    }
}
