﻿using Autofac;
using BronzeAgeTribes.Logic.Infrastructure;
using BronzeAgeTribes.Logic.Infrastructure.Interfaces;
using BronzeAgeTribes.Logic.Infrastructure.SimulationEvents;
using System.Linq;

namespace BronzeAgeTribes.Logic.Data.Game
{
    public class ConstructDevelopmentPopJob : PopJob
    {
        private Development _development;
        private InteractionOption _interactionOption;

        public ConstructDevelopmentPopJob(
            WorldHex location,
            ConstructDevelopmentPopJobType jobType,
            Development development,
            InteractionOption interactionOption)
            : base(location, jobType)
        {
            _development = development;
            _interactionOption = interactionOption;
        }

        public override void AdvanceTurn(ISimulationEventBus simulationEventBus)
        {
            base.AdvanceTurn(simulationEventBus);

            var rate = 1f;
            if(Worker.Group.GetExpectedOutput(ResourceType.Production) < 0)
            {
                rate = 0.5f;
            }

            Godot.GD.Print("build development: rate " + rate);

            if(Worker != null && Worker.MovingFromJob == null && Worker.InteractionTo == null)
            {
                _development.ConstructProgress += (1 / (float)_interactionOption.Turns) * rate;
                Godot.GD.Print(_development.ConstructProgress);
            }

            if(_development.ConstructProgress >= 1)
            {
                _development.IsConstructed = true;
                _development.SetCurrentInteraction(null);

                foreach (var feature in Location.Features.Where(f => f.BackingType.ClearedByDevelopment).ToArray())
                {
                    Location.RemoveFeature(feature);
                }

                simulationEventBus.SendEvent(new WorldChangedSimulationEvent(Location.Yield()));

                var localPlayerEmpire = IocManager.IocContainer.Resolve<ILocalPlayerInfo>().Empire;

                if (Worker != null && localPlayerEmpire == Worker.Owner)
                {
                    IocManager.IocContainer.Resolve<ISimpleNotificationBuilder>()
                        .SendLookAtNotification(
                            "notification_builddone",
                            $"{_development.BackingType.Name} Constructed",
                            $"Construction of a {_development.BackingType.Name} has been completed by {Worker.Group.Name}.",
                            Worker.Location.Position);
                }
            }
        }
    }
}
