﻿using BronzeAgeTribes.Logic.Infrastructure.Interfaces;
using System.Linq;

namespace BronzeAgeTribes.Logic.Data.Game
{
    public class PopJob
    {
        public int Id { get; set; }
        public WorldHex Location { get; }
        public PopJobType JobType { get; }
        public Pop Worker { get; set; }
        public Pop MovingOutWorker { get; set; }

        public PopJob(
            WorldHex location,
            PopJobType jobType)
        {
            Location = location;
            JobType = jobType;
        }

        public virtual void AdvanceTurn(ISimulationEventBus simulationEventBus)
        {
        }

        public float GetExpectedOutput(
            ResourceType resource, 
            bool predictIfWorker)
        {
            if ((Worker == null || Worker.MovingFromJob != null) && !predictIfWorker)
            {
                return 0;
            }

            var factor = 1f;

            if (Worker != null)
            {
                factor += Worker.Traits
                    .Sum(t => t.JobBonuses.Where(x => x.Category == JobType.Category).Sum(x => x.Factor));

                if(Worker.InteractionTo != null)
                {
                    factor = 0;
                }
                else if(Worker.IsBeingRaided)
                {
                    factor = 0.5f;
                }

                if (Worker.InteractionTo != null
                    && Worker.InteractionType == PopInteractionType.Raid)
                {
                    switch (resource)
                    {
                        case ResourceType.Food:
                            return 0;
                        case ResourceType.Production:
                            return 0;
                        case ResourceType.Luxuries:
                            return 1;
                        case ResourceType.Trade:
                            return 0;
                        case ResourceType.Wealth:
                            return 1;
                        case ResourceType.Legitimacy:
                            return 0;
                        default:
                            return 0;
                    }
                }
            }

            switch (resource)
            {
                case ResourceType.Food:
                    return (JobType.BaseFoodFactor * Location.GetBaseOutput(resource) + JobType.FoodFromJob) * factor;
                case ResourceType.Production:
                    return (JobType.BaseProductionFactor * Location.GetBaseOutput(resource) + JobType.ProductionFromJob) * factor;
                case ResourceType.Luxuries:
                    return (JobType.BaseLuxuriesFactor * Location.GetBaseOutput(resource) + JobType.LuxuriesFromJob) * factor;
                case ResourceType.Trade:
                    return (JobType.BaseTradeFactor * Location.GetBaseOutput(resource) + JobType.TradeFromJob) * factor;
                case ResourceType.Wealth:
                    return (JobType.BaseWealthFactor * Location.GetBaseOutput(resource) + JobType.WealthFromJob) * factor;
                case ResourceType.Legitimacy:
                    return (JobType.BaseLegitimacyFactor * Location.GetBaseOutput(resource) + JobType.LegitimacyFromJob) * factor;
            }

            return 0;
        }

        public bool CanWork(Pop pop)
        {
            return CanWork(pop.Species, pop.Culture);
        }

        public bool CanWork(Species species, Culture culture)
        {
            return !species.Traits.Concat(culture.Traits)
                .Any(t => t.ForbiddenJobs.Contains(JobType.Category));
        }
    }
}
