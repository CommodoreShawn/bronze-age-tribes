﻿using Autofac;
using BronzeAgeTribes.Logic.Infrastructure;
using BronzeAgeTribes.Logic.Infrastructure.Interfaces;
using BronzeAgeTribes.Logic.Infrastructure.SimulationEvents;
using System.Linq;

namespace BronzeAgeTribes.Logic.Data.Game
{
    public class ConstructBuildingPopJob : PopJob
    {
        private Building _building;
        private InteractionOption _interactionOption;

        public ConstructBuildingPopJob(
            WorldHex location,
            ConstructBuildingPopJobType jobType,
            Building building,
            InteractionOption interactionOption)
            : base(location, jobType)
        {
            _building = building;
            _interactionOption = interactionOption;
        }

        public override void AdvanceTurn(ISimulationEventBus simulationEventBus)
        {
            base.AdvanceTurn(simulationEventBus);

            var rate = 1f;
            if(Worker.Group.GetExpectedOutput(ResourceType.Production) < 0)
            {
                rate = 0.5f;
            }

            if(Worker != null && Worker.MovingFromJob == null && Worker.InteractionTo == null)
            {
                _building.ConstructProgress += (1 / (float)_interactionOption.Turns) * rate;
            }

            if(_building.ConstructProgress >= 1)
            {
                _building.IsConstructed = true;
                _building.SetCurrentInteraction(null);

                foreach (var feature in Location.Features.Where(f => f.BackingType.ClearedByDevelopment).ToArray())
                {
                    Location.RemoveFeature(feature);
                }

                simulationEventBus.SendEvent(new WorldChangedSimulationEvent(Location.Yield()));

                var localPlayerEmpire = IocManager.IocContainer.Resolve<ILocalPlayerInfo>().Empire;

                if (Worker != null && localPlayerEmpire == Worker.Owner)
                {
                    IocManager.IocContainer.Resolve<ISimpleNotificationBuilder>()
                        .SendLookAtNotification(
                            "notification_builddone",
                            $"{_building.BackingType.Name} Constructed",
                            $"Construction of a {_building.BackingType.Name} has been completed by {Worker.Group.Name}.",
                            Worker.Location.Position);
                }
            }
        }
    }
}
