﻿using BronzeAgeTribes.Logic.Data.Game;
using BronzeAgeTribes.Logic.Data.Serialization;
using BronzeAgeTribes.Logic.Infrastructure;
using BronzeAgeTribes.Logic.Infrastructure.Interfaces;
using Godot;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BronzeAgeTribes.Logic.Data
{
    public class UrbanDevelopment : Development
    {
        private Building _building;

        public override bool HasJobs => (CurrentInteraction?.HasJobs ?? BackingType.ProvidedJobs.Any()) || (Building?.HasJobs ?? false);

        public UrbanDevelopment(
            DevelopmentType backingType,
            WorldHex hex,
            bool isConstructed,
            BuildingStyle buildingStyle)
            :base(backingType, hex, isConstructed, buildingStyle)
        {
        }

        public Building Building
        {
            get => _building;
            set
            {
                _building = value;
                Hex.RecalculateTraits();
                Hex.RecalculateJobs();
            }
        }

        public override IEnumerable<PopJob> MakeJobs(WorldHex location)
        {
            IEnumerable<PopJob> jobs;

            if (CurrentInteraction?.HasJobs ?? false)
            {
                jobs = CurrentInteraction.MakeJobs(location, this);
            }
            else
            {
                jobs = BackingType.ProvidedJobs.Select(j => j.MakeJob(location, this));
            }

            if(Building != null)
            {
                jobs = jobs.Concat(Building.MakeJobs(location));
            }

            GD.Print(jobs.Count());

            return jobs;
        }

        public override float GetExpectedOutput(ResourceType resource)
        {
            var output = base.GetExpectedOutput(resource);

            if(Building != null && Building.IsConstructed)
            {
                output += Building.GetExpectedOutput(resource);
            }

            return output;
        }
    }
}
