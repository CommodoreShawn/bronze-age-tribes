﻿namespace BronzeAgeTribes.Logic.Data
{
    public class WorldParameters
    {
        public string Seed { get; set; }
        public int WorldRadius { get; set; }
        public int TargetRegionSize { get; set; }
        public float RainDistanceScale { get; set; }
        public TerrainType[] RainSources { get; set; }
        public TerrainType[] TerrainTypes { get; set; }
        public BiomeMapping[] BiomeMappings { get; set; }
        public CellFeatureType RiverFeature { get; set; }
        public float RiverAdditionalFeatureChance { get; set; }
        public CellFeatureType[] RiverAdditionalFeatures { get; set; }
        public CreepSpawn[] Creeps;
    }
}
