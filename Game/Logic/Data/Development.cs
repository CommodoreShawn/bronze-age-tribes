﻿using BronzeAgeTribes.Logic.Data.Game;
using BronzeAgeTribes.Logic.Data.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BronzeAgeTribes.Logic.Data
{
    public class Development
    {
        public DevelopmentType BackingType { get; private set; }
        public TerrainTrait[] Traits => BackingType.Traits;

        public InteractionOption CurrentInteraction { get; private set; }

        public virtual bool HasJobs => CurrentInteraction?.HasJobs ?? BackingType.ProvidedJobs.Any();

        public WorldHex Hex { get; private set; }

        public float ConstructProgress { get; set; }
        public bool IsConstructed { get; internal set; }

        public BuildingStyle BuildingStyle { get; private set; }

        public Development(
            DevelopmentType backingType,
            WorldHex hex,
            bool isConstructed,
            BuildingStyle buildingStyle)
        {
            Hex = hex;
            BackingType = backingType;
            BuildingStyle = buildingStyle;

            IsConstructed = isConstructed;
            if(isConstructed)
            {
                ConstructProgress = 1;
            }
            else
            {
                ConstructProgress = 0;
            }
        }

        public void SetCurrentInteraction(InteractionOption interactionOption)
        {
            if(interactionOption == null && !IsConstructed)
            {
                Hex.Development = null;
                Hex.RecalculateJobs();
            }
            else if (CurrentInteraction != interactionOption)
            {
                CurrentInteraction = interactionOption;
                Hex.RecalculateJobs();
            }
        }

        public virtual IEnumerable<PopJob> MakeJobs(WorldHex location)
        {
            if (CurrentInteraction?.HasJobs ?? false)
            {
                return CurrentInteraction.MakeJobs(location, this);
            }

            return BackingType.ProvidedJobs.Select(j => j.MakeJob(location, this));
        }

        public virtual void WriteTo(SerializedObject developmentRoot)
        {
            BackingType.WriteTo(developmentRoot, this);
        }

        public virtual float GetExpectedOutput(ResourceType resource)
        {
            if (IsConstructed)
            {
                return BackingType.GetExpectedOutput(resource);
            }

            return 0;
        }
    }
}
