﻿using BronzeAgeTribes.Logic.Data.Game;
using BronzeAgeTribes.Logic.Data.Serialization;
using BronzeAgeTribes.Logic.Infrastructure;
using BronzeAgeTribes.Logic.Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BronzeAgeTribes.Logic.Data
{
    public class Terrain
    {
        public TerrainType BackingType { get; private set; }
        public InteractionOption CurrentInteraction { get; private set; }

        public bool HasJobs => CurrentInteraction?.HasJobs ?? false;

        private WorldHex _hex;

        public float Height => BackingType.Height;
        public string Name => BackingType.Name;
        public TerrainTrait[] Traits => BackingType.Traits;
        public float CellFeatureChance => BackingType.CellFeatureChance;
        public CellFeatureType[] CellFeatures => BackingType.CellFeatures;
        public float AdjacentCellFeatureChance => BackingType.AdjacentCellFeatureChance;
        public CellFeatureType[] AdjacentCellFeatures => BackingType.AdjacentCellFeatures;

        public Terrain(
            TerrainType backingType,
            WorldHex hex)
        {
            _hex = hex;
            BackingType = backingType;
        }

        public void SetCurrentInteraction(InteractionOption interactionOption)
        {
            if (CurrentInteraction != interactionOption)
            {
                CurrentInteraction = interactionOption;
                _hex.RecalculateJobs();
            }
        }

        public IEnumerable<PopJob> MakeJobs(WorldHex location)
        {
            if (CurrentInteraction?.HasJobs ?? false)
            {
                return CurrentInteraction.MakeJobs(location, this);
            }

            return Enumerable.Empty<PopJob>();
        }

        public void WriteTo(SerializedObject terrainRoot)
        {
            terrainRoot.Set("terrain_id", BackingType.Id);
            terrainRoot.Set("current_interaction", CurrentInteraction?.Id);
        }

        public static Terrain ReadFrom(SerializedObject terrainRoot, WorldHex hex, IResourceTracker resourceTracker)
        {
            var terrainType = terrainRoot.GetObjectReference(
                "terrain_id",
                resourceTracker.TerrainTypes,
                t => t.Id);

            // At this time, it doesn't look like this is needed. No interaction options on terrain
            //var currentInteraction = terrainRoot.GetOptionalObjectReference(
            //        "current_interaction",
            //        terrainType.InteractionOptions)

            return new Terrain(
                terrainType,
                hex);
        }
    }
}
