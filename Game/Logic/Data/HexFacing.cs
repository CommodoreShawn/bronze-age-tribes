﻿namespace BronzeAgeTribes.Logic.Data
{
    public enum HexFacing
    {
        NorthEast = 0,
        East = 1,
        SouthEast = 2,
        SouthWest = 3,
        West = 4,
        NorthWest = 5
    }
}
