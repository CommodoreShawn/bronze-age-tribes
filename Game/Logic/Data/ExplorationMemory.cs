﻿using BronzeAgeTribes.Logic.Data.Serialization;
using BronzeAgeTribes.Logic.Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BronzeAgeTribes.Logic.Data
{
    public class ExplorationMemory
    {
        public DevelopmentType DevelopmentType { get; set; }
        public List<CellFeatureType> CellFeatureTypes { get; set; }

        public ExplorationMemory()
        {
            CellFeatureTypes = new List<CellFeatureType>();
        }

        public void WriteTo(SerializedObject memoryRoot)
        {
            if(DevelopmentType != null)
            {
                memoryRoot.Set("development", DevelopmentType.Id);
            }

            foreach(var featureType in CellFeatureTypes)
            {
                memoryRoot.CreateChild("feature").Set("id", featureType.Id);
            }
        }

        public static ExplorationMemory ReadFrom(SerializedObject memoryRoot, IResourceTracker resourceTracker)
        {
            return new ExplorationMemory
            {
                DevelopmentType = memoryRoot.GetOptionalObjectReferenceOrDefault(
                    "development",
                    resourceTracker.DevelopmentTypes,
                    x => x.Id),
                CellFeatureTypes = memoryRoot.GetChildren("feature")
                    .Select(o => o.GetObjectReference(
                        "id",
                        resourceTracker.FeatureTypes,
                        x => x.Id))
                    .ToList()
            };
        }
    }
}
