﻿using BronzeAgeTribes.Logic.Data.Game;
using BronzeAgeTribes.Logic.Data.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BronzeAgeTribes.Logic.Data
{
    public class Building
    {
        public BuildingType BackingType { get; private set; }
        public TerrainTrait[] Traits => BackingType.Traits;

        public InteractionOption CurrentInteraction { get; private set; }

        public bool HasJobs => CurrentInteraction?.HasJobs ?? BackingType.ProvidedJobs.Any();

        private UrbanDevelopment _development;

        public float ConstructProgress { get; set; }
        public bool IsConstructed { get; internal set; }

        public BuildingStyle BuildingStyle { get; private set; }

        public Building(
            BuildingType backingType,
            UrbanDevelopment development,
            bool isConstructed,
            BuildingStyle buildingStyle)
        {
            _development = development;
            BackingType = backingType;
            BuildingStyle = buildingStyle;

            IsConstructed = isConstructed;
            if(isConstructed)
            {
                ConstructProgress = 1;
            }
            else
            {
                ConstructProgress = 0;
            }
        }

        public void SetCurrentInteraction(InteractionOption interactionOption)
        {
            if(interactionOption == null && !IsConstructed)
            {
                _development.Building = null;
                _development.Hex.RecalculateJobs();
            }
            else if (CurrentInteraction != interactionOption)
            {
                CurrentInteraction = interactionOption;
                _development.Hex.RecalculateJobs();
            }
        }

        public IEnumerable<PopJob> MakeJobs(WorldHex location)
        {
            if (CurrentInteraction?.HasJobs ?? false)
            {
                return CurrentInteraction.MakeJobs(location, this);
            }

            return BackingType.ProvidedJobs.Select(j => j.MakeJob(location, this));
        }

        public float GetExpectedOutput(ResourceType resource)
        {
            return BackingType.GetExpectedOutput(resource);
        }

        public virtual void WriteTo(SerializedObject buildingRoot)
        {
            BackingType.WriteTo(buildingRoot, this);
        }
    }
}
