﻿public enum ResourceType
{
    Food = 0,
    Production = 1,
    Luxuries = 2,
    Trade = 3,
    Wealth = 4,
    Legitimacy = 5
}