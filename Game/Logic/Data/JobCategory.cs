﻿public enum JobCategory
{
    Foraging = 0,
    Hunting = 1,
    Farming = 2,
    Crafting = 3,
    Mining = 4,
    Woodcutting = 5,
    Building = 6,
    Preaching = 7,
}