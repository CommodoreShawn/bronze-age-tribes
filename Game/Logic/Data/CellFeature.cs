﻿using BronzeAgeTribes.Logic.Data.Game;
using BronzeAgeTribes.Logic.Data.Serialization;
using BronzeAgeTribes.Logic.Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BronzeAgeTribes.Logic.Data
{
    public class CellFeature
    {
        public CellFeatureType BackingType { get; private set; }
        public TerrainTrait[] Traits => BackingType.Traits;

        public InteractionOption CurrentInteraction { get; private set; }

        public bool HasJobs => CurrentInteraction?.HasJobs ?? false;

        private WorldHex _hex;

        public float ClearProgress { get; set; }
        
        public CellFeature(
            CellFeatureType backingType,
            WorldHex hex)
        {
            _hex = hex;
            BackingType = backingType;
        }

        public void SetCurrentInteraction(InteractionOption interactionOption)
        {
            if (CurrentInteraction != interactionOption)
            {
                CurrentInteraction = interactionOption;
                _hex.RecalculateJobs();
            }
        }

        public IEnumerable<PopJob> MakeJobs(WorldHex location)
        {
            if (CurrentInteraction?.HasJobs ?? false)
            {
                return CurrentInteraction.MakeJobs(location, this);
            }

            return Enumerable.Empty<PopJob>();
        }

        public void WriteTo(SerializedObject featureRoot)
        {
            featureRoot.Set("feature_id", BackingType.Id);
            featureRoot.Set("current_interaction", CurrentInteraction?.Id);
            featureRoot.Set("clear_progress", ClearProgress);
        }

        public static CellFeature ReadFrom(SerializedObject featureRoot, WorldHex hex, IResourceTracker resourceTracker)
        {
            var featureType = featureRoot.GetObjectReference(
                "feature_id",
                resourceTracker.FeatureTypes,
                t => t.Id);

            var currentInteraction = featureRoot.GetOptionalObjectReference(
                    "current_interaction",
                    featureType.InteractionOptions,
                    x => x.Id);

            return new CellFeature(featureType, hex)
            {
                CurrentInteraction = currentInteraction,
                ClearProgress = featureRoot.GetFloat("clear_progress")
            };
        }

        public float GetExpectedOutput(ResourceType resource)
        {
            return BackingType.GetExpectedOutput(resource);
        }
    }
}