﻿namespace BronzeAgeTribes.Logic.Data.Serialization
{
    public class SaveFile
    {
        public string FileName => Summary.FileName;
        public SerializedObject Root { get; set; }
        public SaveSummary Summary { get; set; }

        public SaveFile()
        {
            Root = new SerializedObject("root");
            Summary = new SaveSummary();
        }
    }
}
