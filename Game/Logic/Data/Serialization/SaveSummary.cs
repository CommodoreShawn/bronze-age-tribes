﻿using System;

namespace BronzeAgeTribes.Logic.Data.Serialization
{
    public class SaveSummary
    {
        public string FileName { get; set; }
        public int PlayedTurns { get; set; }
        public DateTime LastPlayed { get; set; }
        public string Seed { get; set; }
        public string PlayerEmpire { get; set; }
        public int SchemaVersion { get; set; }
    }
}
