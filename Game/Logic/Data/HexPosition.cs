﻿using BronzeAgeTribes.Logic.Data.Serialization;
using Godot;

namespace BronzeAgeTribes.Logic.Data
{
    public struct HexPosition
    {
        public readonly int HexX;
        public readonly int HexY;
        public readonly int RealX;
        public readonly int RealY;

        public HexPosition(int hexX, int hexY)
        {
            HexX = hexX;
            HexY = hexY;
            if (HexY % 2 == 0)
            {
                RealX = Constants.VERTICIES_PER_HEX / 2 + hexX * Constants.VERTICIES_PER_HEX;
            }
            else
            {
                RealX = (hexX + 1) * Constants.VERTICIES_PER_HEX;
            }
            RealY = 4 + hexY * (Constants.VERTICIES_PER_HEX - 1);
        }

        public float DistanceTo(HexPosition other)
        {
            var dx = RealX - other.RealX;
            var dy = RealY - other.RealY;

            return Mathf.Sqrt(dx * dx + dy * dy);
        }

        public float DistanceTo(float otherRealX, float otherRealY)
        {
            var dx = RealX - otherRealX;
            var dy = RealY - otherRealY;

            return Mathf.Sqrt(dx * dx + dy * dy);
        }

        public override string ToString()
        {
            return $"HexPos({HexX}, {HexY})";
        }

        public void WriteTo(SerializedObject posRoot)
        {
            posRoot.Set("hex_x", HexX);
            posRoot.Set("hex_y", HexY);
        }

        public static HexPosition ReadFrom(SerializedObject posRoot)
        {
            return new HexPosition(
                posRoot.GetInt("hex_x"),
                posRoot.GetInt("hex_y"));
        }
    }
}
