﻿using BronzeAgeTribes.Logic.Data.Game;
using BronzeAgeTribes.Logic.Data.Serialization;
using BronzeAgeTribes.Logic.Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BronzeAgeTribes.Logic.Data
{
    public class WorldRegion
    {
        public string Id { get; private set; }
        public IEnumerable<WorldHex> Hexes { get; }
        public HexPosition Center { get; private set; }
        public TerrainType TerrainType { get; set; }
        public BiomeType BiomeType { get; set; }
        public List<WorldRegion> Neighbors { get; set; }
        public AdministrativeGroup ClaimedBy { get; set; }
        private List<WorldHex> _hexes;
        public string Name { get; set; }

        public float Temperature { get; set; }
        public float Rainfall { get; set; }
        public float Drainage { get; set; }

        public WorldRegion()
        {
            Id = Guid.NewGuid().ToString();
            _hexes = new List<WorldHex>();
            Hexes = _hexes.AsReadOnly();
        }

        public void SetHexes(IEnumerable<WorldHex> hexes)
        {
            _hexes.Clear();
            _hexes.AddRange(hexes);

            DetermineCenter();
        }

        public void RemoveHex(WorldHex hex)
        {
            _hexes.Remove(hex);
            DetermineCenter();
        }

        public  void AddHex(WorldHex hex)
        {
            _hexes.Add(hex);
            DetermineCenter();
        }

        private void DetermineCenter()
        {
            if (_hexes.Any())
            {
                Center = new HexPosition(
                    (int)_hexes.Average(h => h.Position.HexX),
                    (int)_hexes.Average(h => h.Position.HexY));
            }
        }

        public void WriteTo(SerializedObject regionRoot)
        {
            regionRoot.Set("id", Id);
            regionRoot.Set("terrain_id", TerrainType.Id);
            regionRoot.Set("biome_id", BiomeType.Id);
            regionRoot.Set("name", Name);
            regionRoot.Set("temperature", Temperature);
            regionRoot.Set("rainfall", Rainfall);
            regionRoot.Set("drainage", Drainage);
        }

        public static WorldRegion ReadFrom(SerializedObject regionRoot, IResourceTracker resourceTracker)
        {
            return new WorldRegion
            {
                Id = regionRoot.GetString("id"),
                TerrainType = regionRoot.GetObjectReference(
                    "terrain_id",
                    resourceTracker.TerrainTypes,
                    x => x.Id),
                BiomeType = regionRoot.GetObjectReference(
                    "biome_id",
                    resourceTracker.BiomeTypes,
                    x => x.Id),
                Name = regionRoot.GetString("name"),
                Temperature = regionRoot.GetFloat("temperature"),
                Rainfall = regionRoot.GetFloat("rainfall"),
                Drainage = regionRoot.GetFloat("drainage"),
            };
        }
    }
}
