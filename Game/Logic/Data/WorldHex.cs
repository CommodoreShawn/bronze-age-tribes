﻿using BronzeAgeTribes.Logic.Data.Game;
using BronzeAgeTribes.Logic.Data.Serialization;
using BronzeAgeTribes.Logic.Infrastructure.Interfaces;
using Godot;
using System.Collections.Generic;
using System.Linq;

namespace BronzeAgeTribes.Logic.Data
{
    public class WorldHex
    {
        public HexPosition Position { get; private set; }
        public WorldRegion Region { get; set; }
        public Vector3 RenderedPos { get; set; }
        private Terrain _terrain;
        private BiomeType _biomeType;
        private List<CellFeature> _cellFeatures;

        public WorldHex[] Neighbors { get; private set; }
        public int RiverFlow { get; set; }
        public HexFacing RiverFlowDirection { get; set; }
        public List<Pop> Pops { get; private set; }
        public IEnumerable<CellFeature> Features { get; private set; }
        private Development _development;
        
        public List<PopJob> Jobs { get; }

        public IEnumerable<TerrainTrait> TerrainTraits { get; private set; }


        public WorldHex(int hexX, int hexY)
        {
            Position = new HexPosition(hexX, hexY);
            Neighbors = new WorldHex[6];
            Pops = new List<Pop>();
            TerrainTraits = Enumerable.Empty<TerrainTrait>();
            Jobs = new List<PopJob>();
            _cellFeatures = new List<CellFeature>();
            Features = _cellFeatures.AsReadOnly();
        }

        public Terrain Terrain
        {
            get => _terrain;
            set
            {
                _terrain = value;
                RecalculateTraits();
                RecalculateJobs();
            }
        }

        public float GetExpectedOutput(ResourceType resource)
        {
            return Jobs.Sum(j => j.GetExpectedOutput(resource, false));
        }

        public BiomeType BiomeType
        {
            get => _biomeType;
            set
            {
                _biomeType = value;
                RecalculateTraits();
                RecalculateJobs();
            }
        }

        public Development Development
        {
            get => _development;
            set
            {
                _development = value;
                RecalculateTraits();
                RecalculateJobs();
            }
        }

        public void AddFeature(CellFeature cellFeature)
        {
            _cellFeatures.Add(cellFeature);
            RecalculateTraits();
            RecalculateJobs();
        }

        public void RemoveFeature(CellFeature cellFeature)
        {
            _cellFeatures.Remove(cellFeature);
            RecalculateTraits();
            RecalculateJobs();
        }
        
        public float GetBaseOutput(ResourceType resource)
        {
            var sum = 0f;

            if(BiomeType != null)
            {
                switch(resource)
                {
                    case ResourceType.Food:
                        sum += BiomeType.FoodWhenWorked;
                        break;
                    case ResourceType.Production:
                        sum += BiomeType.ProductionWhenWorked;
                        break;
                    case ResourceType.Luxuries:
                        sum += BiomeType.LuxuriesWhenWorked;
                        break;
                    case ResourceType.Trade:
                        sum += BiomeType.TradeWhenWorked;
                        break;
                    case ResourceType.Wealth:
                        sum += BiomeType.WealthWhenWorked;
                        break;
                    case ResourceType.Legitimacy:
                        sum += BiomeType.LegitimacyWhenWorked;
                        break;
                }
            }

            foreach(var feature in _cellFeatures)
            {
                sum += feature.GetExpectedOutput(resource);
            }

            if(Development != null && Development.IsConstructed)
            {
                sum += Development.GetExpectedOutput(resource);
            }

            return sum;
        }

        public IEnumerable<string> GetProductionDetails(ResourceType resource)
        {
            var jobNet = Jobs
                .Select(j => j.GetExpectedOutput(resource, false))
                .ToArray();

            var jobProd = jobNet
                .Where(n => n > 0)
                .Sum();

            var jobCon = jobNet
                .Where(n => n < 0)
                .Sum();

            var details = new List<string>();

            if (jobProd > 0)
            {
                details.Add($"+{jobProd:N0} from job production");
            }
            if (jobCon < 0)
            {
                details.Add($"{jobCon:N0} from job consumption");
            }

            return details;
        }

        public void RecalculateTraits()
        {
            TerrainTraits = (_terrain?.Traits ?? Enumerable.Empty<TerrainTrait>())
                .Concat(_biomeType?.Traits ?? Enumerable.Empty<TerrainTrait>())
                .Concat(Features?.SelectMany(f => f.Traits ?? Enumerable.Empty<TerrainTrait>()) ?? Enumerable.Empty<TerrainTrait>())
                .Concat(_development?.Traits ?? Enumerable.Empty<TerrainTrait>())
                .Where(t => t != null)
                .ToArray();
        }

        public void RecalculateJobs()
        {
            var newJobs = new List<PopJob>();

            if(Development?.HasJobs ?? false)
            {
                newJobs.AddRange(Development.MakeJobs(this));
            }
            if (_terrain?.HasJobs ?? false)
            {
                newJobs.AddRange(_terrain.MakeJobs(this));
            }
            if (_cellFeatures.Any(cf => cf.HasJobs))
            {
                newJobs.AddRange(_cellFeatures
                    .SelectMany(cf => cf.MakeJobs(this)));
            }

            if (!newJobs.Any() && BiomeType?.BaseJob != null)
            {
                newJobs.Add(new PopJob(this, BiomeType.BaseJob));
            }

            for(var i = 0; i < newJobs.Count; i++)
            {
                newJobs[i].Id = i;
            }

            for(var i = 0; i < newJobs.Count && i < Jobs.Count; i++)
            {
                newJobs[i].Worker = Jobs[i].Worker;
                
                if (Jobs[i].Worker != null)
                {
                    Jobs[i].Worker.Job = newJobs[i];
                }
            }

            foreach(var job in newJobs)
            {
                if (job.Worker == null)
                {
                    foreach (var pop in Pops)
                    {
                        if (!newJobs.Any(j => j.Worker == pop))
                        {
                            job.Worker = pop;
                            pop.Job = job;
                        }
                    }
                }
            }

            var idlePops = Pops
                .Where(p => !newJobs.Any(j => j.Worker == p))
                .ToArray();
            foreach(var pop in idlePops)
            {
                pop.Job = null;
            }

            Jobs.Clear();
            Jobs.AddRange(newJobs);
        }

        public void WriteTo(SerializedObject hexRoot)
        {
            hexRoot.Set("hex_x", Position.HexX);
            hexRoot.Set("hex_y", Position.HexY);
            hexRoot.Set("region_id", Region.Id);
            hexRoot.Set("river_flow", RiverFlow);
            hexRoot.Set("river_flow_direction", RiverFlowDirection);
            if (Terrain != null)
            {
                Terrain.WriteTo(hexRoot.CreateChild("terrain"));
            }
            if (BiomeType != null)
            {
                hexRoot.Set("biome_type", BiomeType.Id);
            }
            foreach (var feature in Features)
            {
                feature.WriteTo(hexRoot.CreateChild("feature"));
            }
            if(Development != null)
            {
                Development.WriteTo(hexRoot.CreateChild("development"));
            }
        }

        public static WorldHex ReadFrom(SerializedObject hexRoot, IResourceTracker resourceTracker, List<WorldRegion> regions)
        {
            var hex = new WorldHex(
                hexRoot.GetInt("hex_x"),
                hexRoot.GetInt("hex_y"));

            var terrainRoot = hexRoot.GetOptionalChild("terrain");
            if(terrainRoot != null)
            {
                hex.Terrain = Terrain.ReadFrom(terrainRoot, hex, resourceTracker);
            }

            hex.BiomeType = hexRoot.GetOptionalObjectReference(
                "biome_type",
                resourceTracker.BiomeTypes,
                bt => bt.Id);
            hex.RiverFlow = hexRoot.GetInt("river_flow");
            hex.RiverFlowDirection = hexRoot.GetEnum<HexFacing>("river_flow_direction");

            foreach (var featureRoot in hexRoot.GetChildren("feature"))
            {
                hex.AddFeature(CellFeature.ReadFrom(featureRoot, hex, resourceTracker));
            }

            var developmentRoot = hexRoot.GetOptionalChild("development");
            if (developmentRoot != null)
            {
                hex.Development = DevelopmentType.ReadFrom(developmentRoot, hex, resourceTracker);
            }

            hex.Region = hexRoot.GetObjectReference(
                "region_id",
                regions,
                x => x.Id);
            hex.Region.AddHex(hex);

            return hex;
        }
    }
}
