using Godot;
using System;

public class OrbitingSun : Spatial
{
    [Export]
    public float DirectionOffset = 0;

    [Export]
    public float SpeedFactor = 0.13f;

    [Export]
    public float RadiusFactor = 0.15f;

    public override void _Ready()
    {
        
    }

    public void Initialize(int mapSize)
    {
        Transform = Transform.Translated(new Vector3(mapSize * 0.5f, 0, mapSize * 0.43f));
        var sun = GetNode<Spatial>("Sun");
        sun.Transform = sun.Transform.Translated(new Vector3(mapSize * RadiusFactor, 0, 0));
        Rotate(Vector3.Up, DirectionOffset);
    }

    public override void _Process(float delta)
    {
        Rotate(Vector3.Up, delta * SpeedFactor);
    }
}
